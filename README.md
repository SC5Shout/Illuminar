# Illuminar
### WORK IN PROGRESS

### How to clone

remember to clone *recursively* because some dependencies are added as submodules.

`git clone --recursive https://gitlab.com/SC5Shout/Illuminar.git`

### How to build
Illuminar uses [premake](https://premake.github.io/) to support cross-platform building. Right now it is Windows only, though.

Everything you need is to run `Win-Gen.bat` script

### Main features to come:
- High-fidelity Physically-Based 3D rendering
- Support for Mac, Linux, Android and iOS
    - Native rendering API support (DirectX, Vulkan, Metal)
- Fully featured viewer and editor applications
- Fully scripted interaction and behavior
- 2D and 3D physics engine
- Procedural terrain and world generation
- Render graph
- shadows

### Long term goals
- Artificial Intelligence
- Audio system
- frendly custom shaders system
