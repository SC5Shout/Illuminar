# Install script for directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/shaderc")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/spirv-tools" TYPE FILE FILES
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/include/spirv-tools/libspirv.h"
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/include/spirv-tools/libspirv.hpp"
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/include/spirv-tools/optimizer.hpp"
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/include/spirv-tools/linker.hpp"
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/include/spirv-tools/instrument.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/SPIRV-Tools.pc"
    "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/SPIRV-Tools-shared.pc"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/external/cmake_install.cmake")
  include("C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/cmake_install.cmake")
  include("C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/tools/cmake_install.cmake")
  include("C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/test/cmake_install.cmake")
  include("C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/examples/cmake_install.cmake")

endif()

