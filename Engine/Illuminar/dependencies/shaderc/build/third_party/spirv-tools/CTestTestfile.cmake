# CMake generated Testfile for 
# Source directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools
# Build directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
if("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
  add_test(spirv-tools-copyrights "C:/Users/mprze/AppData/Local/Programs/Python/Python38-32/python.exe" "utils/check_copyright.py")
  set_tests_properties(spirv-tools-copyrights PROPERTIES  WORKING_DIRECTORY "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools" _BACKTRACE_TRIPLES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;340;add_test;C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
  add_test(spirv-tools-copyrights "C:/Users/mprze/AppData/Local/Programs/Python/Python38-32/python.exe" "utils/check_copyright.py")
  set_tests_properties(spirv-tools-copyrights PROPERTIES  WORKING_DIRECTORY "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools" _BACKTRACE_TRIPLES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;340;add_test;C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
  add_test(spirv-tools-copyrights "C:/Users/mprze/AppData/Local/Programs/Python/Python38-32/python.exe" "utils/check_copyright.py")
  set_tests_properties(spirv-tools-copyrights PROPERTIES  WORKING_DIRECTORY "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools" _BACKTRACE_TRIPLES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;340;add_test;C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
  add_test(spirv-tools-copyrights "C:/Users/mprze/AppData/Local/Programs/Python/Python38-32/python.exe" "utils/check_copyright.py")
  set_tests_properties(spirv-tools-copyrights PROPERTIES  WORKING_DIRECTORY "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools" _BACKTRACE_TRIPLES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;340;add_test;C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/CMakeLists.txt;0;")
else()
  add_test(spirv-tools-copyrights NOT_AVAILABLE)
endif()
subdirs("external")
subdirs("source")
subdirs("tools")
subdirs("test")
subdirs("examples")
