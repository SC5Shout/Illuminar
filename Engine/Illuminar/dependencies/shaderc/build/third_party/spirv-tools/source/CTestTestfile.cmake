# CMake generated Testfile for 
# Source directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/source
# Build directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("opt")
subdirs("reduce")
subdirs("fuzz")
subdirs("link")
