# Install script for directory: C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/third_party/SPIRV-Tools/source/link

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/shaderc")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/Debug/SPIRV-Tools-link.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/Release/SPIRV-Tools-link.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/MinSizeRel/SPIRV-Tools-link.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/RelWithDebInfo/SPIRV-Tools-link.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets.cmake"
         "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets-debug.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets-minsizerel.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets-relwithdebinfo.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/third_party/spirv-tools/source/link/CMakeFiles/Export/SPIRV-Tools-link/cmake/SPIRV-Tools-linkTargets-release.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/SPIRV-Tools-link/cmake" TYPE FILE FILES "C:/Users/mprze/source/repos/Illuminar/Illuminar/dependencies/shaderc/build/SPIRV-Tools-linkConfig.cmake")
endif()

