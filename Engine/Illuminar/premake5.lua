project "Illuminar"
    kind "StaticLib"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    pchheader "Illuminar_pch.h"
    pchsource "src/Illuminar_pch.cpp"

    files { 
        "src/**.h", 
        "src/**.c", 
        "src/**.hpp", 
        "src/**.cpp",
    }

    includedirs {
		"src",
        "%{IncludeDir.SDL2}",
        "%{IncludeDir.Glad}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.assimp}",
        "%{IncludeDir.spdlog}",
        "%{IncludeDir.spirv_cross}",
        "%{IncludeDir.shaderc}",
        "%{IncludeDir.yaml_cpp}",
        "%{IncludeDir.Box2D}",
	}

    filter "system:windows"
        systemversion "latest"
        
        defines { 
            "ILLUMINAR_PLATFORM_WINDOWS",
        }

    filter "configurations:Debug"
		defines "ILLUMINAR_DEBUG"
		symbols "On"
        
        links { 
            "dependencies/SDL2/lib/x64/SDL2main",
            "dependencies/SDL2/lib/x64/SDL2",
            "dependencies/SDL2/lib/x64/SDL2_image",

            "dependencies/assimp5/lib/Debug/assimp-vc142-mtd",

            "dependencies/shaderc/build/libshaderc/Debug/shaderc",
            "dependencies/shaderc/build/libshaderc_util/Debug/shaderc_util",

            "dependencies/shaderc/build/third_party/glslang/glslang/Debug/glslangd",
            "dependencies/shaderc/build/third_party/glslang/glslang/Debug/GenericCodeGend",
            "dependencies/shaderc/build/third_party/glslang/glslang/Debug/MachineIndependentd",
            "dependencies/shaderc/build/third_party/glslang/glslang/OSDependent/Windows/Debug/OSDependentd",

            "dependencies/shaderc/build/third_party/glslang/hlsl/Debug/HLSLd",
    
            "dependencies/shaderc/build/third_party/glslang/OGLCompilersDLL/Debug/OGLCompilerd",

            "dependencies/shaderc/build/third_party/glslang/SPIRV/Debug/SPIRVd",

            "dependencies/shaderc/build/third_party/spirv-tools/source/Debug/SPIRV-Tools",
            "dependencies/shaderc/build/third_party/spirv-tools/source/opt/Debug/SPIRV-Tools-opt",

            "Glad",
            "spirv-cross",
            "yaml-cpp",
            "spdlog",
            "opengl32",
            "Box2D",
        }

    filter "configurations:Release"
        defines "ILLUMINAR_RELEASE"
        optimize "On"

        links { 
            "dependencies/SDL2/lib/x64/SDL2main",
            "dependencies/SDL2/lib/x64/SDL2",
            "dependencies/SDL2/lib/x64/SDL2_image",
            
            "dependencies/assimp5/lib/Release/assimp-vc142-mt",

            "dependencies/shaderc/build/libshaderc/Release/shaderc",
            "dependencies/shaderc/build/libshaderc_util/Release/shaderc_util",

            "dependencies/shaderc/build/third_party/glslang/glslang/Release/glslang",
            "dependencies/shaderc/build/third_party/glslang/glslang/Release/GenericCodeGen",
            "dependencies/shaderc/build/third_party/glslang/glslang/Release/MachineIndependent",
            "dependencies/shaderc/build/third_party/glslang/glslang/OSDependent/Windows/Release/OSDependent",
            "dependencies/shaderc/build/third_party/glslang/hlsl/Release/HLSL",
            "dependencies/shaderc/build/third_party/glslang/OGLCompilersDLL/Release/OGLCompiler",
            "dependencies/shaderc/build/third_party/glslang/SPIRV/Release/SPIRV",

            "dependencies/shaderc/build/third_party/spirv-tools/source/Release/SPIRV-Tools",
            "dependencies/shaderc/build/third_party/spirv-tools/source/opt/Release/SPIRV-Tools-opt",

            "Glad",
            "spirv-cross",
            "yaml-cpp",
            "spdlog",
            "opengl32",
        }

include "Tests"