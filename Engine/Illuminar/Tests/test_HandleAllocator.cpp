#include <gtest/gtest.h>
#include <memory_resource>

//todo: this function is inside the pch. I have come up with a better way to handle them...
[[nodiscard]] static constexpr size_t operator ""_MB(size_t n)
{
	return n * 1024 * 1024;
}

#include <Utility/Illuminar_Handle.h>
#include <GraphicsEngine/Memory/Illuminar_HandleAllocator.h>

TEST(HandleAllocator, NewObject_ST)
{
	using namespace Illuminar;
	using namespace Illuminar::GFX;

	HandleAllocator allocator;

	for(int i = 0; i < 1000; ++i) {
		Handle<int> handle;

		handle = allocator.new_object<int>(i);
		EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
		int value = *allocator.handle_cast<int*>(handle);
		EXPECT_EQ(value, i);
	}
}

TEST(HandleAllocator, DeleteObject_ST)
{
	using namespace Illuminar;
	using namespace Illuminar::GFX;

	HandleAllocator allocator;

	std::vector<Handle<int>> handles(1000);
	std::fill(handles.begin(), handles.end(), allocator.new_object<int>(5));
		
	for (auto handle : handles) {
		allocator.delete_object<int>(handle);
		EXPECT_EQ(handle.getID(), HandleBase::INVALID_HANDLE_ID);
		int* ptr = allocator.handle_cast<int*>(handle);
		EXPECT_EQ(ptr, nullptr);
	}
}

TEST(HandleAllocator, NewObject_MT)
{
	using namespace Illuminar;
	using namespace Illuminar::GFX;

	HandleAllocator allocator;

	std::mutex m;
	std::vector<Handle<int>> handles;

	std::thread t1([&]() {
		for (int i = 0; i < 1000; ++i) {
			std::lock_guard g(m);
			auto handle = handles.emplace_back(allocator.new_object<int>(i));

			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int value = *allocator.handle_cast<int*>(handle);
			EXPECT_EQ(value, i);
		}
	});
	std::thread t2([&]() {
		for (int i = 1000; i < 2000; ++i) {
			std::lock_guard g(m);
			auto handle = handles.emplace_back(allocator.new_object<int>(i));

			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int value = *allocator.handle_cast<int*>(handle);
			EXPECT_EQ(value, i);
		}
	});
	std::thread t3([&]() {
		for (int i = 2000; i < 3000; ++i) {
			std::lock_guard g(m);
			auto handle = handles.emplace_back(allocator.new_object<int>(i));

			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int value = *allocator.handle_cast<int*>(handle);
			EXPECT_EQ(value, i);
		}
	});

	t1.join();
	t2.join();
	t3.join();

	EXPECT_EQ(handles.size(), 3000);
	for (auto handle : handles) {
		int* ptr = allocator.handle_cast<int*>(handle);
		EXPECT_NE(ptr, nullptr);
		int value = *ptr;
		EXPECT_TRUE(value >= 0 && value < 3000);
	}
}

TEST(HandleAllocator, DeleteObject_MT)
{
	using namespace Illuminar;
	using namespace Illuminar::GFX;

	HandleAllocator allocator;

	std::mutex m;
	std::vector<Handle<int>> handles(3000);
	std::fill(handles.begin(), handles.end(), allocator.new_object<int>(5));

	std::thread t1([&]() {
		for (int i = 0; i < 1000; ++i) {
			std::lock_guard g(m);
			auto& handle = handles[i];
			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			
			allocator.delete_object<int>(handle);
			EXPECT_EQ(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int* ptr = allocator.handle_cast<int*>(handle);
			EXPECT_EQ(ptr, nullptr);
		}
	});

	std::thread t2([&]() {
		for (int i = 1000; i < 2000; ++i) {
			std::lock_guard g(m);
			auto& handle = handles[i];
			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			
			allocator.delete_object<int>(handle);
			EXPECT_EQ(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int* ptr = allocator.handle_cast<int*>(handle);
			EXPECT_EQ(ptr, nullptr);
		}
	});

	std::thread t3([&]() {
		for (int i = 2000; i < 3000; ++i) {
			std::lock_guard g(m);
			auto& handle = handles[i];
			EXPECT_NE(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			
			allocator.delete_object<int>(handle);
			EXPECT_EQ(handle.getID(), HandleBase::INVALID_HANDLE_ID);
			int* ptr = allocator.handle_cast<int*>(handle);
			EXPECT_EQ(ptr, nullptr);
		}
	});

	t1.join();
	t2.join();
	t3.join();

	EXPECT_EQ(handles.size(), 3000);
	for (auto handle : handles) {
		int* ptr = allocator.handle_cast<int*>(handle);
		EXPECT_EQ(handle.getID(), HandleBase::INVALID_HANDLE_ID);
		EXPECT_EQ(ptr, nullptr);
	}
}