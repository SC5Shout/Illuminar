#include <gtest/gtest.h>

//todo: these functions are inside the pch. I have to come up with a better way to handle them...
static constexpr size_t OBJECT_ALIGNMENT = alignof(std::max_align_t);
[[nodiscard]] static constexpr size_t align(size_t v)
{
	return (v + (OBJECT_ALIGNMENT - 1)) & -OBJECT_ALIGNMENT;
}

[[nodiscard]] static constexpr size_t operator ""_MB(size_t n)
{
	return n * 1024 * 1024;
}

#include "GraphicsEngine/Illuminar_CommandBuffer.h"

TEST(CommandBuffer, CommandQueue_ST)
{
	using namespace Illuminar::GFX;

	CommandQueue queue(false);

	int value = 0;
	for (int i = 0; i < 1000; ++i) {
		queue.Submit([&value]() {
			++value;
		});
	}

	queue.Flush();
	bool result = queue.Execute();
	EXPECT_EQ(result, true);
	EXPECT_EQ(value, 1000);
}

TEST(CommandBuffer, CommandQueue_MT)
{
	using namespace Illuminar::GFX;

	CommandQueue queue;

	std::atomic_uint32_t value;

	for (int i = 0; i < 1000; ++i) {
		queue.Submit([&value]() {
			int old = value.fetch_add(1, std::memory_order_relaxed);
			EXPECT_EQ(value.load(std::memory_order_relaxed), old + 1);
		});
	}

	queue.Flush();

	std::thread t([&value, &queue]() {
		while (true) {
			if (!queue.Execute()) {
				break;
			}
		}
	});

	queue.Flush();
	queue.Quit();
	t.join();

	EXPECT_EQ(value, 1000);
}