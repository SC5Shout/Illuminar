#pragma once

#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <string_view>

#include <random>

#include <utility>
#include <algorithm>
#include <functional>
#include <variant>
#include <any>

#include <memory>
#include <memory_resource>

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <tuple>
#include <queue>
#include <deque>
#include <list>

#include <atomic>
#include <thread>
#include <future>
#include <condition_variable>
#include <execution>
#include <semaphore>

#include <cmath>
#include <filesystem>

#include <type_traits>
#include <optional>

#include <numeric>

#include <chrono>

#include <stdexcept>
#include <exception>

//----------------------------

#define BIT(x) 1 << x

template<typename T>
static constexpr bool is_8_bit_v = sizeof(T) == 1 ? true : false;

template<typename T>
static constexpr bool is_16_bit_v = sizeof(T) == 2 ? true : false;

template<typename T>
static constexpr bool is_32_bit_v = sizeof(T) == 4 ? true : false;

template<typename T>
static constexpr bool is_64_bit_v = sizeof(T) == 8 ? true : false;

template<typename T>
struct is_int { static constexpr bool value = false; };

template<> struct is_int<int8_t>  { static constexpr bool value = true; };
template<> struct is_int<uint8_t> { static constexpr bool value = true; };

template<> struct is_int<int16_t>  { static constexpr bool value = true; };
template<> struct is_int<uint16_t> { static constexpr bool value = true; };

template<> struct is_int<int32_t>  { static constexpr bool value = true; };
template<> struct is_int<uint32_t> { static constexpr bool value = true; };

template<> struct is_int<int64_t>  { static constexpr bool value = true; };
template<> struct is_int<uint64_t> { static constexpr bool value = true; };

template<typename T>
static constexpr bool is_int_v = is_int<T>::value;


[[nodiscard]] static constexpr size_t operator ""_KB(size_t n)
{
	return n * 1024;
}

[[nodiscard]] static constexpr size_t operator ""_MB(size_t n)
{
	return n * 1024 * 1024;
}

[[nodiscard]] static constexpr size_t operator ""_GB(size_t n)
{
	return n * 1024 * 1024 * 1024;
}

template <int A, int B>
struct compile_time_pow
{
	static const int value = A * compile_time_pow<A, B - 1>::value;
};

template <int A>
struct compile_time_pow<A, 0>
{
	static const int value = 1;
};

template<int A, int B>
static constexpr auto compile_time_pow_v = compile_time_pow<A, B>::value;

template <typename P, typename T>
[[nodiscard]] static constexpr inline P* add(P* a, T b)
{
	return (P*)(uintptr_t(a) + uintptr_t(b));
}

template <typename P, typename T>
[[nodiscard]] static constexpr inline P* sub(P* a, T b) 
{
	return (P*)(uintptr_t(a) - uintptr_t(b));
}

static constexpr size_t OBJECT_ALIGNMENT = alignof(std::max_align_t);
[[nodiscard]] static constexpr size_t align(size_t v)
{
	return (v + (OBJECT_ALIGNMENT - 1)) & -OBJECT_ALIGNMENT;
}

template <typename P>
[[nodiscard]] static inline P* align(P* p, size_t alignment)
{
	// alignment must be a power-of-two
	//assert(alignment && !(alignment & alignment - 1));
	return (P*)((uintptr_t(p) + alignment - 1) & ~(alignment - 1));
}

template <typename P>
[[nodiscard]] static inline P* align(P* p, size_t alignment, size_t offset)
{
	P* const r = align(add(p, offset), alignment);
	//assert(add(r, -offset) >= p);
	return r;
}

[[nodiscard]] static inline size_t align(size_t size, size_t alignment)
{
	size_t padding = (alignment - (size % alignment)) % alignment;
	return size + padding;

	//return (size + alignment - 1) - size % alignment;
	//return (size + (alignment - 1)) & ~(alignment - 1);
}

template <typename Iterator, typename T>
static inline void radix_sort(Iterator first, Iterator last)
{
	if(first == last) {
		return;
	}

	const T max_divisor = std::pow(10, std::log10(*std::max_element(first, last)));
	for(T divisor = 1; divisor < max_divisor; divisor *= 10) {
		std::array<std::vector<T>, 10> buckets;
		std::for_each(first, last, [&buckets, divisor](const auto i) {
			buckets[(i / divisor) % 10].push_back(i);
		});
		auto out = first;
		for(const auto& bucket : buckets) {
			out = std::copy(bucket.begin(), bucket.end(), out);
		}
	}
}

#include "Thread/Illuminar_TicketMutex.h"

#include "Memory/Illuminar_Ref.h"
#include "Math/Illuminar_Math.h"

#include "Utility/Illuminar_LoggingSystem.h"
#include "Utility/Illuminar_Hash.h"
#include "Utility/Illuminar_MakeID.h"
#include "Utility/Illuminar_UUID.h"
#include "Utility/Illuminar_Handle.h"

namespace Illuminar {
	using AssetID = Hash32;
	using AssetPackageID = Hash32;
	using ResourceID = uint32_t;
	using ResourceLoaderTypeID = Hash32;

	static constexpr uint32_t INVALID_ASSET_ID = std::numeric_limits<uint32_t>::max();
	static constexpr uint32_t INVALID_RESOURCE_ID = std::numeric_limits<uint32_t>::max();
	static constexpr size_t INVALID_INDEX = std::numeric_limits<size_t>::max();

	namespace GFX {
		using RenderGraphResourceHandle = uint16_t;
		static constexpr uint16_t INVALID_RENDER_GRAPH_RESOURCE_HANDLE = std::numeric_limits<uint16_t>::max();
	}

	static constexpr size_t CACHE_LINE = 64;
}

template<typename T>
[[nodiscard]] static constexpr inline T operator |(T a, T b)
{
	return static_cast<T>(static_cast<int>(a) | static_cast<int>(b));
}

template<typename T>
[[nodiscard]] static constexpr inline T operator &(T a, T b)
{
	return static_cast<T>(static_cast<int>(a) & static_cast<int>(b));
}

template<typename T>
[[nodiscard]] static constexpr inline T& operator |=(T& a, T b)
{
	return a = a | b;
}

template <typename T>
static void UpdateState(T& state, const T& expected, std::invocable auto&& func, bool force = false)
{
	if (force || state != expected) {
		state = expected;
		func();
	}
}

template <typename T>
[[nodiscard]] static bool NeedsUpdateState(T& state, const T& expected, bool force = false)
{
	if (force || state != expected) {
		state = expected;
		return true;
	}
	return false;
}