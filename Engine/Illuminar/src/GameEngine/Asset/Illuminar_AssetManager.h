#pragma once

#include "Illuminar_AssetPackage.h"

namespace Illuminar {
	struct AssetManager
	{
		~AssetManager() = default;

		[[nodiscard]] static AssetPackage& AddAssetPackage(AssetPackageID packageID);
		[[nodiscard]] static AssetPackage* AddAssetPackageByVirtualfilename(AssetPackageID packageID, const char* virtualFilename);
		static void RemoveAssetPackage(AssetPackageID packageID);

		[[nodiscard]] static AssetPackage* tryGetAssetPackageByAssetPackageID(AssetPackageID packageID);
		[[nodiscard]] static AssetPackage& getAssetPackageByAssetPackageID(AssetPackageID packageID);

		[[nodiscard]] static const Asset* tryGetAssetByAssetID(AssetID assetID);
		[[nodiscard]] static const Asset& getAssetByAssetID(AssetID assetID);

	private:
		AssetManager() = default;
		AssetManager(const AssetManager& other) = delete;
		AssetManager& operator=(const AssetManager& other) = delete;

		static AssetManager& get()
		{
			static AssetManager instance;
			return instance;
		}

		std::vector<std::unique_ptr<AssetPackage>> assetPackages;
	};
}