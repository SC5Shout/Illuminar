#pragma once

#include "Illuminar_Asset.h"

namespace Illuminar {
	using AssetPackageID = Hash32;
	struct AssetPackage
	{
		AssetPackage() = default;
		~AssetPackage() = default;

		AssetPackage(const AssetPackage& other) = delete;
		AssetPackage& operator=(const AssetPackage& other) = delete;

		AssetPackage(AssetPackageID id) :
			id(id)
		{
		}

		void AddAsset(AssetID assetID, std::string_view virtualFilename);

		[[nodiscard]] const Asset* tryGetAssetByAssetID(AssetID assetID);
		[[nodiscard]] const char* tryGetVirtualFilenameByAssetID(AssetID assetID);

		AssetPackageID id;
		std::vector<Asset> assets;
	};
}