#pragma once

#include "Utility/Illuminar_Hash.h"

namespace Illuminar {
	using AssetID = Hash32;

	struct Asset
	{
		Asset() = default;
		Asset(std::string_view name)
			: virtualFilename(name)
		{
		}

		AssetID id;
		std::string virtualFilename;
	};
}