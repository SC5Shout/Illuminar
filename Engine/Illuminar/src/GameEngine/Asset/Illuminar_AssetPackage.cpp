#include "Illuminar_pch.h"
#include "Illuminar_AssetPackage.h"

namespace Illuminar {
	void AssetPackage::AddAsset(AssetID assetID, std::string_view virtualFilename)
	{
		auto it = std::lower_bound(assets.cbegin(), assets.cend(), assetID, [](const Asset& left, AssetID right) {
			return left.id < right;
		});
		Asset& asset = *assets.insert(it, Asset());
		asset.id = assetID;
		asset.virtualFilename = virtualFilename;
	}

	const Asset* AssetPackage::tryGetAssetByAssetID(AssetID assetID)
	{
		auto it = std::lower_bound(assets.cbegin(), assets.cend(), assetID, [](const Asset& left, AssetID right) {
			return left.id < right;
		});
		return (it != assets.cend() && it->id == assetID) ? &(*it) : nullptr;
	}

	const char* AssetPackage::tryGetVirtualFilenameByAssetID(AssetID assetID)
	{
		const Asset* asset = tryGetAssetByAssetID(assetID);
		return (nullptr != asset) ? asset->virtualFilename.c_str() : nullptr;
	}
}