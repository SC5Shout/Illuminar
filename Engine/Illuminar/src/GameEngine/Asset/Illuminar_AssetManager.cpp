#include "Illuminar_pch.h"
#include "Illuminar_AssetManager.h"

namespace Illuminar {
	AssetPackage& AssetManager::AddAssetPackage(AssetPackageID packageID)
	{
		auto package = std::make_unique<AssetPackage>(packageID);
		get().assetPackages.push_back(std::move(package));
		return *get().assetPackages.back();
	}

	AssetPackage* AssetManager::AddAssetPackageByVirtualfilename(AssetPackageID packageID, const char* virtualFilename)
	{
		AssetPackage* assetPackage = new AssetPackage(packageID);

		EngineLogError("Not implemented yet");

		return assetPackage;
	}

	void AssetManager::RemoveAssetPackage(AssetPackageID packageID)
	{
		std::erase_if(get().assetPackages, [packageID](const auto& assetPackage) { return assetPackage->id == packageID; });
	}

	AssetPackage* AssetManager::tryGetAssetPackageByAssetPackageID(AssetPackageID packageID)
	{
		auto it = std::find_if(get().assetPackages.begin(), get().assetPackages.end(), [packageID](const auto& assetPackage) {
			return (assetPackage->id == packageID);
		});

		return (it != get().assetPackages.cend()) ? (*it).get() : nullptr;
	}

	AssetPackage& AssetManager::getAssetPackageByAssetPackageID(AssetPackageID packageID)
	{
		AssetPackage* package = tryGetAssetPackageByAssetPackageID(packageID);
		if(!package) {
			EngineLogError("Unkon asset package ID");
			__debugbreak();
		}
		return *package;
	}

	const Asset* AssetManager::tryGetAssetByAssetID(AssetID assetID)
	{
		for(const auto& assetPackage : get().assetPackages) {
			const Asset* asset = assetPackage->tryGetAssetByAssetID(assetID);;
			if(asset) {
				return asset;
			}
		} return nullptr;
	}

	const Asset& AssetManager::getAssetByAssetID(AssetID assetID)
	{
		const Asset* asset = tryGetAssetByAssetID(assetID);
		return *asset;
	}
}