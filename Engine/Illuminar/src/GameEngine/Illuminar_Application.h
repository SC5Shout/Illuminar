#pragma once

#include "Window/Illuminar_Window.h"

#include "Input/Illuminar_Input.h"
#include "Illuminar_Timer.h"

#include "GraphicsEngine/RenderTarget/Illuminar_SwapChain.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"
#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

namespace Illuminar {
	struct ApplicationCreateInfo
	{
		uint32_t w = 1;
		uint32_t h = 1;
		std::string name = "Illuminar Application";
	};

	struct Application
	{
		Application(const ApplicationCreateInfo& createInfo);
		virtual ~Application();

		void MainLoop();

		virtual void OnEvent([[maybe_unused]] SDL_Event& event) {}
		virtual void Update([[maybe_unused]] Timestep deltaTime) {}

	protected:
		std::thread renderThread;

		SDL_Event event;
		Ref<Window> window = nullptr;

		Timestep deltaTime = 0.0f;
		double lastTime = 0.0f;		
		
		bool quit = false;
	};

	std::unique_ptr<Application> CreateApplication();
}