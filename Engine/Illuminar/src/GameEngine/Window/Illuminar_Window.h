#pragma once

#include "SDL.h"

#include "GraphicsEngine/RenderTarget/Illuminar_SwapChain.h"

namespace Illuminar {
	struct WindowCreateInfo
	{
		const char* name = "Untitled Window";
		Vector2<uint32_t> position = { SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED };
		Vector2<uint32_t> size = { 800, 600 };
		uint32_t flags = SDL_WINDOW_SHOWN;
	};

	struct ImGuiWindow;
	struct Window : RefCount
	{
		Window() = default;
		virtual ~Window() = default;

		[[nodiscard]] static Ref<Window> Create(const WindowCreateInfo& createInfo);

		[[nodiscard]] inline SDL_Window* getHandle() const { return window; }

		virtual void MakeCurrent() = 0;
		
		inline void Present() { swapChain->Present(this); }

		template<typename T = uint32_t>
		[[nodiscard]] inline Vector2<T> getSize() const 
		{
			int w, h;
			SDL_GetWindowSize(window, &w, &h);
			return Vector2<T>{ (T)w, (T)h };
		}

		[[nodiscard]] inline Vector2<uint32_t> getPosition() const 
		{ 
			int x, y;
			SDL_GetWindowPosition(window, &x, &y);
			return { (uint32_t)x, (uint32_t)y };
		};

		[[nodiscard]] inline uint32_t getWindowID() const { return SDL_GetWindowID(window); }

		inline void setPosition(const Vector2<uint32_t>& position) { SDL_SetWindowPosition(window, position.x, position.y); }
		inline void setSize(const Vector2<uint32_t>& size) { SDL_SetWindowSize(window, size.x, size.y); }
		inline void setName(const char* name) { SDL_SetWindowTitle(window, name); }
		inline void MinimizeWindow() { SDL_MinimizeWindow(window); }
		inline void MaximizeWindow() { SDL_MaximizeWindow(window); }
		inline void ShowWindow() { SDL_ShowWindow(window); }
		inline void HideWindow() { SDL_HideWindow(window); }
		inline void setFocus() { SDL_RaiseWindow(window); }
		inline void setOpacity(float opacity) { SDL_SetWindowOpacity(window, opacity); }

		[[nodiscard]] inline uint32_t getFlags() const { return SDL_GetWindowFlags(window); }
		[[nodiscard]] inline bool isMinimized() const { return (getFlags() & SDL_WINDOW_MINIMIZED) != 0; }
		[[nodiscard]] inline bool isMaximized() const { return (getFlags() & SDL_WINDOW_MAXIMIZED) != 0; }
		[[nodiscard]] inline bool isFocused() const { return (getFlags() & SDL_WINDOW_INPUT_FOCUS) != 0; }

	//protected:
		Ref<GFX::SwapChain> swapChain = GFX::SwapChain::Create();
		SDL_Window* window = nullptr;
	};
}