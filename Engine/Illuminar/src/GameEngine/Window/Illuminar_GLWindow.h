#pragma once

#include "GameEngine/Window/Illuminar_Window.h"

namespace Illuminar {
	struct GLWindow : Window
	{
		GLWindow() = default;
		GLWindow(const WindowCreateInfo& createInfo);
		~GLWindow() override;

		void MakeCurrent() override;
	};
}