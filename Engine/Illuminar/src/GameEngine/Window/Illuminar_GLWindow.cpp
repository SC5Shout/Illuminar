#include "Illuminar_pch.h"
#include "Illuminar_GLWindow.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"
#include "GraphicsEngine/OpenGL/Illuminar_GLRenderer.h"

namespace Illuminar {
	GLWindow::GLWindow(const WindowCreateInfo& createInfo)
	{
		window = SDL_CreateWindow(createInfo.name, createInfo.position.x, createInfo.position.y, createInfo.size.x, createInfo.size.y, createInfo.flags | SDL_WINDOW_OPENGL);
	}

	GLWindow::~GLWindow()
	{
		//should (or even can) window be destroyed by the render thread?
		SDL_DestroyWindow(window);
	}

	void GLWindow::MakeCurrent()
	{
		GFX::RenderCommand::Submit([window = window]() {
			SDL_GL_MakeCurrent(window, GFX::RenderCommand::as<GFX::GLRenderer>()->context);
		});
	}
}