#include "Illuminar_pch.h"
#include "Illuminar_Window.h"

#include "GraphicsEngine/Illuminar_Renderer.h"
#include "Illuminar_GLWindow.h"

namespace Illuminar {
	Ref<Window> Window::Create(const WindowCreateInfo& createInfo)
	{
		if (GFX::Renderer::api == GFX::Renderer::API::OpenGL) {
			return Ref<GLWindow>::Create(createInfo);
		} else return nullptr;
		return nullptr;
	}
}
