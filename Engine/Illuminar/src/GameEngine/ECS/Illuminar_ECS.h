#pragma once

#include "Illuminar_ECSSystem.h"

namespace Illuminar {
	struct ECS
	{
		struct Listener
		{
			template<auto FuncT, typename Type>
			void Connect(Type&& type)
			{
				signals.push_back(std::bind(FuncT, std::forward<Type>(type), std::placeholders::_1, std::placeholders::_2));
			}

			void Disconnect()
			{
				signals.clear();
			}

		private:
			friend struct ECS;
			void Execute(ECS& ecs, EntityHandle handle)
			{
				for (auto& signal : signals) {
					signal(ecs, handle);
				}
			}

			std::vector<std::function<void(ECS&, EntityHandle)>> signals;
		};

		typedef std::vector<std::pair<size_t, size_t>> Entity;
		typedef std::pair<size_t, Entity> EntityRawType;

		ECS() = default;
		~ECS();

		EntityHandle CreateEntity();
		bool DestroyEntity(EntityHandle handle);

		void Update(ECSSystemList& systems, [[maybe_unused]] Timestep deltaTime);

		template<typename Component, typename ... Args>
		inline Component* AddComponent(EntityHandle entity, Args&& ... args)
		{
			components[Component::ID].push_back(new Component(std::forward<Args>(args)...));
			Component* newComponent = (Component*)components[Component::ID].back();
			newComponent->entity = entity;
			HandleToEntity(entity).emplace_back(Component::ID, components.at(Component::ID).size() - 1);

			if (onCreateListeners.contains(Component::ID)) {
				onCreateListeners[Component::ID].Execute(*this, entity);
			}

			return newComponent;
		}

		template<typename Component>
		inline bool RemoveComponent(EntityHandle entity)
		{
			return RemoveComponentInternal(entity, Component::ID);
		}

		template<typename Component>
		[[nodiscard]] inline Component* getComponent(EntityHandle entity)
		{
			return (Component*)getComponentInternal(HandleToEntity(entity), components[Component::ID], Component::ID);
		}

		template<typename Func>
		inline void each(Func&& func)
		{
			for(const auto& entityRaw : entities) {
				EntityHandle entity = (EntityHandle)entityRaw;
				func(entity);
			}
		}

		template<typename T>
		[[nodiscard]] inline std::vector<T*>& getComponentsOfTypeT()
		{
			return reinterpret_cast<std::vector<T*>&>(components[T::ID]);
		}

		template<typename T>
		[[nodiscard]] inline const std::vector<T*>& getComponentsOfTypeT() const
		{
			return reinterpret_cast<const std::vector<T*>&>(components[T::ID]);
		}

		[[nodiscard]] inline const std::vector<EntityRawType*>& getEntities() const
		{
			return entities;
		}

		template<typename Component>
		Listener& OnCreate()
		{
			return onCreateListeners[Component::ID];
		}

		template<typename Component>
		Listener& OnDestroy()
		{
			return onDestroyListeners[Component::ID];
		}

	//private:
		ECS(const ECS& other) = delete;
		ECS& operator=(const ECS& other) = delete;

		bool RemoveComponentInternal(EntityHandle handle, size_t componentID);
		void DeleteComponent(size_t componentID, size_t index);

		[[nodiscard]] BaseECSComponent* getComponentInternal(Entity& entityComponents, std::vector<BaseECSComponent*>& vector, size_t componentID);

		[[nodiscard]] size_t FindLeastCommonComponent(const std::vector<size_t>& componentTypes);

		[[nodiscard]] inline EntityRawType* HandleToRawType(EntityHandle handle) const { return (EntityRawType*)handle; }
		[[nodiscard]] inline size_t HandleToEntityIndex(EntityHandle handle) const { return HandleToRawType(handle)->first; }
		[[nodiscard]] inline Entity& HandleToEntity(EntityHandle handle) const { return HandleToRawType(handle)->second; }

		std::vector<EntityRawType*> entities;
		std::unordered_map<size_t, std::vector<BaseECSComponent*>> components;

		std::unordered_map<size_t, Listener> onCreateListeners;
		std::unordered_map<size_t, Listener> onDestroyListeners;
	};
}