#include "Illuminar_pch.h"
#include "Illuminar_ECS.h"

namespace Illuminar {
    ECS::~ECS()
    {
        for(auto& componentType : components) {
            for(const auto& component : componentType.second) {
                delete component;
            }
        }

        for(const auto& entity : entities) {
            delete entity;
        }
    }

    EntityHandle ECS::CreateEntity()
    {
        auto newEntity = new EntityRawType;
        EntityHandle handle = (EntityHandle)newEntity;

        newEntity->first = entities.size();
        entities.push_back(std::move(newEntity));
        return handle;
    }

    bool ECS::DestroyEntity(EntityHandle handle)
    {
        const Entity& entityComponents = HandleToEntity(handle);
        for(const auto [componentID, index] : entityComponents) {
            DeleteComponent(componentID, index);
        }

        if(entities.size() > 1) {
            const size_t destIndex = HandleToEntityIndex(handle);
            const size_t srcIndex = entities.size() - 1;
            delete entities[destIndex];
            entities[destIndex] = std::move(entities[srcIndex]);
            entities[destIndex]->first = destIndex;
            entities.pop_back();
        } else {
            delete entities[0];
            entities.pop_back();
        }

        return true;
    }

    void ECS::Update(ECSSystemList& systems, Timestep deltaTime)
    {
        //TODO: better naming
        std::vector<std::vector<BaseECSComponent*>*> componentVectors;
        std::vector<BaseECSComponent*> componentsToUpdate;
        for(const auto& system : systems) {
            const auto& componentTypes = system->componentTypes;
            if(componentTypes.size() == 1U) {
                const auto& component = components.at(componentTypes.at(0));
                for(BaseECSComponent* c : component) {
                    system->Update(deltaTime, &c);
                }
            } else {
                componentsToUpdate.resize(Max(componentsToUpdate.size(), componentTypes.size()));
                componentVectors.resize(Max(componentVectors.size(), componentTypes.size()));

                for(size_t i = 0; i < componentTypes.size(); i++) {
                    componentVectors[i] = &components[componentTypes[i]];
                }

                const size_t minSizeIndex = FindLeastCommonComponent(componentTypes);

                const auto& c = *componentVectors.at(minSizeIndex);
                for(const auto& c2 : c) {
                    componentsToUpdate[minSizeIndex] = c2;

                    Entity& entityComponents = HandleToEntity(componentsToUpdate.at(minSizeIndex)->entity);

                    bool isValid = true;
                    for(size_t j = 0; j < componentTypes.size(); j++) {
                        if(j == minSizeIndex) {
                            continue;
                        }

                        componentsToUpdate[j] = getComponentInternal(entityComponents, *componentVectors[j], componentTypes[j]);
                        if(!componentsToUpdate.at(j)) {
                            isValid = false;
                            break;
                        }
                    }

                    if(isValid) {
                        system->Update(deltaTime, &componentsToUpdate[0]);
                    }
                }
            }
        }
    }

    bool ECS::RemoveComponentInternal(EntityHandle handle, size_t componentID)
    {
        Entity& entityComponents = HandleToEntity(handle);
        for(size_t i = 0; i < entityComponents.size(); i++) {
            const auto& [ID, index] = entityComponents.at(i);
            if(componentID == ID) {
                DeleteComponent(ID, index);

                const size_t indexToRemove = i;
                const size_t lastIndex = entityComponents.size() - 1;
                entityComponents[indexToRemove] = entityComponents[lastIndex];
                entityComponents.pop_back();

                return true;
            }
        } return false;
    }

    BaseECSComponent* ECS::getComponentInternal(Entity& entityComponents, std::vector<BaseECSComponent*>& vector, size_t componentID)
    {
        for(const auto& [ID, index] : entityComponents) {
            if(ID == componentID) {
                return vector[index];
            }
        } return nullptr;
    }

    void ECS::DeleteComponent(size_t componentID, size_t index)
    {
        auto& componentsPool = components[componentID];

        const size_t lastIndex = componentsPool.size() - 1;

        if (onDestroyListeners.contains(componentID)) {
            onDestroyListeners[componentID].Execute(*this, componentsPool.back()->entity);
        }

        if(index == lastIndex) {
            componentsPool.pop_back();
            return;
        }

        const auto lastComponent = componentsPool.back();
        const size_t componentToRemove = index;
        componentsPool[componentToRemove] = componentsPool[lastIndex];

        Entity& entityComponents = HandleToEntity(lastComponent->entity);
        for(auto& [ID, componentIndex] : entityComponents) {
            if(componentID == ID && lastIndex == componentIndex) {
                componentIndex = index;
                break;
            }
        }

        componentsPool.pop_back();
    }

    size_t ECS::FindLeastCommonComponent(const std::vector<size_t>& componentTypes)
    {
        size_t minSize = -1;
        size_t minIndex = -1;
        for(size_t i = 0; i < componentTypes.size(); i++) {
            const size_t size = components.at(componentTypes.at(i)).size();
            if(size <= minSize) {
                minSize = size;
                minIndex = i;
            }
        } 
        return minIndex;
    }
}