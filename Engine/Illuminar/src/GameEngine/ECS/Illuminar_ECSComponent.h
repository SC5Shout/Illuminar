#pragma once

namespace Illuminar {
	typedef void* EntityHandle;
	static constexpr EntityHandle INVALID_ENTITY_HANDLE = nullptr;

	struct BaseECSComponent 
	{
		virtual ~BaseECSComponent() = default;
		EntityHandle entity = INVALID_ENTITY_HANDLE;
	};

	template<typename T>
	struct ECSComponent : BaseECSComponent
	{
		virtual ~ECSComponent() override = default;

		static const size_t ID;
		static const size_t SIZE;
	};

	//TODO: consider using IDs from 0....x instead of hash codes
	template<typename T>
	const size_t ECSComponent<T>::ID(typeid(T).hash_code());

	template<typename T>
	const size_t ECSComponent<T>::SIZE(sizeof(T));
}