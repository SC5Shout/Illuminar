#pragma once

#include "GameEngine/Illuminar_Timestep.h"
#include "Illuminar_ECSComponent.h"

namespace Illuminar {
	struct BaseECSSystem
	{
		virtual ~BaseECSSystem() = default;

		virtual void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components) = 0;

	protected:
		friend struct ECS;

		void AddComponentType(size_t componentType)
		{
			componentTypes.push_back(componentType);
		}

		std::vector<size_t> componentTypes;
	};

	template<typename ... Args>
	struct ECSSystem : BaseECSSystem
	{
		ECSSystem()
		{
			(AddComponentType(Args::ID), ...);
		}

		virtual ~ECSSystem() = default;
	};

	struct ECSSystemList
	{
		template<typename System, typename... Args>
		inline void AddSystem(Args&& ... args)
		{
			systems.push_back(std::make_unique<System>(std::forward<Args>(args)...));
		}

		inline void RemoveSystem(BaseECSSystem& system)
		{
			std::erase_if(systems, [&](const auto& toRemove) { return &system == toRemove.get(); });
		}

		[[nodiscard]] inline BaseECSSystem* operator[](size_t index)
		{
			return systems[index].get();
		}

		[[nodiscard]] inline size_t size()
		{
			return systems.size();
		}

		[[nodiscard]] auto begin() { return systems.begin(); }
		[[nodiscard]] auto end() { return systems.end(); }

	private:
		std::vector<std::unique_ptr<BaseECSSystem>> systems;
	};
}