#include "Illuminar_pch.h"
#include "Illuminar_Scene.h"

#include "box2d/box2d.h"

#include "GraphicsEngine/Renderer/Illuminar_Renderer2D.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar {
	void UpdateNativeScript::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		NativeScriptComponent& script = *(NativeScriptComponent*)components[0];

		if(script.instance) {
			script.instance->Update(deltaTime);
		}
	}

	template<typename T>
	struct CopySystem : ECSSystem<T>
	{
		CopySystem(ECS& copyTo)
			: copyTo(copyTo)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components)
		{
			const T& t = *(T*)components[0];


		}

		ECS& copyTo;
	};

	Ref<Scene> Scene::Copy(const Ref<Scene>& other)
	{
		auto target = Ref<Scene>::Create();

		Ref<GFX::ForwardPlusRenderer> sceneRenderer = nullptr;

		ECS& ecs = target->ecs;

		for (auto entityRawPtr : other->ecs.getEntities()) {
			ECS::Entity entity = ecs.HandleToEntity(entityRawPtr);
			ecs.CreateEntity();

		}

		target->viewport = other->viewport;

		return target;
	}

	Scene::Scene()
	{
		sceneRenderer = GFX::ForwardPlusRenderer::Create();

		//renderer2D.Create();

		renderSystems.AddSystem<DirectionalLightSystem>(this);
		renderSystems.AddSystem<PointLightSystem>(this);
		renderSystems.AddSystem<SpotLightSystem>(this);
		renderSystems.AddSystem<MeshRenderSystem>(this);
		renderSystems.AddSystem<SkylightSystem>(this);
		
		render2DSystems.AddSystem<SpriteRenderSystem>(this);
		
		scriptSystems.AddSystem<UpdateNativeScript>();
		
		start2DPhysicsSystems.AddSystem<CreateRigidBodyPhysics2D>(this);
		start2DPhysicsSystems.AddSystem<CreateBoxColliderPhysics2D>(this);
		physics2DUpdateSystem.AddSystem<UpdatePhysics2D>(this);
	}

	Scene::~Scene()
	{
	}

	void Scene::StartSimulation()
	{
		physicsWorld = new b2World(b2Vec2(0.0f, -9.8f));

		ecs.Update(start2DPhysicsSystems, Timestep());
	}

	void Scene::EndSimulation()
	{
		delete physicsWorld;
		physicsWorld = nullptr;
	}

	void Scene::setViewport(uint32_t w, uint32_t h)
	{
		viewport.x = w; 
		viewport.y = h;
	}

	void Scene::UpdateRuntime([[maybe_unused]] Timestep deltaTime)
	{
		{
			for (auto component : ecs.getComponentsOfTypeT<NativeScriptComponent>()) {
				NativeScriptComponent& scriptComponent = *(NativeScriptComponent*)component;
				if (scriptComponent.scriptType != NativeScriptType::EmptyScript) {
					if (!scriptComponent.instance) {
						scriptComponent.instance = scriptComponent.CreateScript();
						scriptComponent.instance->entity = Entity{ scriptComponent.entity, this };
						scriptComponent.instance->Create();
					}
				}
			}
		}

		ecs.Update(scriptSystems, deltaTime);

		if (Entity camera = getMainCamera()) {
			if (camera.handle && camera.scene == this) {
				SceneCamera& sceneCamera = *camera.getComponent<CameraComponent>();

				//TODO: do not hardcode width and height
				sceneCamera.setViewport(16, 9);

				const Mat4& transform = camera.getComponent<TransformComponent>()->getTransform();

				//fbo->Bind();

				//renderer2D.Begin(sceneCamera, transform);
				//ecs.Update(render2DSystems, deltaTime);
				//renderer2D.End();

				//fbo->Unbind();
				//sceneRenderer->Begin(sceneCamera, glm::inverse(transform));
				//ecs.Update(renderSystems, deltaTime);
				//sceneRenderer->End();
			}
		}
	}

	void Scene::UpdateEditor([[maybe_unused]] Timestep deltaTime, const Camera& camera, const Mat4& viewMatrix)
	{
		{
			for (auto component : ecs.getComponentsOfTypeT<NativeScriptComponent>()) {
				NativeScriptComponent& scriptComponent = *(NativeScriptComponent*)component;
				if (scriptComponent.scriptType != NativeScriptType::EmptyScript) {
					if (!scriptComponent.instance) {
						scriptComponent.instance = scriptComponent.CreateScript();
						scriptComponent.instance->entity = Entity{ scriptComponent.entity, this };
						scriptComponent.instance->Create();
					}
				}
			}
		}

		if(physicsWorld) {
			const int32_t velocityIterations = 6;
			const int32_t positionIterations = 2;
			physicsWorld->Step((float)deltaTime, velocityIterations, positionIterations);

			ecs.Update(physics2DUpdateSystem, deltaTime);
		}

		ecs.Update(scriptSystems, deltaTime);

		auto cameraCopy = camera;
		cameraCopy.projection = glm::perspectiveFov(glm::radians(45.0f), (float)viewport.x, (float)viewport.y, 0.01f, 1000.0f);
		sceneRenderer->Begin(cameraCopy, viewMatrix);
		ecs.Update(renderSystems, deltaTime);
		sceneRenderer->End();

		//temp
		//fbo->Bind();
		//
		//renderer2D.Begin(camera, glm::inverse(viewMatrix));
		//ecs.Update(render2DSystems, deltaTime);
		//renderer2D.End();
		//
		////temp
		//fbo->Unbind();
	}

	Entity Scene::CreateEntity(const std::string& name, Entity parent)
	{
		auto entity = Entity(this);
		auto id = entity.AddComponent<IDComponent>(UUID{});
		auto transform = entity.AddComponent<TransformComponent>();
		auto tag = entity.AddComponent<TagComponent>(name);
		auto hierarchy = entity.AddComponent<HierarchyComponent>();

		if(parent != INVALID_ENTITY) {
			ParentEntity(entity, parent);
		}

		return entity;
	}

	Entity Scene::CreateEntityWithID(const std::string& name, UUID ID, Entity parent)
	{
		auto entity = Entity(this);
		auto id = entity.AddComponent<IDComponent>(ID); 
		auto transform = entity.AddComponent<TransformComponent>();
		auto tag = entity.AddComponent<TagComponent>(name);
		auto hierarchy = entity.AddComponent<HierarchyComponent>();

		if(parent != INVALID_ENTITY) {
			ParentEntity(entity, parent);
		}

		return entity;
	}

	Entity Scene::CreateCameraEntity(const std::string& name, SceneCamera::ProjectionType projectionType, Entity parent)
	{
		auto entity = CreateEntity(name, parent);
		auto camera = entity.AddComponent<CameraComponent>();
		camera->camera.projectionType = projectionType;

		return entity;
	}

	Entity Scene::CreateLightEntity(const std::string& name, LightType lightType, Entity parent)
	{
		auto entity = CreateEntity(name, parent);
		auto light = entity.AddComponent<LightComponent>();
		light->type = lightType;

		return entity;
	}

	Entity Scene::CreateMeshEntity(const std::string& name, MeshResourceID meshID, Entity parent)
	{
		auto& meshManager = ResourceManagers::get().meshResourceManager;
		
		MeshResource* resource = meshManager.tryGetResourceByID(meshID);
		if (!resource) {
			EngineLogFatal("Could not find {}", name);
			__debugbreak();
		}
		//todo better check
		if (!resource->Loaded()) {
			return {};
		}

		Entity entity = CreateEntity(name, parent);
		auto meshComp = entity.AddComponent<MeshComponent>();
		meshComp->meshID = resource->id;
		meshComp->name = resource->mesh->name;

		BuildMeshHierarchy(entity, resource, resource->scene, resource->scene->mRootNode);

		return entity;
	}

	void Scene::DestroyEntity(Entity entity)
	{
		const auto& children = entity.getChildrenID();
		std::for_each(children.crbegin(), children.crend(), [this](auto childID) {
			DestroyEntity(FindEntityByID(childID));
		});

		//for(auto childId : children) {
		//	DestroyEntity(FindEntityByHandle(childId));
		//}

		UnparentEntity(entity);
		ecs.DestroyEntity(entity);
	}

	Entity Scene::FindEntityByID(UUID id)
	{
		const auto& components = ecs.getComponentsOfTypeT<IDComponent>();
		for (const auto component : components) {
			if (component->id == id) {
				return Entity(component->entity, this);
			}
		}
	
		return Entity{};
	}
	
	void Scene::ConvertToLocalSpace(Entity entity)
	{
		const Entity parent = FindEntityByID(entity.getParentID());
	
		if(!parent) {
			return;
		}
	
		auto transform = entity.getComponent<TransformComponent>();
		const Mat4 parentTransform = getWorldSpaceTransformMatrix(parent);
		const Mat4 localTransform = glm::inverse(parentTransform) * transform->getTransform();
		DecomposeTransform(localTransform, transform->translation, transform->rotation, transform->scale);
	}
	
	void Scene::ConvertToWorldSpace(Entity entity)
	{
		const Entity parent = FindEntityByID(entity.getParentID());
	
		if(!parent) {
			return;
		}
	
		Mat4 transform = getTransformRelativeToParent(entity);
		auto entityTransform = entity.getComponent<TransformComponent>();
		DecomposeTransform(transform, entityTransform->translation, entityTransform->rotation, entityTransform->scale);
	}
	
	Mat4 Scene::getTransformRelativeToParent(Entity entity)
	{
		Mat4 transform(1.0f);
	
		Entity parent = FindEntityByID(entity.getParentID());
	
		if(parent) {
			transform = getTransformRelativeToParent(parent);
		}
	
		return transform * entity.getComponent<TransformComponent>()->getTransform();
	}
	
	Mat4 Scene::getWorldSpaceTransformMatrix(Entity entity)
	{
		Mat4 transform = entity.getComponent<TransformComponent>()->getTransform();
	
		while(Entity parent = FindEntityByID(entity.getParentID())) {
			transform = parent.getComponent<TransformComponent>()->getTransform() * transform;
			entity = parent;
		}
	
		return transform;
	}
	
	TransformComponent Scene::getWorldSpaceTransform(Entity entity)
	{
		Mat4 transform = getWorldSpaceTransformMatrix(entity);
		TransformComponent transformComponent;
	
		DecomposeTransform(transform, transformComponent.translation, transformComponent.rotation, transformComponent.scale);
	
		//glm::quat rotationQuat = glm::quat(transformComponent.rotation);
		//transformComponent.Up = glm::normalize(glm::rotate(rotationQuat, glm::vec3(0.0f, 1.0f, 0.0f)));
		//transformComponent.Right = glm::normalize(glm::rotate(rotationQuat, glm::vec3(1.0f, 0.0f, 0.0f)));
		//transformComponent.Forward = glm::normalize(glm::rotate(rotationQuat, glm::vec3(0.0f, 0.0f, -1.0f)));
	
		return transformComponent;
	}
	
	void Scene::ParentEntity(Entity entity, Entity parent)
	{
		if(parent.isDescendantOf(entity)) {
			UnparentEntity(parent);
	
			Entity newParent = FindEntityByID(entity.getParentID());
			if(newParent) {
				UnparentEntity(entity);
				ParentEntity(parent, newParent);
			}
		} else {
			Entity previousParent = FindEntityByID(entity.getParentID());
			if(previousParent) {
				UnparentEntity(entity);
			}
		}
	
		entity.setParentID(parent.getID());
		parent.getChildrenID().push_back(entity.getID());
	
		ConvertToLocalSpace(entity);
	}
	
	void Scene::UnparentEntity(Entity entity, bool convertToWorldSpace)
	{
		Entity parent = FindEntityByID(entity.getParentID());
		if(!parent) {
			return;
		}
	
		auto& parentChildren = parent.getChildrenID();
		parentChildren.erase(std::remove(parentChildren.begin(), parentChildren.end(), entity.getID()), parentChildren.end());
	
		if(convertToWorldSpace) {
			ConvertToWorldSpace(entity);
		}
	
		entity.setParentID(0);
	}

	Entity Scene::getMainCamera()
	{
		for(const auto& component : ecs.getComponentsOfTypeT<CameraComponent>()) {
			const CameraComponent& cameraComponent = *(CameraComponent*)component;
			return Entity(cameraComponent.entity, this);
		} return Entity(INVALID_ENTITY_HANDLE, nullptr);
	}

	void Scene::BuildMeshHierarchy(Entity parent, MeshResource* resource, const void* assimpScene, void* assimpNode)
	{
		aiScene* aScene = (aiScene*)assimpScene;
		aiNode* node = (aiNode*)assimpNode;

		if (node == aScene->mRootNode && node->mNumMeshes == 0) {
			for (uint32_t i = 0; i < node->mNumChildren; i++)
				BuildMeshHierarchy(parent, resource, assimpScene, node->mChildren[i]);

			return;
		}

		if (node->mNumMeshes == 1) {
			auto nodeName = node->mName.C_Str();

			Entity nodeEntity = CreateEntity(nodeName, parent);

			uint32_t submeshIndex = node->mMeshes[0];
			auto submeshComponent = nodeEntity.AddComponent<SubmeshComponent>();
			submeshComponent->submeshIndex = submeshIndex;
			submeshComponent->meshID = resource->id;
			submeshComponent->submesh = resource->mesh->submeshes[submeshIndex];

			auto& transformComponent = *nodeEntity.getComponent<TransformComponent>();
			transformComponent = resource->mesh->submeshes[submeshIndex].transform;
		} else if (node->mNumMeshes > 1) {
			for (uint32_t i = 0; i < node->mNumMeshes; i++) {
				uint32_t submeshIndex = node->mMeshes[i];
				auto meshName = aScene->mMeshes[submeshIndex]->mName.C_Str();

				Entity meshEntity = CreateEntity(meshName, parent);
				auto submeshComponent = meshEntity.AddComponent<SubmeshComponent>();
				submeshComponent->submeshIndex = submeshIndex;
				submeshComponent->meshID = resource->id;
				submeshComponent->submesh = resource->mesh->submeshes[submeshIndex];

				auto& transformComponent = *meshEntity.getComponent<TransformComponent>();
				transformComponent = resource->mesh->submeshes[submeshIndex].transform;
			}
		}

		for (uint32_t i = 0; i < node->mNumChildren; i++)
			BuildMeshHierarchy(parent, resource, assimpScene, node->mChildren[i]);
	}
}