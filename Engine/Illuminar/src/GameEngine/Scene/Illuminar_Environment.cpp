#include "Illuminar_pch.h"
#include "Illuminar_Environment.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "glad/glad.h"

namespace Illuminar {
    std::shared_ptr<Environment> Environment::CreatePreethamSky(float turbidity, float azimuth, float inclination)
    {
        using namespace GFX;
    
        const uint32_t cubemapSize = 2048;
        auto cubemap = TextureCubemap::Create({ 
            .name = "PreethamSky",
            .w = cubemapSize, 
            .h = cubemapSize, 
            .parameters = TextureParameters().Format(TextureFormat::RGBA32_FLOAT).Wrap(TextureWrap::Clamp_To_Edge)
        });
    
        const auto shader = ShaderCache::getShader("PreethamSky");

        GFX::Material::CreateInfo materialInfo;
        materialInfo.shader = shader;
        materialInfo.name = "PreethamSky";
        materialInfo.materialDomain = MaterialDomain::Surface;
        materialInfo.blendMode = BlendMode::Opaque;
        materialInfo.shadingModel = ShadingModel::Unlit;
        auto material = MaterialCache::Create(materialInfo);
        
        material->set("u_parameters.turbidity", turbidity);
        material->set("u_parameters.azimuth", azimuth);
        material->set("u_parameters.inclination", inclination);
        material->Bind();
    
        cubemap->BindImage(0, 0, ImageMapBit::Write, TextureFormat::RGBA32_FLOAT);
        GFX::RenderCommand::DispatchCompute(cubemapSize / 32, cubemapSize / 32, 6);
        cubemap->GenerateMipmap();

        RenderCommand::MemoryBarrier(MemoryBarrierType::ShaderImageAccess);
    
        return std::make_shared<Environment>(cubemap, cubemap);
    }
    
    std::shared_ptr<Environment> Environment::CreateFromHDRTexture(std::string_view filepath)
    {
        //todo: cache env maps
        using namespace Illuminar::GFX;
    
        int width, height, nrComponents;
        float* data = stbi_loadf(filepath.data(), &width, &height, &nrComponents, 0);
        auto equirectangularTexture = Texture2D::Create({
            .name = "equirectangularTexture",
            .w = (uint32_t)width,
            .h = (uint32_t)height,
            .pixels = (const void*)data,
            .parameters = TextureParameters().Format(nrComponents == 4 ? TextureFormat::RGBA32_FLOAT : TextureFormat::RGB32_FLOAT).Wrap(TextureWrap::Clamp_To_Edge)
        });
    
        GFX::RenderCommand::Submit([data]() {
            stbi_image_free(data);
        });
    
        //------------------------------------------------------------------------------------------------------------
    
        static constexpr uint32_t radianceCubemapSize = 2048;
        auto radianceCubemap = TextureCubemap::Create({
            .name = "radianceCubemap",
            .w = radianceCubemapSize,
            .h = radianceCubemapSize,
            .parameters = TextureParameters().Format(TextureFormat::RGBA32_FLOAT).Wrap(TextureWrap::Clamp_To_Edge)
        });
    
        auto equToCubemap = ShaderCache::getShader("ComputeEquToCubemap");
        equToCubemap->Bind();
    
        radianceCubemap->BindImage(0, 0, ImageMapBit::Write, TextureFormat::RGBA32_FLOAT);
        equirectangularTexture->Bind();

        RenderCommand::DispatchCompute(radianceCubemapSize / 32, radianceCubemapSize / 32, 6);

        RenderCommand::MemoryBarrier(MemoryBarrierType::ShaderImageAccess);

        //------------------------------------------------------------------------------------------------------------
    
        static constexpr uint32_t irradianceCubemapSize = 32;
        auto irradianceCubemap = TextureCubemap::Create({
            .name = "irradianceCubemap",
            .w = irradianceCubemapSize,
            .h = irradianceCubemapSize,
            .parameters = TextureParameters().Format(TextureFormat::RGBA32_FLOAT).Wrap(TextureWrap::Clamp_To_Edge)
        });
    
        auto computeIrradiance = ShaderCache::getShader("ComputeIrradiance");
        computeIrradiance->Bind();
    
        irradianceCubemap->BindImage(0, 0, ImageMapBit::Write, TextureFormat::RGBA32_FLOAT);
        radianceCubemap->Bind();
        RenderCommand::DispatchCompute(irradianceCubemapSize / 32, irradianceCubemapSize / 32, 6);

        irradianceCubemap->GenerateMipmap();

        RenderCommand::MemoryBarrier(MemoryBarrierType::ShaderImageAccess);

        auto result = std::make_shared<Environment>(radianceCubemap, irradianceCubemap);
        result->filepath = filepath;
    
        return result;
    }
}