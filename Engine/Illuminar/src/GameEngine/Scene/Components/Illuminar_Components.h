#pragma once

#include "GameEngine/Resource/Mesh/Illuminar_MeshResource.h"

#include "GameEngine/Scene/Illuminar_SceneCamera.h"
#include "GameEngine/Script/Illuminar_NativeScriptEngine.h"
#include "GameEngine/Script/Illuminar_NativeScriptType.h"

#include "GameEngine/Scene/Illuminar_Environment.h"

#include "GraphicsEngine/Illuminar_GFX.h"

namespace Illuminar {	
	struct IDComponent : ECSComponent<IDComponent>
	{
		IDComponent() = default;
		IDComponent(UUID ID) : id(ID) {}
		IDComponent(uint64_t ID) : id(ID) {}

		UUID id = 0;
	};

	struct TagComponent : ECSComponent<TagComponent>
	{
		TagComponent() = default;
		TagComponent(const std::string& tag) : tag(tag) {}

		std::string tag;

		operator std::string& () { return tag; }
		operator const std::string& () const { return tag; }
	};

	struct TransformComponent : ECSComponent<TransformComponent>
	{
		TransformComponent() = default;
		TransformComponent(const TransformComponent&) = default;

		TransformComponent(const Mat4& transform)
		{
			DecomposeTransform(transform, translation, rotation, scale);
		}

		TransformComponent& operator=(const Mat4& transform)
		{
			DecomposeTransform(transform, translation, rotation, scale);
			return *this;
		}

		[[nodiscard]] inline Mat4 getTransform() const
		{
			return glm::translate(glm::mat4(1.0f), translation) * glm::toMat4(glm::quat(rotation)) * glm::scale(glm::mat4(1.0f), scale);
		}

		Vector3<float> translation = { 0.0f, 0.0f, 0.0f };
		Vector3<float> rotation = { 0.0f, 0.0f, 0.0f };
		Vector3<float> scale = { 1.0f, 1.0f, 1.0f };
	};

	struct HierarchyComponent : ECSComponent<HierarchyComponent>
	{
		HierarchyComponent() = default;
		HierarchyComponent(const HierarchyComponent& other) = default;
		HierarchyComponent(UUID parent)
			: parent(parent)
		{
		}

		std::vector<UUID> children;
		UUID parent = 0;
	};

	struct MeshComponent : ECSComponent<MeshComponent>
	{
		using MeshResourceID = uint32_t;

		MeshComponent() = default;
		MeshComponent(MeshResourceID meshID) : meshID(meshID) {}

		std::string name = "NULL";

		std::shared_ptr<GFX::MaterialList> materialList = std::make_shared<GFX::MaterialList>();

		MeshResourceID meshID = INVALID_RESOURCE_ID;
	};

	struct SubmeshComponent : ECSComponent<SubmeshComponent>
	{
		using MeshResourceID = uint32_t;

		GFX::Submesh submesh;
		MeshResourceID meshID = INVALID_RESOURCE_ID;
		uint32_t submeshIndex = 0;
	};

	struct SpriteComponent : ECSComponent<SpriteComponent>
	{
		uint32_t color = 0xffffffff;
	};

	struct CameraComponent : ECSComponent<CameraComponent>
	{
		CameraComponent() = default;
		CameraComponent(const CameraComponent& other) = default;

		SceneCamera camera;
		bool primary = true;

		operator SceneCamera& () { return camera; }
		operator const SceneCamera& () const { return camera; }
	};

	enum struct LightType : uint8_t
	{
		Directional,
		Point,
		Spot
	};

	struct LightComponent : ECSComponent<LightComponent>
	{
		Vector3<float> radiance = 1.0f;
		float intensity = 1.0f;
		float fallOff = 1.0f;
		float minRadius = 1.0f;
		float radius = 10.0f;

		float outerCutOff = 0.0f;
		float innerCutOff = 0.0f;

		float lightSize = 2.0f;

		LightType type = LightType::Directional;

		bool castsShadows = true;
		bool softShadows = true;
		bool nvidiaPcss = false;
	};

	struct SkylightComponent : ECSComponent<SkylightComponent>
	{
		std::shared_ptr<Environment> environment = nullptr;

		Vector3<float> turbidityAzimuthInclination = { 2.0f, 5.0f, 1.5f };
		float intensity = 1.0f;
		bool dynamicSky = true;
	};

	enum struct RigidBodyType : uint8_t
	{
		Static,
		Dynamic,
		Kinematic
	};

	struct RigidBody2DComponent : ECSComponent<RigidBody2DComponent>
	{
		void* body = nullptr;

		RigidBodyType bodyType = RigidBodyType::Static;
		bool fixedRotation = false;
	};

	struct BoxCollider2DComponent : ECSComponent<BoxCollider2DComponent>
	{
		Vector2<float> offset = { 0.0f,0.0f };
		Vector2<float> size = { 0.5f, 0.5f };

		void* fixture = nullptr;

		float density = 1.0f;
		float friction = 0.5f;
		float restitution = 0.0f;
		float restitutionThreshold = 0.5f;
	};

	struct NativeScriptComponent : ECSComponent<NativeScriptComponent>
	{
		NativeScript* instance = nullptr;

		std::function<NativeScript*()> CreateScript;
		std::function<void(NativeScriptComponent*)> DestroyScript;

		NativeScriptType scriptType = NativeScriptType::EmptyScript;

		template<typename T>
		void Bind()
		{
			CreateScript = []() { return static_cast<NativeScript*>(new T()); };
			DestroyScript = [](NativeScriptComponent* component) { delete component->instance; component->instance = nullptr; };
		}
	};
}