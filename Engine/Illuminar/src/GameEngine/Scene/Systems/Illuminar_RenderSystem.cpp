#include "Illuminar_pch.h"
#include "Illuminar_RenderSystem.h"

#include "GameEngine/Scene/Illuminar_Scene.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar {
	void MeshRenderSystem::Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const SubmeshComponent& meshComponent = *(SubmeshComponent*)components[1];

		MeshResource* meshResource = ResourceManagers::get().meshResourceManager.tryGetResourceByID(meshComponent.meshID);

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);

		if (meshResource && meshResource->Loaded()) {
			scene->sceneRenderer->AddSubmesh(meshResource->mesh, meshComponent.submesh, transform);
		}
	}

	void SpriteRenderSystem::Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components)
	{
		TransformComponent& transformComponent = *(TransformComponent*)components[0];
		SpriteComponent& sprite = *(SpriteComponent*)components[1];

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);

		//auto& renderer = scene->renderer2D;
		//
		//Vector4<float> pos = transform * Vector4<float>{ -0.5f, -0.5f, 0.0f, 1.0f };
		//renderer.MoveTo({ pos.x, pos.y });
		//
		//pos = transform * Vector4<float>{ 0.5f, -0.5f, 0.0f, 1.0f };
		//renderer.LineTo({ pos.x, pos.y });
		//
		//pos = transform * Vector4<float>{ 0.5f, 0.5f, 0.0f, 1.0f };
		//renderer.LineTo({ pos.x, pos.y });
		//
		//pos = transform * Vector4<float>{ -0.5f, 0.5f, 0.0f, 1.0f };
		//renderer.LineTo({ pos.x, pos.y });
		//
		//renderer.Fill(sprite.color);
	}
}