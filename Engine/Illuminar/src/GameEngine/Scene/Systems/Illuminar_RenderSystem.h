#pragma once

#include "GameEngine/Scene/Components/Illuminar_Components.h"

namespace Illuminar {
	struct Scene;
	struct MeshRenderSystem : ECSSystem<TransformComponent, SubmeshComponent>
	{
		MeshRenderSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct SpriteRenderSystem : ECSSystem<TransformComponent, SpriteComponent>
	{
		SpriteRenderSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};
}