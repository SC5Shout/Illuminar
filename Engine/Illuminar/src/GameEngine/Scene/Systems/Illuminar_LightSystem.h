#pragma once

#include "GameEngine/Scene/Components/Illuminar_Components.h"

namespace Illuminar {
	struct SkylightSystem : ECSSystem<TransformComponent, SkylightComponent>
	{
		SkylightSystem(Scene* scene)
			: scene(scene)
		{
		}
	
		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);
	
		Scene* scene;
	};

	struct DirectionalLightSystem : ECSSystem<TransformComponent, LightComponent>
	{
		DirectionalLightSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct PointLightSystem : ECSSystem<TransformComponent, LightComponent>
	{
		PointLightSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct SpotLightSystem : ECSSystem<TransformComponent, LightComponent>
	{
		SpotLightSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};
}