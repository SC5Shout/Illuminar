#include "Illuminar_pch.h"
#include "Illuminar_LightSystem.h"

#include "GameEngine/Scene/Illuminar_Scene.h"

namespace Illuminar {
	void SkylightSystem::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const SkylightComponent& skyComponent = *(SkylightComponent*)components[1];
	
		scene->sceneRenderer->environment = skyComponent.environment;
		scene->sceneRenderer->intensity = skyComponent.intensity;
	}

	void DirectionalLightSystem::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const LightComponent& directionalLight = *(LightComponent*)components[1];

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);

		if (directionalLight.type == LightType::Directional) {
			GFX::ForwardPlusRenderer::DirectionalLight light;
			light.direction = -glm::normalize(glm::mat3(transform) * glm::vec3(1.0f));
			light.radiance = directionalLight.radiance;
			light.intensity = directionalLight.intensity;
			light.lightSize = directionalLight.lightSize;
			light.nvidiaPcss = directionalLight.nvidiaPcss;
			light.softShadows = directionalLight.softShadows;
				
			scene->sceneRenderer->directionalLights.push_back(light);
		}
	}

	void PointLightSystem::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		LightComponent& pointLight = *(LightComponent*)components[1];

		if (pointLight.type == LightType::Point) {
			Entity e = Entity{ components[0]->entity, scene };
			const auto& transform = scene->getTransformRelativeToParent(e);
			const auto [t, r, s] = getTransformDecomposition(transform);

			GFX::ForwardPlusRenderer::PointLight light;
			light.position = t;
			light.intensity = pointLight.intensity;
			light.radiance = pointLight.radiance;
			light.fallOff = pointLight.fallOff;
			light.radius = pointLight.radius;
			light.minRadius = pointLight.minRadius;

			scene->sceneRenderer->pointLights.push_back(light);
		}
	}

	void SpotLightSystem::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const LightComponent& spotLight = *(LightComponent*)components[1];

		if (spotLight.type == LightType::Spot) {
			Entity e = Entity{ components[0]->entity, scene };
			const auto& transform = scene->getTransformRelativeToParent(e);
			const auto [t, r, s] = getTransformDecomposition(transform);

			GFX::ForwardPlusRenderer::SpotLight light;
			light.position = t;
			light.direction = -glm::normalize(r);
			light.intensity = spotLight.intensity;
			light.radiance = spotLight.radiance;
			light.fallOff = spotLight.fallOff;
			light.radius = spotLight.radius;
			light.minRadius = spotLight.minRadius;
			light.outerCutOff = spotLight.outerCutOff;
			light.innerCutOff = spotLight.innerCutOff;

			//scene->sceneRenderer->spotLights.push_back(light);
		}
	}
}