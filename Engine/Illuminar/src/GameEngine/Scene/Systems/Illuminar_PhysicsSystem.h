#pragma once

#include "GameEngine/Scene/Components/Illuminar_Components.h"

namespace Illuminar {
	struct Scene;
	struct CreateRigidBodyPhysics2D : ECSSystem<TransformComponent, RigidBody2DComponent>
	{
		CreateRigidBodyPhysics2D(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct CreateBoxColliderPhysics2D : ECSSystem<TransformComponent, BoxCollider2DComponent, RigidBody2DComponent>
	{
		CreateBoxColliderPhysics2D(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct UpdatePhysics2D : ECSSystem<TransformComponent, RigidBody2DComponent>
	{
		UpdatePhysics2D(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};
}