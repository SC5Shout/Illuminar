#include "Illuminar_pch.h"
#include "Illuminar_PhysicsSystem.h"

#include "GameEngine/Scene/Illuminar_Scene.h"

#include "box2d/box2d.h"

namespace Illuminar {
	[[nodiscard]] static inline b2BodyType RigidBodyTypeToBox2D(RigidBodyType bodyType)
	{
		switch (bodyType) {
			case RigidBodyType::Static: return b2_staticBody;
			case RigidBodyType::Dynamic: return b2_dynamicBody;
			case RigidBodyType::Kinematic: return b2_kinematicBody;
		} return b2_staticBody;
	}

	void CreateRigidBodyPhysics2D::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		TransformComponent& transformComponent = *(TransformComponent*)components[0];
		RigidBody2DComponent& rigidComponent = *(RigidBody2DComponent*)components[1];

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);
		const auto [t, r, s] = getTransformDecomposition(transform);

		b2BodyDef bodyDef;
		bodyDef.type = RigidBodyTypeToBox2D(rigidComponent.bodyType);
		bodyDef.position.Set(t.x, t.y);
		bodyDef.angle = r.z;

		b2Body* body = scene->physicsWorld->CreateBody(&bodyDef);
		body->SetFixedRotation(rigidComponent.fixedRotation);
		rigidComponent.body = body;
	}

	void CreateBoxColliderPhysics2D::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		TransformComponent& transformComponent = *(TransformComponent*)components[0];
		BoxCollider2DComponent& boxColliderComponent = *(BoxCollider2DComponent*)components[1];
		RigidBody2DComponent& rigidComponent = *(RigidBody2DComponent*)components[2];

		if (rigidComponent.body && boxColliderComponent.fixture) {
			b2Body* body = (b2Body*)rigidComponent.body;
			body->DestroyFixture((b2Fixture*)boxColliderComponent.fixture);
		}

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);
		const auto [t, r, s] = getTransformDecomposition(transform);

		b2PolygonShape polygonShape;
		polygonShape.SetAsBox(boxColliderComponent.size.x * s.x, boxColliderComponent.size.y * s.y);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &polygonShape;
		fixtureDef.density = boxColliderComponent.density;
		fixtureDef.friction = boxColliderComponent.friction;
		fixtureDef.restitution = boxColliderComponent.restitution;
		fixtureDef.restitutionThreshold = boxColliderComponent.restitutionThreshold;

		b2Body* body = (b2Body*)rigidComponent.body;
		body->CreateFixture(&fixtureDef);
	}

	void UpdatePhysics2D::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		TransformComponent& transformComponent = *(TransformComponent*)components[0];
		RigidBody2DComponent& rigidComponent = *(RigidBody2DComponent*)components[1];

		b2Body* body = (b2Body*)rigidComponent.body;
		if (body) {
			const auto& position = body->GetPosition();
			transformComponent.translation.x = position.x;
			transformComponent.translation.y = position.y;
			transformComponent.rotation.z = body->GetAngle();
		}
	}
}