#include "Illuminar_pch.h"
#include "Illuminar_SceneCamera.h"

namespace Illuminar {
#undef near
#undef far

	void SceneCamera::setPerspective(float fov, float near, float far)
	{
		projectionType = ProjectionType::Perspective;
		perspectiveFov = fov;
		perspectiveNear = near;
		perspectiveFar = far;
	}

	void SceneCamera::setOrthographic(float size, float near, float far)
	{
		projectionType = ProjectionType::Orthographic;
		orthographicSize = size;
		orthographicNear = near;
		orthographicFar = far;
	}

	Vector4<float> SceneCamera::CalculateOrthographicsBounds() const
	{
		const float fwidth = static_cast<float>(width);
		const float fheight = static_cast<float>(height);

		const float aspect = fwidth / fheight;

		const float forthographicsSize = static_cast<float>(orthographicSize);

		const float left = -forthographicsSize * aspect;
		const float right = forthographicsSize * aspect;
		const float bottom = -forthographicsSize;
		const float top = forthographicsSize;

		return { left, right, bottom, top };
	}

	void SceneCamera::setViewport(uint32_t width, uint32_t height)
	{
		this->width = width;
		this->height = height;

		const float fwidth = static_cast<float>(width);
		const float fheight = static_cast<float>(height);

		if(projectionType == ProjectionType::Perspective) {
			projection = glm::perspectiveFov(Radians(perspectiveFov), fwidth, fheight, perspectiveNear, perspectiveFar);
		} else {
			const float aspect = fwidth / fheight;

			const float forthographicsSize = static_cast<float>(orthographicSize);

			const float left = -forthographicsSize * aspect;
			const float right = forthographicsSize * aspect;
			const float bottom = -forthographicsSize;
			const float top = forthographicsSize;
			projection = glm::ortho(left, right, bottom, top);
		}
	}
}
