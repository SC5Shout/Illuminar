#pragma once

#include "GraphicsEngine/Renderer/Illuminar_Camera.h"

namespace Illuminar {
	struct SceneCamera : Camera
	{
		enum struct ProjectionType
		{
			Perspective,
			Orthographic
		}; ProjectionType projectionType = ProjectionType::Orthographic;

		void setPerspective(float fov = 45.0f, float near = 0.1f, float far = 1000.0f);
		void setOrthographic(float size = 5.0f, float near = -1.0f, float far = 1.0f);

		// x: left; y: right; z: bottom; w: top
		Vector4<float> CalculateOrthographicsBounds() const;

		void setViewport(uint32_t width, uint32_t height);

		float perspectiveFov = 45.0f, perspectiveNear = 0.1f, perspectiveFar = 1000.0f;
		float orthographicSize = 5.0f, orthographicNear = -1.0f, orthographicFar = 1.0f;

		uint32_t width = 0;
		uint32_t height = 0;
	};
}