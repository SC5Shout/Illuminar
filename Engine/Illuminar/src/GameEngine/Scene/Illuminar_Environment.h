#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"

namespace Illuminar {
	struct Environment
	{
		[[nodiscard]] static std::shared_ptr<Environment> CreatePreethamSky(float turbidity, float azimuth, float inclination);
		[[nodiscard]] static std::shared_ptr<Environment> CreateFromHDRTexture(std::string_view filepath = "assets/textures/skybox/birchwood_4k.hdr");
	
		Environment() = default;
		Environment(const Ref<GFX::TextureCubemap>& radianceMap, const Ref<GFX::TextureCubemap>& irradianceMap)
			: radianceMap(radianceMap), irradianceMap(irradianceMap) {}
	
		~Environment() = default;
	
		//default null for serialization
		std::string filepath = "NULL";
		Ref<GFX::TextureCubemap> radianceMap = nullptr;
		Ref<GFX::TextureCubemap> irradianceMap = nullptr;
	};
}