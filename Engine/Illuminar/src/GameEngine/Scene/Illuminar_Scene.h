#pragma once

#include "Illuminar_Entity.h"
#include "GameEngine/Script/Illuminar_NativeScripts.h"

#include "GraphicsEngine/Renderer/Illuminar_Renderer2D.h"
#include "GraphicsEngine/Renderer/Illuminar_ForwardPlusRenderer.h"

#include "Systems/Illuminar_LightSystem.h"
#include "Systems/Illuminar_RenderSystem.h"
#include "Systems/Illuminar_PhysicsSystem.h"

#include "Illuminar_Environment.h"

class b2World;

namespace Illuminar {
	struct Scene;

	struct UpdateNativeScript : ECSSystem<NativeScriptComponent>
	{
		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);
	};

	struct GFX::Renderer2D;

	static constexpr Vector2<uint32_t> DEFAULT_SCENE_VIEWPORT{ 1600, 900 };

	struct Scene : RefCount
	{
		[[nodiscard]] inline static Ref<Scene> Create()
		{
			return Ref<Scene>::Create();
		}

		[[nodiscard]] static Ref<Scene> Copy(const Ref<Scene>& source);

		Scene();
		~Scene();

		void StartSimulation();
		void EndSimulation();

		void setViewport(uint32_t w, uint32_t h);
		void UpdateRuntime([[maybe_unused]] Timestep deltaTime);
		void UpdateEditor([[maybe_unused]] Timestep deltaTime, const Camera& camera, const Mat4& viewMatrix);

		[[nodiscard]] Entity CreateEntity(const std::string& name, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateEntityWithID(const std::string& name, UUID ID, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateCameraEntity(const std::string& name, SceneCamera::ProjectionType projectionType, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateLightEntity(const std::string& name, LightType lightType, Entity parent = INVALID_ENTITY);

		[[nodiscard]] Entity CreateMeshEntity(const std::string& name, MeshResourceID meshID, Entity parent = INVALID_ENTITY);

		void DestroyEntity(Entity entity);

		[[nodiscard]] Entity FindEntityByID(UUID id) ;
		
		void ConvertToLocalSpace(Entity entity);
		void ConvertToWorldSpace(Entity entity);

		[[nodiscard]] Mat4 getTransformRelativeToParent(Entity entity);
		[[nodiscard]] Mat4 getWorldSpaceTransformMatrix(Entity entity);
		[[nodiscard]] TransformComponent getWorldSpaceTransform(Entity entity);
		
		void ParentEntity(Entity entity, Entity parent);
		void UnparentEntity(Entity entity, bool convertToWorldSpace = true);

		[[nodiscard]] Entity getMainCamera();

		void BuildMeshHierarchy(Entity parent, MeshResource* mesh, const void* assimpScene, void* assimpNode);

		Ref<GFX::ForwardPlusRenderer> sceneRenderer = nullptr;

		ECS ecs;
		ECSSystemList renderSystems;
		ECSSystemList render2DSystems;
		ECSSystemList scriptSystems;

		ECSSystemList start2DPhysicsSystems;
		ECSSystemList physics2DUpdateSystem;

		b2World* physicsWorld = nullptr;

		Vector2<uint32_t> viewport = DEFAULT_SCENE_VIEWPORT;
	};
}