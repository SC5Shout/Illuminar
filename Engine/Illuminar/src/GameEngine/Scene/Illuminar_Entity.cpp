#include "Illuminar_pch.h"
#include "Illuminar_Entity.h"
#include "Illuminar_Scene.h"

#include "Components/Illuminar_Components.h"

namespace Illuminar {
	Entity::Entity(Scene* scene)
		: scene(scene), ecs(&scene->ecs)
	{
		handle = ecs->CreateEntity();
	}

	Entity::Entity(EntityHandle handle, Scene* scene)
		: handle(handle), scene(scene), ecs(&scene->ecs)
	{
	}

	void Entity::setParentID(UUID parentID)
	{
		getComponent<HierarchyComponent>()->parent = parentID;
	}
	
	UUID Entity::getParentID() const
	{
		return getComponent<HierarchyComponent>()->parent;
	}
	
	std::vector<UUID>& Entity::getChildrenID() const
	{
		return getComponent<HierarchyComponent>()->children;
	}
   
	//const std::vector<EntityHandle>& Entity::getChildren()
	//{
	//	return getComponent<HierarchyComponent>()->children;
	//}

	//bool Entity::HasParent() const
	//{
	//	return scene->FindEntityByID(getParentID());
	//}

	bool Entity::isAncesterOf(Entity entity) const
	{
		const auto& children = getChildrenID();
	
		if(children.empty()) {
			return false;
		}
	
		for(UUID child : children) {
			if(child == entity.getID()) {
				return true;
			}
		}
	
		for(UUID child : children) {
			if(scene->FindEntityByID(child).isAncesterOf(entity)) {
				return true;
			}
		}
	
		return false;
	}
	
	bool Entity::isDescendantOf(Entity entity) const
	{
		return entity.isAncesterOf(*this);
	}
	
	UUID Entity::getID() const
	{
		return getComponent<IDComponent>()->id;
	}
}