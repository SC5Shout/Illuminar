#pragma once

namespace Illuminar {
	struct Timestep
	{
		Timestep() = default;
		Timestep(double time) : time(time) {}

		operator double() const { return time; }

		double getSeconds() const { return time; }
		double getMilliseconds() const { return time * 1000.0; }

		auto operator <=>(const Timestep&) const = default;

	private:
		double time;
	};
}