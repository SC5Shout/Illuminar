#include "Illuminar_pch.h"
#include "Illuminar_Serializer.h"

#include "yaml-cpp/yaml.h"

#include "GameEngine/Illuminar_Timer.h"

namespace YAML {
	template<>
	struct convert<glm::vec3>
	{
		[[nodiscard]] static inline Node encode(const glm::vec3& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			return node;
		}

		[[nodiscard]] static inline bool decode(const Node& node, glm::vec3& rhs)
		{
			if(!node.IsSequence() || node.size() != 3)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			return true;
		}
	};

	template<>
	struct convert<glm::vec4>
	{
		[[nodiscard]] static inline Node encode(const glm::vec4& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			node.push_back(rhs.w);
			return node;
		}

		[[nodiscard]] static inline bool decode(const Node& node, glm::vec4& rhs)
		{
			if(!node.IsSequence() || node.size() != 4)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			rhs.w = node[3].as<float>();
			return true;
		}
	};

	template<>
	struct convert<glm::quat>
	{
		static Node encode(const glm::quat& rhs)
		{
			Node node;
			node.push_back(rhs.w);
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			return node;
		}

		static bool decode(const Node& node, glm::quat& rhs)
		{
			if(!node.IsSequence() || node.size() != 4)
				return false;

			rhs.w = node[0].as<float>();
			rhs.x = node[1].as<float>();
			rhs.y = node[2].as<float>();
			rhs.z = node[3].as<float>();
			return true;
		}
	};

	template<typename T>
	struct convert<Illuminar::Vector2<T>>
	{
		[[nodiscard]] static inline Node encode(const Illuminar::Vector2<T>& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			return node;
		}

		[[nodiscard]] static inline bool decode(const Node& node, Illuminar::Vector2<T>& rhs)
		{
			if(!node.IsSequence() || node.size() != 2)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			return true;
		}
	};

	template<typename T>
	struct convert<Illuminar::Vector3<T>>
	{
		[[nodiscard]] static inline Node encode(const Illuminar::Vector3<T>& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			return node;
		}

		[[nodiscard]] static inline bool decode(const Node& node, Illuminar::Vector3<T>& rhs)
		{
			if(!node.IsSequence() || node.size() != 3)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			return true;
		}
	};

	template<typename T>
	struct convert<Illuminar::Vector4<T>>
	{
		[[nodiscard]] static inline Node encode(const Illuminar::Vector4<T>& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			node.push_back(rhs.w);
			return node;
		}

		[[nodiscard]] static inline bool decode(const Node& node, Illuminar::Vector4<T>& rhs)
		{
			if(!node.IsSequence() || node.size() != 4)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			rhs.w = node[3].as<float>();
			return true;
		}
	};

	template<typename T>
	Emitter& operator<<(Emitter& out, const Illuminar::Vector2<T>& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << EndSeq;
		return out;
	}

	template<typename T>
	Emitter& operator<<(Emitter& out, const Illuminar::Vector3<T>& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << v.z << EndSeq;
		return out;
	}

	template<typename T>
	Emitter& operator<<(Emitter& out, const Illuminar::Vector4<T>& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << v.z << v.w << EndSeq;
		return out;
	}
	//--------------------------------------------------------------------------
	Emitter& operator<<(Emitter& out, const glm::vec2& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << EndSeq;
		return out;
	}

	Emitter& operator<<(Emitter& out, const glm::vec3& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << v.z << EndSeq;
		return out;
	}

	Emitter& operator<<(Emitter& out, const glm::vec4& v)
	{
		out << Flow;
		out << BeginSeq << v.x << v.y << v.z << v.w << EndSeq;
		return out;
	}

	Emitter& operator<<(Emitter& out, const glm::quat& v)
	{
		out << Flow;
		out << BeginSeq << v.w << v.x << v.y << v.z << EndSeq;
		return out;
	}
}

namespace Illuminar {
	SceneSerializer::SceneSerializer(Scene& scene)
		: scene(scene)
	{
	}

	static void SerializeEntity(YAML::Emitter& out, Entity entity)
	{
		UUID uuid = entity.getComponent<IDComponent>()->id;
		out << YAML::BeginMap;
		out << YAML::Key << "Entity";	
		out << YAML::Value << uuid;

		if(const auto& tagComponentPtr = entity.getComponent<TagComponent>()) {
			out << YAML::Key << "TagComponent";
			out << YAML::BeginMap;
				out << YAML::Key << "Tag" << YAML::Value << tagComponentPtr->tag;
			out << YAML::EndMap; 
		}

		if(const auto& transformComponentPtr = entity.getComponent<TransformComponent>()) {
			out << YAML::Key << "TransformComponent";
			out << YAML::BeginMap;
				out << YAML::Key << "Translation" << YAML::Value << transformComponentPtr->translation;
				out << YAML::Key << "Rotation" << YAML::Value << transformComponentPtr->rotation;
				out << YAML::Key << "Scale" << YAML::Value << transformComponentPtr->scale;
			out << YAML::EndMap; 
		}

		if (const auto& hierarchyComponentPtr = entity.getComponent<HierarchyComponent>()) {
			out << YAML::Key << "HierarchyComponent";
			out << YAML::BeginMap;
				out << YAML::Key << "Parent" << YAML::Value << hierarchyComponentPtr->parent;
				
				out << YAML::Key << "Children";
				out << YAML::Value << YAML::BeginSeq;
		
				for (auto child : hierarchyComponentPtr->children) {
					out << YAML::BeginMap;
					out << YAML::Key << "Child" << YAML::Value << child;
					out << YAML::EndMap;
				}
				out << YAML::EndSeq;
		
			out << YAML::EndMap;
		}

		if (const auto& meshComponentPtr = entity.getComponent<MeshComponent>()) {
			out << YAML::Key << "MeshComponent";
			out << YAML::BeginMap;
				out << YAML::Key << "Name" << YAML::Value << meshComponentPtr->name;
				out << YAML::Key << "MeshID" << YAML::Value << meshComponentPtr->meshID;
			out << YAML::EndMap;
		}

		if (const auto& submeshComponentPtr = entity.getComponent<SubmeshComponent>()) {
			out << YAML::Key << "SubmeshComponent";
			out << YAML::BeginMap;
				out << YAML::Key << "MeshID" << YAML::Value << submeshComponentPtr->meshID;
				out << YAML::Key << "Index" << YAML::Value << submeshComponentPtr->submeshIndex;
			out << YAML::EndMap;
		}

		auto CameraProjectionTypeToString = [](SceneCamera::ProjectionType type)
		{
			using enum SceneCamera::ProjectionType;
			switch (type) {
				case Perspective: return "Perspective";
				case Orthographic: return "Orthographic";
			}

			EngineLogError("CameraProjectionTypeToString(): Invalid projection type. Returned Perspective");
			return "Perspective";
		};

		if(const auto& cameraComponentPtr = entity.getComponent<CameraComponent>()) {
			out << YAML::Key << "CameraComponent";

			out << YAML::BeginMap; 
				auto& camera = cameraComponentPtr->camera;
				out << YAML::Key << "Camera" << YAML::Value;

					out << YAML::BeginMap; 
						out << YAML::Key << "ProjectionType" << YAML::Value << CameraProjectionTypeToString(camera.projectionType);

						if(camera.projectionType == SceneCamera::ProjectionType::Perspective) {
							out << YAML::Key << "PerspectiveFOV" << YAML::Value << camera.perspectiveFov;
							out << YAML::Key << "PerspectiveNear" << YAML::Value << camera.perspectiveNear;
							out << YAML::Key << "PerspectiveFar" << YAML::Value << camera.perspectiveFar;
						} else if(camera.projectionType == SceneCamera::ProjectionType::Orthographic) {
							out << YAML::Key << "OrthographicSize" << YAML::Value << camera.orthographicSize;
							out << YAML::Key << "OrthographicNear" << YAML::Value << camera.orthographicNear;
							out << YAML::Key << "OrthographicFar" << YAML::Value << camera.orthographicFar;
						}
					out << YAML::EndMap;

				out << YAML::Key << "Primary" << YAML::Value << cameraComponentPtr->primary;
			out << YAML::EndMap; 
		}

		if(const auto& spriteComponentPtr = entity.getComponent<SpriteComponent>()) {
			out << YAML::Key << "SpriteComponent";

			out << YAML::BeginMap;
				out << YAML::Key << "color" << YAML::Value << spriteComponentPtr->color;
			out << YAML::EndMap;
		}

		auto LightTypeTypeToString = [](LightType type)
		{
			using enum LightType;
			switch (type) {
				case Directional: return "Directional";
				case Point: return "Point";
				case Spot: return "Spot";
			}

			EngineLogError("LightTypeTypeToString(): Invalid projection type. Returned Directional");
			return "Directional";
		};

		//Should we serialize based on type to save few lines?
		if (const auto& lightComponentPtr = entity.getComponent<LightComponent>()) {
			out << YAML::Key << "LightComponent";

			out << YAML::BeginMap;
				out << YAML::Key << "Type" << YAML::Value << LightTypeTypeToString(lightComponentPtr->type);
				out << YAML::Key << "Radiance" << YAML::Value << lightComponentPtr->radiance;
				out << YAML::Key << "Intensity" << YAML::Value << lightComponentPtr->intensity;
				out << YAML::Key << "FallOff" << YAML::Value << lightComponentPtr->fallOff;
				out << YAML::Key << "MinRadius" << YAML::Value << lightComponentPtr->minRadius;
				out << YAML::Key << "Radius" << YAML::Value << lightComponentPtr->radius;
				out << YAML::Key << "OuterCutOff" << YAML::Value << lightComponentPtr->outerCutOff;
				out << YAML::Key << "InnerCutOff" << YAML::Value << lightComponentPtr->innerCutOff;
				out << YAML::Key << "LightSize" << YAML::Value << lightComponentPtr->lightSize;
				out << YAML::Key << "CastsShadows" << YAML::Value << lightComponentPtr->castsShadows;
				out << YAML::Key << "SoftShadows" << YAML::Value << lightComponentPtr->softShadows;
				out << YAML::Key << "NvidiaPcss" << YAML::Value << lightComponentPtr->nvidiaPcss;
			out << YAML::EndMap;
		}

		//Should we serialize based on Dynamic/Image Type to save few lines?
		if (const auto& skyComponentPtr = entity.getComponent<SkylightComponent>()) {
			if (skyComponentPtr->environment) {
				out << YAML::Key << "SkylightComponent";
			
				out << YAML::BeginMap;
					out << YAML::Key << "Filepath" << YAML::Value << skyComponentPtr->environment->filepath;
					out << YAML::Key << "Turbidity" << YAML::Value << skyComponentPtr->turbidityAzimuthInclination.x;
					out << YAML::Key << "Azimuth" << YAML::Value << skyComponentPtr->turbidityAzimuthInclination.y;
					out << YAML::Key << "Inclination" << YAML::Value << skyComponentPtr->turbidityAzimuthInclination.z;
					out << YAML::Key << "Intensity" << YAML::Value << skyComponentPtr->intensity;
					out << YAML::Key << "DynamicSky" << YAML::Value << skyComponentPtr->dynamicSky;
				out << YAML::EndMap;
			}
		}

		auto BodyTypeToString = [](RigidBodyType type)
		{
			using enum RigidBodyType;
			switch (type) {
				case Static: return "Static";
				case Dynamic: return "Dynamic";
				case Kinematic: return "Kinematic";
			}

			EngineLogError("BodyTypeToString(): Invalid body type. Returned Static");
			return "Static";
		};

		if (const auto& rigidBodyComponentPtr = entity.getComponent<RigidBody2DComponent>()) {
			out << YAML::Key << "RigidBody2DComponent";

			out << YAML::BeginMap;
				out << YAML::Key << "Type" << YAML::Value << BodyTypeToString(rigidBodyComponentPtr->bodyType);
				out << YAML::Key << "FixedRotation" << YAML::Value << rigidBodyComponentPtr->fixedRotation;
			out << YAML::EndMap;
		}

		if (const auto& boxColliderComponentPtr = entity.getComponent<BoxCollider2DComponent>()) {
			out << YAML::Key << "BoxCollider2DComponent";

			out << YAML::BeginMap;
				out << YAML::Key << "Offset" << YAML::Value << boxColliderComponentPtr->offset;
				out << YAML::Key << "Size" << YAML::Value << boxColliderComponentPtr->size;								
				out << YAML::Key << "Fixture" << YAML::Value << boxColliderComponentPtr->fixture;							
				out << YAML::Key << "Density" << YAML::Value << boxColliderComponentPtr->density;
				out << YAML::Key << "Friction" << YAML::Value << boxColliderComponentPtr->friction;
				out << YAML::Key << "Restitution" << YAML::Value << boxColliderComponentPtr->restitution;
				out << YAML::Key << "RestitutionThreshold" << YAML::Value << boxColliderComponentPtr->restitutionThreshold;
			out << YAML::EndMap;
		}

		out << YAML::EndMap;
	}

	void SceneSerializer::Serialize(const std::string& filepath)
	{
		YAML::Emitter out;
		out << YAML::BeginMap;
			out << YAML::Key << "Scene" << YAML::Value << "Untitled";
			out << YAML::Key << "Entities" << YAML::Value << YAML::BeginSeq;
		
			scene.ecs.each([&](auto entityHandle) {
				Entity entity = { entityHandle, &scene };
				if(!entity)
					return;

				SerializeEntity(out, entity);
			});

			out << YAML::EndSeq;
		out << YAML::EndMap;

		std::ofstream fout(filepath);
		fout << out.c_str();
	}

	bool SceneSerializer::Deserialize(const std::string& filepath)
	{
		bool fileExists = std::filesystem::exists(filepath);
		if(fileExists) {
			std::ifstream stream(filepath);
			std::stringstream strStream;
			strStream << stream.rdbuf();

			const auto data = YAML::Load(strStream.str());
			if(!data["Scene"]) {
				return false;
			}

			std::string sceneName = data["Scene"].as<std::string>();
			const auto entities = data["Entities"];
			if(entities) {
				for(const auto entity : entities) {
					uint64_t id = entity["Entity"].as<uint64_t>();

					std::string name;
					const auto tagComponent = entity["TagComponent"];
					if (tagComponent) {
						name = tagComponent["Tag"].as<std::string>();
					}
					Entity deserializedEntity = scene.CreateEntityWithID(name, id);

					const auto transformNode = entity["TransformComponent"];
					if(transformNode) {
						auto transformComponent = deserializedEntity.getComponent<TransformComponent>();
						transformComponent->translation = transformNode["Translation"].as<Vector3<float>>();
						transformComponent->rotation = transformNode["Rotation"].as<Vector3<float>>();
						transformComponent->scale = transformNode["Scale"].as<Vector3<float>>();
					}

					const auto hierarchyNode = entity["HierarchyComponent"];
					if (hierarchyNode) {
						auto hierarchyComponent = deserializedEntity.getComponent<HierarchyComponent>();
						hierarchyComponent->parent = hierarchyNode["Parent"].as<uint64_t>();
					
						auto children = hierarchyNode["Children"];
						if (children) {
							hierarchyComponent->children.reserve(children.size());
							for (auto child : children) {
								hierarchyComponent->children.push_back(child["Child"].as<uint64_t>());
							}
						}
					}

					const auto meshNode = entity["MeshComponent"];
					if (meshNode) {
						auto meshComponent = deserializedEntity.AddComponent<MeshComponent>();
						meshComponent->name = meshNode["Name"].as<std::string>();
						meshComponent->meshID = meshNode["MeshID"].as<MeshResourceID>();
					}

					const auto submeshNode = entity["SubmeshComponent"];
					if (submeshNode) {
						auto submeshComponent = deserializedEntity.AddComponent<SubmeshComponent>();
						submeshComponent->meshID = submeshNode["MeshID"].as<MeshResourceID>();
						submeshComponent->submeshIndex = submeshNode["Index"].as<uint32_t>();
					}

					const auto cameraNode = entity["CameraComponent"];
					if(cameraNode) {
						auto cameraComponent = deserializedEntity.AddComponent<CameraComponent>();

						auto CameraProjectionStringToType = [](std::string_view projection)
						{
							using enum SceneCamera::ProjectionType;
							if (projection == "Orthographic") {
								return Orthographic;
							} else if (projection == "Perspective") {
								return Perspective;
							}

							EngineLogError("CameraProjectionStringToType: Invalid string projection type. Returned Perspective");
							return Perspective;
						};

						const auto& cameraProperties = cameraNode["Camera"];

						auto& camera = cameraComponent->camera;

						camera.projectionType = CameraProjectionStringToType(cameraProperties["ProjectionType"].as<std::string>());
						if(camera.projectionType == SceneCamera::ProjectionType::Perspective) {
							camera.perspectiveFov = cameraProperties["PerspectiveFOV"].as<float>();
							camera.perspectiveNear = cameraProperties["PerspectiveNear"].as<float>();
							camera.perspectiveFar = cameraProperties["PerspectiveFar"].as<float>();
						} else if(camera.projectionType == SceneCamera::ProjectionType::Orthographic) {
							camera.orthographicSize = cameraProperties["OrthographicSize"].as<float>();
							camera.orthographicNear = cameraProperties["OrthographicNear"].as<float>();
							camera.orthographicFar = cameraProperties["OrthographicFar"].as<float>();
						}
					}

					const auto spriteNode = entity["SpriteComponent"];
					if(spriteNode) {
						auto spriteComponent = deserializedEntity.AddComponent<SpriteComponent>();
						spriteComponent->color = spriteNode["color"].as<uint32_t>();
					}

					const auto lightNode = entity["LightComponent"];
					if (lightNode) {
						auto LightStringToType = [](std::string_view type)
						{
							using enum LightType;
							if(type == "Directional") return Directional;
							if(type == "Point") return Point;
							if(type == "Spot") return Spot;
	
							EngineLogError("LightStringToType(): Invalid string light type. Returned Directional");
							return Directional;
						};

						auto lightComponent = deserializedEntity.AddComponent<LightComponent>();
						lightComponent->type = LightStringToType(lightNode["Type"].as<std::string>());
						lightComponent->radiance = lightNode["Radiance"].as<Vector3<float>>();
						lightComponent->intensity = lightNode["Intensity"].as<float>();
						lightComponent->fallOff = lightNode["FallOff"].as<float>();
						lightComponent->minRadius = lightNode["MinRadius"].as<float>();
						lightComponent->radius = lightNode["Radius"].as<float>();
						lightComponent->outerCutOff = lightNode["OuterCutOff"].as<float>();
						lightComponent->innerCutOff = lightNode["InnerCutOff"].as<float>();
						lightComponent->lightSize = lightNode["LightSize"].as<float>();
						lightComponent->castsShadows = lightNode["CastsShadows"].as<bool>();
						lightComponent->softShadows = lightNode["SoftShadows"].as<bool>();
						lightComponent->nvidiaPcss = lightNode["NvidiaPcss"].as<bool>();
					}

					const auto skyNode = entity["SkylightComponent"];
					if (skyNode) {
						auto LightStringToType = [](std::string_view type)
						{
							using enum LightType;
							if (type == "Directional") return Directional;
							if (type == "Point") return Point;
							if (type == "Spot") return Spot;

							EngineLogError("LightStringToType(): Invalid string light type. Returned Directional");
							return Directional;
						};

						auto skyComponent = deserializedEntity.AddComponent<SkylightComponent>();
						skyComponent->turbidityAzimuthInclination = {
							skyNode["Turbidity"].as<float>(),
							skyNode["Azimuth"].as<float>(),
							skyNode["Inclination"].as<float>()
						};
						skyComponent->intensity = skyNode["Intensity"].as<float>();
						skyComponent->dynamicSky = skyNode["DynamicSky"].as<bool>();

						auto filepath = skyNode["Filepath"].as<std::string>();

						if (filepath != "NULL") {
							if (std::filesystem::exists(filepath)) {
								skyComponent->environment = Environment::CreateFromHDRTexture(filepath);
							} else {
								EngineLogError("SceneSerializer::Deserialize(): Invalid Sky light map filepath.");
								skyComponent->dynamicSky = true;
								skyComponent->environment = Environment::CreatePreethamSky(
									skyComponent->turbidityAzimuthInclination.x, 
									skyComponent->turbidityAzimuthInclination.y, 
									skyComponent->turbidityAzimuthInclination.z
								);
							}
						}
					}

					const auto rigidBody2DNode = entity["RigidBody2DComponent"];
					if (rigidBody2DNode) {
						auto BodyStringToType = [](std::string_view type)
						{
							using enum RigidBodyType;
	
							if(type == "Static") return Static;
							if(type == "Dynamic") return Dynamic;
							if(type == "Kinematic") return Kinematic;

							EngineLogError("BodyStringToType(): Invalid string body type. Returned Static");
							return Static;
						};

						auto bodyComponent = deserializedEntity.AddComponent<RigidBody2DComponent>();
						bodyComponent->bodyType = BodyStringToType(rigidBody2DNode["Type"].as<std::string>());
						bodyComponent->fixedRotation = rigidBody2DNode["FixedRotation"].as<bool>();
					}

					const auto boxCollider2DNode = entity["BoxCollider2DComponent"];
					if (boxCollider2DNode) {
						auto colliderComponent = deserializedEntity.AddComponent<BoxCollider2DComponent>();
						colliderComponent->offset = boxCollider2DNode["Offset"].as<Vector2<float>>();
						colliderComponent->size = boxCollider2DNode["Size"].as<Vector2<float>>();

						colliderComponent->density = boxCollider2DNode["Density"].as<float>();
						colliderComponent->friction = boxCollider2DNode["Friction"].as<float>();
						colliderComponent->restitution = boxCollider2DNode["Restitution"].as<float>();
						colliderComponent->restitutionThreshold = boxCollider2DNode["RestitutionThreshold"].as<float>();
					}
				}
			}

			return true;
		}
		
		return false;
	}

	//TODO: use the real material values, not invalid ids
	void MaterialSerializer::Serialize(const std::string& filepath, const Ref<GFX::Material>& material)
	{
		YAML::Emitter out;
		out << YAML::BeginMap << YAML::Key << "Material";
			out << YAML::BeginMap;
				out << YAML::Key << "name" << YAML::Value << std::filesystem::path(filepath).filename().stem().string();
				out << YAML::Key << "shading" << YAML::Value << "Unlit";
				out << YAML::Key << "shader" << YAML::Value << INVALID_ASSET_ID;

				out << YAML::Key << "Colors" << YAML::BeginSeq;
					out << YAML::BeginMap;
						out << YAML::Key << "baseColor" << YAML::Value << material->get<Vector4<float>>("u_material.baseColor");
					out << YAML::EndMap;
				out << YAML::EndSeq;

				out << YAML::Key << "Maps" << YAML::BeginSeq;
					out << YAML::BeginMap << YAML::Key << "baseMap";
						out << YAML::BeginMap;
							out << YAML::Key << "assetID" << YAML::Value << INVALID_ASSET_ID;
							out << YAML::Key << "scale" << YAML::Value << Vector2<float>(1.0f, 1.0f);
							out << YAML::Key << "offset" << YAML::Value << Vector2<float>(0.0f, 0.0f);
						out << YAML::EndMap;
					out << YAML::EndMap;

					out << YAML::BeginMap << YAML::Key << "normalMap";
						out << YAML::BeginMap;
							out << YAML::Key << "assetID" << YAML::Value << INVALID_ASSET_ID;
							out << YAML::Key << "scale" << YAML::Value << Vector2<float>(1.0f, 1.0f);
							out << YAML::Key << "offset" << YAML::Value << Vector2<float>(0.0f, 0.0f);
						out << YAML::EndMap;
					out << YAML::EndMap;

		
					out << YAML::BeginMap << YAML::Key << "ambientOcclusionMap";
						out << YAML::BeginMap;
							out << YAML::Key << "assetID" << YAML::Value << INVALID_ASSET_ID;
							out << YAML::Key << "scale" << YAML::Value << Vector2<float>(1.0f, 1.0f);
							out << YAML::Key << "offset" << YAML::Value << Vector2<float>(0.0f, 0.0f);
						out << YAML::EndMap;
					out << YAML::EndMap;

					out << YAML::BeginMap << YAML::Key << "metallicMap";
						out << YAML::BeginMap;
							out << YAML::Key << "assetID" << YAML::Value << INVALID_ASSET_ID;
							out << YAML::Key << "scale" << YAML::Value << Vector2<float>(1.0f, 1.0f);
							out << YAML::Key << "offset" << YAML::Value << Vector2<float>(0.0f, 0.0f);
						out << YAML::EndMap;
					out << YAML::EndMap;

					out << YAML::BeginMap << YAML::Key << "emissionMap";
						out << YAML::BeginMap;
							out << YAML::Key << "assetID" << YAML::Value << INVALID_ASSET_ID;
							out << YAML::Key << "scale" << YAML::Value << Vector2<float>(1.0f, 1.0f);
							out << YAML::Key << "offset" << YAML::Value << Vector2<float>(0.0f, 0.0f);
						out << YAML::EndMap;
					out << YAML::EndMap;

				out << YAML::EndSeq;

			out << YAML::EndMap;
		out << YAML::EndMap;

		std::ofstream fout(filepath);
		fout << out.c_str();
	}

	bool MaterialSerializer::Deserialize(const std::string& filepath, MaterialFile& material)
	{
		const bool fileExists = std::filesystem::exists(filepath);
		if(fileExists) {
			std::ifstream stream(filepath);
			std::stringstream strStream;
			strStream << stream.rdbuf();
		
			Timer::Start();
			const auto data = YAML::Load(strStream.str());
		
			if(!data["Material"]) {
				return false;
			}
		
			const auto materialData = data["Material"];
			
			const auto name = materialData["name"].as<std::string>();
			const auto shading = materialData["shading"].as<std::string>(); 
			{
				material.shaderAssetID = materialData["shader"].as<AssetID::ID>();
			}

			const auto colorsNode = materialData["Colors"]; 
			{
				material.baseColor = colorsNode[0]["baseColor"].as<Vector4<float>>();
			}

			const auto floatsNode = materialData["Floats"];
			{
				material.metalness = floatsNode[0]["metalness"].as<float>();
				material.roughness = floatsNode[0]["roughness"].as<float>();
				material.emission = floatsNode[0]["emission"].as<float>();
			}

			const auto mapsNode = materialData["Maps"];
			{
				const auto baseMap = mapsNode[0]["baseMap"];
				{
					material.baseMapAssetID = baseMap["assetID"].as<AssetID::ID>();
					const auto scale = baseMap["scale"].as<Vector2<float>>();
					const auto offset = baseMap["offset"].as<Vector2<float>>();
				}

				const auto normalMap = mapsNode[1]["normalMap"];
				{
					material.normalMapAssetID = normalMap["assetID"].as<AssetID::ID>();
					const auto scale = normalMap["scale"].as<Vector2<float>>();
					const auto offset = normalMap["offset"].as<Vector2<float>>();
				}

				const auto ambientOcclusionMap = mapsNode[2]["ambientOcclusionMap"];
				{
					const AssetID textureID = ambientOcclusionMap["assetID"].as<AssetID::ID>();
					const auto scale = ambientOcclusionMap["scale"].as<Vector2<float>>();
					const auto offset = ambientOcclusionMap["offset"].as<Vector2<float>>();
				}

				const auto metalnessMap = mapsNode[3]["metalnessMap"];
				{
					material.metalnessMapAssetID = metalnessMap["assetID"].as<AssetID::ID>();
					const auto scale = metalnessMap["scale"].as<Vector2<float>>();
					const auto offset = metalnessMap["offset"].as<Vector2<float>>();
				}

				const auto roughnessMap = mapsNode[3]["roughnessMap"];
				{
					material.roughnessMapAssetID = roughnessMap["assetID"].as<AssetID::ID>();
					const auto scale = roughnessMap["scale"].as<Vector2<float>>();
					const auto offset = roughnessMap["offset"].as<Vector2<float>>();
				}

				const auto emissionMap = mapsNode[4]["emissionMap"];
				{
					material.emissionMapAssetID = emissionMap["assetID"].as<AssetID::ID>();
					const auto scale = emissionMap["scale"].as<Vector2<float>>();
					const auto offset = emissionMap["offset"].as<Vector2<float>>();
				}
			}

			return true;
		}

		return false;
	}
}