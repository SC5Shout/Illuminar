#pragma once

#include "GameEngine/Scene/Illuminar_Scene.h"
#include "GraphicsEngine/Illuminar_Material.h"

namespace Illuminar {

	/*
	
		static constexpr const char* s_AlbedoColorUniform = "u_MaterialUniforms.AlbedoColor";
		static constexpr const char* s_UseNormalMapUniform = "u_MaterialUniforms.UseNormalMap";
		static constexpr const char* s_MetalnessUniform = "u_MaterialUniforms.Metalness";
		static constexpr const char* s_RoughnessUniform = "u_MaterialUniforms.Roughness";
		static constexpr const char* s_EmissionUniform = "u_MaterialUniforms.Emission";

		static constexpr const char* s_AlbedoMapUniform = "u_AlbedoTexture";
		static constexpr const char* s_NormalMapUniform = "u_NormalTexture";
		static constexpr const char* s_MetalnessMapUniform = "u_MetalnessTexture";
		static constexpr const char* s_RoughnessMapUniform = "u_RoughnessTexture";

	*/

	struct MaterialFile
	{
		Vector4<float> baseColor = 0.0f;

		AssetID baseMapAssetID = INVALID_ASSET_ID;
		AssetID normalMapAssetID = INVALID_ASSET_ID;
		AssetID metalnessMapAssetID = INVALID_ASSET_ID;
		AssetID roughnessMapAssetID = INVALID_ASSET_ID;
		AssetID emissionMapAssetID = INVALID_ASSET_ID;

		AssetID shaderAssetID = INVALID_ASSET_ID;

		float metalness = 0.0f;
		float roughness = 0.0f;
		float emission = 0.0f;

		bool useNormalMap = false;
	};

	struct MaterialSerializer
	{
		void Serialize(const std::string& filepath, const Ref<GFX::Material>& material);
		bool Deserialize(const std::string& filepath, MaterialFile& material);
	};

	struct SceneSerializer
	{
		SceneSerializer(Scene& scene);

		void Serialize(const std::string& filepath);
		bool Deserialize(const std::string& filepath);

	private:
		Scene& scene;
	};
}