#include "Illuminar_pch.h"
#include "Illuminar_Application.h"

#include "Asset/Illuminar_AssetManager.h"

namespace Illuminar {
	Application::Application(const ApplicationCreateInfo& createInfo)
	{
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			EngineLogFatal("SDL: failed to init {}", SDL_GetError());
			__debugbreak();
		}
		SDL_GL_LoadLibrary(NULL);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
		
		Timer::Start();
		LoggingSystem::Init();
		
		WindowCreateInfo windowInfo;
		windowInfo.name = createInfo.name.c_str();
		windowInfo.size = { createInfo.w, createInfo.h };
		window = Window::Create(windowInfo);

		GFX::RenderCommand::Init(window);

		renderThread = std::thread([]() {
			while (true) {
				if (!GFX::RenderCommand::Execute()) {
					break;
				}
			}
		});
	}

	Application::~Application()
	{
		ResourceStreamer::FlushAllQueues();

		GFX::RenderCommand::Flush();
		GFX::RenderCommand::Quit();
		renderThread.join();
	}

	void Application::MainLoop()
	{
		uint32_t index = 0;
		while(!quit) {		
			while(SDL_PollEvent(&event)) {
				if(event.type == SDL_QUIT) {
					quit = true;
				}
			
				if(event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window->getHandle())) {
					quit = true;
				}
			
				OnEvent(event);
			
				Input::get().ProcessEvent(event);
			} Input::get().Update(deltaTime);
		
			if (GFX::RenderCommand::BeginFrame()) {
				window->MakeCurrent();
			
			//	//float r = (std::sin(lastTime) + 1.0f) * 0.5f;
			//	GFX::RenderCommand::setViewport(0, 0, 1600, 900);
			//	GFX::RenderCommand::setClearColor(0.5f, 0.5f, 0.5f, 1.0f);
			//	GFX::RenderCommand::Clear(GFX::BufferBit::Color | GFX::BufferBit::Depth);
			//
				Update(deltaTime);
			
				window->Present();
			
				GFX::RenderCommand::EndFrame();
			}
		
			ResourceStreamer::Dispatch();	
		
			double time = Timer::Elapsed();
			deltaTime = time - lastTime;
			lastTime = time;
		}
	}
}