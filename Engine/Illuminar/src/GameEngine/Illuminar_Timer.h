#pragma once

typedef std::chrono::high_resolution_clock hr_clock;
typedef std::chrono::nanoseconds clock_freq;

namespace Illuminar {
	struct Timer
	{
		~Timer() = default;

		inline static void Start() { get().start = hr_clock::now(); }
		inline static void Stop() { get().stop = hr_clock::now(); }

		[[nodiscard]] inline static double Elapsed()
		{
			get().current = hr_clock::now();
			return std::chrono::duration_cast<std::chrono::duration<double>>(get().current - get().start).count();
		}

		[[nodiscard]] inline static double Difference()
		{
			return std::chrono::duration_cast<std::chrono::duration<double>>(get().stop - get().start).count();
		}

		[[nodiscard]] inline static double Current()
		{
			get().current = hr_clock::now();
			return std::chrono::duration_cast<std::chrono::duration<double>>(get().current.time_since_epoch()).count();
		}

		[[nodiscard]] inline static double ElapsedMillis() { return Elapsed() * 1000.0f; }

	private:
		Timer() = default;

		Timer(const Timer& other) = delete;
		Timer& operator=(const Timer& other) = delete;

		static Timer& get()
		{
			static Timer instance;
			return instance;
		}

		hr_clock::time_point start;
		hr_clock::time_point stop;
		hr_clock::time_point current;
	};
}