#include "Illuminar_pch.h"
#include "Illuminar_Project.h"

#include "yaml-cpp/yaml.h"

namespace Illuminar {
	std::shared_ptr<Project> Project::currentProject = nullptr;

	Project::Project(std::string_view filepath)
	{
		if(!std::filesystem::exists(filepath)) {
			EngineLogFatal("Project file {} does not exists", filepath);
			__debugbreak();
		}

		std::ifstream stream{ std::string(filepath) };
		std::stringstream strStream;
		strStream << stream.rdbuf();

		YAML::Node data = YAML::Load(strStream.str());
		if(!data["Project"]) {
			EngineLogFatal("Project file {} is in a bad format", filepath);
			__debugbreak();
		}

		YAML::Node rootNode = data["Project"];
		if(!rootNode["Name"]) {
			EngineLogFatal("Project {} hasn't got a name", filepath);
			__debugbreak();
		}

		info.name = rootNode["Name"].as<std::string>();
		info.assetDirectory = rootNode["AssetDirectory"].as<std::string>();
		info.startScene = rootNode["StartScene"].as<std::string>();

		std::filesystem::path projectPath = filepath;
		info.projectDirectory = projectPath.parent_path();

		package = &AssetManager::AddAssetPackage(info.name);
		for(const auto& entry : std::filesystem::recursive_directory_iterator(info.assetDirectory)) {
			if(entry.is_regular_file()) {
				package->AddAsset(entry.path().generic_string().c_str(), entry.path().generic_string().c_str());
			}
		}
	}
}