#pragma once

#include "GameEngine/Asset/Illuminar_AssetManager.h"

namespace Illuminar {
	static constexpr const char* DEFAULT_PROJECT_NAME = "Illuminar Project";
	static constexpr const char* DEFAULT_ASSET_DIRECTOR = "assets";
	static constexpr const char* DEFAULT_PROJECT_DIRECTOR = "";
	static constexpr const char* DEFAULT_START_SCENE = "";

	struct ProjectCreateInfo
	{
		std::string name = DEFAULT_PROJECT_NAME;
		std::filesystem::path assetDirectory = DEFAULT_ASSET_DIRECTOR;
		std::filesystem::path projectDirectory = DEFAULT_PROJECT_DIRECTOR;
		std::filesystem::path startScene = DEFAULT_START_SCENE;
	};

	struct Project
	{
		[[nodiscard]] inline static std::shared_ptr<Project> Load(std::string_view filepath)
		{
			auto result = std::make_shared<Project>(filepath);
			Project::setActiveProject(result);
			return result;
		}

		[[nodiscard]] inline static std::shared_ptr<Project> Create(const ProjectCreateInfo& info)
		{
			auto result = std::make_shared<Project>(info);
			Project::setActiveProject(result);
			return result;
		}

		Project(const ProjectCreateInfo& info)
			: info(info)
		{
			package = &AssetManager::AddAssetPackage(info.name);
			for(const auto& entry : std::filesystem::recursive_directory_iterator(info.assetDirectory)) {
				if(entry.is_regular_file()) {
					package->AddAsset(entry.path().filename().string().c_str(), entry.path().generic_string().c_str());
				}
			}
		}

		Project(std::string_view filepath);

		[[nodiscard]] inline static const std::string& getName()
		{
			return currentProject->info.name;
		}

		[[nodiscard]] inline static std::filesystem::path getDirectory()
		{
			return currentProject->info.projectDirectory;
		}

		[[nodiscard]] inline static std::filesystem::path getAssetDirectory()
		{
			return currentProject->info.projectDirectory / currentProject->info.assetDirectory;
		}

		static inline void setActiveProject(const std::shared_ptr<Project>& project)
		{ 
			currentProject = project;
		}

		[[nodiscard]] static inline std::shared_ptr<Project> getActiveProject()
		{ 
			return currentProject;
		}

		[[nodiscard]] inline AssetPackage* getAssetPackage() const { return package; }

	//private:
		static std::shared_ptr<Project> currentProject;
		
		AssetPackage* package = nullptr;

		ProjectCreateInfo info;
	};
}