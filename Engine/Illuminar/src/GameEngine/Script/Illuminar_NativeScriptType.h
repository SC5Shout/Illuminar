#pragma once

namespace Illuminar {
	enum struct NativeScriptType
	{
		EmptyScript,
		CameraController,
		Count
	};
}