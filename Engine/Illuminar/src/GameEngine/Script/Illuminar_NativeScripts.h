#pragma once

#include "Illuminar_NativeScriptEngine.h"
#include "Illuminar_NativeScriptType.h"

#include "GameEngine/Scene/Components/Illuminar_Components.h"
#include "GameEngine/Input/Illuminar_Input.h"

namespace Illuminar {
	struct CameraController : NativeScript
	{
		void Update([[maybe_unused]] Timestep deltaTime) override
		{
			auto& translation = getComponent<TransformComponent>()->translation;

			float speed = 5.0f;

			if(Input::KeyDown(Keyboard::A)) {
				translation.x -= speed * (float)deltaTime;
			} else if(Input::KeyDown(Keyboard::D)) translation.x += speed * (float)deltaTime;

			if(Input::KeyDown(Keyboard::W)) {
				translation.y += speed * (float)deltaTime;
			} else if(Input::KeyDown(Keyboard::S)) translation.y -= speed * (float)deltaTime;
		}
	};

	inline void BindNativeScript(NativeScriptType type, NativeScriptComponent& nativeScript)
	{
		switch(type) {
			case NativeScriptType::EmptyScript:
				nativeScript.DestroyScript(&nativeScript);
				break;
			case NativeScriptType::CameraController:
				nativeScript.Bind<CameraController>();
				break;
		}
	}

	constexpr inline static std::array<std::pair<const char*, NativeScriptType>, (size_t)NativeScriptType::Count> nativeScriptTypes = {
		std::make_pair("Empty script", NativeScriptType::EmptyScript),
		std::make_pair("Camera controller", NativeScriptType::CameraController),
	};
}