#pragma once

#include "GameEngine/Scene/Illuminar_Entity.h"

namespace Illuminar {
	struct NativeScript
	{
		virtual ~NativeScript() = default;

		template<typename T>
		[[nodiscard]] T* getComponent()
		{
			return entity.getComponent<T>();
		}

	protected:
		virtual void Create() {}
		virtual void Destroy() {}
		virtual void Update([[maybe_unused]] Timestep deltaTime) {}

	private:
		friend struct Scene;
		friend struct UpdateNativeScript;

		Entity entity;
	};
}