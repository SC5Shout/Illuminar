#pragma once

#include "SDL.h"

#include "Illuminar_Keyboard.h"
#include "Illuminar_Mouse.h"

#include "GameEngine/Illuminar_Timestep.h"

namespace Illuminar {
	struct Input
	{
		Input() = default;

		static bool KeyDown(Keyboard key);
		static bool ButtonDown(Mouse button, uint32_t clicks = 0);
		static bool ButtonClicked(Mouse button, uint32_t clicks = 0);

		[[nodiscard]] inline static const Vector2<float>& getMousePosition() { return get().mousePosition; }
		[[nodiscard]] inline static const float getMouseX() { return get().mousePosition.x; }
		[[nodiscard]] inline static const float getMouseY() { return get().mousePosition.y; }

		[[nodiscard]] inline static const Vector2<float>& getMouseRelPosition() { return get().mouseRelPosition; }
		[[nodiscard]] inline static float getMouseRelX() { return get().mouseRelPosition.x; }
		[[nodiscard]] inline static float getMouseRelY() { return get().mouseRelPosition.y; }

		[[nodiscard]] inline static const Vector2<float>& getMouseWheel() { return get().mouseWheel; }
		[[nodiscard]] inline static const float getWheelX() { return get().mouseWheel.x; }
		[[nodiscard]] inline static const float getWheelY() { return get().mouseWheel.y; }

	private:
	friend struct Application;

		~Input() = default;

		Input(const Input& other) = delete;
		Input& operator=(const Input& other) = delete;

		static Input& get()
		{
			static Input instance;
			return instance;
		}

		// Mouse buttons: 0 - currently unavailable; 1 - left; 2 - middle; 3 - right; 4 and 5 extra ones
		bool mousePressed[6] = { false };
		bool mouseClicked[6] = { false };
		bool keyDown[512] = { false };

		float mouseDownTime[6] = { -1.0f };

		Vector2<float> mousePosition;
		Vector2<float> mouseRelPosition;
		Vector2<float> mouseWheel;

		void Update(Timestep deltaTime);
		void ProcessEvent(SDL_Event& event);
	};
}