#pragma once

namespace Illuminar {
#define ILLUMINAR_BUTTON(X) (1 << ((X)-1))
	enum struct Mouse : uint8_t
	{
		Left = 1,
		Middle = 2,
		Right = 3,
		X1 = 4,
		X2 = 5,

		Button = 6,

		LMask = ILLUMINAR_BUTTON(Left),
		MMask = ILLUMINAR_BUTTON(Middle),
		RMask = ILLUMINAR_BUTTON(Right),
		X1Mask = ILLUMINAR_BUTTON(X1),
		X2Mask = ILLUMINAR_BUTTON(X2),

		Button2 = ILLUMINAR_BUTTON(Button),
	};

	inline constexpr std::string_view ConvertMouseButton(Mouse button)
	{
		switch(button) {
			case Mouse::Left: return "Mouse button left";
			case Mouse::Middle: return "Mouse button middle";
			case Mouse::Right: return "Mouse button right";
			case Mouse::X1: return "Mouse button X1";
			case Mouse::X2: return "Mouse button X2";

			case Mouse::Button: return "Mouse button button";

			case Mouse::X1Mask: return "Mouse button X1Mask";
			case Mouse::X2Mask: return "Mouse button X2Mask";

			case Mouse::Button2: break;
		}

		if(button == Mouse::LMask) return "Mouse button LMask";
		if(button == Mouse::MMask) return "Mouse button MMask";
		if(button == Mouse::RMask) return "Mouse button rMask";

		return "None";
	}
}