#include "Illuminar_pch.h"
#include "Illuminar_Input.h"

namespace Illuminar {
	void Input::Update(Timestep deltaTime)
	{
		for(uint32_t i = 0; i < 6; ++i) {
			mouseClicked[i] = mousePressed[i] && mouseDownTime[i] < 0.0f;
			mouseDownTime[i] = mousePressed[i] ? (mouseDownTime[i] < 0.0f ? 0.0f : mouseDownTime[i] + static_cast<float>(deltaTime)) : -1.0f;
		}
	}

	bool Input::KeyDown(Keyboard key)
	{
		return get().keyDown[(uint16_t)key];
	}

	bool Input::ButtonDown(Mouse button, uint32_t clicks)
	{
		return get().mousePressed[(uint16_t)button];
	}

	bool Input::ButtonClicked(Mouse button, uint32_t clicks)
	{
		return get().mouseClicked[(uint16_t)button];
	}

	void Input::ProcessEvent(SDL_Event& event)
	{
		switch(event.type) {
			case SDL_MOUSEMOTION:
				mousePosition.x = static_cast<float>(event.motion.x);
				mousePosition.y = static_cast<float>(event.motion.y);
				mouseRelPosition.x = static_cast<float>(event.motion.xrel);
				mouseRelPosition.y = static_cast<float>(event.motion.yrel);
				break;
			case SDL_MOUSEWHEEL:
				if(event.wheel.x > 0) mouseWheel.x += 1.0f;
				if(event.wheel.x < 0) mouseWheel.x -= 1.0f;
				if(event.wheel.y > 0) mouseWheel.y += 1.0f;
				if(event.wheel.y < 0) mouseWheel.y -= 1.0f;
				break;
			case SDL_MOUSEBUTTONDOWN:
				if(event.button.button == (uint8_t)Mouse::Left) mousePressed[1] = true;
				if(event.button.button == (uint8_t)Mouse::Middle) mousePressed[2] = true;
				if(event.button.button == (uint8_t)Mouse::Right) mousePressed[3] = true;
				if(event.button.button == (uint8_t)Mouse::X1) mousePressed[4] = true;
				if(event.button.button == (uint8_t)Mouse::X2) mousePressed[5] = true;
				break;
			case SDL_MOUSEBUTTONUP:
				if(event.button.button == (uint8_t)Mouse::Left) mousePressed[1] = false;
				if(event.button.button == (uint8_t)Mouse::Middle) mousePressed[2] = false;
				if(event.button.button == (uint8_t)Mouse::Right) mousePressed[3] = false;
				if(event.button.button == (uint8_t)Mouse::X1) mousePressed[4] = false;
				if(event.button.button == (uint8_t)Mouse::X2) mousePressed[5] = false;
				break;
			case SDL_KEYDOWN:
				keyDown[event.key.keysym.scancode] = true;
				break;
			case SDL_KEYUP:
				keyDown[event.key.keysym.scancode] = false;
				break;
		}
	}
}