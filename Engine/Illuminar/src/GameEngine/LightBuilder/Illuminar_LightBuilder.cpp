#include "Illuminar_pch.h"
#include "Illuminar_LightBuilder.h"

namespace Illuminar {
	LightBuilder::LightBuilder(LightType type)
		: type(type)
	{
	}

	Entity LightBuilder::Build(const std::string& name, Ref<Scene>& scene, Entity parent)
	{
		auto entity = scene->CreateLightEntity(name, type, parent);
		auto& light = *entity.getComponent<LightComponent>();
		light.radiance = this->light.radiance;
		light.intensity = this->light.intensity;
		light.fallOff = this->light.fallOff;
		light.minRadius = this->light.minRadius;
		light.radius = this->light.radius;
		light.outerCutOff = this->light.outerCutOff;
		light.innerCutOff = this->light.innerCutOff;
		light.lightSize = this->light.lightSize;
		light.castsShadows = this->light.castsShadows;
		light.softShadows = this->light.softShadows;
		light.nvidiaPcss = this->light.nvidiaPcss;

		auto transformComponent = entity.getComponent<TransformComponent>();
		transformComponent->translation = position;
		transformComponent->rotation = direction;

		const auto& transform = scene->getTransformRelativeToParent(entity);
		const auto [t, r, s] = getTransformDecomposition(transform);

		if (type == LightType::Point) {
			GFX::ForwardPlusRenderer::PointLight rendererLight;
			rendererLight.position = t;
			rendererLight.intensity = light.intensity;
			rendererLight.radiance = light.radiance;
			rendererLight.fallOff = light.fallOff;
			rendererLight.radius = light.radius;
			rendererLight.minRadius = light.minRadius;
		}

		return entity;
	}

	LightBuilder& LightBuilder::Position(const Vector3<float>& position)
	{
		this->position = position;
		return *this;
	}

	LightBuilder& LightBuilder::Direction(const Vector3<float>& direction)
	{
		this->direction = direction;
		return *this;
	}

	LightBuilder& LightBuilder::Radiance(const Vector3<float>& radiance)
	{
		light.radiance = radiance;
		return *this;
	}

	LightBuilder& LightBuilder::Intensity(float intensity)
	{
		light.intensity = intensity;
		return *this;
	}

	LightBuilder& LightBuilder::FallOff(float fallOff)
	{
		light.fallOff = fallOff;
		return *this;
	}

	LightBuilder& LightBuilder::Radius(float radius)
	{
		light.radius = radius;
		return *this;
	}

	LightBuilder& LightBuilder::MinRadius(float minRadius)
	{
		light.minRadius = minRadius;
		return *this;
	}

	LightBuilder& LightBuilder::OuterCutOff(float outerCutOff)
	{
		light.outerCutOff = outerCutOff;
		return *this;
	}

	LightBuilder& LightBuilder::InnerCutOff(float innerCutOff)
	{
		light.innerCutOff = innerCutOff;
		return *this;
	}

	LightBuilder& LightBuilder::CastsShadows(bool shadows)
	{
		light.castsShadows = shadows;
		return *this;
	}

	LightBuilder& LightBuilder::SoftShadows(bool soft)
	{
		light.softShadows = soft;
		return *this;
	}

	LightBuilder& LightBuilder::NvidiaPcss(bool nvidia)
	{
		light.nvidiaPcss = nvidia;
		return *this;
	}

	LightBuilder& LightBuilder::LightSize(float lightSize)
	{
		light.lightSize = lightSize;
		return *this;
	}
}