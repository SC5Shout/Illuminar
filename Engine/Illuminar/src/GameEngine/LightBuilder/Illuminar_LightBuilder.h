#pragma once

#include "GameEngine/Scene/Illuminar_Scene.h"

namespace Illuminar {
    struct LightBuilder
    {
        LightBuilder(LightType type);
        ~LightBuilder() = default;

        LightBuilder(LightBuilder const& rhs) = default;
        LightBuilder(LightBuilder&& rhs) = default;

        LightBuilder& operator=(LightBuilder const& rhs) = default;
        LightBuilder& operator=(LightBuilder&& rhs) = default;

        Entity Build(const std::string& name, Ref<Scene>& scene, Entity parent = Entity{ INVALID_ENTITY_HANDLE, nullptr });

        LightBuilder& Position(const Vector3<float>& position);
        LightBuilder& Direction(const Vector3<float>& direction);
        LightBuilder& Radiance(const Vector3<float>& radiance);
        LightBuilder& Intensity(float intensity);
        LightBuilder& FallOff(float fallOff);
        LightBuilder& Radius(float radius);
        LightBuilder& MinRadius(float minRadius);
        LightBuilder& OuterCutOff(float outerCutOff);
        LightBuilder& InnerCutOff(float innerCutOff);

        LightBuilder& CastsShadows(bool shadows);
        LightBuilder& SoftShadows(bool soft);
        LightBuilder& NvidiaPcss(bool nvidia);
        LightBuilder& LightSize(float lightSize);

    private:
        friend struct Scene;

        LightType type;
        LightComponent light;

        Vector3<float> position = 1.0f;
        Vector3<float> direction = 0.0f;
    };
}