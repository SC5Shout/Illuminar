#pragma once

#include "GameEngine/Resource/Shader/Illuminar_ShaderResourceLoader.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLShader.h"

namespace Illuminar {
	struct GLShaderResourceLoader final : ShaderResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("GLShader");

		GLShaderResourceLoader(ResourceManager& resourceManager)
			: ShaderResourceLoader(resourceManager)
		{
		}

		~GLShaderResourceLoader() override = default;

		[[nodiscard]] ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool Deserialize(const std::string & filepath) override;
		void Process() override;
		[[nodiscard]] bool Dispatch() override;

		[[nodiscard]] const uint32_t ShaderTypeFromString(std::string_view type) const override;

		std::unordered_map<uint32_t, std::vector<uint32_t>> CompileGLSLToVulkanSpirvAndCache(const std::unordered_map<uint32_t, std::string>& shaderSource, uint32_t hash);
		void CompileFromVulkanToOpengl(const std::unordered_map<uint32_t, std::vector<uint32_t>>& vulkanBinaries, uint32_t hash);

		void ParsePushConstant(const spirv_cross::ShaderResources& resources, const spirv_cross::Compiler& compiler);

		std::unordered_map<std::string, GFX::UniformBlock> pushConstantBlocks;
		std::unordered_map<std::string, GFX::SampledUniform> sampledUniforms;

		bool existVkShaderCacheFile = false;
		bool existGLShaderCacheFile = false;

		std::unordered_map<uint32_t, std::vector<uint32_t>> vkBinaries;
	};
}