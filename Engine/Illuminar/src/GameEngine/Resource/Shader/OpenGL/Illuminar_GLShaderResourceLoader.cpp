#include "Illuminar_pch.h"
#include "Illuminar_GLShaderResourceLoader.h"

namespace Illuminar {
	static shaderc_shader_kind GLShaderTypeToShadercShaderType(uint32_t glshaderType)
	{
		switch(glshaderType) {
			case GL_VERTEX_SHADER: return shaderc_vertex_shader;
			case GL_FRAGMENT_SHADER: return shaderc_fragment_shader;
			case GL_TESS_CONTROL_SHADER: return shaderc_tess_control_shader;
			case GL_TESS_EVALUATION_SHADER: return shaderc_tess_evaluation_shader;
			case GL_GEOMETRY_SHADER: return shaderc_geometry_shader;
			case GL_COMPUTE_SHADER: return shaderc_compute_shader;
		}
		//should never happen
		return shaderc_vertex_shader;
	}

	bool GLShaderResourceLoader::Deserialize(const std::string& filepath)
	{
		if(!std::filesystem::exists(filepath)) {
			EngineLogError("Shader \"{}\" does not exist", filepath);
			return false;
		}
		
		this->filepath = filepath;
		auto path = std::filesystem::path(filepath).parent_path();
		
		const auto filename = std::filesystem::path(filepath).filename();
		const std::string filenameWithoutExtension = filename.stem().string();
		const std::string cachedPath = path.generic_string() + "/cache/";
		cachedFilename = cachedPath + filenameWithoutExtension;
		
		std::filesystem::create_directories(cachedPath);
		
		const auto source = LoadFromFile(filepath);
		const uint32_t hash = Hash32::GenerateFNVHash(source.c_str());

		existVkShaderCacheFile = std::filesystem::exists(cachedFilename + ".vkcache");
		if(!existVkShaderCacheFile) {				
			const auto shaderSource = Preprocess(source);
			vkBinaries = CompileGLSLToVulkanSpirvAndCache(shaderSource, hash);
		}

		static constexpr uint32_t MAX_TRIES = 3;
		uint32_t tries = MAX_TRIES;
		//if ReadCachedBinaryData fails it means that the shader code has changed
		while(!ReadCachedBinaryData(".vkcache", vkBinaries, hash)) {
			if (--tries == 0) {
				EngineLogWarn("GLShader loader ReadCachedBinaryData(): cannot read {} cached .vkcache", filepath);
				__debugbreak();
			}
		
			//recompile
			const auto shaderSource = Preprocess(source);
			vkBinaries = CompileGLSLToVulkanSpirvAndCache(shaderSource, hash);
		
			//and read data again
			[[maybe_unused]] bool val = ReadCachedBinaryData(".vkcache", vkBinaries, hash);
		}
		
		existGLShaderCacheFile = std::filesystem::exists(cachedFilename + ".glcache");
		if(!existGLShaderCacheFile) {
			CompileFromVulkanToOpengl(vkBinaries, hash);
		}

		tries = MAX_TRIES;
		//if ReadCachedBinaryData fails it means that the shader code has changed
		while(!ReadCachedBinaryData(".glcache", finalShaderBinaries, hash)) {
			if (--tries == 0) {
				EngineLogWarn("GLShader loader ReadCachedBinaryData(): cannot read {} cached .glcache", filepath);
				__debugbreak();
			}

			//recompile
			CompileFromVulkanToOpengl(vkBinaries, hash);

			//and read data again
			[[maybe_unused]] bool val = ReadCachedBinaryData(".glcache", finalShaderBinaries, hash);
		}

		return true;
	}

	void GLShaderResourceLoader::Process()
	{
		for(const auto& [type, binaries] : vkBinaries) {
			spirv_cross::Compiler comp(binaries);
		
			spirv_cross::ShaderResources res = comp.get_shader_resources();
			ParsePushConstant(res, comp);
		
			for (const auto& resource : res.sampled_images) {
				auto& type = comp.get_type(resource.base_type_id);
				auto binding = comp.get_decoration(resource.id, spv::DecorationBinding);
				const auto& name = resource.name;
				uint32_t dimension = type.image.dim;

				auto& uniform = sampledUniforms[name];
				uniform.name = name;
				uniform.binding = binding;
				uniform.type = GFX::UniformType::Sampler2D;
			}
		}
	}

	bool GLShaderResourceLoader::Dispatch()
	{
		std::filesystem::path p(filepath);
		resource->shader = GFX::Shader::Create(filepath, std::move(finalShaderBinaries));
		
		GFX::GLShader* glShader = (GFX::GLShader*)resource->shader.get();
		glShader->filepath = this->filepath;
		if (!pushConstantBlocks.empty()) {
			glShader->pushConstantBlocks = std::move(pushConstantBlocks);
			glShader->getPushConstantLocations();
		}
		
		if (!sampledUniforms.empty()) {
			glShader->sampledUniforms = std::move(sampledUniforms);
			glShader->getSampledUniformLocations();
		}

		return FullyLoaded();
	}

	GFX::UniformType ConvertSpirvCrossTypeToUniformType(spirv_cross::SPIRType type)
	{
		switch(type.basetype) {
			case spirv_cross::SPIRType::Boolean: return GFX::UniformType::Bool;
			case spirv_cross::SPIRType::Int: return GFX::UniformType::Int;
			case spirv_cross::SPIRType::UInt64: return GFX::UniformType::UInt64;
			case spirv_cross::SPIRType::Float:
				if(type.columns == 4) return GFX::UniformType::Mat4;
	
				if(type.vecsize == 1) return GFX::UniformType::Float;
				if(type.vecsize == 2) return GFX::UniformType::Vec2;
				if(type.vecsize == 3) return GFX::UniformType::Vec3;
				if(type.vecsize == 4) return GFX::UniformType::Vec4;
		} return GFX::UniformType::None;
	}

	void GLShaderResourceLoader::ParsePushConstant(const spirv_cross::ShaderResources& resources, const spirv_cross::Compiler& compiler)
	{
		for(const auto& resource : resources.push_constant_buffers) {
			const auto& bufferType = compiler.get_type(resource.base_type_id);
	
			const std::string& bufferName = resource.name;
			size_t bufferSize = compiler.get_declared_struct_size(bufferType);
			size_t memberCount = bufferType.member_types.size();
	
			auto& buffer = pushConstantBlocks[bufferName];
			buffer.blockName = bufferName;
			buffer.size = bufferSize;
			buffer.blockType = GFX::UniformBlockType::PushConstant;
	
			for(uint32_t i = 0; i < (uint32_t)memberCount; ++i) {
				const auto& type = compiler.get_type(bufferType.member_types[i]);
				const std::string& memberName = compiler.get_member_name(bufferType.self, i);
				size_t size = compiler.get_declared_struct_member_size(bufferType, i);
				uint32_t offset = compiler.type_struct_member_offset(bufferType, i);
	
				std::string uniformName = bufferName + "." + memberName;
				if(type.array.size()) {
					uint32_t stride = compiler.type_struct_member_array_stride(bufferType, i);
					size_t elementsCount = size / stride;
	
					for(size_t i = 0; i < elementsCount; ++i) {
						std::string name = uniformName + "[" + std::to_string(i) + "]";
						auto& uniform = buffer.uniforms[name];
						uniform.name = name;
						uniform.type = ConvertSpirvCrossTypeToUniformType(type);
						uniform.size = size;
						uniform.offset = offset;
	
						offset += stride;
					}
				} else {
					auto& uniform = buffer.uniforms[uniformName];
					uniform.name = uniformName;
					uniform.type = ConvertSpirvCrossTypeToUniformType(type);
					uniform.size = size;
					uniform.offset = offset;
				}
			}
		}
	}

	const uint32_t GLShaderResourceLoader::ShaderTypeFromString(std::string_view type) const
	{
		if(type == "vertex") {
			return GL_VERTEX_SHADER;
		}
		if(type == "fragment") {
			return GL_FRAGMENT_SHADER;
		}
		if(type == "geometry") {
			return GL_GEOMETRY_SHADER;
		}
		if(type == "tess evaluation") {
			return GL_TESS_EVALUATION_SHADER;
		}
		if(type == "tess control") {
			return GL_TESS_CONTROL_SHADER;
		}
		if(type == "compute") {
			return GL_COMPUTE_SHADER;
		}

		return GL_NONE;
	}

	std::unordered_map<uint32_t, std::vector<uint32_t>> GLShaderResourceLoader::CompileGLSLToVulkanSpirvAndCache(const std::unordered_map<uint32_t, std::string>& shaderSource, uint32_t hash)
	{
		std::unordered_map<uint32_t, std::vector<uint32_t>> resultBinaries;
		options.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_2);
		
		const bool optimize = false;
		if(optimize)
			options.SetOptimizationLevel(shaderc_optimization_level_performance);
		
		std::ofstream file(cachedFilename + ".vkcache", std::ofstream::binary);

		file.write((const char*)&hash, sizeof(hash));

		uint32_t shadersCount = static_cast<uint32_t>(shaderSource.size());
		file.write((const char*)&shadersCount, sizeof(shadersCount));
		resultBinaries.reserve(shadersCount);
		
		for(const auto& [type, source] : shaderSource) {
			shaderc::SpvCompilationResult compilationResult = compiler.CompileGlslToSpv(source, GLShaderTypeToShadercShaderType(type), filepath.c_str(), options);
		
			if(compilationResult.GetCompilationStatus() != shaderc_compilation_status_success) {
				file.close();
				std::filesystem::remove(cachedFilename + ".vkcache");
		
				EngineLogError(compilationResult.GetErrorMessage());
				__debugbreak();
			}
		
			const auto shaderData = std::vector<uint32_t>(compilationResult.cbegin(), compilationResult.cend());
		
			resultBinaries[type] = shaderData;
		
			size_t shaderDataSize = shaderData.size();
			file.write((const char*)&shaderDataSize, sizeof(size_t));
			file.write((const char*)&type, sizeof(uint32_t));
			file.write((const char*)shaderData.data(), sizeof(uint32_t) * shaderData.size());
		}
		
		file.close();

		return resultBinaries;
	}

	void GLShaderResourceLoader::CompileFromVulkanToOpengl(const std::unordered_map<uint32_t, std::vector<uint32_t>>& vulkanBinaries, uint32_t hash)
	{
		options.SetTargetEnvironment(shaderc_target_env_opengl_compat, shaderc_env_version_opengl_4_5);
		const bool optimize = false;
		if(optimize)
			options.SetOptimizationLevel(shaderc_optimization_level_performance);
		
		const auto source = LoadFromFile(filepath);
		const auto shaderSource = Preprocess(source);
		
		std::ofstream file(cachedFilename + ".glcache", std::ofstream::binary);

		file.write((const char*)&hash, sizeof(hash));

		uint32_t shadersCount = static_cast<uint32_t>(shaderSource.size());
		file.write((const char*)&shadersCount, sizeof(shadersCount));
		
		for(const auto& [type, binaries] : vulkanBinaries) {
			spirv_cross::CompilerGLSL glsl(binaries);
			//ParsePushConstant(glsl);
		
			std::string source = glsl.compile();
		
			shaderc::SpvCompilationResult compilationResult = compiler.CompileGlslToSpv(source, GLShaderTypeToShadercShaderType(type), filepath.c_str(), options);
		
			if(compilationResult.GetCompilationStatus() != shaderc_compilation_status_success) {
				file.close();
				std::filesystem::remove(cachedFilename + ".glcache");
		
				EngineLogError(compilationResult.GetErrorMessage());
				__debugbreak();
			}
		
			const auto shaderData = std::vector<uint32_t>(compilationResult.cbegin(), compilationResult.cend());
		
			size_t shaderDataSize = shaderData.size();
			file.write((const char*)&shaderDataSize, sizeof(size_t));
			file.write((const char*)&type, sizeof(uint32_t));
			file.write((const char*)shaderData.data(), sizeof(uint32_t) * shaderData.size());
		}
		
		file.close();
	}
}