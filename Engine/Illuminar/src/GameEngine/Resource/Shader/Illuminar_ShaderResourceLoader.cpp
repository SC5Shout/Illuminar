#include "Illuminar_pch.h"
#include "Illuminar_ShaderResourceLoader.h"

namespace Illuminar {
	std::string ShaderResourceLoader::LoadFromFile(std::string_view filePath)
	{
		std::ifstream openFile{std::string(filePath)};
		std::stringstream sstream;

		if(!openFile.is_open()) {
			std::cout << "Could not find the file: " << filePath << std::endl;
		}

		sstream << openFile.rdbuf() << std::endl;

		return sstream.str();
	}

	bool ShaderResourceLoader::ReadCachedBinaryData(const std::string& cacheFormat, std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries, uint32_t hash)
	{
		std::unordered_map<uint32_t, std::vector<uint32_t>> result;

		std::ifstream f(cachedFilename + cacheFormat, std::ifstream::binary);

		uint32_t currentHash = 0;
		f.read((char*)&currentHash, sizeof(currentHash));
		if (hash != currentHash) {
			//shader code has changed, need to recompile
			return false;
		}

		uint32_t shadersCount = 0;
		f.read((char*)&shadersCount, sizeof(shadersCount));

		for(uint32_t i = 0; i < shadersCount; ++i) {
			size_t size = 0;
			uint32_t type = 0;
			f.read((char*)&size, sizeof(size_t));
			f.read((char*)&type, sizeof(uint32_t));
			std::vector<uint32_t> binary(size);
			f.read((char*)binary.data(), sizeof(uint32_t) * size);
			result[type] = std::move(binary);
		}

		binaries = result;

		return true;
	}

	std::unordered_map<uint32_t, std::string> ShaderResourceLoader::Preprocess(std::string_view source)
	{
		std::unordered_map<uint32_t, std::string> shaderSources;

		const char* typeToken = "#type";
		size_t typeTokenLength = strlen(typeToken);
		size_t pos = source.find(typeToken, 0);
		while(pos != std::string::npos) {
			size_t eol = source.find_first_of("\r\n", pos);
			if(eol == std::string::npos) {
				EngineLogError("Syntax error");
				__debugbreak();
			}
			size_t begin = pos + typeTokenLength + 1;
			std::string_view type = source.substr(begin, eol - begin);

			if(type != "vertex" && type != "fragment" && type != "geometry" && type != "tess evaluation" && type != "tess control" && type != "compute") {
				EngineLogError("Invalid shader type specified."
							   "Valid types are: vertex; fragment; geometry; tess evaluation; tess control; compute");
				__debugbreak();
			}

			size_t nextLinePos = source.find_first_not_of("\r\n", eol);
			pos = source.find(typeToken, nextLinePos);
			shaderSources[ShaderTypeFromString(type)] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
		}

		return shaderSources;
	}
}