#include "Illuminar_pch.h"
#include "Illuminar_ShaderResourceManager.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

#include "Illuminar_ShaderResourceLoader.h"
#include "OpenGL/Illuminar_GLShaderResourceLoader.h"

#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar {
	ResourceLoader* ShaderResourceManager::CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeId)
	{
		return new GLShaderResourceLoader(*this);
	}

	ShaderResource* ShaderResourceManager::getShaderResourceByAssetID(AssetID assetID) const
	{
		return getResourceByAssetID(assetID);
	}

	ShaderResourceID ShaderResourceManager::LoadShaderResourceByAssetID(AssetID assetID, ResourceListener* listener, bool reload)
	{
		ResourceLoaderTypeID resourceLoaderTypeID;

		if(GFX::Renderer::api == GFX::Renderer::API::OpenGL) {
			resourceLoaderTypeID = GLShaderResourceLoader::TYPE_ID;
		} else {
			EngineLogError("Only opengl shader resources are available ATM");
			return INVALID_RESOURCE_ID;
		} 

		ShaderResource* resource = getResourceByAssetID(assetID);
		const Asset* asset = AssetManager::tryGetAssetByAssetID(assetID);

		bool load = (reload && nullptr != asset);

		if(!resource && asset) {
			resource = &resources.push();
			resource->resourceManager = this;
			resource->assetID = assetID;
			resource->resourceLoaderTypeID = resourceLoaderTypeID;
			load = true;
		}

		ShaderResourceID resourceID;
		if(resource) {
			resourceID = resource->id;
			if (listener) {
				resource->ConnectListener(*listener);
			}
		} else resourceID = std::numeric_limits<uint32_t>::max();

		if(load) {
			ResourceStreamer::AddLoadRequest(ResourceStreamer::LoadRequest(*asset, resourceLoaderTypeID, reload, *this, resourceID));
		}

		return resourceID;
	}

	void ShaderResourceManager::setInvalidResourceId(ShaderResourceID& shaderResourceID) const
	{
		shaderResourceID = INVALID_RESOURCE_ID;
	}

	void ShaderResourceManager::DestroyResource(ShaderResourceID shaderResourceID)
	{
		resources.pop(shaderResourceID);
	}
}