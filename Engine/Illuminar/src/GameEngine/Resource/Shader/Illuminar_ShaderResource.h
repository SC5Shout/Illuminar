#pragma once

#include "GameEngine/Resource/Illuminar_Resource.h"
#include "GraphicsEngine/Illuminar_Shader.h"

namespace Illuminar {
	using ShaderResourceID = uint32_t;

	struct ShaderResource final : Resource
	{
	friend struct ShaderResourceLoader;

		struct PackedElements
		{
			enum : uint32_t
			{
				Count = 2048
			};
		};

		ShaderResource() = default;
		~ShaderResource() override = default;

		ShaderResource(const ShaderResource& other) = delete;
		ShaderResource& operator=(const ShaderResource& other) = delete;

		ShaderResource(ShaderResource&& other) = default;
		ShaderResource& operator=(ShaderResource&& other) = default;

		void Initialize(ResourceID id);
		void Deinitialize();

		[[nodiscard]] inline const Ref<GFX::Shader>& getShader() const { return shader; }
		void setShader(const Ref<GFX::Shader>& shader);

		Ref<GFX::Shader> shader = nullptr;

		std::string filepath;
	};
}