#pragma once
#include "GameEngine/Resource/Illuminar_ResourceManager.h"

#include "Illuminar_ShaderResourceLoader.h"

namespace Illuminar {
	struct ShaderResource;

	using ShaderResourceID = uint32_t;

	struct ShaderResourceManager final : ResourceManagerTemplate<ShaderResource, ShaderResourceID, ShaderResource::PackedElements::Count>
	{
		ShaderResourceManager() = default;
		virtual ~ShaderResourceManager() override = default;

		ShaderResourceManager(const ShaderResourceManager&) = delete;
		ShaderResourceManager& operator=(const ShaderResourceManager&) = delete;

		[[nodiscard]] ShaderResource* getShaderResourceByAssetID(AssetID assetID) const;
		ShaderResourceID LoadShaderResourceByAssetID(AssetID assetID, ResourceListener* listener = nullptr, bool reload = false);
		void setInvalidResourceId(ShaderResourceID& shaderResourceID) const;

		void DestroyResource(ShaderResourceID shaderResourceID);

		[[nodiscard]] virtual ResourceLoader* CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID) override;
	};
}