#include "Illuminar_pch.h"
#include "Illuminar_ShaderResource.h"

namespace Illuminar {
	void ShaderResource::Initialize(ResourceID id)
	{
		Resource::Initialize(id);
	}

	void ShaderResource::Deinitialize()
	{
		shader.Reset();
		Resource::Deinitialize();
	}

	void ShaderResource::setShader(const Ref<GFX::Shader>& shader)
	{
		if(this->shader) {
			state = LoadingState::Unloaded;
		}
		this->shader = shader;
		state = LoadingState::Loaded;
	}
}