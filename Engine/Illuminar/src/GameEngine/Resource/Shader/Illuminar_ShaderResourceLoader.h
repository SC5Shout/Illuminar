#pragma once

#include "GameEngine/Resource/Illuminar_ResourceLoader.h"
#include "Illuminar_ShaderResource.h"

#include "glad/glad.h"

#include <shaderc/shaderc.hpp>
#include <spirv_cross.hpp>
#include <spirv_glsl.hpp>

namespace Illuminar {	
	struct ShaderResourceLoader : ResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("invalid_abstract_type");

		ShaderResourceLoader(ResourceManager& resourceManager)
			: ResourceLoader(resourceManager)
		{
		}

		~ShaderResourceLoader() override = default;

		void Initialize(const Asset& asset, bool reload, Resource& resource)
		{
			this->asset = &asset;
			this->reload = reload;
			this->resource = static_cast<ShaderResource*>(&resource);
		}

		[[nodiscard]] virtual ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool NeedsDeserialization() const override { return true; }
		[[nodiscard]] virtual bool Deserialize(const std::string& filepath) override = 0;

		[[nodiscard]] bool NeedsProcessing() const override { return true; }
		virtual void Process() override = 0;

		[[nodiscard]] virtual bool Dispatch() override = 0;

		[[nodiscard]] bool FullyLoaded() override { return true; }

		[[nodiscard]] std::string LoadFromFile(std::string_view filePath);

		[[nodiscard]] bool ReadCachedBinaryData(const std::string& cacheFormat, std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries, uint32_t hash);

		[[nodiscard]] std::unordered_map<uint32_t, std::string> Preprocess(std::string_view source);
		[[nodiscard]] virtual const uint32_t ShaderTypeFromString(std::string_view type) const = 0;

		shaderc::Compiler compiler;
		shaderc::CompileOptions options;

		std::unordered_map<uint32_t, std::vector<uint32_t>> finalShaderBinaries;

		std::string filepath;
		std::string cachedFilename;

		ShaderResource* resource = nullptr;
	};
}