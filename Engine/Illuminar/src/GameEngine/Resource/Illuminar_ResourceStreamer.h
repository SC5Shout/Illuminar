#pragma once

#include "Thread/Illuminar_ThreadSafeQueue.h"

namespace Illuminar {
	struct Resource;
	struct ResourceManager;
	struct ResourceLoader;
	struct Asset;

	struct ResourceStreamer
	{
		~ResourceStreamer();

		struct LoadRequest
		{
			LoadRequest() = default;
			inline LoadRequest(const Asset& asset, ResourceLoaderTypeID resourceLoaderTypeID, bool reload, ResourceManager& resourceManager, ResourceID resourceID) :
				asset(&asset),
				resourceLoaderTypeID(resourceLoaderTypeID),
				reload(reload),
				resourceManager(&resourceManager),
				resourceID(resourceID)
			{
			}

			mutable ResourceLoader* resourceLoader = nullptr;

			const Asset* asset = nullptr;
			ResourceManager* resourceManager = nullptr;

			ResourceLoaderTypeID resourceLoaderTypeID = INVALID_RESOURCE_ID;
			ResourceID resourceID = INVALID_RESOURCE_ID;

			bool reload = false;
			bool loadingFailed = false;

			[[nodiscard]] Resource& getResource() const;

			std::strong_ordering operator <=>(const LoadRequest&) const = default;
		};

		static void AddLoadRequest(LoadRequest&& loadRequest);
		static void Dispatch();
		static void FlushAllQueues();

		[[nodiscard]] static inline uint32_t getLoadingResourceCount()
		{
			return get().loadRequestCount.load(std::memory_order_relaxed);
		}

	private:
		ResourceStreamer();
		ResourceStreamer(const ResourceStreamer& other) = delete;
		ResourceStreamer& operator=(const ResourceStreamer& other) = delete;

		static ResourceStreamer& get()
		{
			static ResourceStreamer instance;
			return instance;
		}

		void Deserialize();
		void Process();
		void FinalizeLoadRequest(const LoadRequest& loadRequest);

		void Assure(LoadRequest& loadRequest);

		std::atomic_uint32_t loadRequestCount{ 0 };

		TicketMutex resourceManagerMutex;
		struct ResourceLoaderType
		{
			uint16_t loadersCount = 0;
			std::vector<ResourceLoader*> freeResourceLoaders;
			std::deque<LoadRequest> waitingLoadRequests;
		};
		static constexpr uint16_t MAX_LOADERS_COUNT = 50;
		std::unordered_map<uint32_t, ResourceLoaderType> resourceLoaderTypeManager;
		std::atomic<uint32_t> deserializationWaitingQueueRequests{ 0 };

		std::atomic_bool quitDeserializationThread{ false };
		std::jthread deserializationThread;

		std::atomic_bool quitProcessingThread{ false };
		std::jthread processingThread;

		ThreadSafeQueue<LoadRequest> deserializationQueue;
		ThreadSafeQueue<LoadRequest> processingQueue;
		ThreadSafeQueue<LoadRequest> dispatchQueue;

		std::deque<LoadRequest> fullyLoadedWaitingQueue;
	};
}