#include "Illuminar_pch.h"
#include "Illuminar_ResourceListener.h"

#include "Illuminar_ResourceManager.h"

namespace Illuminar {
	void ResourceListener::DisconnectFromResourceByID(ResourceID resourceID)
	{
		if (resourceID == INVALID_RESOURCE_ID) {
			EngineLogError("ResourceListiner::DisconnectFromResourceByID(): invalid resource id {}", resourceID);
		}

		std::vector<ResourceConnection> resourceConnectionsToDisconnect;
		resourceConnectionsToDisconnect.reserve(connections.size());
		for (const ResourceConnection& resourceConnection : connections) {
			if (resourceConnection.resourceID == resourceID) {
				resourceConnectionsToDisconnect.push_back(resourceConnection);
			}
		}

		for (const ResourceConnection& resourceConnection : resourceConnectionsToDisconnect) {
			resourceConnection.manager->getResourceByID(resourceConnection.resourceID).DisconnectListener(*this);
		}
	}

	void ResourceListener::DisconnectFromAllResources()
	{
		while (!connections.empty()) {
			const ResourceConnection& resourceConnection = connections[0];
			resourceConnection.manager->getResourceByID(resourceConnection.resourceID).DisconnectListener(*this);
		}
	}
}