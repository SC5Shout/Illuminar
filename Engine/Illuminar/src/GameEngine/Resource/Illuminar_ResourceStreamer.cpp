#include "Illuminar_pch.h"
#include "Illuminar_ResourceStreamer.h"

#include "Illuminar_Resource.h"
#include "Illuminar_ResourceLoader.h"
#include "Illuminar_ResourceManager.h"

namespace Illuminar {
	Resource& ResourceStreamer::LoadRequest::getResource() const
	{
		auto& resourceMan = resourceManager;
		return resourceMan->getResourceByID(resourceID);
	}

	ResourceStreamer::ResourceStreamer()
	{
		deserializationThread = std::jthread([&]() {
			while (!deserializationQueue.QuitRequested()) {
				Deserialize();
			}
		});

		processingThread = std::jthread([&]() {
			while (!processingQueue.QuitRequested()) {
				Process();
			}
		});
	}

	ResourceStreamer::~ResourceStreamer()
	{
		deserializationQueue.RequestQuit();
		processingQueue.RequestQuit();
	}

	void ResourceStreamer::AddLoadRequest(LoadRequest&& loadRequest)
	{
		++get().loadRequestCount;
		loadRequest.getResource().setLoadingState(Resource::LoadingState::Loading);

		get().deserializationQueue.push(std::move(loadRequest));
	}

	void ResourceStreamer::Dispatch()
	{
		while (true) {
			if (get().dispatchQueue.empty()) {
				break;
			}

			if (auto f = get().dispatchQueue.try_pop()) {
				auto loadRequest = std::move(*f);
				if (loadRequest.loadingFailed || loadRequest.resourceLoader->Dispatch()) {
					get().FinalizeLoadRequest(loadRequest);
				} else get().fullyLoadedWaitingQueue.push_back(std::move(loadRequest));
			}
		}

		for (const auto& loadRequest : get().fullyLoadedWaitingQueue) {
			if (loadRequest.resourceLoader->FullyLoaded()) {
				get().FinalizeLoadRequest(loadRequest);

				std::erase(get().fullyLoadedWaitingQueue, loadRequest);
			}
		}
	}

	void ResourceStreamer::FlushAllQueues()
	{
		bool everythingFlushed = false;
		do {
			everythingFlushed = (get().deserializationQueue.empty() && get().deserializationWaitingQueueRequests.load(std::memory_order_relaxed) == 0);

			if (everythingFlushed) {
				everythingFlushed = get().processingQueue.empty();
			}

			if (everythingFlushed) {
				everythingFlushed = (get().dispatchQueue.empty() && get().fullyLoadedWaitingQueue.empty());
			}

			Dispatch();

			if (!everythingFlushed) {
				using namespace std::chrono_literals;
				std::this_thread::sleep_for(1ms);
			}
		} while (get().loadRequestCount);

		if (0 != get().loadRequestCount) {
			EngineLogError("Invalid number of in flight load requests");
		}
	}

	void ResourceStreamer::Deserialize()
	{
		if (auto f = deserializationQueue.try_pop()) {
			LoadRequest loadRequest = std::move(*f);
			Assure(loadRequest);
			auto& resourceLoader = loadRequest.resourceLoader;

			if (resourceLoader) {
				resourceLoader->Initialize(*loadRequest.asset, loadRequest.reload, loadRequest.getResource());

				if (resourceLoader->NeedsDeserialization()) {
					const std::string& filepath = resourceLoader->getAsset().virtualFilename;
					if (filepath != "") {
						if (resourceLoader->Deserialize(filepath)) {
							if (resourceLoader->NeedsProcessing()) {
								processingQueue.push(std::move(loadRequest));
							} else dispatchQueue.push(std::move(loadRequest));
						} else {
							loadRequest.loadingFailed = true;
							dispatchQueue.push(std::move(loadRequest));
						}
					} else {
						std::cout << "ResourceStreamer error: the asset has no path\n";
						__debugbreak();
					}
				} else processingQueue.push(std::move(loadRequest));
			}
		}
	}

	void ResourceStreamer::Process()
	{
		if (auto f = processingQueue.try_pop()) {
			LoadRequest loadRequest = std::move(*f);
			loadRequest.resourceLoader->Process();

			dispatchQueue.push(std::move(loadRequest));
		}
	}

	void ResourceStreamer::FinalizeLoadRequest(const LoadRequest& loadRequest)
	{
		{
			std::unique_lock resourceManagerMutexLock(resourceManagerMutex);

			auto loaderID = loadRequest.resourceLoaderTypeID;
			if (resourceLoaderTypeManager.contains(loaderID)) {
				auto& [loadersCount, freeResourceLoaders, waitingLoadRequests] = resourceLoaderTypeManager[loaderID];
				freeResourceLoaders.push_back(loadRequest.resourceLoader);
				if (!waitingLoadRequests.empty()) {
					LoadRequest waitingLoadRequest = waitingLoadRequests.front();
					waitingLoadRequests.pop_front();
					if (0 == deserializationWaitingQueueRequests) {
						EngineLogError("Invalid deserialization waiting queue requests");
						__debugbreak();
					}
					--deserializationWaitingQueueRequests;
					resourceManagerMutexLock.unlock();

					deserializationQueue.push(waitingLoadRequest);
				}
			} else {
				EngineLogError("Could not finalize loader for asset: {}", loadRequest.asset->virtualFilename);
			}
		}

		if (0 == loadRequestCount) {
			EngineLogError("Invalid number of in flight load requests");
			__debugbreak();
		}

		loadRequest.getResource().setLoadingState(loadRequest.loadingFailed ? Resource::LoadingState::Failed : Resource::LoadingState::Loaded);
		--loadRequestCount;
	}

	void ResourceStreamer::Assure(LoadRequest& loadRequest)
	{
		const ResourceLoaderTypeID resourceLoaderTypeID = loadRequest.resourceLoaderTypeID;
		auto& newLoader = loadRequest.resourceLoader;
		const std::lock_guard resourceManagerGuard(resourceManagerMutex);

		if (resourceLoaderTypeManager.contains(resourceLoaderTypeID)) {
			auto& [loadersCount, freeResourceLoaders, waitingLoadRequests] = resourceLoaderTypeManager[resourceLoaderTypeID];
			if (freeResourceLoaders.empty()) {
				if (loadersCount < MAX_LOADERS_COUNT) {
					newLoader = loadRequest.resourceManager->CreateResourceLoader(resourceLoaderTypeID);

					if (newLoader == nullptr) {
						EngineLogError("Invalid load request resource loader");
						__debugbreak();
					}
					++loadersCount;
				} else {
					waitingLoadRequests.push_back(loadRequest);
					++deserializationWaitingQueueRequests;
				}
			} else {
				newLoader = freeResourceLoaders.back();
				freeResourceLoaders.pop_back();
			}
		} else {
			ResourceLoaderType resourceLoaderType;
			resourceLoaderType.loadersCount = 1;
			resourceLoaderTypeManager.emplace(resourceLoaderTypeID, resourceLoaderType);

			newLoader = loadRequest.resourceManager->CreateResourceLoader(resourceLoaderTypeID);
		}
	}
}
