#pragma once

#include "GameEngine/Asset/Illuminar_Asset.h"

#include "Illuminar_ResourceManager.h"

namespace Illuminar {
	struct ResourceLoader
	{
		virtual ~ResourceLoader() = default;

		ResourceLoader(const ResourceLoader&) = delete;
		ResourceLoader& operator=(const ResourceLoader&) = delete;

		ResourceLoader(ResourceManager& resourceManager)
			: resourceManager(resourceManager)
		{
		}

		virtual void Initialize(const Asset& asset, bool reload, Resource& resource) = 0;

		[[nodiscard]] inline const Asset& getAsset() const { return *asset; }
		[[nodiscard]] inline ResourceManager& getResourceManager() const { return resourceManager; }

		[[nodiscard]] virtual ResourceLoaderTypeID getResourceLoaderTypeID() const = 0;

		[[nodiscard]] virtual bool NeedsDeserialization() const = 0;
		[[nodiscard]] virtual bool Deserialize(const std::string& filepath) = 0;

		[[nodiscard]] virtual bool NeedsProcessing() const = 0;
		virtual void Process() = 0;

		[[nodiscard]] virtual bool Dispatch() = 0;

		[[nodiscard]] virtual bool FullyLoaded() = 0;

	protected:
		ResourceManager& resourceManager;
		const Asset* asset = nullptr;
		bool reload = false;
	};
}