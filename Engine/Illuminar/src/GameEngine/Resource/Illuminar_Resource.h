#pragma once

namespace Illuminar {
	struct ResourceManager;
	struct ResourceListener;

	struct Resource
	{
		enum struct LoadingState
		{
			Unloaded,
			Loading,
			Loaded,
			Unloading,
			Failed
		};

		Resource() = default;
		virtual ~Resource() = default;

		Resource(const Resource& other) = delete;
		Resource& operator=(const Resource& other) = delete;

		Resource(Resource&& other) = default;
		Resource& operator=(Resource&& other) = default;

		void Initialize(ResourceID id);
		void Deinitialize();

		void ConnectListener(ResourceListener& resourceListener);
		void DisconnectListener(ResourceListener& resourceListener);

		void setLoadingState(LoadingState state);
		[[nodiscard]] bool Loaded() const { return state == LoadingState::Loaded; }

		std::vector<ResourceListener*> listeners;

		ResourceID id = INVALID_RESOURCE_ID;
		AssetID assetID;
		ResourceLoaderTypeID resourceLoaderTypeID;
		LoadingState state = LoadingState::Unloaded;

		ResourceManager* resourceManager = nullptr;
	};
}