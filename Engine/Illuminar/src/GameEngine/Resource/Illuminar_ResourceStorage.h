#pragma once

#include <array>
#include <inttypes.h>

#include "Illuminar_Resource.h"

namespace Illuminar {
	template<typename T, uint32_t MAXIMUM_NUMBER_OF_ELEMENTS = 2048>
	struct ResourceStorage
	{
		static_assert(std::is_base_of_v<Resource, T>, "Type must derive from Resource");

		ResourceStorage()
		{
			for(uint32_t i = 0; i < MAXIMUM_NUMBER_OF_ELEMENTS; ++i) {
				indices[i].id = i;
				indices[i].next = static_cast<uint16_t>(i + 1);
			}
		}

		~ResourceStorage()
		{
			// If there are any data left alive, smash them
			for(size_t i = 0; i < m_size; ++i) {
				data[i].Deinitialize();
			}
		}

		[[nodiscard]] inline T& push()
		{
			Index& index = indices[freeListDequeue];
			freeListDequeue = index.next;
			index.id += NEW_OBJECT_ID_ADD;
			index.index = static_cast<uint16_t>(m_size++);

			// Initialize the added element
			// to avoid some nasty STL issues
			// TODO: check if it is really the case
			T& element = data[index.index];
			element.Initialize(index.id);

			return element;
		}

		inline void pop(uint32_t id)
		{
			Index& index = indices[id & INDEX_MASK];
			T& element = data[index.index];

			// Deinitialize the removed element
			// to avoid some nasty STL issues
			// TODO: check if it is really the case
			element.Deinitialize();
			--m_size;

			if(index.index != m_size) {
				element = std::move(data[m_size]);
				indices[element.id & INDEX_MASK].index = index.index;
			}

			// Update free list
			index.index = USHRT_MAX;
			indices[freeListEnqueue].next = (id & INDEX_MASK);
			freeListEnqueue = (id & INDEX_MASK);
		}

		[[nodiscard]] inline uint32_t max_count() const { return MAXIMUM_NUMBER_OF_ELEMENTS; }
		[[nodiscard]] inline uint32_t size() const { return m_size; }

		[[nodiscard]] inline T& getByIndex(uint32_t index) const { return data[index]; }
		[[nodiscard]] inline T& getByID(uint32_t id) const { return data[indices[id & INDEX_MASK].index]; }

		[[nodiscard]] inline T* tryGetByID(uint32_t id) const
		{
			if(id != INVALID_RESOURCE_ID) {
				const Index& index = indices[id & INDEX_MASK];
				return (index.id == id && index.index != INVALID_RESOURCE_ID) ? &data[index.index] : nullptr;
			} return nullptr;
		}

		[[nodiscard]] inline bool isIDValid(uint32_t id) const
		{
			if(id != INVALID_RESOURCE_ID) {
				const Index& index = indices[id & INDEX_MASK];
				return (index.id == id && index.index != INVALID_RESOURCE_ID);
			} return false;
		}

		static constexpr uint32_t INDEX_MASK = 0xffff;
		static constexpr uint32_t NEW_OBJECT_ID_ADD = 0x10000;

		struct Index final
		{
			uint32_t id = 0;
			uint16_t index = 0;
			uint16_t next = 0;
		};

		//m_ to avoid naming collision with size() method
		uint32_t m_size = 0;
		mutable std::array<T, MAXIMUM_NUMBER_OF_ELEMENTS> data;
		std::array<Index, MAXIMUM_NUMBER_OF_ELEMENTS> indices;
		uint16_t freeListEnqueue = MAXIMUM_NUMBER_OF_ELEMENTS - 1;
		uint16_t freeListDequeue = 0;
	};
}