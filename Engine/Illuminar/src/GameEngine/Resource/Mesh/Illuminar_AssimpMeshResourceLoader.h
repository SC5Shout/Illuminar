#pragma once

#include "Illuminar_MeshResourceLoader.h"

#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/LogStream.hpp>
#include <assimp/DefaultLogger.hpp>

#include "GraphicsEngine/Illuminar_SparseTextureArray.h"

namespace Illuminar {
	struct AssimpMeshResourceLoader final : MeshResourceLoader
	{
		struct Submesh
		{
			Mat4 transform = Mat4(1.0f);

			std::string nodeName;
			std::string meshName;

			uint32_t baseVertex = 0;
			uint32_t firstIndex = 0;
			uint32_t materialIndex = 0;
			uint32_t vertexCount = 0;
			uint32_t indexCount = 0;
		};

		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("mesh");

		AssimpMeshResourceLoader(ResourceManager& resourceManager)
			: MeshResourceLoader(resourceManager)
		{
		}

		~AssimpMeshResourceLoader() override = default;

		[[nodiscard]] ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool Deserialize(const std::string& filepath) override;
		void Process() override;

		[[nodiscard]] bool Dispatch() override;

	private:
		[[nodiscard]] bool FullyLoaded() override;
 
		void ExtractMaterial(const Ref<GFX::Shader>& shader, const aiScene* scene);

		void TraverseNodes(aiNode* node, const Mat4& parentTransform = Mat4(1.0f), uint32_t level = 0);

		std::unordered_map<aiNode*, std::vector<uint32_t>> nodeMap;

		std::vector<TextureResourceID> texturesIDs;
		std::vector<Ref<GFX::Material>> materials;

		std::vector<Submesh> submeshes;
		std::vector<MeshResource::Vertex> vertices;
		std::vector<uint32_t> elements;

		std::filesystem::path filepath;
	};
}