#include "Illuminar_pch.h"
#include "Illuminar_AssimpMeshResourceLoader.h"

#include "GraphicsEngine/Illuminar_GraphicsPipelineCache.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLGraphicsPipeline.h"

#include "stb_image.h"
#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar {
	inline Mat4 Mat4FromAssimpMat4(const aiMatrix4x4& matrix)
	{
		Mat4 result;
		//the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
		result[0][0] = matrix.a1; result[1][0] = matrix.a2; result[2][0] = matrix.a3; result[3][0] = matrix.a4;
		result[0][1] = matrix.b1; result[1][1] = matrix.b2; result[2][1] = matrix.b3; result[3][1] = matrix.b4;
		result[0][2] = matrix.c1; result[1][2] = matrix.c2; result[2][2] = matrix.c3; result[3][2] = matrix.c4;
		result[0][3] = matrix.d1; result[1][3] = matrix.d2; result[2][3] = matrix.d3; result[3][3] = matrix.d4;
		return result;
	}

	struct LogStream : public Assimp::LogStream
	{
		static void Initialize()
		{
			if (Assimp::DefaultLogger::isNullLogger()) {
				Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
				Assimp::DefaultLogger::get()->attachStream(new LogStream, Assimp::Logger::Err | Assimp::Logger::Warn);
			}
		}

		virtual void write(const char* message) override
		{
			std::cout << "assimp error: " << message << "\n";
			//EngineLogError("Assimp error: {0}", message);
		}
	};

	bool AssimpMeshResourceLoader::Deserialize(const std::string& filepath)
	{
		LogStream::Initialize();

		static constexpr uint32_t meshFlags =
			aiProcess_CalcTangentSpace |        // Create binormals/tangents just in case
			aiProcess_Triangulate |             // Make sure we're triangles
			aiProcess_SortByPType |             // Split meshes by primitive type
			aiProcess_GenNormals |              // Make sure we have legit normals
			aiProcess_GenUVCoords |             // Convert UVs if required 
			aiProcess_OptimizeMeshes |          // Batch draws where possible
			aiProcess_JoinIdenticalVertices |
			aiProcess_ValidateDataStructure;    // Validation

		resource->importer = std::make_unique<Assimp::Importer>();
		resource->scene = resource->importer->ReadFile(filepath, meshFlags);

		this->filepath = filepath;
		return true;
	}

	void AssimpMeshResourceLoader::Process()
	{
		std::vector<MeshResource::Vertex> vertices;
		std::vector<uint32_t> elements;
		std::vector<Submesh> submeshes;

		uint32_t vertexCount = 0;
		uint32_t indexCount = 0;

		submeshes.reserve(resource->scene->mNumMeshes);
		for (uint32_t m = 0; m < resource->scene->mNumMeshes; m++) {
			aiMesh* mesh = resource->scene->mMeshes[m];

			Submesh& submesh = submeshes.emplace_back();
			submesh.baseVertex = vertexCount;
			submesh.firstIndex = indexCount;
			submesh.materialIndex = mesh->mMaterialIndex;
			submesh.vertexCount = mesh->mNumVertices;
			submesh.indexCount = mesh->mNumFaces * 3;
			submesh.meshName = mesh->mName.C_Str();

			vertexCount += mesh->mNumVertices;
			indexCount += submesh.indexCount;

			vertices.reserve(mesh->mNumVertices);
			for (uint32_t i = 0; i < mesh->mNumVertices; i++) {
				MeshResource::Vertex& vertex = vertices.emplace_back();
				vertex.position = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
				vertex.normal = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };

				if (mesh->HasTangentsAndBitangents()) {
					vertex.tangent = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
					vertex.binormal = { mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z };
				}

				if (mesh->HasTextureCoords(0))
					vertex.uv = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };
			}

			elements.reserve(3u * mesh->mNumFaces);
			for (uint32_t i = 0; i < mesh->mNumFaces; i++) {
				elements.push_back(mesh->mFaces[i].mIndices[0]);
				elements.push_back(mesh->mFaces[i].mIndices[1]);
				elements.push_back(mesh->mFaces[i].mIndices[2]);
			}
		}

		//if the old resource loader is reused inside the ResourceStreamer, we either need to clear the old data or move the new data
		this->vertices = std::move(vertices);
		this->elements = std::move(elements);
		this->submeshes = std::move(submeshes);

		nodeMap.clear();
		TraverseNodes(resource->scene->mRootNode);
	}

	bool AssimpMeshResourceLoader::Dispatch()
	{
		auto& mesh = resource->mesh;
		mesh = GFX::Mesh::Create();

		static const GFX::BufferLayout layout = {
			{ GFX::DataType::RGB32_FLOAT, "a_position" },
			{ GFX::DataType::RGB32_FLOAT, "a_normal" },
			{ GFX::DataType::RGB32_FLOAT, "a_tangent" },
			{ GFX::DataType::RGB32_FLOAT, "a_binormal" },
			{ GFX::DataType::RG32_FLOAT, "a_uv" },
		};

		mesh->vbo = GFX::VertexBuffer::Create(layout, sizeof(MeshResource::Vertex) * vertices.size(), vertices.data(), GFX::Usage::Static_Draw);
		mesh->ebo = GFX::ElementBuffer::Create(sizeof(uint32_t) * elements.size(), elements.data(), GFX::Usage::Static_Draw);

		//GFX::GraphicsPipeline::CreateInfo pipelineInfo;
		//pipelineInfo.drawMode = GFX::DrawMode::Triangles;
		//pipelineInfo.bufferLayouts.push_back(resource->vbo->getLayout());
		//pipelineInfo.shader = GFX::ShaderCache::getShader("Lit");
		//pipelineInfo.depthState.func = GFX::DepthTest::Less;
		//pipelineInfo.depthState.mask = GFX::DepthMask::All;
		//pipelineInfo.rasterizerState.cullMode = GFX::CullMode::None;
		//
		//resource->pipeline = GFX::GraphicsPipelineCache::Create(pipelineInfo);
		mesh->name = filepath.filename().stem().string();

		ExtractMaterial(GFX::ShaderCache::getShader("Lit"), resource->scene);

		uint32_t i = 0;
		mesh->submeshes.reserve(submeshes.size());
		for (const auto& submesh : submeshes) {
			auto& newSubmesh = mesh->submeshes.emplace_back();
			newSubmesh.parent = mesh.get();
			newSubmesh.transform = submesh.transform;
			newSubmesh.name = submesh.meshName;
			newSubmesh.material = materials[submesh.materialIndex];
			newSubmesh.baseVertex = submesh.baseVertex;
			newSubmesh.firstIndex = submesh.firstIndex;
			newSubmesh.indexCount = submesh.indexCount;
			newSubmesh.vertexCount = submesh.vertexCount;
		}

		vertices.clear();
		elements.clear();
		materials.clear();
		submeshes.clear();
		nodeMap.clear();

		return FullyLoaded();
	}

	bool AssimpMeshResourceLoader::FullyLoaded()
	{
		for (auto id : texturesIDs) {
			if (!ResourceManagers::get().textureResourceManager.getResourceByID(id).Loaded()) {
				return false;
			}
		}

		return true;
	}

	void AssimpMeshResourceLoader::ExtractMaterial(const Ref<GFX::Shader>& shader, const aiScene* scene)
	{
		if (scene->HasMaterials()) {
			materials.resize(scene->mNumMaterials);

			auto& textureManger = ResourceManagers::get().textureResourceManager;
			auto filename = filepath.filename().stem().string();

			for (uint32_t i = 0; i < scene->mNumMaterials; i++) {
				auto aiMaterial = scene->mMaterials[i];
				auto aiMaterialName = aiMaterial->GetName();

				std::string materialName = std::string(aiMaterialName.C_Str());
				if (materialName.empty()) {
					materialName = filename + std::to_string(i);
				}

				GFX::Material::CreateInfo materialInfo;
				materialInfo.shader = shader;
				materialInfo.name = materialName;
				materialInfo.materialDomain = GFX::MaterialDomain::Surface;
				materialInfo.shadingModel = GFX::ShadingModel::Lit;
				materialInfo.cullMode = GFX::CullMode::Back;
				materialInfo.drawMode = GFX::DrawMode::Triangles;
				//TODO: setup based based on an assimp texture parms
				materialInfo.blendMode = GFX::BlendMode::Opaque;
				auto mi = GFX::MaterialCache::Create(materialInfo);
				materials[i] = mi;

				Vector4<float> albedoColor(0.8f);
				aiColor3D aiColor;
				if (aiMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, aiColor) == AI_SUCCESS)
					albedoColor = { aiColor.r, aiColor.g, aiColor.b, 1.0f };

				float shininess, metalness;
				if (aiMaterial->Get(AI_MATKEY_SHININESS, shininess) != aiReturn_SUCCESS)
					shininess = 80.0f;

				if (aiMaterial->Get(AI_MATKEY_REFLECTIVITY, metalness) != aiReturn_SUCCESS)
					metalness = 0.0f;

				const float roughness = 1.0f - glm::sqrt(shininess / 100.0f);

				mi->set("u_material.baseColor", albedoColor);
				mi->set("u_material.roughness", roughness);
				mi->set("u_material.metalness", metalness);

				aiString aiTexPath;
				bool hasAlbedoMap = aiMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &aiTexPath) == AI_SUCCESS;
				bool fallback = !hasAlbedoMap;
				if (hasAlbedoMap) {
					std::filesystem::path path = filepath;
					auto parentPath = path.parent_path();
					parentPath /= std::filesystem::path(aiTexPath.data);
					std::string texturePath = parentPath.generic_string();

					TextureResourceID baseMapID = textureManger.LoadTextureResourceByAssetID(AssetID{ texturePath }, "stb");
					texturesIDs.push_back(baseMapID);

					mi->set("u_baseMap", textureManger.tryGetResourceByID(baseMapID));
					mi->set("u_material.hasBaseMap", 1);
				}

				bool hasNormalMap = aiMaterial->GetTexture(aiTextureType_NORMALS, 0, &aiTexPath) == AI_SUCCESS;
				fallback = !hasNormalMap;
				if (hasNormalMap) {
					std::filesystem::path path = filepath;
					auto parentPath = path.parent_path();
					parentPath /= std::filesystem::path(aiTexPath.data);
					std::string texturePath = parentPath.generic_string();

					TextureResourceID normalMapID = textureManger.LoadTextureResourceByAssetID(AssetID{ texturePath }, "stb");
					texturesIDs.push_back(normalMapID);

					mi->set("u_normalMap", textureManger.tryGetResourceByID(normalMapID));
					mi->set("u_material.hasNormalMap", 1);
				}

				bool hasRoughnessMap = aiMaterial->GetTexture(aiTextureType_SHININESS, 0, &aiTexPath) == AI_SUCCESS;
				fallback = !hasRoughnessMap;
				if (hasRoughnessMap) {
					std::filesystem::path path = filepath;
					auto parentPath = path.parent_path();
					parentPath /= std::filesystem::path(aiTexPath.data);
					std::string texturePath = parentPath.generic_string();

					TextureResourceID roughnessMapID = textureManger.LoadTextureResourceByAssetID(AssetID{ texturePath }, "stb");
					texturesIDs.push_back(roughnessMapID);
					mi->set("u_roughnessMap", textureManger.tryGetResourceByID(roughnessMapID));
					mi->set("u_material.hasRoughnessMap", 1);
				}

				bool metalnessTextureFound = false;
				for (uint32_t p = 0; p < aiMaterial->mNumProperties; p++) {
					auto prop = aiMaterial->mProperties[p];

					if (prop->mType == aiPTI_String) {
						uint32_t strLength = *(uint32_t*)prop->mData;
						std::string str(prop->mData + 4, strLength);

						std::string key = prop->mKey.data;
						if (key == "$raw.ReflectionFactor|file") {
							std::filesystem::path path = filepath;
							auto parentPath = path.parent_path();
							parentPath /= std::filesystem::path(aiTexPath.data);
							std::string texturePath = parentPath.generic_string();

							TextureResourceID metalnessMapID = textureManger.LoadTextureResourceByAssetID(AssetID{ texturePath }, "stb");
							texturesIDs.push_back(metalnessMapID);

							mi->set("u_metalnessMap", textureManger.tryGetResourceByID(metalnessMapID));
							mi->set("u_material.hasMetalnessMap", 1);

							break;
						}
					}
				}
			}
		} else {
			for (uint32_t i = 0; i < scene->mNumMaterials; i++) {
				GFX::Material::CreateInfo materialInfo;
				materialInfo.shader = shader;
				materialInfo.name = "Default material";
				materialInfo.materialDomain = GFX::MaterialDomain::Surface;
				materialInfo.shadingModel = GFX::ShadingModel::Lit;
				materialInfo.cullMode = GFX::CullMode::Back;
				materialInfo.blendMode = GFX::BlendMode::Opaque;
				materialInfo.drawMode = GFX::DrawMode::Triangles;
				auto mi = GFX::MaterialCache::Create(materialInfo);

				mi->set("u_material.baseColor", Vector4<float>(0.8f));
				mi->set("u_material.roughness", 0.8f);
				mi->set("u_material.metalness", 0.0f);

				mi->set("u_AlbedoTexture", GFX::RenderCommand::getWhiteTexture());
				mi->set("u_MetalnessTexture", GFX::RenderCommand::getWhiteTexture());
				mi->set("u_RoughnessTexture", GFX::RenderCommand::getWhiteTexture());
				materials.push_back(mi);
			}
		}

		resource->mesh->materials = std::make_shared<GFX::MaterialList>(materials.size());
		for (size_t i = 0; i < materials.size(); i++) {
			resource->mesh->materials->SetMaterial((uint32_t)i, materials[i]);
		}
	}

	void AssimpMeshResourceLoader::TraverseNodes(aiNode* node, const Mat4& parentTransform, uint32_t level)
	{
		Mat4 transform = parentTransform * Mat4FromAssimpMat4(node->mTransformation);
		nodeMap[node].resize(node->mNumMeshes);
		for (uint32_t i = 0; i < node->mNumMeshes; i++) {
			uint32_t mesh = node->mMeshes[i];
			auto& submesh = submeshes[mesh];
			submesh.nodeName = node->mName.C_Str();
			submesh.transform = transform;
			nodeMap[node][i] = mesh;
		}

		for (uint32_t i = 0; i < node->mNumChildren; i++) {
			TraverseNodes(node->mChildren[i], transform, level + 1);
		}
	}
}
