#pragma once

#include "GameEngine/Resource/Illuminar_ResourceManager.h"

#include "Illuminar_MeshResourceLoader.h"

namespace Illuminar {
	struct MeshResource;

	using MeshResourceID = uint32_t;

	struct MeshResourceManager final : ResourceManagerTemplate<MeshResource, MeshResourceID, MeshResource::PackedElements::Count>
	{
		MeshResourceManager() = default;
		virtual ~MeshResourceManager() override = default;
		MeshResourceManager(const MeshResourceManager&) = delete;
		MeshResourceManager& operator=(const MeshResourceManager&) = delete;

		[[nodiscard]] MeshResource* getMeshResourceByAssetID(AssetID assetID) const;
		MeshResourceID LoadMeshResourceByAssetID(AssetID assetID, ResourceListener* listener = nullptr, bool reload = false);
		void setInvalidResourceId(MeshResourceID& meshResourceID) const;

		void DestroyResource(MeshResourceID meshResourceID);

		[[nodiscard]] virtual ResourceLoader* CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID) override;
	};
}