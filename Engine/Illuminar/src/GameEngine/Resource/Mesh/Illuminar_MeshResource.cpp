#include "Illuminar_pch.h"
#include "Illuminar_MeshResource.h"

namespace Illuminar {
	MeshResource::MeshResource(MeshResource&& other) noexcept
	{
		Resource::operator=(std::move(other));

		//std::swap(vbo, other.vbo);
		//std::swap(ebo, other.ebo);
		//std::swap(buffer, other.buffer);
		//std::swap(pipeline, other.pipeline);
		//std::swap(submeshes, other.submeshes);
		//std::swap(materials, other.materials);
	}

	MeshResource& MeshResource::operator=(MeshResource&& other) noexcept
	{
		if (this == &other) {
			return *this;
		}

		std::swap(id, other.id);
		std::swap(assetID, other.assetID);
		std::swap(resourceLoaderTypeID, other.resourceLoaderTypeID);
		std::swap(state, other.state);

		//std::swap(vbo, other.vbo);
		//std::swap(ebo, other.ebo);
		//std::swap(buffer, other.buffer);
		//std::swap(pipeline, other.pipeline);
		//std::swap(submeshes, other.submeshes);
		//std::swap(materials, other.materials);

		return *this;
	}

	void MeshResource::Initialize(MeshResourceID id)
	{
		Resource::Initialize(id);
	}

	void MeshResource::Deinitialize()
	{
		//vbo.Reset();
		//ebo.Reset();
		//buffer.Reset();
		//pipeline.Reset();
		//materials.reset();
		//submeshes.clear();

		Resource::Deinitialize();
	}
}