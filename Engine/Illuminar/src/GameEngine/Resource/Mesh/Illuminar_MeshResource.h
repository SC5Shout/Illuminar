#pragma once

#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include "GameEngine/Resource/Illuminar_Resource.h"
#include "GraphicsEngine/Illuminar_GFX.h"
#include "GraphicsEngine/Illuminar_Mesh.h"

namespace Illuminar {
	using MeshResourceID = uint32_t;

	struct MeshResource final : Resource
	{
		struct PackedElements
		{
			enum : uint32_t
			{
				Count = 2048
			};
		};

		struct Vertex
		{
			Vector3<float> position = 0.0f;
			Vector3<float> normal = 0.0f;
			Vector3<float> tangent = 0.0f;
			Vector3<float> binormal = 0.0f;
			Vector2<float> uv = 0.0f;
		};

		MeshResource() = default;
		~MeshResource() override = default;

		MeshResource(MeshResource&& other) noexcept;
		MeshResource& operator=(MeshResource&& other) noexcept;

		void Initialize(MeshResourceID id);
		void Deinitialize();

		Ref<GFX::Mesh> mesh = nullptr;

		//importer needs to be alive because of Scene::BuildMeshHierarchy(), but actually it should not be here.
		//maybe BuildMeshHierarchy should be implemented inside the loader class???
		const aiScene* scene = nullptr;
		std::shared_ptr<Assimp::Importer> importer = nullptr;
	};
}