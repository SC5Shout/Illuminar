#pragma once

#include "Illuminar_MeshResource.h"

#include <array>

namespace Illuminar {
	struct PlaneVertices
	{
		PlaneVertices()
		{
			Vector3<float> edge1 = vertices[1].position - vertices[0].position;
			Vector3<float> edge2 = vertices[2].position - vertices[0].position;
			Vector2<float> deltaUV1 = vertices[1].uv - vertices[0].uv;
			Vector2<float> deltaUV2 = vertices[2].uv - vertices[0].uv;

			Vector3<float> tangent1, bitangent1;
			Vector3<float> tangent2, bitangent2;

			float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

			tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
			tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
			tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

			bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
			bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
			bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);

			edge1 = vertices[2].position - vertices[0].position;
			edge2 = vertices[3].position - vertices[0].position;
			deltaUV1 = vertices[2].uv - vertices[0].uv;
			deltaUV2 = vertices[3].uv - vertices[0].uv;

			f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

			tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
			tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
			tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

			bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
			bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
			bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);

			vertices[0].tangent = tangent1;
			vertices[0].binormal = bitangent1;

			vertices[1].tangent = tangent1;
			vertices[1].binormal = bitangent1;

			vertices[2].tangent = tangent1;
			vertices[2].binormal = bitangent1;

			vertices[3].tangent = tangent2;
			vertices[3].binormal = bitangent2;
		}

		MeshResource::Vertex vertices[4] = {
			{
				Vector3<float>(-1.0f, 0.0f, -1.0f),
				Vector3<float>(0.0f, 1.0f, 0.0f),
				Vector3<float>(),
				Vector3<float>(),
				Vector2<float>(0.0f, 1.0f),
			}, 

			{
				Vector3<float>(-1.0f, 0.0f, 1.0f),
				Vector3<float>(0.0f, 1.0f, 0.0f),
				Vector3<float>(0.0f),
				Vector3<float>(0.0f),
				Vector2<float>(0.0f, 0.0f),
			},

			{
				Vector3<float>(1.0f, 0.0f, 1.0f),
				Vector3<float>(0.0f, 1.0f, 0.0f),
				Vector3<float>(0.0f),
				Vector3<float>(0.0f),
				Vector2<float>(1.0f, 0.0f),
			},

			{
				Vector3<float>(1.0f, 0.0f, -1.0f),
				Vector3<float>(0.0f, 1.0f, 0.0f),
				Vector3<float>(0.0f),
				Vector3<float>(0.0f),
				Vector2<float>(1.0f, 1.0f),
			},
		};
	};
}