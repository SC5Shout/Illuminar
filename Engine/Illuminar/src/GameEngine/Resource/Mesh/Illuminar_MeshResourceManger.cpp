#include "Illuminar_pch.h"

#include "Illuminar_MeshResourceManager.h"

#include "GameEngine/Resource/Illuminar_Resource.h"
#include "Illuminar_AssimpMeshResourceLoader.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

namespace Illuminar {
	ResourceLoader* MeshResourceManager::CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID)
	{
		return new AssimpMeshResourceLoader(*this);
	}

	MeshResource* MeshResourceManager::getMeshResourceByAssetID(AssetID assetID) const
	{
		return getResourceByAssetID(assetID);
	}

	MeshResourceID MeshResourceManager::LoadMeshResourceByAssetID(AssetID assetID, ResourceListener* listener, bool reload)
	{
		ResourceLoaderTypeID resourceLoaderTypeID = AssimpMeshResourceLoader::TYPE_ID;

		MeshResource* resource = getResourceByAssetID(assetID);
		const Asset* asset = AssetManager::tryGetAssetByAssetID(assetID);

		if(!asset) {
			EngineLogError("Cannot find mesh asset: {}", assetID.id);
			return INVALID_RESOURCE_ID;
		}

		bool load = (reload && asset);

		if(!resource && asset) {
			resource = &resources.push();
			resource->resourceManager = this;
			resource->assetID = assetID;
			resource->resourceLoaderTypeID = resourceLoaderTypeID;
			load = true;
		}

		MeshResourceID resourceID;
		if(resource) {
			resourceID = resource->id;
			if (listener) {
				resource->ConnectListener(*listener);
			}
		} else resourceID = std::numeric_limits<uint32_t>::max();

		if(load) {
			ResourceStreamer::AddLoadRequest(ResourceStreamer::LoadRequest(*asset, resourceLoaderTypeID, reload, *this, resourceID));
		}

		return resourceID;
	}

	void MeshResourceManager::setInvalidResourceId(MeshResourceID& meshResourceID) const
	{
		meshResourceID = INVALID_RESOURCE_ID;
	}

	void MeshResourceManager::DestroyResource(MeshResourceID meshResourceID)
	{
		resources.pop(meshResourceID);
	}
}