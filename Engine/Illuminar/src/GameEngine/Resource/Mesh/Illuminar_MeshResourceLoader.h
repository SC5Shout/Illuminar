#pragma once

#include "GameEngine/Resource/Illuminar_ResourceLoader.h"
#include "Illuminar_MeshResource.h"

namespace Illuminar {
	struct MeshResourceLoader : ResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("invalid_abstract_type");

		MeshResourceLoader(ResourceManager& resourceManager)
			: ResourceLoader(resourceManager)
		{
		}

		~MeshResourceLoader() override = default;

		void Initialize(const Asset& asset, bool reload, Resource& resource)
		{
			this->asset = &asset;
			this->reload = reload;
			this->resource = static_cast<MeshResource*>(&resource);
		}

		[[nodiscard]] virtual ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool NeedsDeserialization() const override { return true; }
		[[nodiscard]] virtual bool Deserialize(const std::string& filepath) override = 0;

		[[nodiscard]] bool NeedsProcessing() const override { return true; }
		virtual void Process() override = 0;

		[[nodiscard]] virtual bool Dispatch() override = 0;

		[[nodiscard]] bool FullyLoaded() override = 0;

	//protected:
		MeshResource* resource = nullptr;
	};
}