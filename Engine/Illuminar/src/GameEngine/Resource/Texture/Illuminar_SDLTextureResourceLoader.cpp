#include "Illuminar_pch.h"
#include "Illuminar_SDLTextureResourceLoader.h"

#include "SDL_image.h"

namespace Illuminar {
	SDLTextureResourceLoader::~SDLTextureResourceLoader()
	{
		SDL_FreeSurface((SDL_Surface*)surface);
	}

	Ref<GFX::Texture> SDLTextureResourceLoader::CreateTexture()
	{
		GFX::Texture2D::CreateInfo createInfo {
			.name = filepath,
			.w = w,
			.h = h,
			.pixels = data,
			.parameters = format
		};
		auto result = GFX::Texture2D::Create(createInfo);
		return result;
	}

	bool SDLTextureResourceLoader::Deserialize(const std::string& filepath)
	{
		this->filepath = filepath;
		auto surface = (SDL_Surface*)this->surface;
		surface = IMG_Load(filepath.c_str());
		w = surface->w;
		h = surface->h;
		bpp = surface->format->BytesPerPixel;
		data = surface->pixels;

		format.format = bpp == 4 ? GFX::TextureFormat::RGBA8_SRGB : GFX::TextureFormat::RGB8;

		return true;
	}
}