#pragma once

#include "GameEngine/Resource/Illuminar_ResourceLoader.h"
#include "Illuminar_TextureResource.h"

namespace Illuminar {
	struct TextureResourceLoader : ResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("invalid_abstract_type");

		TextureResourceLoader(ResourceManager& resourceManager)
			: ResourceLoader(resourceManager)
		{
		}

		~TextureResourceLoader() override = default;

		TextureResourceLoader(const TextureResourceLoader&) = delete;
		TextureResourceLoader& operator=(const TextureResourceLoader&) = delete;

		void Initialize(const Asset& asset, bool reload, Resource& resource)
		{
			this->asset = &asset;
			this->reload = reload;
			this->resource = static_cast<TextureResource*>(&resource);
		}

		[[nodiscard]] virtual Ref<GFX::Texture> CreateTexture() = 0;

		[[nodiscard]] virtual ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool NeedsDeserialization() const override { return true; }
		[[nodiscard]] virtual bool Deserialize(const std::string& filepath) override = 0;

		[[nodiscard]] bool NeedsProcessing() const override { return true; }
		virtual void Process() override = 0;

		[[nodiscard]] bool Dispatch() override;

		[[nodiscard]] bool FullyLoaded() override { return true; }

	private:
		TextureResource* resource = nullptr;
	};
}