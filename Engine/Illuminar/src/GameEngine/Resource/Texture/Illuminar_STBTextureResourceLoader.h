#pragma once

#include "Illuminar_TextureResourceLoader.h"

namespace Illuminar {
	struct STBTextureResourceLoader final : TextureResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("stb");

		STBTextureResourceLoader(ResourceManager& resourceManager)
			: TextureResourceLoader(resourceManager)
		{
		}

		~STBTextureResourceLoader() override;
		STBTextureResourceLoader(const STBTextureResourceLoader&) = delete;
		STBTextureResourceLoader& operator=(const STBTextureResourceLoader&) = delete;

		[[nodiscard]] Ref<GFX::Texture> CreateTexture() override;

		[[nodiscard]] ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool Deserialize(const std::string& filepath) override;
		void Process() override {}

	private:
		uint32_t w = 0;
		uint32_t h = 0;
		uint16_t bpp = 0;
		const void* data = nullptr;

		std::string filepath;

		GFX::TextureParameters format;
	};
}