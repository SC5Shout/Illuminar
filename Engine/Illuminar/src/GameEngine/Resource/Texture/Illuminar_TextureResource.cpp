#include "Illuminar_pch.h"
#include "Illuminar_TextureResource.h"

namespace Illuminar {
	void TextureResource::Initialize(ResourceID id)
	{
		Resource::Initialize(id);
	}

	void TextureResource::Deinitialize()
	{
		texture.Reset();
		texture = nullptr;
		Resource::Deinitialize();
	}

	void TextureResource::setTexture(const Ref<GFX::Texture>& newTexture)
	{
		if(texture) {
			setLoadingState(LoadingState::Unloaded);
		}
		texture = newTexture;
		setLoadingState(LoadingState::Loaded);
	}
}