#include "Illuminar_pch.h"
#include "Illuminar_TextureResourceManager.h"

#include "GameEngine/Resource/Illuminar_Resource.h"
#include "GameEngine/Resource/Texture/Illuminar_SDLTextureResourceLoader.h"
#include "GameEngine/Resource/Texture/Illuminar_STBTextureResourceLoader.h"
#include "GameEngine/Resource/Texture/Illuminar_TextureResource.h"
#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

namespace Illuminar {
	ResourceLoader* TextureResourceManager::CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeId)
	{
		if (resourceLoaderTypeId == STBTextureResourceLoader::TYPE_ID) {
			return new STBTextureResourceLoader(*this);
		} return new SDLTextureResourceLoader(*this);
	}

	TextureResource* TextureResourceManager::getTextureResourceByAssetID(AssetID assetID) const
	{
		return getResourceByAssetID(assetID);
	}

	TextureResourceID TextureResourceManager::LoadTextureResourceByAssetID(AssetID assetID, ResourceLoaderTypeID resourceLoaderTypeID, ResourceListener* listener, bool reload)
	{
		TextureResource* resource = getTextureResourceByAssetID(assetID);
		const Asset* asset = AssetManager::tryGetAssetByAssetID(assetID);

		bool load = (reload && nullptr != asset);

		if(!resource && asset) {
			resource = &resources.push();
			resource->resourceManager = this;
			resource->assetID = assetID;
			resource->resourceLoaderTypeID = resourceLoaderTypeID;
			load = true;
		}

		TextureResourceID resourceID;
		if(resource) {
			resourceID = resource->id;
			if (listener) {
				resource->ConnectListener(*listener);
			}
		} else resourceID = INVALID_RESOURCE_ID;

		if(load) {
			
			ResourceStreamer::AddLoadRequest(ResourceStreamer::LoadRequest(*asset, resourceLoaderTypeID, reload, *this, resourceID));
		}

		return resourceID;
	}

	void TextureResourceManager::setInvalidResourceId(TextureResourceID& textureResourceID) const
	{
		textureResourceID = INVALID_RESOURCE_ID;
	}

	void TextureResourceManager::DestroyResource(TextureResourceID shaderResourceID)
	{
		resources.pop(shaderResourceID);
	}
}