#include "Illuminar_pch.h"
#include "Illuminar_TextureResourceLoader.h"

namespace Illuminar {
	bool TextureResourceLoader::Dispatch()
	{
		resource->texture = CreateTexture();
		return true;
	}
}