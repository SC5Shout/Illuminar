#pragma once

#include "GameEngine/Resource/Illuminar_Resource.h"

#include "GraphicsEngine/Illuminar_Texture.h"

namespace Illuminar {
	using TextureResourceID = uint32_t;

	struct TextureResource final : Resource
	{
	friend struct TextureResourceLoader;

		struct PackedElements
		{
			enum : uint32_t
			{
				Count = 2048
			};
		};

		TextureResource() = default;
		~TextureResource() override = default;

		TextureResource(const TextureResource& other) = delete;
		TextureResource& operator=(const TextureResource& other) = delete;

		TextureResource(TextureResource&& other) = default;
		TextureResource& operator=(TextureResource&& other) = default;

		void Initialize(ResourceID id);
		void Deinitialize();

		[[nodiscard]] inline const Ref<GFX::Texture>& getTexture() const { return texture; }
		[[nodiscard]] inline bool isSRGBA() const { return sRgba; }

		void setTexture(const Ref<GFX::Texture>& texture);

		bool sRgba = false;
		Ref<GFX::Texture> texture = nullptr;
	};
}