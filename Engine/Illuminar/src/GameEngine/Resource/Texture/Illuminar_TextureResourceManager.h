#pragma once

#include "GameEngine/Resource/Illuminar_ResourceManager.h"
#include "Illuminar_TextureResourceLoader.h"

namespace Illuminar {
	struct TextureResource;

	struct TextureResourceManager final : ResourceManagerTemplate<TextureResource, TextureResourceID, TextureResource::PackedElements::Count>
	{
		TextureResourceManager() = default;
		virtual ~TextureResourceManager() override = default;
		TextureResourceManager(const TextureResourceManager&) = delete;
		TextureResourceManager& operator=(const TextureResourceManager&) = delete;

		[[nodiscard]] TextureResource* getTextureResourceByAssetID(AssetID assetID) const;
		TextureResourceID LoadTextureResourceByAssetID(AssetID assetID, ResourceLoaderTypeID resourceLoaderTypeID, ResourceListener* listener = nullptr, bool reload = false);
		void setInvalidResourceId(TextureResourceID& textureResourceID) const;

		[[nodiscard]] virtual ResourceLoader* CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID) override;
		void DestroyResource(TextureResourceID shaderResourceID);
	};
}