#pragma once

#include "Illuminar_TextureResourceLoader.h"

namespace Illuminar {
	struct SDLTextureResourceLoader final : TextureResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("png");

		SDLTextureResourceLoader(ResourceManager& resourceManager)
			: TextureResourceLoader(resourceManager)
		{
		}

		~SDLTextureResourceLoader() override;
		SDLTextureResourceLoader(const SDLTextureResourceLoader&) = delete;
		SDLTextureResourceLoader& operator=(const SDLTextureResourceLoader&) = delete;

		[[nodiscard]] Ref<GFX::Texture> CreateTexture() override;

		[[nodiscard]] ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool Deserialize(const std::string& filepath) override;
		void Process() override {}

	private:
		uint32_t w = 0;
		uint32_t h = 0;
		uint16_t bpp = 0;
		const void* data = nullptr;

		void* surface = nullptr;

		std::string filepath;

		GFX::TextureParameters format;
	};
}