#include "Illuminar_pch.h"
#include "Illuminar_STBTextureResourceLoader.h"

#include "stb_image.h"

namespace Illuminar {
	STBTextureResourceLoader::~STBTextureResourceLoader()
	{
		stbi_image_free((void*)data);
	}

	Ref<GFX::Texture> STBTextureResourceLoader::CreateTexture()
	{
		GFX::Texture2D::CreateInfo createInfo{
			.name = filepath,
			.w = w,
			.h = h,
			.pixels = data,
			.parameters = format
		};
		auto result = GFX::Texture2D::Create(createInfo);
		return result;
	}

	bool STBTextureResourceLoader::Deserialize(const std::string& filepath)
	{
		this->filepath = filepath;

		int width, height, nrComponents;
		data = stbi_load(filepath.c_str(), &width, &height, &nrComponents, 0);

		w = width;
		h = height;
		bpp = nrComponents;

		format.format = bpp == 4 ? GFX::TextureFormat::RGBA8_SRGB : GFX::TextureFormat::RGB8;

		return true;
	}
}