#include "Illuminar_pch.h"
#include "Illuminar_MaterialResourceLoader.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

#include "GameEngine/Serializer/Illuminar_Serializer.h"

namespace Illuminar {
	bool MaterialResourceLoader::Deserialize(const std::string& filepath)
	{
		MaterialSerializer serializer;
		serializer.Deserialize(filepath, resource->materialFile);

		return true;
	}

	void MaterialResourceLoader::Process()
	{
		//TODO: process could setup default materials
		// need to figure it out how to allocate space for material's uniform buffer
	}

	bool MaterialResourceLoader::Dispatch()
	{
		const auto& material = resource->materialFile;
		
		auto shaderResource = ResourceManagers::get().shaderResourceManager.getShaderResourceByAssetID(material.shaderAssetID);
		const auto& shader = shaderResource->shader;
		
		const auto textureResource = ResourceManagers::get().textureResourceManager.getTextureResourceByAssetID(material.baseMapAssetID);
		
		//resource->material = GFX::Material::Create(shader, "Default material");
		//resource->material->set("material.baseColor", material.baseColor);
		//resource->material->set("u_baseMap", textureResource);

		return FullyLoaded();
	}
}