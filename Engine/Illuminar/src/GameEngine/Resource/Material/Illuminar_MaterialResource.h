#pragma once

#include "GameEngine/Resource/Illuminar_Resource.h"

#include "GameEngine/Resource/Shader/Illuminar_ShaderResource.h"

#include "GraphicsEngine/Illuminar_Material.h"
#include "GameEngine/Serializer/Illuminar_Serializer.h"

namespace Illuminar {
	using MaterialResourceID = uint32_t;
	using TextureResourceID = uint32_t;

	struct MaterialResource final : Resource
	{
		friend struct MaterialResourceLoader;

		struct PackedElements
		{
			enum : uint32_t
			{
				Count = 2048
			};
		};

		MaterialResource() = default;
		~MaterialResource() override = default;

		MaterialResource(const MaterialResource& other) = delete;
		MaterialResource& operator=(const MaterialResource& other) = delete;

		MaterialResource(MaterialResource&& other) = default;
		MaterialResource& operator=(MaterialResource&& other) = default;

		void Initialize(ResourceID id);
		void Deinitialize();

		Ref<GFX::Material> material = nullptr;
		MaterialFile materialFile;
	};
}