#pragma once

#include "GameEngine/Resource/Illuminar_ResourceLoader.h"
#include "Illuminar_MaterialResource.h"

namespace Illuminar {
	struct MaterialResourceLoader : ResourceLoader
	{
		static constexpr uint32_t TYPE_ID = Hash32::CompileTimeFNV("MATERIAL_LOADER_TYPE");

		MaterialResourceLoader(ResourceManager& resourceManager)
			: ResourceLoader(resourceManager)
		{
		}

		~MaterialResourceLoader() override = default;

		void Initialize(const Asset& asset, bool reload, Resource& resource)
		{
			this->asset = &asset;
			this->reload = reload;
			this->resource = static_cast<MaterialResource*>(&resource);
		}

		[[nodiscard]] virtual ResourceLoaderTypeID getResourceLoaderTypeID() const override { return TYPE_ID; }

		[[nodiscard]] bool NeedsDeserialization() const override { return true; }
		[[nodiscard]] virtual bool Deserialize(const std::string& filepath) override;

		[[nodiscard]] bool NeedsProcessing() const override { return true; }
		virtual void Process() override;

		[[nodiscard]] virtual bool Dispatch() override;

		[[nodiscard]] bool FullyLoaded() override { return true; }

		MaterialResource* resource = nullptr;
	};
}