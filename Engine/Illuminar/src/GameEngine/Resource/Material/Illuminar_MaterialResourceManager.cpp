#include "Illuminar_pch.h"
#include "Illuminar_MaterialResourceManager.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

#include "Illuminar_MaterialResourceLoader.h"

namespace Illuminar {
	ResourceLoader* MaterialResourceManager::CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeId)
	{
		return new MaterialResourceLoader(*this);
	}

	MaterialResource* MaterialResourceManager::getMaterialResourceByAssetID(AssetID assetID) const
	{
		return getResourceByAssetID(assetID);
	}

	MaterialResourceID MaterialResourceManager::LoadMaterialResourceByAssetID(AssetID assetID, ResourceListener* listener, bool reload)
	{
		ResourceLoaderTypeID resourceLoaderTypeID = MaterialResourceLoader::TYPE_ID;
		MaterialResource* resource = getResourceByAssetID(assetID);
		const Asset* asset = AssetManager::tryGetAssetByAssetID(assetID);

		bool load = (reload && nullptr != asset);

		if(!resource && asset) {
			resource = &resources.push();
			resource->resourceManager = this;
			resource->assetID = assetID;
			resource->resourceLoaderTypeID = resourceLoaderTypeID;
			load = true;
		}

		MaterialResourceID resourceID;
		if(resource) {
			resourceID = resource->id;
			if (listener) {
				resource->ConnectListener(*listener);
			}
		} else resourceID = std::numeric_limits<uint32_t>::max();

		if(load) {
			ResourceStreamer::AddLoadRequest(ResourceStreamer::LoadRequest(*asset, resourceLoaderTypeID, reload, *this, resourceID));
		}

		return resourceID;
	}

	void MaterialResourceManager::setInvalidResourceId(MaterialResourceID& materialResourceId) const
	{
		materialResourceId = INVALID_RESOURCE_ID;
	}

	void MaterialResourceManager::DestroyResource(MaterialResourceID materialResourceId)
	{
		resources.pop(materialResourceId);
	}
}