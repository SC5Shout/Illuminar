#include "Illuminar_pch.h"
#include "Illuminar_MaterialResource.h"

namespace Illuminar {
	void MaterialResource::Initialize(ResourceID id)
	{
		Resource::Initialize(id);
	}

	void MaterialResource::Deinitialize()
	{
		Resource::Deinitialize();

		material.Reset();
		material = nullptr;
	}
}