#pragma once
#include "GameEngine/Resource/Illuminar_ResourceManager.h"

#include "Illuminar_MaterialResourceLoader.h"

namespace Illuminar {
	struct MaterialResource;

	using MaterialResourceID = uint32_t;
	using TextureResourceID = uint32_t;

	struct MaterialResourceManager final : ResourceManagerTemplate<MaterialResource, MaterialResourceID, MaterialResource::PackedElements::Count>
	{
		MaterialResourceManager() = default;
		virtual ~MaterialResourceManager() override = default;
		MaterialResourceManager(const MaterialResourceManager&) = delete;
		MaterialResourceManager& operator=(const MaterialResourceManager&) = delete;

		[[nodiscard]] MaterialResource* getMaterialResourceByAssetID(AssetID assetID) const;
		MaterialResourceID LoadMaterialResourceByAssetID(AssetID assetID, ResourceListener* listener, bool reload);
		void setInvalidResourceId(MaterialResourceID& materialResourceId) const;

		void DestroyResource(MaterialResourceID materialResourceID);

		[[nodiscard]] virtual ResourceLoader* CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID) override;
	};
}