#include "Illuminar_pch.h"
#include "Illuminar_Resource.h"

#include "Illuminar_ResourceListener.h"

namespace Illuminar {
	void Resource::Initialize(ResourceID id)
	{
		this->id = id;
	}

	void Resource::Deinitialize()
	{
		if (state == LoadingState::Loading || state == LoadingState::Unloading) {
			EngineLogFatal("Resource deinitialized while in-flight inside the resource streamer");
			__debugbreak();
		}

		if(state != LoadingState::Unloaded && state != LoadingState::Failed) {
			setLoadingState(LoadingState::Unloaded);
		}

		const ResourceListener::ResourceConnection resourceConnection(resourceManager, id);
		for (auto listener : listeners) {
			auto connectionIterator = std::find_if(listener->connections.begin(), listener->connections.end(),
				[resourceConnection](const auto& currentResourceConnection) { 
					return (currentResourceConnection.manager == resourceConnection.manager && currentResourceConnection.resourceID == resourceConnection.resourceID);
				}
			);
			//ASSERT(connectionIterator != resourceListener->mResourceConnections.end(), "Invalid connection iterator")
			//	resourceListener->mResourceConnections.erase(connectionIterator);
		}

		resourceManager = nullptr;
		id = INVALID_RESOURCE_ID;
		assetID = INVALID_RESOURCE_ID;
		resourceLoaderTypeID = INVALID_RESOURCE_ID;
		listeners.clear();
	}

	void Resource::ConnectListener(ResourceListener& resourceListener)
	{
		auto iterator = std::lower_bound(listeners.begin(), listeners.end(), &resourceListener);
		if (iterator == listeners.end() || *iterator != &resourceListener) {
			listeners.insert(iterator, &resourceListener);
			resourceListener.connections.emplace_back(resourceManager, id);
			resourceListener.OnLoadingStateChange(*this);
		}
	}

	void Resource::DisconnectListener(ResourceListener& resourceListener)
	{
		auto iterator = std::lower_bound(listeners.begin(), listeners.end(), &resourceListener);
		if (iterator != listeners.end() && *iterator == &resourceListener) {
			{
				const ResourceListener::ResourceConnection resourceConnection(resourceManager, id);
				auto connectionIterator = std::find_if(resourceListener.connections.begin(), resourceListener.connections.end(),
					[resourceConnection](const ResourceListener::ResourceConnection& currentResourceConnection) { 
						return (currentResourceConnection.manager == resourceConnection.manager && currentResourceConnection.resourceID == resourceConnection.resourceID);
					}
				);
				//ASSERT(connectionIterator != resourceListener.mResourceConnections.end(), "Invalid connection iterator")
				//	resourceListener.mResourceConnections.erase(connectionIterator);
			}
			listeners.erase(iterator);
		}
	}

	void Resource::setLoadingState(LoadingState loadingState)
	{
		state = loadingState;
		for (ResourceListener* resourceListener : listeners) {
			//this can throw if a resource won't be destroyed inside the "final" listener derived class
			resourceListener->OnLoadingStateChange(*this);
		}
	}
}