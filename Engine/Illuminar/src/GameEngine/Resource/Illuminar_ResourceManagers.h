#pragma once

#include "Mesh/Illuminar_MeshResourceManager.h"
#include "Texture/Illuminar_TextureResourceManager.h"
#include "Shader/Illuminar_ShaderResourceManager.h"
#include "Material/Illuminar_MaterialResourceManager.h"

namespace Illuminar {
	struct ResourceManagers
	{
		static ResourceManagers& get()
		{
			static ResourceManagers instance;
			return instance;
		}

		[[nodiscard]] static inline uint32_t getAllResourceCount()
		{
			return
				get().meshResourceManager.getResourceCount() +
				get().textureResourceManager.getResourceCount() +
				get().shaderResourceManager.getResourceCount() +
				get().materialResourceManager.getResourceCount();
		}

		ResourceManagers() = default;
		~ResourceManagers() = default;

		ResourceManagers(const ResourceManagers& other) = delete;
		ResourceManagers& operator=(const ResourceManagers& other) = delete;

		MeshResourceManager meshResourceManager;
		TextureResourceManager textureResourceManager;
		ShaderResourceManager shaderResourceManager;
		MaterialResourceManager materialResourceManager;
	};
}