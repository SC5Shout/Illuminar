#pragma once

namespace Illuminar {
	struct ResourceManager;
	struct Resource;

	struct ResourceListener
	{
		struct ResourceConnection
		{
			ResourceConnection(ResourceManager* resourceManager, ResourceID resourceId) :
				manager(resourceManager),
				resourceID(resourceId)
			{
			}

			ResourceManager* manager = nullptr;
			ResourceID resourceID = INVALID_RESOURCE_ID;
		};

		virtual void OnLoadingStateChange(const Resource& resource) = 0;

		[[nodiscard]] inline const std::vector<ResourceConnection>& getResourceConnections() const { return connections; }
		[[nodiscard]] inline bool operator<(ResourceListener* right) { return (this < right); }

	private:
		friend struct Resource;

		void DisconnectFromResourceByID(ResourceID resourceID);
		void DisconnectFromAllResources();

		std::vector<ResourceConnection> connections;
	};
}