#pragma once

#include "Illuminar_Resource.h"
#include "Illuminar_ResourceStorage.h"

#include "GameEngine/Asset/Illuminar_AssetManager.h"

namespace Illuminar {
	struct ResourceLoader;

	struct ResourceManager
	{
		friend struct RendererImpl;
		friend struct ResourceStreamer;

		ResourceManager() = default;
		virtual ~ResourceManager() = default;

		ResourceManager(const ResourceManager&) = delete;
		ResourceManager& operator=(const ResourceManager&) = delete;

		[[nodiscard]] virtual uint32_t getResourceCount() const = 0;
		[[nodiscard]] virtual Resource& getResourceByIndex(uint32_t index) const = 0;
		[[nodiscard]] virtual Resource& getResourceByID(ResourceID resourceID) const = 0;
		[[nodiscard]] virtual Resource* tryGetResourceByID(ResourceID resourceID) const = 0;

		inline void setResourceLoadingState(Resource& resource, Resource::LoadingState loadingState) { resource.setLoadingState(loadingState); }

		[[nodiscard]] virtual ResourceLoader* CreateResourceLoader(ResourceLoaderTypeID resourceLoaderTypeID) = 0;
	};

	template <typename T, typename TYPE_ID, uint32_t MAXIMUM_NUMBER_OF_ELEMENTS>
	struct ResourceManagerTemplate : ResourceManager
	{
		typedef ResourceStorage<T, MAXIMUM_NUMBER_OF_ELEMENTS> Resources;

		ResourceManagerTemplate() = default;
		virtual ~ResourceManagerTemplate() override = default;

		ResourceManagerTemplate(const ResourceManagerTemplate&) = delete;
		ResourceManagerTemplate& operator=(const ResourceManagerTemplate&) = delete;

		[[nodiscard]] virtual uint32_t getResourceCount() const override { return resources.size(); }
		[[nodiscard]] virtual T& getResourceByIndex(uint32_t index) const override { return resources.getByIndex(index); }
		[[nodiscard]] virtual T& getResourceByID(TYPE_ID id) const override { return resources.getByID(id); }
		[[nodiscard]] virtual T* tryGetResourceByID(TYPE_ID id) const override { return resources.tryGetByID(id); }

		[[nodiscard]] inline T* getResourceByAssetID(AssetID assetID) const
		{
			for(auto& resource : resources.data) {
				if(resource.assetID == assetID) {
					return &resource;
				}
			} return nullptr;
		}

		Resources resources;
	};
}