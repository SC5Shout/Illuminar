#pragma once

#define MiE_PI 3.14159265359

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace Illuminar {
	typedef glm::mat2 Mat2;
	typedef glm::mat3 Mat3;
	typedef glm::mat4 Mat4;

	template<typename T>
	using Vector2 = glm::vec<2, T, glm::highp>;

	template<typename T>
	using Vector3 = glm::vec<3, T, glm::highp>;

	template<typename T>
	using Vector4 = glm::vec<4, T, glm::highp>;

	template<typename T>
	static inline std::ostream& operator<<(std::ostream& os, const Vector2<T>& dt)
	{
		os << "[" << dt.x << ", " << dt.y << "]";
		return os;
	}

	template<typename T>
	static inline std::ostream& operator<<(std::ostream& os, const Vector3<T>& dt)
	{
		os << "[" << dt.x << ", " << dt.y << ", " << dt.z << "]";
		return os;
	}
	template<typename T>
	static inline std::ostream& operator<<(std::ostream& os, const Vector4<T>& dt)
	{
		os << "[" << dt.x << ", " << dt.y << ", " << dt.z << ", " << dt.w << "]";
		return os;
	}

	template<typename T>
	[[nodiscard]] static inline constexpr T Clamp(T value, T minimum, T maximum)
	{
		return value > minimum ? value < maximum ? value : maximum : minimum;
	}

	template<typename T>
	[[nodiscard]] static inline Vector2<T> ImClamp(const Vector2<T>& v, const Vector2<T>& mn, Vector2<T> mx) { return Vector2<T>((v.x < mn.x) ? mn.x : (v.x > mx.x) ? mx.x : v.x, (v.y < mn.y) ? mn.y : (v.y > mx.y) ? mx.y : v.y); }

	//inline Vector2 Clamp(const Vector2& value, const Vector2& min, const Vector2& max)
	//{

	//}

	template<typename T>
	[[nodiscard]] static inline constexpr T Degrees(T angleInRadians)
	{
		return angleInRadians * T(57.29577951307855);
		//return angleInRadians * static_cast<T>(180.0f / M_PI);
	}

	template<typename T>
	[[nodiscard]] static inline constexpr T Radians(T angleInDegrees)
	{
		return angleInDegrees * T(0.0174532925199444);
		//return angleInRadians * static_cast<T>(M_PI / 180.0f);
	}

	[[nodiscard]] static inline Mat4 Translate(const Vector3<float>& v)
	{
		return glm::translate(Mat4(1.0f), v);
	}

	template<typename T>
	[[nodiscard]] static inline Mat4 Rotate(T radians, const Vector3<float>& v)
	{
		return glm::rotate(Mat4(1.0f), radians, v);
	}

	[[nodiscard]] static inline Mat4 Scale(const Vector3<float>& v)
	{
		return glm::scale(Mat4(1.0f), v);
	}

	[[nodiscard]] static inline Mat4 Inverse(const Mat4& m)
	{
		return glm::inverse(m);
	}

	//Calcualates T parameter of Lerp, if value (product of Lerp) is given
	template<typename T>
	[[nodiscard]] static inline constexpr float InverseLerp(const T& a, const T& b, const T& value)
	{
		return (value - a) / (b - a);
	}

	template<typename T>
	[[nodiscard]] static inline constexpr T Min(T left, T right)
	{
		return left < right ? left : right;
	}

	template<typename T>
	[[nodiscard]] static inline constexpr T Max(T left, T right)
	{
		return left >= right ? left : right;
	}

	template<typename T>
	[[nodiscard]] static inline constexpr T Saturate(T value, T min = static_cast<T>(0.0f), T max = static_cast<T>(1.0f))
	{
		return value < static_cast<T>(min) ? static_cast<T>(min) : value > static_cast<T>(max) ? static_cast<T>(max) : value;
	}

	/*
		alignment - from 0.0f to 1.0f
		-
		size - size of the component we want to be aligned
		-
		min - offset from right margin  ------>
		max - offset from left margin  <-------
	*/
	template<typename T>
	[[nodiscard]] static inline Vector2<T> getAlignedPosition(const Vector2<T>& alignment, const Vector2<T>& size, const Vector2<T>& rightMargin, const Vector2<T>& leftMargin, const Vector2<T>& min = 0.0f, const Vector2<T>& max = 0.0f)
	{
		Vector2<T> result = rightMargin + min;
		if(alignment.x > 0.0f) result.x = Max(rightMargin.x + min.x, rightMargin.x + (leftMargin.x - rightMargin.x - size.w - max.x) * alignment.x);
		if(alignment.y > 0.0f) result.y = Max(rightMargin.y + min.y, rightMargin.y + (leftMargin.y - rightMargin.y - size.h - max.y) * alignment.y);
		return result;
	}

	template<typename T>
	[[nodiscard]] static inline Vector2<T> CrossProduct2D(const Vector2<T>& vector)
	{
		return { -vector.y, vector.x };
	}

	template<typename T>
	static inline bool DecomposeTransform(const Mat4& transform, Vector3<T>& translation, Vector3<T>& rotation, Vector3<T>& scale)
	{
		using namespace glm;

		mat4 LocalMatrix(transform);

		if(epsilonEqual(LocalMatrix[3][3], static_cast<float>(0), epsilon<T>()))
			return false;

		if(
			epsilonNotEqual(LocalMatrix[0][3], static_cast<T>(0), epsilon<T>()) ||
			epsilonNotEqual(LocalMatrix[1][3], static_cast<T>(0), epsilon<T>()) ||
			epsilonNotEqual(LocalMatrix[2][3], static_cast<T>(0), epsilon<T>())) {

			LocalMatrix[0][3] = LocalMatrix[1][3] = LocalMatrix[2][3] = static_cast<T>(0);
			LocalMatrix[3][3] = static_cast<T>(1);
		}

		translation = vec3(LocalMatrix[3]);
		LocalMatrix[3] = vec4(0, 0, 0, LocalMatrix[3].w);

		vec3 Row[3] = { 1.0f }, Pdum3 = 1.0f;

		for(length_t i = 0; i < 3; ++i)
			for(length_t j = 0; j < 3; ++j)
				Row[i][j] = LocalMatrix[i][j];

		scale.x = length(Row[0]);
		Row[0] = detail::scale(Row[0], static_cast<T>(1));
		scale.y = length(Row[1]);
		Row[1] = detail::scale(Row[1], static_cast<T>(1));
		scale.z = length(Row[2]);
		Row[2] = detail::scale(Row[2], static_cast<T>(1));

		rotation.y = asin(-Row[0][2]);
		if(cos(rotation.y) != 0) {
			rotation.x = atan2(Row[1][2], Row[2][2]);
			rotation.z = atan2(Row[0][1], Row[0][0]);
		} else {
			rotation.x = atan2(-Row[2][0], Row[1][1]);
			rotation.z = 0;
		}

		return true;
	}

	[[nodiscard]] static inline auto getTransformDecomposition(const Mat4& transform)
	{
		Vector3<float> translate = 1.0f;
		Vector3<float> rotate = 1.0f; 
		Vector3<float> scale = 1.0f;
		DecomposeTransform(transform, translate, rotate, scale);
		return std::make_tuple(translate, rotate, scale);
	}

	[[nodiscard]] static inline uint32_t CalculateMipCount(uint32_t width, uint32_t height)
	{
		return (uint32_t)std::floor(std::log2(glm::min(width, height))) + 1;
	}
}
