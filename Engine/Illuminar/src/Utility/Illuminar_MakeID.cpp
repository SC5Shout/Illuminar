#include "Illuminar_pch.h"
#include "Illuminar_MakeID.h"

namespace Illuminar {
	MakeID::MakeID(const uint16_t max_id)
	{
		ranges.resize(1);

		ranges[0].first = 0;
		ranges[0].last = max_id;
	}

	MakeID::~MakeID()
	{
		ranges.clear();
	}

	bool MakeID::CreateID(uint16_t& id)
	{
		if(ranges[0].first <= ranges[0].last) {
			id = ranges[0].first;

			// If current range is full and there is another one, that will become the new current range
			if(ranges[0].first == ranges[0].last && ranges.size() > 1) {
				DestroyRange(0);
			} else ++ranges[0].first;
			return true;
		}

		// No available ID left
		return false;
	}

	bool MakeID::CreateRangeID(uint16_t& id, const uint16_t count)
	{
		uint16_t i = 0;
		do {
			const uint16_t range_count = 1u + ranges[i].last - ranges[i].first;
			if(count <= range_count) {
				id = ranges[i].first;

				// If current range is full and there is another one, that will become the new current range
				if(count == range_count && i + static_cast<uint16_t>(1U) < ranges.size()) {
					DestroyRange(i);
				} else ranges[i].first += count;
				return true;
			}
			++i;
		} while(i < ranges.size());

		// No range of free IDs was large enough to create the requested continuous ID sequence
		return false;
	}

	bool MakeID::DestroyID(const uint16_t id)
	{
		return DestroyRangeID(id, 1);
	}

	bool MakeID::DestroyRangeID(const uint16_t id, const uint16_t count)
	{
		const uint16_t end_id = static_cast<uint16_t>(id + count);

		// Binary search of the range list
		uint16_t i0 = 0u;
		uint16_t i1 = static_cast<uint16_t>(ranges.size()) - 1u;

		for(;;) {
			const uint16_t i = (i0 + i1) / 2u;

			if(id < ranges[i].first) {
				// Before current range, check if neighboring
				if(end_id >= ranges[i].first) {
					if(end_id != ranges[i].first)
						return false; // Overlaps a range of free IDs, thus (at least partially) invalid IDs

					// Neighbor id, check if neighboring previous range too
					if(i > i0 && id - 1 == ranges[i - 1].last) {
						// Merge with previous range
						ranges[i - 1].last = ranges[i].last;
						DestroyRange(i);
					} else ranges[i].first = id;
					return true;
				} else {
					// Non-neighbor id
					if(i != i0) {
						// Cull upper half of list
						i1 = i - 1u;
					} else {
						// Found our position in the list, insert the deleted range here
						InsertRange(i);
						ranges[i].first = id;
						ranges[i].last = end_id - 1u;
						return true;
					}
				}
			} else if(id > ranges[i].last) {
				// After current range, check if neighboring
				if(id - 1 == ranges[i].last) {
					// Neighbor id, check if neighboring next range too
					if(i < i1 && end_id == ranges[i + static_cast<uint16_t>(1)].first) {
						// Merge with next range
						ranges[i].last = ranges[i + static_cast<uint16_t>(1)].last;
						DestroyRange(i + 1u);
					} else ranges[i].last += count;
					return true;
				} else {
					// Non-neighbor id
					if(i != i1) {
						// Cull bottom half of list
						i0 = i + 1u;
					} else {
						// Found our position in the list, insert the deleted range here
						InsertRange(i + 1u);
						ranges[i + static_cast<uint16_t>(1)].first = id;
						ranges[i + static_cast<uint16_t>(1)].last = end_id - 1u;
						return true;
					}
				}
			} else return false;
		}
	}

	bool MakeID::IsID(const uint16_t id) const
	{
		// Binary search of the range list
		uint16_t i0 = 0u;
		uint16_t i1 = static_cast<uint16_t>(ranges.size()) - 1u;

		for(;;) {
			const uint16_t i = (i0 + i1) / 2u;

			if(id < ranges[i].first) {
				if(i == i0)
					return true;

				// Cull upper half of list
				i1 = i - 1u;
			} else if(id > ranges[i].last) {
				if(i == i1)
					return true;

				// Cull bottom half of list
				i0 = i + 1u;
			} else {
				// Inside a free block, not a valid ID
				return false;
			}
		}
	}

	uint16_t MakeID::getAvailableIDs() const
	{
		uint16_t count = static_cast<uint16_t>(ranges.size());
		uint16_t i = 0;

		do {
			count += ranges[i].last - ranges[i].first;
			++i;
		} while(i < ranges.size());

		return count;
	}

	uint16_t MakeID::getLargestContinuousRange() const
	{
		uint16_t max_count = 0;
		uint16_t i = 0;

		do {
			uint16_t count = ranges[i].last - ranges[i].first + 1u;
			if(count > max_count)
				max_count = count;

			++i;
		} while(i < ranges.size());

		return max_count;
	}

	void MakeID::InsertRange(const uint16_t index)
	{
		ranges.insert(ranges.begin() + index, { 0, std::numeric_limits<uint16_t>::max() });

		//if(ranges.size() >= ranges.capacity) {
		//	ranges = static_cast<Range*>(m_Allocator.reallocate(ranges, sizeof(Range) * m_Capacity, (m_Capacity + m_Capacity) * sizeof(Range), 1));
		//	m_Capacity += m_Capacity;
		//}

		//::memmove(ranges.data() + index + 1, ranges.data() + index, (ranges.size() - index) * sizeof(Range));
		//++ranges.size();
	}

	void MakeID::DestroyRange(const uint16_t index)
	{
		ranges.erase(ranges.begin() + index);

		//--ranges.size();
		//::memmove(ranges.data() + index, ranges.data() + index + 1, (ranges.size() - index) * sizeof(Range));
	}
}