#include "Illuminar_pch.h"

#include "Illuminar_LoggingSystem.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"

#include <filesystem>

namespace Illuminar {
	std::shared_ptr<spdlog::logger> LoggingSystem::s_CoreLogger;
	std::shared_ptr<spdlog::logger> LoggingSystem::s_ClientLogger;

	void LoggingSystem::Init()
	{
		std::filesystem::create_directories("logs");

		std::vector<spdlog::sink_ptr> illuminarSinks =
		{
			std::make_shared<spdlog::sinks::stdout_color_sink_mt>(),
			std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/Illuminar.log", true)
		};

		std::vector<spdlog::sink_ptr> appSinks =
		{
			std::make_shared<spdlog::sinks::stdout_color_sink_mt>(),
			std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/APP.log", true)
		};

		illuminarSinks[0]->set_pattern("%^[%T] %n: %v%$");
		illuminarSinks[1]->set_pattern("[%T] [%l] %n: %v");
		appSinks[0]->set_pattern("%^[%T] %n: %v%$");
		appSinks[1]->set_pattern("[%T] [%l] %n: %v");

		s_CoreLogger = std::make_shared<spdlog::logger>("Illuminar", illuminarSinks.begin(), illuminarSinks.end());
		s_CoreLogger->set_level(spdlog::level::trace);

		s_ClientLogger = std::make_shared<spdlog::logger>("Application", appSinks.begin(), appSinks.end());
		s_ClientLogger->set_level(spdlog::level::trace);
	}

	void LoggingSystem::Shutdown()
	{
		s_ClientLogger.reset();
		s_CoreLogger.reset();
		spdlog::drop_all();
	}
}
