#pragma once

namespace Illuminar {
	struct MakeID final
	{
		MakeID(const uint16_t max_id = std::numeric_limits<uint16_t>::max());
		~MakeID();

		bool CreateID(uint16_t& id);
		bool CreateRangeID(uint16_t& id, const uint16_t count);

		bool DestroyID(const uint16_t id);
		bool DestroyRangeID(const uint16_t id, const uint16_t count);

		[[nodiscard]] bool IsID(const uint16_t id) const;

		[[nodiscard]] uint16_t getAvailableIDs() const;
		[[nodiscard]] uint16_t getLargestContinuousRange() const;

	private:
		struct Range
		{
			uint16_t first;
			uint16_t last;
		};

		std::vector<Range> ranges;

		MakeID& operator=(const MakeID&) = delete;
		MakeID(const MakeID&) = delete;

		void InsertRange(const uint16_t index);
		void DestroyRange(const uint16_t index);
	};
}