#pragma once

namespace Illuminar {
	struct UUID
	{
		UUID();
		UUID(uint64_t id);
		UUID(const UUID& other);

		operator uint64_t () { return id; }
		operator const uint64_t() const { return id; }

	private:
		uint64_t id;
	};
}