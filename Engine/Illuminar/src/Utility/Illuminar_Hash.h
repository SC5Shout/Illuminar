#pragma once

namespace Illuminar {
	/* FNV-1a implementation's from https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function */
	template<typename T>
	struct Hash
	{
		using ID = T;

		static_assert(is_int_v<ID>, "Only integers may be result of hash");
		static_assert(sizeof(ID) != 4 || sizeof(ID) != 8, "Only 32 or 64 bit integers can be hashed");

		static constexpr ID fnvOffsetBasis = is_64_bit_v<ID> ? 0xcbf29ce484222325 : 0x811C9DC5;
		static constexpr ID fnvPrime = is_64_bit_v<ID> ? 0x100000001B3 : 0x01000193;

		Hash() = default;

		Hash(ID& id, const char* string, const ID hash = fnvOffsetBasis)
		{
			id = CompileTimeFNV(string, hash);
			this->id = id;
		}

		constexpr Hash(const char* string, const ID hash = fnvOffsetBasis)
		{
			id = CompileTimeFNV(string, hash);
		}

		Hash(ID& id, const std::string& string, const ID hash = fnvOffsetBasis)
			: Hash(id, string.c_str(), hash)
		{
		}

		Hash(const std::string& string, const ID hash = fnvOffsetBasis)
			: Hash(string.c_str(), hash)
		{
		}

		constexpr Hash(ID id)
			: id(id)
		{
		}

		[[nodiscard]] operator ID() const
		{
			return id;
		}

		[[nodiscard]] static constexpr inline ID CompileTimeFNV(const char* string, const ID hash = fnvOffsetBasis) noexcept
		{
			return ('\0' == string[0]) ? hash : CompileTimeFNV(&string[1], (hash ^ static_cast<ID>(string[0])) * fnvPrime);
		}

		static constexpr inline ID GenerateFNVHash(const char* str)
		{
			const size_t length = strlen(str) + 1;
			uint32_t hash = fnvOffsetBasis;
			for (size_t i = 0; i < length; ++i) {
				hash ^= *str++;
				hash *= fnvPrime;
			}
			return hash;
		}

		ID id = 0;
	};

	using Hash32 = Hash<uint32_t>;
	using Hash64 = Hash<uint64_t>;
}