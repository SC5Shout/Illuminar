#include "Illuminar_pch.h"
#include "Illuminar_UUID.h"

namespace Illuminar {
	static std::random_device RANDOM_DEVICE;
	static std::mt19937_64 RANDOM_ENGINE(RANDOM_DEVICE());
	static std::uniform_int_distribution<uint64_t> UNIFORM_DISTRIBUTION;

	UUID::UUID()
		: id(UNIFORM_DISTRIBUTION(RANDOM_ENGINE))
	{
	}

	UUID::UUID(uint64_t id)
		: id(id)
	{
	}

	UUID::UUID(const UUID& other)
		: id(other.id)
	{
	}
}