#pragma once

#include <numeric>

namespace Illuminar {
	struct HandleBase
	{
		using HandleID = uint32_t;
		static constexpr const HandleID INVALID_HANDLE_ID = HandleID{ std::numeric_limits<HandleID>::max() };

		HandleBase() = default;

		HandleBase(HandleID ID)
			: object(ID) {}

		HandleBase(const HandleBase& other) = default;
		HandleBase(HandleBase&& other) noexcept
			: object(other.object)
		{
			other.object = INVALID_HANDLE_ID;
		}

		HandleBase& operator=(const HandleBase& other) = default;
		HandleBase& operator=(HandleBase&& other) noexcept
		{
			std::swap(object, other.object);
			return *this;
		}

		[[nodiscard]] inline operator bool() const { return object != INVALID_HANDLE_ID; }

		inline void clear() { object = INVALID_HANDLE_ID; }

		inline bool operator==(const HandleBase& other) const { return object == other.object; }
		inline bool operator!=(const HandleBase& other) const { return object != other.object; }

		[[nodiscard]] inline HandleID getID() const { return object; }
		[[nodiscard]] inline bool valid() const { return object != INVALID_HANDLE_ID; }

	protected:
		HandleID object = INVALID_HANDLE_ID;
	};

	template <typename T>
	struct Handle : HandleBase
	{
		using HandleBase::HandleBase;

		template<typename U>
		Handle(const Handle<U>& base) requires (std::is_base_of_v<T, U> || std::is_same_v<T, U>)
			: HandleBase(base) 
		{
		}
	};
}