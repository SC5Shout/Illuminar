#pragma once

#define SPDLOG_COMPILED_LIB

#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace Illuminar {
	struct LoggingSystem
	{
		static void Init();
		static void Shutdown();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};

	#define EngineLogTrace(...)	Illuminar::LoggingSystem::GetCoreLogger()->trace(__VA_ARGS__)
	#define EngineLogInfo(...)	Illuminar::LoggingSystem::GetCoreLogger()->info(__VA_ARGS__)
	#define EngineLogWarn(...)	Illuminar::LoggingSystem::GetCoreLogger()->warn(__VA_ARGS__)
	#define EngineLogError(...)	Illuminar::LoggingSystem::GetCoreLogger()->error(__VA_ARGS__)
	#define EngineLogFatal(...)	Illuminar::LoggingSystem::GetCoreLogger()->critical(__VA_ARGS__)
}

// Client Logging Macros
#define LogTrace(...)	Illuminar::LoggingSystem::GetClientLogger()->trace(__VA_ARGS__)
#define LogInfo(...)	Illuminar::LoggingSystem::GetClientLogger()->info(__VA_ARGS__)
#define LogWarn(...)	Illuminar::LoggingSystem::GetClientLogger()->warn(__VA_ARGS__)
#define LogError(...)	Illuminar::LoggingSystem::GetClientLogger()->error(__VA_ARGS__)
#define LogFatal(...)	Illuminar::LoggingSystem::GetClientLogger()->critical(__VA_ARGS__)