#pragma once

namespace Illuminar {
	template<typename T>
	struct ThreadSafeQueue
	{
		constexpr ThreadSafeQueue() = default;
	
		void push(std::convertible_to<T> auto&& func)
		{
			{
				std::scoped_lock lock(mutex);
				queue.push(std::forward<decltype(func)>(func));
			}
	
			cv.notify_one();
		}
	
		[[nodiscard]] T pop()
		{
			T item = pop_opt().value();
			return item;
		}
	
		[[nodiscard]] std::optional<T> try_pop()
		{
			auto item = pop_opt();
			return item;
		}
	
		[[nodiscard]] inline size_t size()
		{
			std::scoped_lock lock(mutex);
			return queue.size();
		}
	
		[[nodiscard]] inline bool empty()
		{
			std::scoped_lock lock(mutex);
			return queue.empty();
		}
	
		inline void RequestQuit()
		{
			quit = true;
			cv.notify_one();
		}
	
		[[nodiscard]] inline bool QuitRequested() const
		{
			return quit;
		}
	
	private:
		[[nodiscard]] std::optional<T> pop_opt()
		{
			std::unique_lock lock(mutex);
			while (queue.empty() && !quit) {
				cv.wait(lock);
			}
	
			if (queue.empty()) {
				return std::nullopt;
			}
	
			auto item = std::move(queue.front());
			queue.pop();
			lock.unlock();
	
			return item;
		}
	
		std::atomic_bool quit = false;
	
		std::condition_variable_any cv;
		TicketMutex mutex;
		std::queue<T> queue;
	};
}