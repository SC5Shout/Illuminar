#pragma once

namespace Illuminar {
	struct RefCount
	{
		void IncrementRefCount() const
		{
			refCount.fetch_add(1, std::memory_order_relaxed);
		}

		void DecrementRefCount() const
		{
			refCount.fetch_sub(1, std::memory_order_acq_rel);
		}

		uint32_t getRefCount() const { return refCount; }

	private:
		mutable std::atomic_uint32_t refCount = 0;
	};

	template<typename T>
	struct Ref
	{
		//static_assert(std::is_base_of_v<RefCount, T>, "Class is not RefCounted!");

		Ref() = default;

		Ref(std::nullptr_t)
			: data(nullptr)
		{
		}

		Ref(T* instance)
			: data(instance)
		{
			IncrementRef();
		}

		template<typename T2>
		Ref(const Ref<T2>& other)
		{
			data = (T*)other.data;
			IncrementRef();
		}

		template<typename T2>
		Ref(Ref<T2>&& other)
		{
			data = (T*)other.data;
			other.data = nullptr;
		}

		~Ref()
		{
			DecrementRef();
		}

		Ref(const Ref<T>& other)
			: data(other.data)
		{
			IncrementRef();
		}

		Ref& operator=(std::nullptr_t)
		{
			DecrementRef();
			data = nullptr;
			return *this;
		}

		Ref& operator=(const Ref<T>& other)
		{
			other.IncrementRef();
			DecrementRef();

			data = other.data;
			return *this;
		}

		template<typename T2>
		Ref& operator=(const Ref<T2>& other)
		{
			other.IncrementRef();
			DecrementRef();

			data = other.data;
			return *this;
		}

		template<typename T2>
		Ref& operator=(Ref<T2>&& other)
		{
			DecrementRef();

			data = other.data;
			other.data = nullptr;
			return *this;
		}

		operator bool() { return data != nullptr; }
		operator bool() const { return data != nullptr; }

		bool operator==(const Ref<T>& other) const
		{
			return data == other.data;
		}

		bool operator!=(const Ref<T>& other) const
		{
			return !(*this == other);
		}

		T* operator->() { return data; }
		const T* operator->() const { return data; }

		T& operator*() { return *data; }
		const T& operator*() const { return *data; }

		T* get() { return data; }
		const T* get() const { return data; }

		void Reset(T* instance = nullptr)
		{
			DecrementRef();
			data = instance;
		}

		template<typename T2>
		Ref<T2> as() const
		{
			return Ref<T2>(*this);
		}

		template<typename... Args>
		static Ref<T> Create(Args&&... args)
		{
			return Ref<T>(new T(std::forward<Args>(args)...));
		}

	private:
		void IncrementRef() const
		{
			if(data) {
				data->IncrementRefCount();
			}
		}

		void DecrementRef() const
		{
			if(data) {
				data->DecrementRefCount();
				if(data->getRefCount() == 0) {
					delete data;
				}
			}
		}

		template<typename T2>
		friend struct Ref;

		T* data = nullptr;
	};
}