#include "Illuminar_pch.h"
#include "OpenGL/Illuminar_GLTexture.h"

#include "Illuminar_Renderer.h"

namespace Illuminar::GFX {
	Ref<Texture2D> Texture2D::Create()
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTexture2D>::Create();
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<Texture2D> Texture2D::Create(const CreateInfo& createInfo)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTexture2D>::Create(createInfo);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<Texture2DArray> Texture2DArray::Create(const CreateInfo& createInfo)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTexture2DArray>::Create(createInfo);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		}
		return nullptr;
	}

	Ref<TextureCubemap> TextureCubemap::Create(const CreateInfo& createInfo)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTextureCubemap>::Create(createInfo);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}