#pragma once

#include "Illuminar_GraphicsPipelineState.h"
#include "Illuminar_Shader.h"
#include "Buffer/Illuminar_BufferLayout.h"

namespace Illuminar::GFX {
	struct GraphicsPipeline : Resource
	{
		struct CreateInfo
		{
			BufferLayout bufferLayout;
			Ref<Shader> shader = nullptr;

			RasterizerState rasterizerState = RasterizerState::getDefault();

			StencilState stencilState = StencilState::getDefault();
			BlendState blendState = BlendState::getDefault();
			DepthState depthState = DepthState::getDefault();
			DrawMode drawMode = DrawMode::Triangles;
		};

		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<GraphicsPipeline> Create(const CreateInfo& createInfo);

		GraphicsPipeline() = default;
		virtual ~GraphicsPipeline() = default;

		GraphicsPipeline(const GraphicsPipeline& other) = delete;
		GraphicsPipeline& operator =(const GraphicsPipeline& other) = delete;

		virtual void Bind() const = 0;
		virtual void Rebind(const CreateInfo& createInfo) = 0;

		virtual const CreateInfo& getInfo() const = 0;
		virtual CreateInfo& getInfoRef() = 0;

		template<typename T> requires std::is_base_of_v<GraphicsPipeline, T>
		[[nodiscard]] inline T* as() const
		{
			return (T*)this;
		}

		uint16_t getID() const
		{
			return id;
		}

		static MakeID makeID;
		uint16_t id = 0;
	};
}