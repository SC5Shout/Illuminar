#pragma once

#include "Memory/Illuminar_CircularBuffer.h"
#include "Thread/Illuminar_TicketMutex.h"

namespace Illuminar::GFX {
	struct CommandBase
	{
		typedef void(*ExecuteFn)(CommandBase* self, uintptr_t& next);
	
		inline CommandBase(ExecuteFn fn)
			: fn(fn)
		{
		}
	
		virtual ~CommandBase() = default;
	
		[[nodiscard]] inline CommandBase* Execute()
		{
			uintptr_t next = 0;
			fn(this, next);
			return reinterpret_cast<CommandBase*>(reinterpret_cast<uintptr_t>(this) + next);
		}
	
		ExecuteFn fn;
	};
	
	template<typename FuncT, typename ... Args>
	struct CustomCommand : CommandBase
	{
		inline CustomCommand(FuncT&& func, Args&& ... args)
			: CommandBase(Execute), func(std::forward<FuncT>(func)), args(std::forward<Args>(args)...)
		{
		}
	
		~CustomCommand() override = default;
	
		static inline void Execute(CommandBase* base, uintptr_t& next)
		{
			next = align(sizeof(CustomCommand));
			CustomCommand* b = static_cast<CustomCommand*>(base);
			std::apply(std::forward<FuncT>(b->func), std::move(b->args));
			b->~CustomCommand();
		}
	
		FuncT func;
		std::tuple<std::remove_reference_t<Args>...> args;
	};
	
	struct NoopCommand : CommandBase
	{
		inline explicit NoopCommand(void* next)
			: CommandBase(Execute), next(size_t((char*)next - (char*)this))
		{
		}
	
		~NoopCommand() override = default;
	
		static inline void Execute(CommandBase* self, uintptr_t& next)
		{
			next = static_cast<NoopCommand*>(self)->next;
		}
	
		uintptr_t next = 0;
	};
	
	struct CommandQueue
	{
		inline CommandQueue(bool threadDispatch = true)
			: buffer(BUFFER_SIZE), THREAD_DISPATCH(threadDispatch)
		{
		}
	
		~CommandQueue() = default;

		template<typename FuncT, typename ... Args>
		inline void Submit(FuncT&& funcT, Args&& ... args)
		{
			AllocateCommand<CustomCommand<FuncT, Args...>>(std::forward<FuncT>(funcT), std::forward<Args>(args)...);
		}
	
		template<typename Class, typename ... Args>
		inline void AllocateCommand(Args&& ... args)
		{
			auto buff = buffer.Allocate<Class>();
			buffer.Construct(buff, std::forward<Args>(args)...);
		}
	
		[[nodiscard]] std::vector<CircularBuffer::Range> WaitForCommands() const;
	
		void Flush();
		[[nodiscard]] bool Execute();
		void ReleaseRange(const CircularBuffer::Range& range);
		void Quit();
	
		mutable TicketMutex mutex;
		mutable std::condition_variable_any cv;
		mutable std::vector<CircularBuffer::Range> commandsToExecute;
	
		CircularBuffer buffer;
	
		static constexpr uint32_t REQUIRED_SIZE = (uint32_t)1_MB;
		static constexpr uint32_t BUFFER_SIZE = (uint32_t)3_MB;
		const bool THREAD_DISPATCH = true;
	
		size_t requiredSize = (REQUIRED_SIZE + CircularBuffer::BLOCK_MASK) & ~CircularBuffer::BLOCK_MASK;
		size_t freeSpace = BUFFER_SIZE;

		std::atomic_bool quit{ false };
	};
}
