#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"

#include "Illuminar_Camera.h"
#include "Illuminar_RenderQueue.h"

#include "GameEngine/Resource/Mesh/Illuminar_MeshResource.h"
#include "GameEngine/Scene/Illuminar_Environment.h"

#include "GraphicsEngine/Memory/Illuminar_GPULinearAllocator.h"

namespace Illuminar::GFX {
	struct ForwardPlusRenderer : RefCount
	{
		constexpr static Vector2<uint32_t> SCREEN_SIZE{ 1600, 900 };

		[[nodiscard]] static inline Ref<ForwardPlusRenderer> Create()
		{
			return Ref<ForwardPlusRenderer>::Create();
		}

		ForwardPlusRenderer();
		~ForwardPlusRenderer() = default;

		void Begin(const Camera& camera, const Mat4& view);
		void End();

		void AddSubmesh(const Ref<Mesh>& mesh, const Submesh& submesh, const Mat4& transform);

		void PreperUniformBuffers();

		//TODO: use render graph		
		struct DepthPrepass
		{
			DepthPrepass(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
			}
		
			void Init();
			void Draw();
		
			ForwardPlusRenderer* renderer;
		
			Ref<Material> material = nullptr;
			Ref<Texture> texture = nullptr;
			Ref<FrameBuffer> fbo = nullptr;
		
			RenderQueue queue;
		} depthPrepass{ this };
		
		struct ShadowPass
		{
			ShadowPass(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
			}
		
			void Init();
			void Draw();
		
			Mat4 getLightSpaceMatrix(const Vector3<float>& lightDir, float nearClip, float clipRange, float splitDist, float& lastSplitDist, uint32_t cascadeIndex);
			std::vector<Mat4> getLightSpaceMatrices(const Vector3<float>& lightDir);

			RenderQueue queue;

			Vector4<float> splitDepth = 0.0f;

			std::vector<Mat4> lightViewMatrices;
		
			static constexpr uint32_t CASCADE_COUNT = 4;
			static constexpr uint32_t SHADOW_SIZE = 4096;
			Ref<Material> material = nullptr;
			Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> texture = nullptr;

			ForwardPlusRenderer* renderer = nullptr;
		
			uint32_t lightViewOffset = 0;
		} shadowPass{ this };
		
		struct LightCulling
		{
			LightCulling(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
			}
		
			void Init();
			void Draw(const Ref<Texture>& depthTexture);
		
			Ref<Shader> shader = nullptr;
		
			ForwardPlusRenderer* renderer = nullptr;
		
			uint32_t workGroupsX = 0;
			uint32_t workGroupsY = 0;
		} lightCulling{ this };
		
		struct ColorPass
		{
			ColorPass(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
			}

			void Init();
			void Draw();

			RenderQueue queue;

			Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> colorMap = nullptr;
			Ref<Texture> depthMap = nullptr;

			ForwardPlusRenderer* renderer = nullptr;
		} colorPass{ this };

		struct CompositePass
		{
			CompositePass(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
			}

			void Init();
			void Draw();

			Ref<Material> material = nullptr;
			Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> colorMap = nullptr;

			ForwardPlusRenderer* renderer = nullptr;
		} compositePass{this};
		
		struct DebugPass
		{
			DebugPass(ForwardPlusRenderer* renderer)
				: renderer(renderer)
			{
				//Init();
			}

			constexpr virtual std::string_view getName() = 0;
			virtual Ref<Texture> getTexture() = 0;

			virtual void Init() = 0;
			virtual void Draw() = 0;

			ForwardPlusRenderer* renderer = nullptr;
		};

		struct PredepthDebug : DebugPass
		{
			PredepthDebug(ForwardPlusRenderer* renderer)
				: DebugPass(renderer)
			{
				Init();
			}
		
			void Init() override;
			void Draw() override;
		
			constexpr std::string_view getName() override { return NAME; }
			Ref<Texture> getTexture() override { return texture; }

			RenderQueue queue;

			Ref<Material> material = nullptr;	
			Ref<Texture> texture = nullptr;
			Ref<FrameBuffer> fbo = nullptr;

			static constexpr std::string_view NAME = "PredepthDebug";
		};
		
		struct ShadowDebugPass : DebugPass
		{
			ShadowDebugPass(ForwardPlusRenderer* renderer)
				: DebugPass(renderer)
			{
				Init();
			}
		
			void Init() override;
			void Draw() override;

			constexpr std::string_view getName() override { return NAME; }
			Ref<Texture> getTexture() override { return texture; }

			Ref<FrameBuffer>  fbo = nullptr;
			Ref<Texture> texture = nullptr;
			Ref<VertexBuffer> vbo = nullptr;
			Ref<Material> material = nullptr;

			static constexpr std::string_view NAME = "ShadowDebug";
		};
		
		struct LightCullingDebug : DebugPass
		{
			LightCullingDebug(ForwardPlusRenderer* renderer)
				: DebugPass(renderer)
			{
				Init();
			}
		
			void Init() override;
			void Draw() override;

			constexpr std::string_view getName() override { return NAME; }
			Ref<Texture> getTexture() override { return texture; }

			RenderQueue queue;
		
			Ref<Material> material = nullptr;
			Ref<Texture> texture = nullptr;
			Ref<FrameBuffer> fbo = nullptr;

			static constexpr std::string_view NAME = "LightCullingDebug";
		};

		std::vector<std::unique_ptr<DebugPass>> debugPasses;

		struct DirectionalLight
		{
			Vector3<float> direction;
			float intensity;
			Vector3<float> radiance;
			float lightSize;

			int nvidiaPcss;
			int softShadows;

			float padding[2];
		};

		struct PointLight
		{
			Vector3<float> position = 0.0f;
			float intensity = 0.0f;
			Vector3<float> radiance = 0.0f;
			float fallOff = 0.0f;

			float radius = 0.0f;
			float minRadius = 0.0f;

			float padding[2] = { 0.0f };
		};

		//todo: maybe we should treat spot lights as a point light with inner and outer cut off and cull them light together instead of having 2 different kinds?
		//I think that's a reasonable solution
		struct SpotLight
		{
			Vector3<float> position;
			float intensity;

			Vector3<float> radiance;
			float fallOff;

			Vector3<float> direction;
			float radius;
			float minRadius;

			float innerCutOff;
			float outerCutOff;

			float padding;
		};

		struct FrameData
		{
			Mat4 projection = Mat4(1.0f);
			Mat4 view = Mat4(1.0f);
		} frameData;

		struct CameraData
		{
			Mat4 viewProjection = Mat4(1.0f);
			Mat4 view = Mat4(1.0f);
		};

		struct SceneData
		{
			Vector4<float> viewPositionEnvironmentMapIntensity = 0;
			uint32_t tilesCountX = 0;

			uint32_t directionalLightCount = 0;
			uint32_t pointLightCount = 0;
			uint32_t spotLightCount = 0;
		};

		struct LightCamera
		{
			Mat4 view;
			Mat4 projection;

			Vector2<uint32_t> screenSize;
			uint32_t pointLightCount;

			float padding[1];
		};

		//empty dir light, only valid if direcitonalLights.size() == 0
		DirectionalLight nullDirLight;
		std::vector<DirectionalLight> directionalLights;
		Ref<UniformBuffer> directionalLightUbo = nullptr;
	
		static constexpr uint32_t MAX_LIGHTS = 1024;
		std::vector<PointLight> pointLights;
		Ref<BufferResource> pointLightsUbo = nullptr;

		Ref<ShaderStorageBuffer> visibleLightIndices = nullptr;

		static constexpr uint32_t MAX_TRANSFORM_OBJECTS = (uint32_t)1_MB;
		Ref<ShaderStorageBuffer> modelTransformBufferSsbo;
		std::vector<Mat4> modelTransformBuffer;

		uint32_t sceneDataOffset = 0;
		uint32_t cameraDataOffset = 0;
		uint32_t lightCameraOffset = 0;
		uint32_t directionalLightViewsOffset = 0;
		uint32_t cascadeLevelsOffset = 0;	

		GPULinearAllocator<BufferType::Uniform, 1_MB> dynamicUbo;

		std::shared_ptr<Environment> environment = nullptr;
		float intensity = 1.0f;

		std::vector<RenderObject> objects;

		Vector3<float> cameraPosition = 0.0f;
	};
}