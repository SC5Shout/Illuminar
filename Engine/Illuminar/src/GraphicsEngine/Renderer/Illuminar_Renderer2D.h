#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"
#include "Illuminar_Camera.h"

namespace Illuminar::GFX {
	struct Renderer2D
	{
		struct Vertex
		{
			Vector2<float> position = 0.0f;
			Vector2<float> uv = 0.0f;
			uint32_t color = 0xffffffff;
			float textureIndex = 0.0f;
		};

		typedef uint32_t DrawIndex;

		void Create();

		void Begin(const Camera& camera, const Mat4& transform);
		void End();

		void MoveTo(const Vector2<float>& position);
		void LineTo(const Vector2<float>& position);
		void ClosePath();

		void ClearPath();

		void Fill(uint32_t color);
		void Fill(Ref<Texture> texture, const Vector4<float>& uv);
		void Fill(Ref<Texture> texture, const Vector2<float> uv[4]);

		void Stroke(uint32_t color, float thickness = 1.0f);

		void Clear();

		void RectPattern(float x, float y, float w, float h);

		void FillRect(float x, float y, float w, float h, uint32_t color);
		void FillRect(float x, float y, float w, float h, const Ref<Texture>& texture, const Vector4<float>& uv);
		void FillRect(float x, float y, float w, float h, const Ref<Texture>& texture, const Vector2<float> uv[4]);

		void DrawRect(float x, float y, float w, float h, uint32_t color, float thickness = 1.0f);

	//private:
		Renderer2D() = default;
		Renderer2D(const Renderer2D& other) = delete;
		Renderer2D& operator=(const Renderer2D& other) = delete;

		struct Point
		{
			Vector2<float> position = 0.0f;
			Vector2<float> distance = 0.0f;
			Vector2<float> normal = 0.0f;
			Vector2<float> miterDistance = 0.0f;
			float length = 0.0f;
		}; std::vector<Point> points;

		void CalculateJoins();
		void CalculateStartOfStrokeWhenNotClosed(const Point& startPoint, uint32_t color, float lineWidth);
		void CalculateEndOfStrokeWhenNotClosed(const Point& endPoint, const Vector2<float>& pointNormalBeforeEndPoint, uint32_t color, float lineWidth);

		[[nodiscard]] bool PointEquals(const Vector2<float>& point1, const Vector2<float>& point2, float tolerance);
		[[nodiscard]] inline Point* getPoint() { return &points.back(); }

		[[nodiscard]] float FindTexture(Ref<Texture>&& texture);

		Ref<VertexBuffer> vbo = nullptr;
		Ref<ElementBuffer> ebo = nullptr;

		Ref<UniformBuffer> cameraUbo = nullptr;

		Ref<Material> material = nullptr;

		std::vector<Vertex> vertices;
		std::vector<DrawIndex> elements;

		std::vector<Ref<Texture>> textures;

		uint32_t currentVertexIndex = 0;

		bool closedPath = false;
	};
}