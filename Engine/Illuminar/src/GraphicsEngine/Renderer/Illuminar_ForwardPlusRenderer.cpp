#include "Illuminar_pch.h"
#include "Illuminar_ForwardPlusRenderer.h"

#include "GraphicsEngine/Illuminar_TextureCache.h"

namespace Illuminar::GFX {
	struct Skybox
	{
		Skybox()
		{
			static constexpr float skyboxVertices[] = {
				-1.0f,  1.0f, 0.1f,
				-1.0f, -1.0f, 0.1f,
				 1.0f, -1.0f, 0.1f,
				 1.0f,  1.0f, 0.1f,
			};
			vbo = VertexBuffer::Create({ { DataType::RGB32_FLOAT, "a_position" } }, sizeof(skyboxVertices), skyboxVertices, Usage::Static_Draw);

			GFX::Material::CreateInfo materialInfo;
			materialInfo.shader = ShaderCache::getShader("Skybox");
			materialInfo.name = "Skybox";
			materialInfo.materialDomain = GFX::MaterialDomain::Surface;
			materialInfo.shadingModel = GFX::ShadingModel::Lit;
			materialInfo.cullMode = GFX::CullMode::Back;
			materialInfo.blendMode = GFX::BlendMode::Opaque;
			materialInfo.drawMode = GFX::DrawMode::TriangleFan;
			material = MaterialCache::Create(materialInfo);
		}
	
		void Draw(const Ref<TextureCubemap>& cubemap, ForwardPlusRenderer& renderer)
		{
			material->set("u_parameters.textureLod", 1.0f);
			material->set("u_parameters.intensity", renderer.intensity);
			material->set("u_skybox", cubemap);

			//this bind is not needed since draw is executed after color pass which binds camera data.
			//renderer.dynamicUbo->BindRange(BufferType::Uniform, 0, renderer.cameraDataOffset, sizeof(ForwardPlusRenderer::CameraData));

			material->depthState.func = GFX::DepthTest::LEqual;
			material->depthState.mask = GFX::DepthMask::All;
			RenderCommand::DrawArrays(vbo, material, 4, 1, 0, 0);
		}
	
		Ref<VertexBuffer> vbo = nullptr;
		Ref<Material> material = nullptr;
	};

	ForwardPlusRenderer::ForwardPlusRenderer()
	{
		depthPrepass.Init();
		shadowPass.Init();
		lightCulling.Init();
		colorPass.Init();
		compositePass.Init();
		
		//debugPasses.push_back(std::make_unique<PredepthDebug>(this));
		//debugPasses.push_back(std::make_unique<ShadowDebugPass>(this));
		//debugPasses.push_back(std::make_unique<LightCullingDebug>(this));
	}

	void ForwardPlusRenderer::Begin(const Camera& camera, const Mat4& view)
	{
		frameData.projection = camera.projection;
		frameData.view = view;

		cameraPosition = glm::inverse(frameData.view)[3];
	}

	void ForwardPlusRenderer::End()
	{
		PreperUniformBuffers();

		depthPrepass.Draw();
		lightCulling.Draw(depthPrepass.texture);
		shadowPass.Draw();
		colorPass.Draw();
		compositePass.Draw();

		for (auto& debug : debugPasses) {
			debug->Draw();
		}
		
		shadowPass.lightViewMatrices.clear();

		modelTransformBuffer.clear();
		pointLights.clear();
		directionalLights.clear();

		dynamicUbo.clear();

		objects.clear();
	}

	void ForwardPlusRenderer::AddSubmesh(const Ref<Mesh>& mesh, const Submesh& submesh, const Mat4& transform)
	{
		modelTransformBuffer.push_back(transform);

		RenderObject& renderObject = objects.emplace_back();
		renderObject.vbo = mesh->vbo;
		renderObject.ebo = mesh->ebo;
		renderObject.material = submesh.material;
		renderObject.elementsCount = submesh.indexCount;
		renderObject.firstIndex = submesh.firstIndex;
		renderObject.baseVertex = submesh.baseVertex;

		Vector3<float> pos, scale, rot;
		[[maybe_unused]] bool v = DecomposeTransform(transform, pos, rot, scale);
		renderObject.distanceFromCamera = glm::distance(cameraPosition, pos);
	}

	void ForwardPlusRenderer::PreperUniformBuffers()
	{
		SceneData sceneData;
		sceneData.viewPositionEnvironmentMapIntensity = Vector4<float>(cameraPosition.x, cameraPosition.y, cameraPosition.z, intensity);
		sceneData.tilesCountX = lightCulling.workGroupsX;
		sceneData.directionalLightCount = (uint32_t)directionalLights.size();
		sceneData.pointLightCount = (uint32_t)pointLights.size();
		sceneData.spotLightCount = 0;// spotLights.size();
		sceneDataOffset = dynamicUbo.allocate(sceneData);

		auto viewProjection = frameData.projection * frameData.view;
		CameraData cameraData;
		cameraData.viewProjection = viewProjection;
		cameraData.view = frameData.view;
		cameraDataOffset = dynamicUbo.allocate(cameraData);

		LightCamera lightCamera;
		lightCamera.view = frameData.view;
		lightCamera.projection = frameData.projection;
		lightCamera.pointLightCount = (uint32_t)pointLights.size();
		lightCamera.screenSize = SCREEN_SIZE;
		lightCameraOffset = dynamicUbo.allocate(lightCamera);

		const auto& light = directionalLights.empty() ? nullDirLight : directionalLights[0];// cpuDirectionalLight;
		shadowPass.lightViewMatrices = shadowPass.getLightSpaceMatrices(light.direction);
		directionalLightViewsOffset = dynamicUbo.allocate(shadowPass.lightViewMatrices);

		static constexpr float cameraFarPlane = 1000.0f;
		static constexpr std::array<float, 4> shadowCascadeLevels{
			cameraFarPlane / 50.0f,
			cameraFarPlane / 25.0f,
			cameraFarPlane / 10.0f,
			cameraFarPlane / 2.0f };
		Vector4<float> cascadeLevels{ shadowCascadeLevels[0], shadowCascadeLevels[1], shadowCascadeLevels[2], shadowCascadeLevels[3] };	
		cascadeLevelsOffset = dynamicUbo.allocate(cascadeLevels);

		if (modelTransformBufferSsbo == nullptr) {
			modelTransformBufferSsbo = ShaderStorageBuffer::Create(sizeof(Mat4) * MAX_TRANSFORM_OBJECTS, nullptr, 1, MapBit::Dynamic_Storage | MapBit::Write);
		}
		modelTransformBufferSsbo->AddData(sizeof(Mat4) * modelTransformBuffer.size(), modelTransformBuffer.data());

		if (pointLightsUbo == nullptr) {
			pointLightsUbo = BufferResource::Create(sizeof(PointLight) * MAX_LIGHTS, nullptr, Usage::Dynamic_Draw);
		}
		pointLightsUbo->AddData(sizeof(PointLight) * pointLights.size(), pointLights.data());

		if (directionalLightUbo == nullptr) {
			directionalLightUbo = UniformBuffer::Create(sizeof(DirectionalLight) * directionalLights.size(), nullptr, 4, MapBit::Dynamic_Storage | MapBit::Write);
		}
		directionalLightUbo->AddData(light);
	}

	void ForwardPlusRenderer::DepthPrepass::Init()
	{
		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = ShaderCache::getShader("PreDepth");
		materialInfo.name = "PreDepth";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.cullMode = GFX::CullMode::Back;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		material = MaterialCache::Create(materialInfo);
		
		material->depthState.func = GFX::DepthTest::LEqual;
		material->depthState.mask = GFX::DepthMask::All;

		Texture2D::CreateInfo depthTextureInfo;
		depthTextureInfo.name = "depthPrepass";
		depthTextureInfo.w = SCREEN_SIZE.x;
		depthTextureInfo.h = SCREEN_SIZE.y;
		depthTextureInfo.shadow = true;
		depthTextureInfo.parameters = TextureParameters().
			Format(TextureFormat::D32_FLOAT).
			Miplevel(1).
			FilterMIN(TextureFilter::Nearest).
			FilterMAG(TextureFilter::Nearest);
		texture = TextureCache::Create(depthTextureInfo);
	
		FrameBuffer::CreateInfo depthFboInfo;
		depthFboInfo.colorAttachmentCount = 0;
		depthFboInfo.depthStencilTexture = texture;
		fbo = FrameBuffer::Create(depthFboInfo);
	}
	
	void ForwardPlusRenderer::DepthPrepass::Draw()
	{
		renderer->dynamicUbo.Bind(0, renderer->cameraDataOffset, sizeof(CameraData));
		renderer->modelTransformBufferSsbo->Bind();
	
		FrameBuffer::State params;
		params.viewport = { 0, 0, SCREEN_SIZE.x, SCREEN_SIZE.y };
		fbo->Bind(params);
	
		auto copiedObjects = renderer->objects;
	
		for (auto& object : copiedObjects) {
			object.material = material;
		}
	
		queue.AddRenderObjects(std::move(copiedObjects));
		queue.Sort();
		queue.Draw();
	
		fbo->Unbind();
	}
	
	void ForwardPlusRenderer::PredepthDebug::Init()
	{
		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = ShaderCache::getShader("DepthDebug");
		materialInfo.name = "DepthDebug";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.cullMode = GFX::CullMode::Back;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		material = MaterialCache::Create(materialInfo);

		material->depthState.func = GFX::DepthTest::Less;
		material->depthState.mask = GFX::DepthMask::All;

		Texture2DArray::CreateInfo textureInfo;
		textureInfo.name = "Predepth";
		textureInfo.w = SCREEN_SIZE.x;
		textureInfo.h = SCREEN_SIZE.y;
		texture = TextureCache::Create(textureInfo);
	
		FrameBuffer::CreateInfo fboInfo;
		fboInfo.colorAttachmentCount = 1;
		fboInfo.colorAttachments[0] = texture;
		fbo = FrameBuffer::Create(fboInfo);
	}
	
	void ForwardPlusRenderer::PredepthDebug::Draw()
	{
		renderer->dynamicUbo.Bind(0, renderer->cameraDataOffset, sizeof(CameraData));
		renderer->modelTransformBufferSsbo->Bind();
	
		auto copiedObjects = renderer->objects;
	
		for (auto& object : copiedObjects) {
			object.material = material;
		}
	
		FrameBuffer::State params;
		params.viewport = { 0, 0, SCREEN_SIZE.x, SCREEN_SIZE.y };
		params.clearColor = 0xff191919;
		fbo->Bind(params);
	
		queue.AddRenderObjects(std::move(copiedObjects));
		queue.Sort();
		queue.Draw();
	
		fbo->Unbind();
	}
	
	void ForwardPlusRenderer::ShadowPass::Init()
	{
		Material::CreateInfo materialInfo;
		materialInfo.shader = ShaderCache::getShader("ShadowMap");
		materialInfo.name = "ShadowMap";
		materialInfo.materialDomain = MaterialDomain::Surface;
		materialInfo.shadingModel = ShadingModel::Lit;
		materialInfo.cullMode = CullMode::Back;
		materialInfo.blendMode = BlendMode::Opaque;
		materialInfo.cullMode = CullMode::Front;
		material = MaterialCache::Create(materialInfo);

		material->depthState.func = DepthTest::Less;
		material->depthState.mask = DepthMask::All;

		Texture2DArray::CreateInfo depthMapInfo;
		depthMapInfo.name = "Shadow";
		depthMapInfo.w = SHADOW_SIZE;
		depthMapInfo.h = SHADOW_SIZE;
		depthMapInfo.depth = 1;
		depthMapInfo.parameters = TextureParameters().
			Format(TextureFormat::D32_FLOAT).
			Miplevel(1).
			FilterMIN(TextureFilter::Nearest).
			FilterMAG(TextureFilter::Nearest).
			Wrap(TextureWrap::Clamp_To_Border);
		depthMapInfo.shadow = true;
		texture = TextureCache::Create(depthMapInfo);
	
		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 0;
		frameBufferInfo.depthStencilTexture = texture;
		fbo = FrameBuffer::Create(frameBufferInfo);
	}

	std::array<Vector3<float>, 8> getFrustumCornersWorldSpace(const Mat4& viewProj)
	{
		std::array<Vector3<float>, 8> frustumCorners = {
			Vector3<float>(-1.0f,  1.0f, -1.0f),
			Vector3<float>(1.0f,  1.0f, -1.0f),
			Vector3<float>(1.0f, -1.0f, -1.0f),
			Vector3<float>(-1.0f, -1.0f, -1.0f),
			Vector3<float>(-1.0f,  1.0f,  1.0f),
			Vector3<float>(1.0f,  1.0f,  1.0f),
			Vector3<float>(1.0f, -1.0f,  1.0f),
			Vector3<float>(-1.0f, -1.0f,  1.0f),
		};

		const Mat4 invCamWorld = glm::inverse(viewProj);
		for (uint32_t i = 0; i < 8; i++) {
			const Vector4<float> invCorner = invCamWorld * Vector4<float>(frustumCorners[i], 1.0f);
			frustumCorners[i] = invCorner / invCorner.w;
		}

		return frustumCorners;
	}

	std::array<Vector3<float>, 8> getFrustumCornersWorldSpace(const Mat4& proj, const Mat4& view)
	{
		return getFrustumCornersWorldSpace(proj * view);
	}

	Mat4 ForwardPlusRenderer::ShadowPass::getLightSpaceMatrix(const Vector3<float>& lightDir, float nearClip, float clipRange, float splitDist, float& lastSplitDist, uint32_t cascadeIndex)
	{
		auto corners = getFrustumCornersWorldSpace(renderer->frameData.projection, renderer->frameData.view);

		for (uint32_t i = 0; i < 4; i++) {
			Vector3<float> dist = corners[i + 4] - corners[i];
			corners[i + 4] = corners[i] + (dist * splitDist);
			corners[i] = corners[i] + (dist * lastSplitDist);
		}

		Vector3<float> center{ 0.0f };
		for (const auto& v : corners) {
			center += v;
		}
		center /= corners.size();

		float radius = 0.0f;
		for (uint32_t i = 0; i < corners.size(); i++) {
			float distance = glm::length(corners[i] - center);
			radius = glm::max(radius, distance);
		}
		radius = std::ceil(radius * 16.0f) / 16.0f;

		Vector3<float> maxExtents = radius;
		Vector3<float> minExtents = -maxExtents;

		float CascadeFarPlaneOffset = 50.0f, CascadeNearPlaneOffset = -50.0f;
		const auto lightView = glm::lookAt(center + lightDir * -minExtents.z, center, Vector3<float>(0.0f, 1.0f, 0.0f));

		Mat4 lightProjection = glm::ortho(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f + CascadeNearPlaneOffset, maxExtents.z - minExtents.z + CascadeFarPlaneOffset);

		// Offset to texel space to avoid shimmering (from https://stackoverflow.com/questions/33499053/cascaded-shadow-map-shimmering)
		Mat4 shadowMatrix = lightProjection * lightView;
		Vector4<float> shadowOrigin = (shadowMatrix * Vector4<float>(0.0f, 0.0f, 0.0f, 1.0f)) * (float)SHADOW_SIZE / 2.0f;
		Vector4<float> roundedOrigin = glm::round(shadowOrigin);
		Vector4<float> roundOffset = roundedOrigin - shadowOrigin;
		roundOffset = roundOffset * 2.0f / (float)SHADOW_SIZE;
		roundOffset.z = 0.0f;
		roundOffset.w = 0.0f;

		lightProjection[3] += roundOffset;

		// Store split distance and matrix in cascade
		//cascades[i].SplitDepth = (nearClip + splitDist * clipRange) * -1.0f;
		//cascades[i].ViewProj = lightProjection * lightView;
		//cascades[i].View = lightView;

		splitDepth[cascadeIndex] = (nearClip + splitDist * clipRange) * -1.0f;

		lastSplitDist = splitDist;

		return lightProjection * lightView;
	}

	std::vector<Mat4> ForwardPlusRenderer::ShadowPass::getLightSpaceMatrices(const Vector3<float>& lightDir)
	{
		const int SHADOW_MAP_CASCADE_COUNT = 4;
		float cascadeSplits[SHADOW_MAP_CASCADE_COUNT];

		float nearClip = 0.1f;
		float farClip = 1000.0f;
		float clipRange = farClip - nearClip;

		float minZ = nearClip;
		float maxZ = nearClip + clipRange;

		float range = maxZ - minZ;
		float ratio = maxZ / minZ;

		float CascadeSplitLambda = 0.92f;

		for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++) {
			float p = (i + 1) / static_cast<float>(SHADOW_MAP_CASCADE_COUNT);
			float log = minZ * std::pow(ratio, p);
			float uniform = minZ + range * p;
			float d = CascadeSplitLambda * (log - uniform) + uniform;
			cascadeSplits[i] = (d - nearClip) / clipRange;
		}

		cascadeSplits[3] = 0.3f;

		float lastSplitDist = 0.0;

		std::vector<Mat4> ret;
		for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++) {
			ret.push_back(getLightSpaceMatrix(lightDir, nearClip, clipRange, cascadeSplits[i], lastSplitDist, i));
		}

		return ret;
	}
	
	void ForwardPlusRenderer::ShadowPass::Draw()
	{
		renderer->dynamicUbo.Bind(2, renderer->directionalLightViewsOffset, 4 * sizeof(Mat4));
		renderer->modelTransformBufferSsbo->Bind();
	
		FrameBuffer::State params;
		params.viewport.width = SHADOW_SIZE;
		params.viewport.height = SHADOW_SIZE;
		fbo->Bind(params);

		auto copiedObjects = renderer->objects;
		for (auto& object : copiedObjects) {
			object.material = material;
		}

		queue.AddRenderObjects(std::move(copiedObjects));
		queue.Sort();
		queue.Draw();
	
		fbo->Unbind();
	}
	
	void ForwardPlusRenderer::ShadowDebugPass::Init()
	{
		Texture2D::CreateInfo mapInfo;
		mapInfo.name = "shadowDebug";
		mapInfo.w = renderer->shadowPass.SHADOW_SIZE;
		mapInfo.h = renderer->shadowPass.SHADOW_SIZE;
		mapInfo.parameters = TextureParameters().Miplevel(1);
		texture = TextureCache::Create(mapInfo);
	
		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 1;
		frameBufferInfo.colorAttachments[0] = texture;
		fbo = FrameBuffer::Create(frameBufferInfo);
	
		static constexpr float vertices[] = {
			-1.0f,  1.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f,
			 1.0f, -1.0f, 1.0f, 0.0f,
			 1.0f,  1.0f, 1.0f, 1.0f
		};
		vbo = VertexBuffer::Create({
			{ DataType::RG32_FLOAT, "a_position" },
			{ DataType::RG32_FLOAT, "a_uv" },
		}, sizeof(vertices), vertices, Usage::Static_Draw);

		auto shader = ShaderCache::getShader("test");

		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = shader;
		materialInfo.name = "test";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.cullMode = GFX::CullMode::Back;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		materialInfo.drawMode = GFX::DrawMode::TriangleFan;
		materialInfo.scissorEnabled = false;
		material = MaterialCache::Create(materialInfo);

		material->depthState.func = GFX::DepthTest::LEqual;
		material->depthState.mask = GFX::DepthMask::All;
	}
	
	void ForwardPlusRenderer::ShadowDebugPass::Draw()
	{
		material->set("u_texture", renderer->shadowPass.texture);
		material->set("u_options.drawDepth", 1.0f);

		FrameBuffer::State params;
		params.viewport.width = renderer->shadowPass.SHADOW_SIZE;
		params.viewport.height = renderer->shadowPass.SHADOW_SIZE;
		fbo->Bind(params);
	
		RenderCommand::DrawArrays(vbo, material, 4, 1, 0, 0);

		fbo->Unbind();
	}
	
	void ForwardPlusRenderer::LightCulling::Init()
	{
		workGroupsX = (SCREEN_SIZE.x + (SCREEN_SIZE.x % 16)) / 16;
		workGroupsY = (SCREEN_SIZE.y + (SCREEN_SIZE.y % 16)) / 16;
		size_t numberOfTiles = workGroupsX * workGroupsY;
	
		shader = ShaderCache::getShader("LightCulling");
	
		renderer->visibleLightIndices = ShaderStorageBuffer::Create(numberOfTiles * sizeof(int) * 1024, nullptr, 14, Usage::Static_Copy);
	}
	
	void ForwardPlusRenderer::LightCulling::Draw(const Ref<Texture>& depthTexture)
	{
		shader->Bind();
	
		renderer->dynamicUbo.Bind(0, renderer->lightCameraOffset, sizeof(LightCamera));
		renderer->pointLightsUbo->Bind(BufferType::ShaderStorage, 1);
		renderer->visibleLightIndices->Bind();
	
		depthTexture->Bind();
	
		RenderCommand::DispatchCompute(workGroupsX, workGroupsY, 1);
		RenderCommand::MemoryBarrier(MemoryBarrierType::ShaderStorage);
	}
	
	void ForwardPlusRenderer::LightCullingDebug::Init()
	{
		auto shader = ShaderCache::getShader("LightDebug");

		Material::CreateInfo materialInfo;
		materialInfo.shader = shader;
		materialInfo.name = "LightDebug";
		materialInfo.materialDomain = MaterialDomain::Surface;
		materialInfo.shadingModel = ShadingModel::Lit;
		materialInfo.blendMode = BlendMode::Opaque;
		material = MaterialCache::Create(materialInfo);

		material->depthState.func = GFX::DepthTest::Less;
		material->depthState.mask = GFX::DepthMask::All;

		Texture2D::CreateInfo textureInfo;
		textureInfo.name = "LightCulling";
		textureInfo.w = SCREEN_SIZE.x;
		textureInfo.h = SCREEN_SIZE.y;
		texture = TextureCache::Create(textureInfo);
	
		FrameBuffer::CreateInfo fboInfo;
		fboInfo.colorAttachmentCount = 1;
		fboInfo.colorAttachments[0] = texture;
		fbo = FrameBuffer::Create(fboInfo);
	}
	
	void ForwardPlusRenderer::LightCullingDebug::Draw()
	{
		renderer->dynamicUbo.Bind(0, renderer->cameraDataOffset, sizeof(CameraData));
		renderer->modelTransformBufferSsbo->Bind();
		renderer->visibleLightIndices->Bind();
	
		material->set("u_info.numberOfTilesX", renderer->lightCulling.workGroupsX);
		material->set("u_info.totalLightCount", renderer->pointLights.size());
		material->Bind();
	
		auto copiedObjects = renderer->objects;
	
		for (auto& object : copiedObjects) {
			object.material = material;
		}
	
		FrameBuffer::State params;
		params.viewport.width = SCREEN_SIZE.x;
		params.viewport.height = SCREEN_SIZE.y;
		params.clearColor = 0xff191919;
		fbo->Bind(params);
	
		queue.AddRenderObjects(std::move(copiedObjects));
		queue.Sort();
		queue.Draw();
	
		fbo->Unbind();
	}

	void ForwardPlusRenderer::ColorPass::Init()
	{
		Texture2D::CreateInfo colorMapInfo;
		colorMapInfo.name = "ColorPass - color";
		colorMapInfo.w = SCREEN_SIZE.x;
		colorMapInfo.h = SCREEN_SIZE.y;
		colorMapInfo.parameters = TextureParameters().
			Format(TextureFormat::RGBA32_FLOAT);
		colorMap = TextureCache::Create(colorMapInfo);

		Texture2D::CreateInfo depthMapInfo;
		depthMapInfo.name = "ColorPass - depth";
		depthMapInfo.w = SCREEN_SIZE.x;
		depthMapInfo.h = SCREEN_SIZE.y;
		depthMapInfo.parameters = TextureParameters().
			Format(TextureFormat::D32_FLOAT).
			FilterMIN(TextureFilter::Nearest).
			FilterMAG(TextureFilter::Nearest).
			Wrap(TextureWrap::Repeat);
		depthMap = TextureCache::Create(depthMapInfo);

		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 1;
		frameBufferInfo.colorAttachments[0] = colorMap;
		frameBufferInfo.depthStencilTexture = depthMap;
		fbo = FrameBuffer::Create(frameBufferInfo);
	}

	void ForwardPlusRenderer::ColorPass::Draw()
	{
		renderer->shadowPass.texture->Bind(2);

		renderer->modelTransformBufferSsbo->Bind();

		renderer->dynamicUbo.Bind(3, renderer->sceneDataOffset, sizeof(SceneData));
		renderer->dynamicUbo.Bind(0, renderer->cameraDataOffset, sizeof(CameraData));
		renderer->dynamicUbo.Bind(2, renderer->directionalLightViewsOffset, 4 * sizeof(Mat4));
		renderer->dynamicUbo.Bind(10, renderer->cascadeLevelsOffset, sizeof(Vector4<float>));

		renderer->directionalLightUbo->Bind();
		renderer->pointLightsUbo->Bind(BufferType::ShaderStorage, 5);
		
		renderer->visibleLightIndices->Bind();

		const auto& environment = renderer->environment;
		if (environment) {
			environment->radianceMap->Bind(0);
			environment->irradianceMap->Bind(1);
		}

		auto copiedObjects = renderer->objects;

		FrameBuffer::State params;
		params.viewport.width = SCREEN_SIZE.x;
		params.viewport.height = SCREEN_SIZE.y;
		fbo->Bind(params);

		queue.AddRenderObjects(std::move(copiedObjects));
		queue.Sort();
		queue.Draw();

		if (environment) {
			static auto skybox = Skybox();
			skybox.Draw(environment->radianceMap, *renderer);
		}

		fbo->Unbind();
	}

	void ForwardPlusRenderer::CompositePass::Init()
	{
		Texture2D::CreateInfo colorMapInfo;
		colorMapInfo.name = "ColorPass - color";
		colorMapInfo.w = SCREEN_SIZE.x;
		colorMapInfo.h = SCREEN_SIZE.y;
		colorMapInfo.parameters = TextureParameters().Miplevel(1);
		colorMap = TextureCache::Create(colorMapInfo);

		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 1;
		frameBufferInfo.colorAttachments[0] = colorMap;
		fbo = FrameBuffer::Create(frameBufferInfo);

		auto shader = ShaderCache::getShader("SceneComposite");

		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = shader;
		materialInfo.name = "SceneComposite";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		materialInfo.cullMode = CullMode::Back;
		materialInfo.drawMode = DrawMode::TriangleFan;

		material = MaterialCache::Create(materialInfo);
		material->depthState.func = DepthTest::Always;
	}

	void ForwardPlusRenderer::CompositePass::Draw()
	{
		material->set("u_texture", renderer->colorPass.colorMap);
		material->set("u_params.exposure", 0.8f);

		FrameBuffer::State params;
		params.viewport.width = SCREEN_SIZE.x;
		params.viewport.height = SCREEN_SIZE.y;
		fbo->Bind(params);
	
		RenderCommand::DrawFullScreenQuad(ShaderCache::getShader("SceneComposite"), material);

		fbo->Unbind();
	}
}