#include "Illuminar_pch.h"
#include "Illuminar_Renderer2D.h"

#include "glad/glad.h"

namespace Illuminar::GFX {
	void Renderer2D::Create()
	{
		constexpr int vtxCount = 40000;
		constexpr int elemCount = (vtxCount - 2) * 3;

		points.reserve(vtxCount);

		static const BufferLayout layout = {
			{ DataType::RG32_FLOAT, "a_position" },
			{ DataType::RG32_FLOAT, "a_uv" },
			{ DataType::RGBA8_UNORM, "a_color" },
			{ DataType::R32_FLOAT, "a_textureIndex" }
		};

		vbo = VertexBuffer::Create(layout, vtxCount * sizeof(Vertex), nullptr, MapBit::Write | MapBit::Dynamic_Storage);
		ebo = ElementBuffer::Create(elemCount * sizeof(DrawIndex), nullptr, MapBit::Write | MapBit::Dynamic_Storage);

		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = ShaderCache::getShader("2d");
		materialInfo.name = "Skybox";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.cullMode = GFX::CullMode::None;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		material = MaterialCache::Create(materialInfo);

		material->depthState.func = GFX::DepthTest::LEqual;
		material->depthState.mask = GFX::DepthMask::All;

		cameraUbo = UniformBuffer::Create(sizeof(Mat4), nullptr, 0, MapBit::Write | MapBit::Dynamic_Storage);
	}

	void Renderer2D::Begin(const Camera& camera, const Mat4& transform)
	{
		Clear();

		cameraUbo->AddData(sizeof(camera.projection), glm::value_ptr(camera.projection * glm::inverse(transform)));
		//material->shader->uniformBuffers[Hash32("Camera")].ubo->AddData(sizeof(camera.projection), glm::value_ptr(camera.projection * glm::inverse(transform)));
		//material->setUniformBufferStruct("Camera", camera.projection * glm::inverse(transform));
	}

	void Renderer2D::End()
	{
		vbo->AddData(sizeof(Vertex) * (uint32_t)vertices.size(), vertices.data());
		ebo->AddData(sizeof(DrawIndex) * (uint32_t)elements.size(), elements.data());
		cameraUbo->Bind();

		if(!textures.empty()) {
			for(size_t i = 0; i < textures.size(); ++i) {
				auto& texture = textures[i];
				texture->Bind((uint32_t)i);
			}
		}

		RenderCommand::DrawElements(vbo, ebo, material, (uint32_t)elements.size(), ElementDataType::Uint32, 1, 0, 0, 0);
	}

	void Renderer2D::MoveTo(const Vector2<float>& position)
	{
		LineTo(position);
	}

	void Renderer2D::LineTo(const Vector2<float>& position)
	{
		Point point;
		if(points.size() > 0) {
			point = *getPoint();
			if(PointEquals(point.position, position, 0.01f / (16.0f / 9.0f))) {
				return;
			}
		}
		point.position = position;

		points.push_back(std::move(point));
	}

	void Renderer2D::ClosePath()
	{
		closedPath = true;
	}

	void Renderer2D::ClearPath()
	{
		points.resize(0);
	}

	void Renderer2D::Fill(uint32_t color)
	{
		const size_t vertexCount = points.size();
		const size_t elementCount = (points.size() - 2) * 3;

		if(vertexCount < 2) {
			return;
		}

		vertices.reserve(vertexCount);
		elements.reserve(elementCount);

		for(uint32_t i = 0; i < (uint32_t)vertexCount; i++) {
			auto& vertex = vertices.emplace_back();
			vertex.position = points[i].position;
			vertex.color = color;
		}
		for(uint32_t i = 2; i < (uint32_t)vertexCount; i++) {
			elements.push_back(currentVertexIndex);
			elements.push_back(currentVertexIndex + i - 1);
			elements.push_back(currentVertexIndex + i);
		}
		currentVertexIndex += (uint32_t)vertexCount;

		ClearPath();
	}

	void Renderer2D::Fill(Ref<Texture> texture, const Vector4<float>& uv)
	{
		if(!texture) {
			ClearPath();
			return;
		}

		const size_t vertexCount = points.size();
		const size_t elementCount = (points.size() - 2) * 3;

		if(vertexCount < 2) {
			return;
		}

		Vector2<float> uvs[4] = {
			{uv.x, uv.y},
			{uv.x, uv.y + uv.w},
			{uv.x + uv.z, uv.y + uv.w},
			{uv.x + uv.z, uv.y}
		};

		float textureIndex = FindTexture(std::move(texture));

		vertices.reserve(vertexCount);
		elements.reserve(elementCount);

		for(uint32_t i = 0; i < (uint32_t)vertexCount; i++) {
			auto& vertex = vertices.emplace_back();
			vertex.position = points[i].position;
			vertex.uv = uvs[i];
			vertex.color = 0xffffffff;
			vertex.textureIndex = textureIndex;
		}
		for(uint32_t i = 2; i < (uint32_t)vertexCount; i++) {
			elements.push_back(currentVertexIndex);
			elements.push_back(currentVertexIndex + i - 1);
			elements.push_back(currentVertexIndex + i);
		}
		currentVertexIndex += (uint32_t)vertexCount;

		ClearPath();
	}

	void Renderer2D::Fill(Ref<Texture> texture, const Vector2<float> uv[4])
	{
		if(!texture) {
			ClearPath();
			return;
		}

		const size_t vertexCount = points.size();
		const size_t elementCount = (points.size() - 2) * 3;

		if(vertexCount < 2) {
			return;
		}

		float textureIndex = FindTexture(std::move(texture));

		vertices.reserve(vertexCount);
		elements.reserve(elementCount);

		for(uint32_t i = 0; i < (uint32_t)vertexCount; i++) {
			auto& vertex = vertices.emplace_back();
			vertex.position = points[i].position;
			vertex.uv = uv[i];
			vertex.color = 0xffffffff;
			vertex.textureIndex = textureIndex;
		}
		for(uint32_t i = 2; i < (uint32_t)vertexCount; i++) {
			elements.push_back(currentVertexIndex);
			elements.push_back(currentVertexIndex + i - 1);
			elements.push_back(currentVertexIndex + i);
		}
		currentVertexIndex += (uint32_t)vertexCount;

		ClearPath();
	}

	void Renderer2D::Stroke(uint32_t color, float thickness)
	{
		CalculateJoins();

		const size_t vertexCount = points.size() * 3;

		size_t start = closedPath ? 0 : 1;
		size_t end = closedPath ? points.size() : points.size() - 1;

		size_t idx1 = currentVertexIndex;
		for(size_t i = 0; i < end; i++) {
			size_t idx2 = (i + 1U) == points.size() ? currentVertexIndex : idx1 + 3;

			elements.push_back(idx2 + 0u);
			elements.push_back(idx1 + 0u);
			elements.push_back(idx1 + 2u);
									   
			elements.push_back(idx1 + 2u);
			elements.push_back(idx2 + 2u);
			elements.push_back(idx2 + 0u);
									   
			elements.push_back(idx2 + 1u);
			elements.push_back(idx1 + 1u);
			elements.push_back(idx1 + 0u);
									   
			elements.push_back(idx1 + 0u);
			elements.push_back(idx2 + 0u);
			elements.push_back(idx2 + 1u);

			idx1 = idx2;
		}
		currentVertexIndex += (uint32_t)vertexCount;

		float lineWidth = thickness * 0.5f;

		if(!closedPath) {
			CalculateStartOfStrokeWhenNotClosed(points[0], color, lineWidth);
		}

		for(size_t i = start; i < end; ++i) {
			const auto& p1 = points[i];

			auto& vertex = vertices.emplace_back();
			vertex.position = p1.position;
			vertex.color = color;

			auto& vertex2 = vertices.emplace_back();
			vertex2.position = p1.position + (p1.miterDistance * lineWidth);
			vertex2.color = color;

			auto& vertex3 = vertices.emplace_back();
			vertex3.position = p1.position - (p1.miterDistance * lineWidth);
			vertex3.color = color;
		}

		if(!closedPath) {
			CalculateEndOfStrokeWhenNotClosed(points[end], points[end - 1].normal, color, lineWidth);
		}

		points.resize(0);
	}

	void Renderer2D::Clear()
	{
		vertices.resize(0);
		elements.resize(0);
		points.resize(0);
		textures.resize(0);

		currentVertexIndex = 0;
	}

	void Renderer2D::RectPattern(float x, float y, float w, float h)
	{
		MoveTo({ x, y });
		LineTo({ x, y + h });
		LineTo({ x + w, y + h });
		LineTo({ x + w, y });
		ClosePath();
	}

	void Renderer2D::FillRect(float x, float y, float w, float h, uint32_t color)
	{
		RectPattern(x, y, w, h);
		Fill(color);
	}

	void Renderer2D::FillRect(float x, float y, float w, float h, const Ref<Texture>& texture, const Vector4<float>& uv)
	{
		RectPattern(x, y, w, h);
		Fill(texture, uv);
	}

	void Renderer2D::FillRect(float x, float y, float w, float h, const Ref<Texture>& texture, const Vector2<float> uv[4])
	{
		RectPattern(x, y, w, h);
		Fill(texture, uv);
	}

	void Renderer2D::DrawRect(float x, float y, float w, float h, uint32_t color, float thickness)
	{
		RectPattern(x, y, w, h);
		Stroke(color, thickness);
	}

	void Renderer2D::CalculateJoins()
	{
		for(size_t i = 0; i < points.size(); ++i) {
			const size_t j = (i + 1U) == points.size() ? 0 : i + 1;
			auto& point1 = points[i];
			const auto& point2 = points[j];

			point1.distance = point2.position - point1.position;
			point1.length = glm::length(point1.distance);
			point1.distance = glm::normalize(point1.distance);
		}

		for(size_t i = 0; i < points.size(); ++i) {
			const size_t j = (i + 1U) == points.size() ? 0 : i + 1;
			auto& point1 = points[i];
			auto& point2 = points[j];
			
			point1.normal = CrossProduct2D(point1.distance);
			point2.normal = CrossProduct2D(point2.distance);

			point2.miterDistance = (point1.normal + point2.normal) * 0.5f;

			const float dmr2 = glm::length2(point2.miterDistance);

			if(dmr2 > 0.000001f) {
				float scale = 1.0f / dmr2;
				if(scale > 600.0f) {
					scale = 600.0f;
				}

				point2.miterDistance *= scale;
			}
		}
	}

	void Renderer2D::CalculateStartOfStrokeWhenNotClosed(const Point& startPoint, uint32_t color, float lineWidth)
	{
		auto& vertex = vertices.emplace_back();
		vertex.position = startPoint.position;
		vertex.color = color;

		auto& vertex2 = vertices.emplace_back();
		vertex2.position = startPoint.position + startPoint.normal * lineWidth;
		vertex2.color = color;

		auto& vertex3 = vertices.emplace_back();
		vertex3.position = startPoint.position - startPoint.normal * lineWidth;
		vertex3.color = color;
	}

	void Renderer2D::CalculateEndOfStrokeWhenNotClosed(const Point& endPoint, const Vector2<float>& pointNormalBeforeEndPoint, uint32_t color, float lineWidth)
	{
		auto& vertex = vertices.emplace_back();
		vertex.position = endPoint.position;
		vertex.color = color;

		auto& vertex2 = vertices.emplace_back();
		vertex2.position = endPoint.position + pointNormalBeforeEndPoint * lineWidth;
		vertex2.color = color;

		auto& vertex3 = vertices.emplace_back();
		vertex3.position = endPoint.position - pointNormalBeforeEndPoint * lineWidth;
		vertex3.color = color;
	}

	bool Renderer2D::PointEquals(const Vector2<float>& point1, const Vector2<float>& point2, float tolerance)
	{
		const Vector2<float> distance = point2 - point1;
		return glm::dot(distance, distance) < tolerance * tolerance;
	}

	float Renderer2D::FindTexture(Ref<Texture>&& texture)
	{
		for(size_t i = 0; i < textures.size(); ++i) {
			if(textures[i] == texture) {
				return static_cast<float>(i + 1);
			}
		}

		textures.push_back(std::move(texture));
		return static_cast<float>(textures.size());
	}
}

































