#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"
#include "GameEngine/Scene/Illuminar_SceneCamera.h"

namespace Illuminar {
	struct DebugLineRenderer
	{
		typedef uint32_t DrawIndex;

		struct LineVertex
		{
			Vector3<float> position;
			uint32_t color;
		};

		static void Create();
		static void Begin(const Camera& camera, const Mat4& transform);
		static void End(float thickness);

		static void PushTransform(const Mat4& transfrom);
		static void PopTransform();

		static void DrawLine(const Vector3<float>& a, const Vector3<float>& b, uint32_t);
		static void DrawFrustum(const SceneCamera& camera);
		static void DrawFrustum(float width, float height, float nearZ, float farZ, float fov);

		static DebugLineRenderer& get()
		{
			static DebugLineRenderer instance;
			return instance;
		}

		Ref<GFX::GraphicsPipeline> vao = nullptr;
		Ref<GFX::VertexBuffer> vbo = nullptr;
		Ref<GFX::ElementBuffer> ebo = nullptr;

		std::vector<LineVertex> vertices;
		std::vector<DrawIndex> elements;

		Mat4 transform = Mat4(1.0f);

		uint32_t currentVertexIndex = 0;
	};
}