#pragma once

#include <GraphicsEngine/Illuminar_Texture.h>
#include <GraphicsEngine/RenderTarget/Illuminar_FrameBuffer.h>

#include "Illuminar_RenderGraphPass.h"

#include <glad/glad.h>

namespace Illuminar::GFX {
	struct RenderGraph;
	struct BaseRenderGraphPass;

	struct BaseRenderGraphResource
	{
		BaseRenderGraphResource() = default;
		BaseRenderGraphResource(const BaseRenderGraphResource&) = default;
		virtual ~BaseRenderGraphResource() = default;

		virtual void Begin(RenderGraph& renderGraph) {}
		virtual void End(RenderGraph& renderGraph) {}

		uint32_t readersCount = 0;
		uint32_t refCount = 0;

		uint8_t priority = 0;

		BaseRenderGraphPass* writer = nullptr;

		BaseRenderGraphPass* begin = nullptr;
		BaseRenderGraphPass* end = nullptr;

		RenderGraphResourceHandle handle;
	};

	template<typename T>
	struct RenderGraphResource : BaseRenderGraphResource
	{
		using CreateInfo = typename T::CreateInfo;

		RenderGraphResource(const CreateInfo& createInfo)
			: createInfo(createInfo)
		{
		}

		virtual ~RenderGraphResource() override = default;

		inline void Begin(RenderGraph& renderGraph) override
		{
			resource.Create(createInfo, renderGraph);
		}

		inline void End(RenderGraph& renderGraph) override
		{
			resource.Destroy(renderGraph);
			resource = {};
		}

		[[nodiscard]] operator const T& () const { return resource; }
		[[nodiscard]] operator T& () const { return resource; }

		T resource{};
		CreateInfo createInfo;
	};

	struct RenderGraphTexture
	{
		struct CreateInfo
		{
			auto operator<=>(const CreateInfo&) const = default;

			std::string_view name;

			uint32_t w = 0;
			uint32_t h = 0;
			const void* pixels = nullptr;
			TextureParameters parameters;

			//todo: rename/remove?/change
			bool shadow = false;
		};

		Ref<Texture> texture = nullptr;

		void Create(const CreateInfo& createInfo, [[maybe_unused]] RenderGraph& renderGraph);
		void Destroy(RenderGraph& renderGraph);
	};

	struct RenderGraphFrameBuffer
	{
		struct CreateInfo
		{
			static constexpr uint8_t MIN_COLOR_ATTACHMENT_COUNT = 1;
			static constexpr uint8_t MAX_COLOR_ATTACHMENT_COUNT = 32;

			auto operator<=>(const CreateInfo&) const = default;

			uint8_t colorAttachmentCount = MIN_COLOR_ATTACHMENT_COUNT;

			std::array<RenderGraphResourceHandle, MAX_COLOR_ATTACHMENT_COUNT> colorAttachments{};
			RenderGraphResourceHandle depthStencilTexture{};

			FrameBufferTarget target = FrameBufferTarget::ReadAndDraw;
		};

		//operator Ref<FrameBuffer>() const
		//{
		//	return frameBuffer;
		//}

		Ref<FrameBuffer> frameBuffer = nullptr;

		void Create(const CreateInfo& createInfo, [[maybe_unused]] RenderGraph& renderGraph);
		void Destroy(RenderGraph& renderGraph);
	};
}