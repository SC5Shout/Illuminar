#pragma once

#include "GraphicsEngine/Illuminar_Texture.h"

namespace Illuminar::GFX {
	/*
	CreateTexture2D() function:
		When a new texture is created, the function checks if the texture already exists in a cache.
		If it does not, the texture is added to the cache with the age that corresponds to the current frame age.

		If it does exist in the cache and we are going to use this texture in the next few frames (CACHE_MAX_AGE)
		the age of the texture is updated based on the current frame age.

	Update() function:
		If the texture is not going to be used in the next few frames, the texture's age it not being updated thus when the age difference
		between the texture's and the current frame age is greater then CACHE_MAX_AGE, the texture is deleted - from the cache and the GPU.

		if are is not enough space in the cache, we want to delete the oldest texture in order to make some space for the new one

	hope that makes sense.
	*/
	struct RenderGraphResourceCache
	{
		struct TextureKey
		{
			auto operator<=>(const TextureKey&) const = default;

			[[nodiscard]] inline size_t getSize() const
			{
				uint32_t formatRGBASize = 4;
				return w * h * depth * formatRGBASize;
			}

			uint32_t w;
			uint32_t h;
			uint32_t depth;
			TextureParameters parameters;
		};

		struct TextureCache
		{
			Ref<Texture> texture = nullptr;
			uint32_t age = 0;
			size_t size = 0;

			[[nodiscard]] inline bool operator>(const TextureCache& other) const { return age > other.age; }
			[[nodiscard]] inline bool operator<(const TextureCache& other) const { return age < other.age; }
		};

		[[nodiscard]] Ref<Texture2D> CreateTexture2D(const Texture2D::CreateInfo& createInfo);

		inline void DestroyTexture(const Ref<Texture>& texture) {}

		void Update();

		static constexpr size_t CACHE_MAX_AGE = 30;
		static constexpr size_t CACHE_CAPACITY = 256u << 20u;

		size_t cacheSize = 0;

		uint32_t age = 0;

		std::map<TextureKey, TextureCache> texturesCache;
	};
}