#include "Illuminar_pch.h"
#include "Illuminar_RenderGraph.h"

namespace Illuminar::GFX {
	RenderGraphResourceCache RenderGraph::resourceCache;

	RenderGraph::RenderGraph()
	{
		passes.reserve(64);
		resources.reserve(64);
	}

	//RenderGraphResourceHandle RenderGraph::CreateResource(const std::shared_ptr<BaseRenderGraphResource>& resource)
	//{
	//	uint16_t index = static_cast<uint16_t>(resources.size());
	//	auto& newResource = resources.emplace_back();
	//	newResource = resource;
	//	return RenderGraphResourceHandle{ index };
	//}

	BaseRenderGraphResource& RenderGraph::getBaseResource(RenderGraphResourceHandle index) const
	{
		assert(index < resources.size());
		return *resources[index].get();
	}

	const std::shared_ptr<BaseRenderGraphResource>& RenderGraph::getBaseResourcePtr(RenderGraphResourceHandle index) const
	{
		assert(index < resources.size());
		return resources[index];
	}

	RenderGraphResourceHandle PassBuilder::CreateTexture(const RenderGraphTexture::CreateInfo& createInfo)
	{
		return rg.CreateTexture(createInfo);
	}

	RenderGraphResourceHandle PassBuilder::CreateFrameBuffer(const RenderGraphFrameBuffer::CreateInfo& createInfo)
	{
		return pass.Use(rg.CreateFrameBuffer(createInfo));
	}

	RenderGraph& RenderGraph::Compile()
	{
		for (auto& pass : passes) {
			pass->refCount = pass->writes.size();

			for (auto readIndex : pass->reads) {
				++resources[readIndex]->readersCount;
			}

			for (auto writeIndex : pass->writes) {
				++resources[writeIndex]->writer = pass.get();
			}
		}

		std::pmr::vector<BaseRenderGraphResource*> stack(&arena);
		stack.reserve(resources.size());
		for (auto& resource : resources) {
			if (resource->readersCount == 0) {
				stack.push_back(resource.get());
			}
		}

		while (!stack.empty()) {
			const BaseRenderGraphResource* const resource = stack.back();
			stack.pop_back();
			BaseRenderGraphPass* const writer = resource->writer;
			if (writer) {
				if (--writer->refCount == 0) {
					const auto& reads = writer->reads;
					for (auto readIndex : reads) {
						BaseRenderGraphResource& r = *resources[readIndex];
						if (--r.readersCount == 0) {
							stack.push_back(&r);
						}
					}
				}
			}
		}

		for (const auto& resource : resources) {
			resource->refCount += resource->readersCount;
		}

		passNodesEnd = std::stable_partition(
			passes.begin(), passes.end(), [](auto const& pPassNode) {
				return !pPassNode->isCulled();
		});

		auto first = passes.begin();
		const auto activePassNodesEnd = passNodesEnd;
		while (first != activePassNodesEnd) {
			auto& passNode = *first;
			first++;
			//assert_invariant(!passNode->isCulled());

			for (auto readIndex : passNode->reads) {
				BaseRenderGraphResource* const resource = resources[readIndex].get();
				resource->begin = resource->begin ? resource->begin : passNode.get();
				resource->end = passNode.get();
			}

			for (auto writeIndex : passNode->writes) {
				BaseRenderGraphResource* const resource = resources[writeIndex].get();
				resource->begin = resource->begin ? resource->begin : passNode.get();
				resource->end = passNode.get();
			}
		}

		for (uint8_t priority = 0; priority < 2; ++priority) {
			for (const auto& resource : resources) {
				if (resource->priority == priority) {
					auto* begin = resource->begin;
					auto* end = resource->end;
					if (begin && end) {
						begin->resourcesToCreate.push_back(resource.get());
						end->resourcesToDestroy.push_back(resource.get());
					}
				}
			}
		}

		return *this;
	}

	void RenderGraph::Execute()
	{
		auto first = passes.begin();
		const auto activePassNodesEnd = passNodesEnd;
		while (first != activePassNodesEnd) {
			auto& passNode = *first;
			first++;
			//assert_invariant(!node->isCulled());

			for (const auto& resource : passNode->resourcesToCreate) {
				resource->Begin(*this);
			}

			passNode->Execute();

			std::for_each(passNode->resourcesToDestroy.crbegin(), passNode->resourcesToDestroy.crend(), [this](const auto& resource) {
				resource->End(*this);
			});

		}

		resources.clear();
		passes.clear();
	}
}