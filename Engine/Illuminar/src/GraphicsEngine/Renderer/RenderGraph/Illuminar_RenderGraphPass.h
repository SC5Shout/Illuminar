#pragma once

namespace Illuminar::GFX {
	struct RenderGraph;
	struct BaseRenderGraphResource;

	struct BaseRenderGraphPass
	{
		BaseRenderGraphPass(RenderGraph& rg);
		virtual ~BaseRenderGraphPass() = default;

		BaseRenderGraphPass(BaseRenderGraphPass const&) = delete;
		BaseRenderGraphPass& operator=(BaseRenderGraphPass const&) = delete;

		virtual void Execute() = 0;

		RenderGraphResourceHandle Read(RenderGraphResourceHandle input);
		RenderGraphResourceHandle Write(RenderGraphResourceHandle output);

		RenderGraphResourceHandle Sample(RenderGraphResourceHandle resource);
		RenderGraphResourceHandle Use(RenderGraphResourceHandle resource);

		std::pmr::vector<BaseRenderGraphResource*> resourcesToCreate;
		std::pmr::vector<BaseRenderGraphResource*> resourcesToDestroy;

		bool isCulled() { return refCount == 0; }

		size_t refCount = 0;

		//protected:
		RenderGraph& rg;

		//private:
			//render buffers // also textures for now
		std::pmr::vector<RenderGraphResourceHandle> reads;
		//textures
		std::pmr::vector<RenderGraphResourceHandle> samples;
		//frame buffers
		std::pmr::vector<RenderGraphResourceHandle> targets;

		std::pmr::vector<RenderGraphResourceHandle> writes;
	};

	template<typename Data, typename FuncT>
	struct RenderGraphPass : BaseRenderGraphPass
	{
		RenderGraphPass(RenderGraph& rg, FuncT&& function)
			: BaseRenderGraphPass(rg), function(std::move(function))
		{
		}

		void Execute() override
		{
			function(rg, data);
		}

		const Data& getData() const { return data; }
		Data& getData() { return data; }

	private:
		FuncT function;
		Data data;
	};
}