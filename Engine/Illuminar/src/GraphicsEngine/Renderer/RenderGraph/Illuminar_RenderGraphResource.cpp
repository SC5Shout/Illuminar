#include "Illuminar_pch.h"
#include "Illuminar_RenderGraphResource.h"

#include "Illuminar_RenderGraph.h"

namespace Illuminar::GFX {
	void RenderGraphTexture::Create(const CreateInfo& createInfo, RenderGraph& renderGraph)
	{
		const Texture2D::CreateInfo info = (Texture2D::CreateInfo&)createInfo;
		//texture = Texture2D::Create(info);
		texture = renderGraph.resourceCache.CreateTexture2D(info);
	}

	void RenderGraphTexture::Destroy(RenderGraph& renderGraph)
	{
		if (texture) {
			renderGraph.resourceCache.DestroyTexture(texture);
			//texture.Reset();
		}
	}

	void RenderGraphFrameBuffer::Create(const CreateInfo& createInfo, RenderGraph& renderGraph)
	{
		FrameBuffer::CreateInfo info;
		info.target = createInfo.target;
		info.colorAttachmentCount = createInfo.colorAttachmentCount;

		for (size_t i = 0; i < info.colorAttachmentCount; ++i) {
			const auto& colorTextureID = createInfo.colorAttachments[i];

			if (colorTextureID != INVALID_RENDER_GRAPH_RESOURCE_HANDLE) {
				const auto& textureResource = renderGraph.getResource<RenderGraphTexture>(colorTextureID);
				info.colorAttachments[i] = textureResource.resource.texture;
			}
		}

		const auto& depthTextureID = createInfo.depthStencilTexture;
		if (depthTextureID != INVALID_RENDER_GRAPH_RESOURCE_HANDLE) {
			const auto& textureResource = renderGraph.getResource<RenderGraphTexture>(depthTextureID);
			info.depthStencilTexture = textureResource.resource.texture;
		}

		frameBuffer = FrameBuffer::Create(info);
	}

	void RenderGraphFrameBuffer::Destroy(RenderGraph& renderGraph)
	{
		if (frameBuffer) {
			frameBuffer.Reset();
		}
	}
}