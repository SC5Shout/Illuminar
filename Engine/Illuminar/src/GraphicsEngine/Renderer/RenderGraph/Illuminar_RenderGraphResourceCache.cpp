#include "Illuminar_pch.h"
#include "Illuminar_RenderGraphResourceCache.h"

namespace Illuminar::GFX {
	Ref<Texture2D> RenderGraphResourceCache::CreateTexture2D(const Texture2D::CreateInfo& createInfo)
	{
		Ref<Texture2D> texture = nullptr;
		const TextureKey key = { createInfo.w, createInfo.h, 1, createInfo.parameters };

		if (texturesCache.contains(key)) {
			auto& cache = texturesCache[key];
			texture = static_cast<Ref<Texture2D>>(cache.texture);
			cache.age = age;
		} else {
			texture = Texture2D::Create(createInfo);
			texturesCache.emplace(key, TextureCache{ texture, age, key.getSize() });
			cacheSize += key.getSize();
		}

		return texture;
	}

	void RenderGraphResourceCache::Update()
	{
		uint32_t oldAge = age++;

		for (auto it = texturesCache.begin(); it != texturesCache.end();) {
			const size_t ageDiff = oldAge - it->second.age;
			if (ageDiff >= CACHE_MAX_AGE) {

				it->second.texture.Reset();
				cacheSize -= it->second.size;
				it = texturesCache.erase(it);

				if (cacheSize < CACHE_CAPACITY) {
					break;
				}
			} else ++it;
		}

		if (cacheSize >= CACHE_CAPACITY) {
			std::vector<std::pair<TextureKey, TextureCache>> cache;
			cache.reserve(texturesCache.size());
			for (const auto& item : texturesCache) {
				cache.push_back(item);
			}

			std::sort(cache.begin(), cache.end(), [](auto const& lhs, auto const& rhs) {
				return lhs.second.age < rhs.second.age;
			});

			auto curr = cache.begin();

			while (cacheSize >= CACHE_CAPACITY) {
				auto it = texturesCache.find(curr->first);

				it->second.texture.Reset();
				cacheSize -= it->second.size;
				texturesCache.erase(it);

				++curr;
			}

			uint32_t oldestAge = cache.front().second.age;
			for (auto& it : texturesCache) {
				it.second.age -= oldestAge;
			}
			age -= oldestAge;
		}
	}
}