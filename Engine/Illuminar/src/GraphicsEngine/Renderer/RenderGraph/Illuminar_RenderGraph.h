#pragma once

#include "Illuminar_RenderGraphPass.h"
#include "Illuminar_RenderGraphResource.h"
#include "Illuminar_RenderGraphResourceCache.h"

/*
	Based on Filament Engine Frame Graph,
	which is inspired from Yuriy O'Donnell 2017 GDC talk
	"FrameGraph: Extensible Rendering Architecture in Frostbite"
*/

namespace Illuminar::GFX {
	/*
	Example use-case:
		using namespace Illuminar::GFX;

		struct ColorPassData
		{
			RenderGraphResourceHandle colorTexture;
			RenderGraphResourceHandle depthTexture;

			RenderGraphResourceHandle renderTarget;
		};

		RenderGraph graph;
		graph.AddPass<ColorPassData>([](PassBuilder& builder, ColorPassData& data) {
			data.colorTexture = builder.CreateTexture({ .w = 1600, .h = 900 });

			TextureParameters params;
			params.DataType(TextureDataType::UnsignedInt24_8).Format(TextureFormat::DEPTH24_STENCIL8);
			data.depthTexture = builder.CreateTexture({ .w = 1600, .h = 900, .parameters = params });

			data.colorTexture = builder.Write(builder.Read(data.colorTexture));
			data.depthTexture = builder.Write(builder.Read(data.depthTexture));

			RenderGraphFrameBuffer::CreateInfo fboInfo;
			fboInfo.w = 1600;
			fboInfo.h = 900;
			fboInfo.colorAttachmentCount = 1;
			fboInfo.colorAttachments[0] = data.colorTexture;
			fboInfo.depthStencilTexture = data.depthTexture;
			data.renderTarget = builder.CreateFrameBuffer(fboInfo);
		},
		[](RenderGraph& rg, const ColorPassData& data) {
			auto& rt = rg.getResource<RenderGraphFrameBuffer>(data.renderTarget);
			rt.resource.frameBuffer->Bind();

			rendering...

			rt.resource.frameBuffer->Unbind();
		});
	*/

	struct RenderGraph;
	struct PassBuilder
	{
		PassBuilder(RenderGraph& renderGraph, BaseRenderGraphPass& pass)
			: rg(renderGraph), pass(pass)
		{
		}

		template<typename T>
		[[nodiscard]] inline RenderGraphResourceHandle CreateResorce(const typename T::CreateInfo& createInfo, uint8_t priority);

		[[nodiscard]] RenderGraphResourceHandle CreateTexture(const RenderGraphTexture::CreateInfo& createInfo);
		[[nodiscard]] RenderGraphResourceHandle CreateFrameBuffer(const RenderGraphFrameBuffer::CreateInfo& createInfo);

		[[nodiscard]] inline RenderGraphResourceHandle Read(RenderGraphResourceHandle input) { return pass.Read(input); }
		[[nodiscard]] inline RenderGraphResourceHandle Sample(RenderGraphResourceHandle input) { return pass.Sample(input); }
		[[nodiscard]] inline RenderGraphResourceHandle Write(RenderGraphResourceHandle output) { return pass.Write(output); }

		RenderGraph& rg;
		BaseRenderGraphPass& pass;
	};

	struct RenderGraph
	{
		RenderGraph();
		~RenderGraph() = default;

		template<typename Data, typename Setup, typename FuncT>
		[[nodiscard]] inline RenderGraphPass<Data, FuncT>& AddPass(Setup setup, FuncT&& func)
		{
			auto& pass = passes.emplace_back();
			pass = std::make_unique<RenderGraphPass<Data, FuncT>>(*this, std::forward<FuncT>(func));

			auto& result = static_cast<RenderGraphPass<Data, FuncT>&>(*pass.get());

			PassBuilder builder(*this, *pass);
			setup(builder, result.getData());

			return result;
		}

		//[[nodiscard]] RenderGraphResourceHandle CreateResource(const std::shared_ptr<BaseRenderGraphResource>& resource);

		template<typename T>
		[[nodiscard]] inline RenderGraphResourceHandle CreateResource(const typename T::CreateInfo& createInfo, uint8_t priority)
		{
			uint16_t index = static_cast<uint16_t>(resources.size());
			auto& resource = resources.emplace_back();
			resource = std::make_shared<RenderGraphResource<T>>(createInfo);
			return RenderGraphResourceHandle{ index };
		}

		[[nodiscard]] inline RenderGraphResourceHandle CreateTexture(const RenderGraphTexture::CreateInfo& createInfo)
		{
			return CreateResource<RenderGraphTexture>(createInfo, 0);
		}

		[[nodiscard]] inline RenderGraphResourceHandle CreateFrameBuffer(const RenderGraphFrameBuffer::CreateInfo& createInfo)
		{
			return CreateResource<RenderGraphFrameBuffer>(createInfo, 1);
		}

		RenderGraph& Compile();
		void Execute();

		[[nodiscard]] BaseRenderGraphResource& getBaseResource(RenderGraphResourceHandle index) const;

		template<typename T>
		[[nodiscard]] inline RenderGraphResource<T>& getResource(RenderGraphResourceHandle index) const
		{
			return static_cast<RenderGraphResource<T>&>(getBaseResource(index));
		}

		[[nodiscard]] const std::shared_ptr<BaseRenderGraphResource>& getBaseResourcePtr(RenderGraphResourceHandle index) const;

		template<typename T>
		[[nodiscard]] inline const std::shared_ptr<T>& getResourcePtr(RenderGraphResourceHandle index) const
		{
			return std::static_pointer_cast<T>(getBaseResourcePtr(index));
		}

		std::pmr::monotonic_buffer_resource arena{ 100_KB };
		std::pmr::vector<std::shared_ptr<BaseRenderGraphResource>> resources{&arena};
		std::pmr::vector<std::unique_ptr<BaseRenderGraphPass>> passes{ &arena };
		std::pmr::vector<std::unique_ptr<BaseRenderGraphPass>>::iterator passNodesEnd;

		static RenderGraphResourceCache resourceCache;
	};

	template<typename T>
	inline RenderGraphResourceHandle PassBuilder::CreateResorce(const typename T::CreateInfo& createInfo, uint8_t priority)
	{
		return rg.CreateResource<T>(createInfo, 0);
	}
}