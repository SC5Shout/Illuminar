#include "Illuminar_pch.h"
#include "Illuminar_RenderGraphPass.h"

#include "Illuminar_RenderGraph.h"

namespace Illuminar::GFX {
	BaseRenderGraphPass::BaseRenderGraphPass(RenderGraph& rg)
		: rg(rg),
		writes(&rg.arena),
		reads(&rg.arena),
		samples(&rg.arena),
		targets(&rg.arena),
		resourcesToCreate(&rg.arena),
		resourcesToDestroy(&rg.arena)
	{
		writes.reserve(64);
		reads.reserve(64);
		samples.reserve(64);
		targets.reserve(64);
		resourcesToCreate.reserve(64);
		resourcesToDestroy.reserve(64);
	}

	RenderGraphResourceHandle BaseRenderGraphPass::Read(RenderGraphResourceHandle input)
	{
		auto pos = std::find_if(reads.begin(), reads.end(),
								[input](RenderGraphResourceHandle currentResource) { return currentResource == input; });
		//register as read only if it has not been registered yet
		if (pos == reads.end()) {
			reads.push_back(input);
		}
		return input;
	}

	RenderGraphResourceHandle BaseRenderGraphPass::Write(RenderGraphResourceHandle output)
	{
		auto pos = std::find_if(writes.begin(), writes.end(),
								[output](RenderGraphResourceHandle currentResource) { return currentResource == output; });

		if (pos == writes.end()) {
			writes.push_back(output);
		}
		return output;

		////register as read only if it has not been registered yet
		//if(pos != writes.end()) {
		//	return *pos;
		//}
		//
		////TODO: why creating a new gpu resource? reimplement so only a new version of the same resource is created
		////consider if we even need a different version, because passes are executed in order
		//const auto& baseResource = rg.getBaseResourcePtr(output);
		//
		//RenderGraphResourceHandle r = rg.CreateResource(baseResource);
		//auto& newBaseResource = rg.getBaseResource(r);
		//writes.push_back(r);
		//
		//return r;
	}

	RenderGraphResourceHandle BaseRenderGraphPass::Sample(RenderGraphResourceHandle resource)
	{
		//Sample is just a special case of Read of a texture/render buffer
		Read(resource);

		auto pos = std::find_if(samples.begin(), samples.end(),
								[resource](RenderGraphResourceHandle currentResource) { return currentResource == resource; });
		//register as read only if it has not been registered yet
		if (pos == samples.end()) {
			samples.push_back(resource);
		}
		return resource;
	}

	RenderGraphResourceHandle BaseRenderGraphPass::Use(RenderGraphResourceHandle resource)
	{
		//Use is just a special case of Read of a frame buffer
		Read(resource);

		auto pos = std::find_if(targets.begin(), targets.end(),
								[resource](RenderGraphResourceHandle currentResource) { return currentResource == resource; });
		//register as read only if it has not been registered yet
		if (pos == targets.end()) {
			targets.push_back(resource);
		}
		return resource;
	}
}
