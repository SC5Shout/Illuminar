#pragma once

namespace Illuminar {
	struct Camera
	{
		[[nodiscard]] inline const bool isDirty() const { return dirty; }
		inline void Clean() const { dirty = false; }

		Mat4 projection = Mat4(1.0f);
		mutable bool dirty = true;
	};
}