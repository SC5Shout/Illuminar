#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"

namespace Illuminar {
	struct RenderObject
	{
		//Ref<GFX::VertexElementBuffer> buffer = nullptr;
		Ref<GFX::VertexBuffer> vbo = nullptr;
		Ref<GFX::ElementBuffer> ebo = nullptr;
		Ref<GFX::Material> material = nullptr;
		//Ref<GFX::GraphicsPipeline> pipeline = nullptr;

		uint64_t sortingKey = 0;

		uint32_t elementsCount = 0;
		uint32_t baseVertex = 0;
		uint32_t firstIndex = 0;

		float distanceFromCamera = 0.0f;

		[[nodiscard]] inline bool operator < (const RenderObject& other) const
		{
			return sortingKey < other.sortingKey;
		}
	};

	struct RenderQueue
	{
		[[nodiscard]] inline constexpr uint32_t FloatFlip(uint32_t f)
		{
			const uint32_t mask = static_cast<uint32_t>(-int(f >> 31) | 0x80000000);
			return (f ^ mask);
		}

		[[nodiscard]] inline constexpr uint32_t DepthToBits(float depth, uint32_t depthBits)
		{
			union { float f; uint32_t i; } f2i;
			f2i.f = depth;
			f2i.i = FloatFlip(f2i.i);			
			return (f2i.i >> (32 - depthBits));	
		}

		void AddRenderObjects(std::vector<RenderObject>&& renderObjects)
		{
			static constexpr uint32_t PIPELINE_STATE_NUMBER_OF_BITS = 16;
			static constexpr uint32_t VERTEX_ARRAY_NUMBER_OF_BITS = 16;
			static constexpr uint32_t RESOURCE_GROUP_NUMBER_OF_BITS = 11;	
			static constexpr uint32_t DEPTH_NUMBER_OF_BITS = 21;

			static constexpr uint32_t PIPELINE_STATE_SHIFT_OPAQUE = 64 - PIPELINE_STATE_NUMBER_OF_BITS;	
			static constexpr uint32_t VERTEX_ARRAY_SHIFT_OPAQUE = PIPELINE_STATE_SHIFT_OPAQUE - VERTEX_ARRAY_NUMBER_OF_BITS;	
			static constexpr uint32_t RESOURCE_GROUP_SHIFT_OPAQUE = VERTEX_ARRAY_SHIFT_OPAQUE - RESOURCE_GROUP_NUMBER_OF_BITS;	
			static constexpr uint32_t DEPTH_SHIFT_OPAQUE = RESOURCE_GROUP_SHIFT_OPAQUE - DEPTH_NUMBER_OF_BITS;			

			static constexpr uint32_t DEPTH_SHIFT_TRANSPARENT = 64 - DEPTH_NUMBER_OF_BITS;				
			static constexpr uint32_t PIPELINE_STATE_SHIFT_TRANSPARENT = DEPTH_SHIFT_TRANSPARENT - PIPELINE_STATE_NUMBER_OF_BITS;	
			static constexpr uint32_t VERTEX_ARRAY_SHIFT_TRANSPARENT = PIPELINE_STATE_SHIFT_TRANSPARENT - VERTEX_ARRAY_NUMBER_OF_BITS;		
			static constexpr uint32_t RESOURCE_GROUP_SHIFT_TRANSPARENT = VERTEX_ARRAY_SHIFT_TRANSPARENT - RESOURCE_GROUP_NUMBER_OF_BITS;

			for(auto& renderObject : renderObjects) {
				const uint32_t quantizedDepth = DepthToBits(transparent ? -renderObject.distanceFromCamera : renderObject.distanceFromCamera, DEPTH_NUMBER_OF_BITS);

				#define RENDER_QUEUE_MAKE_MASK(x) ((1u << (x)) - 1u)
				#define RENDER_QUEUE_HASH(x, bits, shift) (uint64_t((x) & RENDER_QUEUE_MAKE_MASK((bits))) << (shift))

				//const uint32_t pipelineStateId = renderObject.pipeline->getID();
				const uint32_t resourceGroupId = renderObject.material.get() ? renderObject.material->getID() : -1;

				uint64_t sortingKey = 0;
				if(transparent) {
					sortingKey =
						RENDER_QUEUE_HASH(quantizedDepth, DEPTH_NUMBER_OF_BITS, DEPTH_SHIFT_TRANSPARENT) |
						//RENDER_QUEUE_HASH(pipelineStateId, PIPELINE_STATE_NUMBER_OF_BITS, PIPELINE_STATE_SHIFT_TRANSPARENT) |
						RENDER_QUEUE_HASH(resourceGroupId, RESOURCE_GROUP_NUMBER_OF_BITS, RESOURCE_GROUP_SHIFT_TRANSPARENT);
				} else {
					sortingKey =
						//RENDER_QUEUE_HASH(pipelineStateId, PIPELINE_STATE_NUMBER_OF_BITS, PIPELINE_STATE_SHIFT_OPAQUE) |
						RENDER_QUEUE_HASH(resourceGroupId, RESOURCE_GROUP_NUMBER_OF_BITS, RESOURCE_GROUP_SHIFT_OPAQUE) |
						RENDER_QUEUE_HASH(quantizedDepth, DEPTH_NUMBER_OF_BITS, DEPTH_SHIFT_OPAQUE);
				}

				renderObject.sortingKey = sortingKey;
			}

			queue.objects = std::move(renderObjects);
		}

		void Sort()
		{
			if(!queue.sorted) {
				std::sort(queue.objects.begin(), queue.objects.end());

				queue.sorted = true;
			}
		}

		void Clear()
		{
			queue.objects.clear();
		}

		void Draw()
		{
			if (queue.objects.size()) {
				auto& firstObject = queue.objects[0];

				Ref<GFX::GraphicsPipeline> currentPipeline = nullptr;
				Ref<GFX::Material> currentMaterial = nullptr;
				Ref<GFX::VertexBuffer> currentVbo = nullptr;
				Ref<GFX::ElementBuffer> currentEbo = nullptr;
				//Ref<GFX::VertexElementBuffer> currentBuffer = nullptr;

				static auto CheckCurrentAndBind = []<typename Current, typename New>(Current& c, const New& n) {
					//.get() because there is a weird behaviour with Ref<GFX::Material> that I haven't investigated yet.
					if (c.get() != n.get()) {
						c = n;
						c->Bind();
					}
				};

				uint32_t meshInstanceIndex = 0;
				for (auto& renderObject : queue.objects) {
					//CheckCurrentAndBind(currentPipeline, renderObject.pipeline);		
					//CheckCurrentAndBind(currentMaterial, renderObject.material);
					//CheckCurrentAndBind(currentVbo, renderObject.vbo);
					//CheckCurrentAndBind(currentEbo, renderObject.ebo);
					//
					////CheckCurrentAndBind(currentBuffer, renderObject.buffer);
					//GFX::RenderCommand::DrawElements(renderObject.elementsCount, GFX::ElementDataType::Uint32, 1, renderObject.firstIndex, renderObject.baseVertex, meshInstanceIndex++);

					GFX::RenderCommand::DrawElements(
						renderObject.vbo, 
						renderObject.ebo, 
						renderObject.material,
						renderObject.elementsCount, 
						GFX::ElementDataType::Uint32, 
						1, 
						renderObject.firstIndex,
						renderObject.baseVertex, 
						meshInstanceIndex++
					);
				}

				Clear();
			}
		}

		struct Queue
		{
			std::vector<RenderObject> objects;
			bool sorted = false;
		}; 
		Queue queue;

		bool transparent = false;

		uint32_t drawCalls = 0;
	};
}