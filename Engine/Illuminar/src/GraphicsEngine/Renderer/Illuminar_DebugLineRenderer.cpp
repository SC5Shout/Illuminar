#include "Illuminar_pch.h"
#include "Illuminar_DebugLineRenderer.h"

namespace Illuminar {
	void DebugLineRenderer::Create()
	{
		/*GFX::BufferLayout layout = {
			{ GFX::DataType::Vector3, "a_position" },
			{ GFX::DataType::UnsignedByte4, "a_color", true },
		};

		get().vbo = GFX::VertexBuffer::Create(0, nullptr, GFX::Usage::Dynamic_Draw, layout);
		get().ebo = GFX::ElementBuffer::Create(0, nullptr, GFX::Usage::Dynamic_Draw);

		get().vao = GFX::VertexArray::Create(1, &get().vbo, get().ebo);

		get().shader = GFX::ShaderManager::AddShader("2dLine.glsl", "assets/shaders/2dLine.glsl");*/
	}

	void DebugLineRenderer::Begin(const Camera& camera, const Mat4& transform)
	{
		get().vertices.clear();
		get().elements.clear();
		get().currentVertexIndex = 0;

		/*get().shader->Bind();

		//TODO: material system
		GFX::RenderCommand::Submit([camera, transform]() {
			get().shader->UniformMatrix4(get().shader->getUniformLocation("u_viewProjection"), camera.projection * glm::inverse(transform));
		});*/
	}

	void DebugLineRenderer::End(float thickness)
	{
		/*get().vao->Bind();
		get().vbo->AddData(sizeof(LineVertex) * get().vertices.size(), get().vertices.data());
		get().ebo->AddData(sizeof(uint32_t) * get().elements.size(), get().elements.data());

		GFX::RenderCommand::setLineThickness(thickness);
		GFX::RenderCommand::DrawElements(GFX::DrawMode::Lines, get().elements.size(), GFX::DataType::UnsignedInt);*/
	}

	void DebugLineRenderer::PushTransform(const Mat4& transfrom)
	{
		get().transform = transfrom;
	}

	void DebugLineRenderer::PopTransform()
	{
		get().transform = Mat4(1.0f);
	}

	void DebugLineRenderer::DrawLine(const Vector3<float>& a, const Vector3<float>& b, uint32_t color)
	{
		const auto ta = get().transform * glm::vec4{ a.x, a.y, a.z, 1.0f };
		const auto tb = get().transform * glm::vec4{ b.x, b.y, b.z, 1.0f };

		get().vertices.push_back({ { ta.x, ta.y, ta.z }, color });
		get().vertices.push_back({ { tb.x, tb.y, tb.z }, color });

		get().elements.push_back(get().currentVertexIndex);
		get().elements.push_back(get().currentVertexIndex + 1);

		get().currentVertexIndex += 2;
	}

	void DebugLineRenderer::DrawFrustum(const SceneCamera& camera)
	{
		switch(camera.projectionType) {
			case SceneCamera::ProjectionType::Orthographic:
				{
					const auto bounds = camera.CalculateOrthographicsBounds();
					const float left = bounds.x;
					const float right = bounds.y;
					const float bottom = bounds.z;
					const float top = bounds.w;

					DrawLine({ right, top, 0.0f }, { right , bottom, 0.0f }, 0xffffffff);
					DrawLine({ right, bottom, 0.0f }, { left , bottom, 0.0f }, 0xffffffff);
					DrawLine({ left, bottom, 0.0f }, { left , top, 0.0f }, 0xffffffff);
					DrawLine({ left, top, 0.0f }, { right , top, 0.0f }, 0xffffffff);
				}
				break;
			case SceneCamera::ProjectionType::Perspective:
				break;
		}
	}

	void DebugLineRenderer::DrawFrustum(float aspectX, float aspectY, float nearZ, float farZ, float fov)
	{
		const float rad = Radians(fov);
		const float zRatio = farZ / nearZ;
		const float nearX = aspectX / 2.0f;
		const float nearY = aspectY / 2.0f;
		const float farX = nearX * zRatio;
		const float farY = nearY * zRatio;

		farZ *= -1;

		DrawLine({ -nearX, nearY, nearZ }, { -nearX, -nearY, nearZ }, 0xffffffff);
		DrawLine({ -nearX, -nearY, nearZ }, { nearX, -nearY, nearZ }, 0xffffffff);
		DrawLine({ nearX, -nearY, nearZ }, { nearX, nearY, nearZ }, 0xffffffff);
		DrawLine({ nearX, nearY, nearZ }, { -nearX, nearY, nearZ }, 0xffffffff);

		DrawLine({ -farX, farY, farZ }, { -farX, -farY, farZ }, 0xffffffff);
		DrawLine({ -farX, -farY, farZ }, { farX, -farY, farZ }, 0xffffffff);
		DrawLine({ farX, -farY, farZ }, { farX, farY, farZ }, 0xffffffff);
		DrawLine({ farX, farY, farZ }, { -farX, farY, farZ }, 0xffffffff);

		DrawLine({ -nearX, nearY, nearZ }, { -farX, farY, farZ }, 0xffffffff);
		DrawLine({ -nearX, -nearY, nearZ }, { -farX, -farY, farZ }, 0xffffffff);
		DrawLine({ nearX, -nearY, nearZ }, { farX, -farY, farZ }, 0xffffffff);
		DrawLine({ nearX, nearY, nearZ }, { farX, farY, farZ }, 0xffffffff);
	}
}