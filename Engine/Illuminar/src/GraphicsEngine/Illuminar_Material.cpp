#include "Illuminar_pch.h"
#include "Illuminar_Material.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar::GFX {
	MakeID Material::makeID;

	Material::Material(const CreateInfo& createInfo)
		: shader(createInfo.shader), name(createInfo.name), drawMode(createInfo.drawMode), castsShadow(true)
	{
		const auto& shaderBuffers = this->shader->getPushConstantBlocks();

		if (shaderBuffers.size() > 0) {
			const auto& buffer = shaderBuffers.begin()->second;
			uniformBuffer = UniformBlockBuffer(buffer.size);
		}

		materialDomain = createInfo.materialDomain;
		blendMode = createInfo.blendMode;
		shadingModel = createInfo.shadingModel;
		depthWrite = createInfo.depthWrite;

		rasterizerState.cullMode = createInfo.cullMode;
		rasterizerState.fillMode = createInfo.fillMode;
		rasterizerState.scissorEnabled = createInfo.scissorEnabled;
		rasterizerState.frontFace = createInfo.frontFace;

		switch (blendMode) {
			case BlendMode::Masked:
			case BlendMode::Opaque:
				blendState.sourceRGB = Blend::One;
				blendState.sourceAlpha = Blend::One;
				blendState.destinationRGB = Blend::Zero;
				blendState.destinationAlpha = Blend::Zero;
				depthState.mask = DepthMask::All;
				break;
			case BlendMode::Transparent:
			case BlendMode::Fade:
				//blendState.sourceRGB = Blend::One;
				//blendState.sourceAlpha = Blend::One;
				blendState.sourceRGB = Blend::Source_Alpha;
				blendState.sourceAlpha = Blend::Source_Alpha;
				blendState.destinationRGB = Blend::One_Minus_Source_Alpha;
				blendState.destinationAlpha = Blend::One_Minus_Source_Alpha;
				depthState.mask = DepthMask::Zero;
				break;
			case BlendMode::Additive:
				blendState.sourceRGB = Blend::One;
				blendState.sourceAlpha = Blend::One;
				blendState.destinationRGB = Blend::One;
				blendState.destinationAlpha = Blend::One;
				depthState.mask = DepthMask::Zero;
				break;
			case BlendMode::Multiply:
				blendState.sourceRGB = Blend::Zero;
				blendState.sourceAlpha = Blend::Zero;
				blendState.destinationRGB = Blend::Source_Color;
				blendState.destinationAlpha = Blend::Source_Color;
				depthState.mask = DepthMask::Zero;
				break;
			case BlendMode::Screen:
				blendState.sourceRGB = Blend::One;
				blendState.sourceAlpha = Blend::One;
				blendState.destinationRGB = Blend::One_Minus_Source_Color;
				blendState.destinationAlpha = Blend::One_Minus_Source_Color;
				depthState.mask = DepthMask::Zero;
				break;
		}

		switch (depthWrite) {
			case DepthWrite::Enabled: depthState.mask = DepthMask::All; break;
			case DepthWrite::Disabled: depthState.mask = DepthMask::Zero; break;
			case DepthWrite::Default: break;
		}
	}

	Ref<Material> Material::Create(const CreateInfo& createInfo)
	{
		auto result = Ref<Material>::Create(createInfo);
		makeID.CreateID(result->id);
		return result;
	}

	void Material::set(const std::string& name, TextureResource* texture)
	{
		const auto uniform = tryGetSampledUniform(name);
		if (!uniform) {
			EngineLogError("Material::set(): Cannot find {}", name);
			return;
		}
	
		uint32_t slot = uniform->binding;
		if (textureResources.size() <= slot) {
			textureResources.resize((size_t)slot + 1);
		} textureResources[slot] = texture;
	}
	
	void Material::set(const std::string& name, const Ref<Texture>& texture)
	{
		const auto uniform = tryGetSampledUniform(name);
		if (!uniform) {
			EngineLogError("Material::set(): Cannot find {}", name);
			return;
		}
	
		uint32_t slot = uniform->binding;
		if (textures.size() <= slot) {
			textures.resize((size_t)slot + 1);
		} textures[slot] = texture;
	}

	void Material::Bind() const
	{
		const auto& pushConstantBlocks = shader->getPushConstantBlocks();
		//
		shader->Bind();
		
		//opengl only :/
		using namespace GFX;
		if (pushConstantBlocks.size()) {
			const auto& buffer = pushConstantBlocks.begin()->second;
			for (const auto& [name, uniform] : buffer.uniforms) {
				switch (uniform.type) {
					case UniformType::Int:
						{
							int value = uniformBuffer.getUniform<int>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::Float:
						{
							float value = uniformBuffer.getUniform<float>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::UInt64:
						{
							uint64_t value = uniformBuffer.getUniform<uint64_t>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::Vec2:
						{
							const Vector2<float> value = uniformBuffer.getUniform<Vector2<float>>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::Vec3:
						{
							const Vector3<float> value = uniformBuffer.getUniform<Vector3<float>>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::Vec4:
						{
							const Vector4<float> value = uniformBuffer.getUniform<Vector4<float>>(uniform.offset);
							shader->setUniform(name, value);
						} break;
		
					case UniformType::Mat4:
						{
							const Mat4 value = uniformBuffer.getUniform<Mat4>(uniform.offset);
							shader->setUniform(name, value);
						} break;
				}
			}
		}
		
		for (size_t i = 0; i < textureResources.size(); ++i) {
			if (textureResources[i] && textureResources[i]->state == Illuminar::Resource::LoadingState::Loaded) {
				if (const auto texture = textureResources[i]->texture) {
					texture->Bind((uint32_t)i);
				}
			}
		}
		
		for (size_t i = 0; i < textures.size(); ++i) {
			if (const auto& texture = textures[i]) {
				texture->Bind((uint32_t)i);
			} else GFX::RenderCommand::getWhiteTexture()->Bind((uint32_t)i);
		}
	}

	const Uniform* Material::tryGetPushConstantUniform(const std::string& name) const
	{
		const auto& buffers = shader->getPushConstantBlocks();
		if (buffers.size()) {
			const auto& buffer = buffers.begin()->second;
			if (buffer.uniforms.find(name) == buffer.uniforms.end()) {
				return nullptr;
			} return &buffer.uniforms.at(name);
		} return nullptr;
	}

	const SampledUniform* Material::tryGetSampledUniform(const std::string& name) const
	{
		const auto& uniforms = shader->getSampledUniforms();
		for (const auto& [uniformName, uniform] : uniforms) {
			if (uniformName == name) {
				return &uniform;
			}
		} 
		return nullptr;
	}
}