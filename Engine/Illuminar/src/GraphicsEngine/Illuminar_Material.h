#pragma once

#include "Illuminar_Shader.h"
#include "Illuminar_GraphicsPipelineState.h"

#include "Uniform/Illuminar_UniformBlockBuffer.h"

#include "GameEngine/Resource/Texture/Illuminar_TextureResource.h"

namespace Illuminar::GFX {
	enum class MaterialDomain : uint8_t
	{
		Surface,
		PostProcess
	};

	enum class BlendMode : uint8_t
	{
		Opaque,
		Transparent,
		Additive,
		Masked,
		Fade,
		Multiply,
		Screen,
	};

	enum class ShadingModel : uint8_t
	{
		Unlit,
		Lit,
		Subsurface,
		Cloth,
	};

	enum struct DepthWrite : uint8_t
	{
		Default, //depth write is choosen based on a blend mode
		Enabled, //depth write is enabled
		Disabled //depth write is disabled
	};

	struct Material : RefCount
	{
		struct CreateInfo
		{
			Ref<Shader> shader = nullptr;
			MaterialDomain materialDomain = MaterialDomain::Surface;
			BlendMode blendMode = BlendMode::Opaque;
			ShadingModel shadingModel = ShadingModel::Lit;

			DepthWrite depthWrite = DepthWrite::Default;

			CullMode cullMode = CullMode::Back;
			FillMode fillMode = FillMode::Solid;
			FrontFace frontFace = FrontFace::CCW;
			bool scissorEnabled = false;
			DrawMode drawMode = DrawMode::Triangles;

			std::string_view name;
		};

		Material(const CreateInfo& createInfo);
		virtual ~Material() = default;

		[[nodiscard("Cannot discard Material create calls")]] static Ref<Material> Create(const CreateInfo& createInfo);

		template<SupportedUniformType T>
		inline void set(const std::string& name, const T& value)
		{
			const auto uniform = tryGetPushConstantUniform(name);
			if (!uniform) {
				EngineLogError("Material::set(): cannot find {}", name);
				return;
			}
		
			uniformBuffer.setUniform(uniform->offset, value);
		
			//for (auto& instance : materialInstances) {
			//	instance->OnMaterialValueUpdated(*uniform);
			//}
		}
		
		void set(const std::string& name, TextureResource* texture);
		void set(const std::string& name, const Ref<Texture>& texture);

		template<SupportedUniformType T>
		[[nodiscard]] inline T& get(const std::string& name)
		{
			const auto uniform = tryGetPushConstantUniform(name);
			if (!uniform) {
				EngineLogError("Material::tryGet(): cannot find {}", name);
				__debugbreak();
			}

			return uniformBuffer.getUniformRef<T>(uniform->offset);
		}

		template<SupportedUniformType T>
		[[nodiscard]] inline const T get(const std::string& name) const
		{
			const auto uniform = tryGetPushConstantUniform(name);
			if (!uniform) {
				EngineLogError("Material::tryGet(): cannot find {}", name);
				__debugbreak();
			}

			return uniformBuffer.getUniform<T>(uniform->offset);
		}

		template<SupportedUniformType T>
		[[nodiscard]] inline const T* tryGet(const std::string& name) const
		{
			const auto uniform = tryGetPushConstantUniform(name);
			if (!uniform) {
				EngineLogError("Material::tryGet(): cannot find {}", name);
				return nullptr;
			}

			return uniformBuffer.tryGetUniform<T>(uniform->offset);
		}

		void Bind() const;
		[[nodiscard]] inline uint16_t getID() const { return id; }

		[[nodiscard]] const Uniform* tryGetPushConstantUniform(const std::string& name) const;
		[[nodiscard]] const SampledUniform* tryGetSampledUniform(const std::string& name) const;

		UniformBlockBuffer uniformBuffer;

		std::vector<TextureResource*> textureResources;
		std::vector<Ref<Texture>> textures;

		Ref<Shader> shader = nullptr;

		MaterialDomain materialDomain = MaterialDomain::Surface;
		BlendMode blendMode = BlendMode::Opaque;
		ShadingModel shadingModel = ShadingModel::Lit;
		DepthWrite depthWrite = DepthWrite::Default;
		std::string name;

		RasterizerState rasterizerState;
		BlendState blendState;
		DepthState depthState;

		DrawMode drawMode;

		int castsShadow: 1;

		static MakeID makeID;
		uint16_t id = 0;
	};

	//temp
	struct MaterialCache
	{
		[[nodiscard]] static inline Ref<Material> Create(const Material::CreateInfo& createInfo)
		{
			if (get().materials.contains(std::string(createInfo.name))) {
				return get().materials[std::string(createInfo.name)];
			}

			auto result = Material::Create(createInfo);
			get().materials[std::string(createInfo.name)] = result;
			return result;
		}

		[[nodiscard]] static inline Ref<Material> getMaterial(const std::string& name)
		{
			if (get().materials.contains(name)) {
				return get().materials[name];
			}
			return nullptr;
		}

		static MaterialCache& get()
		{
			static MaterialCache instance;
			return instance;
		}

		std::unordered_map<std::string, Ref<Material>> materials;
	};
}