#pragma once

#include "Buffer/Illuminar_Buffer.h"
#include "Illuminar_GraphicsPipeline.h"
#include "Illuminar_Material.h"

namespace Illuminar::GFX {
	struct MaterialTypes
	{
		static constexpr const char* baseColor = "u_material.baseColor";
		static constexpr const char* useNormalMapUniform = "u_material.useNormalMap";
		static constexpr const char* metalnessUniform = "u_material.metalness";
		static constexpr const char* roughnessUniform = "u_material.roughness";
		static constexpr const char* emissionUniform = "u_material.emission";
		static constexpr const char* baseMapUniform = "u_baseTexture";
		static constexpr const char* normalMapUniform = "u_normalTexture";
		static constexpr const char* metalnessMapUniform = "u_metalnessTexture";
		static constexpr const char* roughnessMapUniform = "u_roughnessTexture";
	};

	struct MaterialList
	{
		MaterialList(size_t count = 1)
		{
			materials.resize(count);
		}

		~MaterialList() = default;

		void SetMaterial(uint32_t index, const Ref<Material>& material)
		{
			if (materials.size() > index) {
				materials[index] = material;
			}
			else {
				materials.resize((size_t)(index + 1));
				materials[index] = material;
			}
		}

		bool hasMaterial(size_t index)
		{
			return materials[index];
		}

		Ref<Material> getMaterial(size_t index)
		{
			assert(hasMaterial(index));
			return materials[index];
		}

		std::vector<Ref<Material>> materials;
	};

	struct Mesh;
	struct Submesh
	{
		Mat4 transform = Mat4(1.0f);

		std::string name;
		Mesh* parent = nullptr;
		Ref<Material> material = nullptr;

		uint32_t baseVertex = 0;
		uint32_t firstIndex = 0;
		uint32_t vertexCount = 0;
		uint32_t indexCount = 0;
	};

	struct Mesh : RefCount
	{
		static Ref<Mesh> Create() { return Ref<Mesh>::Create(); }

		Ref<VertexBuffer> vbo = nullptr;
		Ref<ElementBuffer> ebo = nullptr;

		std::vector<Submesh> submeshes;
		std::shared_ptr<MaterialList> materials;

		std::string name;
	};
}