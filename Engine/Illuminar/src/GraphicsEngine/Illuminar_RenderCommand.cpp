#include "Illuminar_pch.h"
#include "Illuminar_RenderCommand.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"
#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"
#include "GameEngine/Asset/Illuminar_AssetManager.h"

#include "GameEngine/Window/Illuminar_Window.h"

#include "Illuminar_Shader.h"
#include "Illuminar_Material.h"
#include "Illuminar_GraphicsPipelineCache.h"

namespace Illuminar::GFX {
	void RenderCommand::Init(const Ref<Window>& window)
	{
		Submit([window]() {
			get().renderer->Init(window);
		});

		struct ShaderPack
		{
			AssetID assetID;
			std::string name;
			ShaderResourceID id;
		};
		std::vector<ShaderPack> shaders;
		auto& defaultShaders = AssetManager::AddAssetPackage("DefaultShaders");
		for (const auto& entry : std::filesystem::recursive_directory_iterator("assets/shaders")) {
			if (std::filesystem::path(entry).extension() == ".glsl") {
				std::string id = entry.path().filename().string();
				std::string name = entry.path().filename().stem().string();
		
				defaultShaders.AddAsset(entry.path().filename().string(), entry.path().generic_string().c_str());
				shaders.emplace_back(id, name, 0);
			}
		}
		
		auto& shaderManager = ResourceManagers::get().shaderResourceManager;
		for (auto& [assetID, name, id] : shaders) {
			id = shaderManager.LoadShaderResourceByAssetID(assetID);
			ShaderResourceCache::AddExistedShader("name", id);
		}
		
		ResourceStreamer::FlushAllQueues();
		
		for (auto [assetID, name, id] : shaders) {
			ShaderCache::AddExistedShader(name, shaderManager.getResourceByID(id).shader);
		}
	}

	bool RenderCommand::BeginFrame()
	{
		//Wait for the current frame to become available
		if (get().frameSkipper.Acquire()) {
			return true;
		}
	
		//if the current frame is busy, flush the command buffer, because the render thread has to dispatch as least Fence::Wait() command
		//Fence::Wait() will update the fence's status and the next frame will be able to check if it's ready to render
		Flush();
		return false;
	}
	
	void RenderCommand::EndFrame()
	{
		//mark this frame as available, reset the fence and create a new one
		get().frameSkipper.Release();
		Flush();
	}

	void RenderCommand::Clear(BufferBit clearBit)
	{
		Submit([clearBit]() {
			get().renderer->Clear(clearBit);
		});
	}

	void RenderCommand::setViewport(int x, int y, uint32_t w, uint32_t h)
	{
		Submit([x, y, w, h]() {
			get().renderer->setViewport(x, y, w, h);
		});
	}

	void RenderCommand::setScissor(int x, int y, int w, int h)
	{
		Submit([x, y, w, h]() {
			get().renderer->setScissor(x, y, w, h);
		});
	}

	void RenderCommand::setClearColor(uint32_t color)
	{
		Submit([color]() {
			get().renderer->setClearColor(color);
		});
	}

	void RenderCommand::setClearColor(float r, float g, float b, float a)
	{
		Submit([r, g, b, a]() {
			get().renderer->setClearColor(r, g, b, a);
		});
	}

	void RenderCommand::setLineThickness(float thickness)
	{
		Submit([thickness]() {
			get().renderer->setLineThickness(thickness);
		});
	}

	void RenderCommand::DrawArrays(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance)
	{
		Submit([vertexCount, instanceCount, firstVertex, firstInstance]() {
			get().renderer->DrawArrays(vertexCount, instanceCount, firstVertex, firstInstance);
		});
	}

	void RenderCommand::DrawElements(uint32_t elementCount, ElementDataType dataType, uint32_t instanceCount, uint32_t firstElement, int32_t vertexOffset, uint32_t firstInstance)
	{
		Submit([elementCount, dataType, instanceCount, firstElement, vertexOffset, firstInstance]() {
			get().renderer->DrawElements(elementCount, dataType, instanceCount, firstElement, vertexOffset, firstInstance);
		});
	}

	void RenderCommand::DrawElementsIndirect(ElementDataType dataType, uint32_t drawCount, uint64_t indirectBufferOffset)
	{
		Submit([dataType, drawCount, indirectBufferOffset]() {
			get().renderer->DrawElementsIndirect(dataType, drawCount, indirectBufferOffset);
		});
	}

	void RenderCommand::DispatchCompute(uint32_t groupX, uint32_t groupY, uint32_t groupZ)
	{
		Submit([groupX, groupY, groupZ]() {
			get().renderer->DispatchCompute(groupX, groupY, groupZ);
		});
	}

	void RenderCommand::Draw(Renderable renderable, const Ref<Material>& material)
	{
		auto vbo = renderable.vbo;
		auto ebo = renderable.ebo;
		auto ibo = renderable.ibo;
		auto pipeline = renderable.pipeline;

		if (!pipeline) {
			GraphicsPipeline::CreateInfo info;
			info.bufferLayout = vbo->getLayout();
			info.shader = material->shader;
			info.rasterizerState = material->rasterizerState;
			info.blendState = material->blendState;
			info.depthState = material->depthState;
			info.drawMode = renderable.drawMode;

			pipeline = GraphicsPipelineCache::Create(info);
		}
		pipeline->Bind();
		if(material) material->Bind();
		vbo->Bind();
		ebo->Bind();

		if (ibo) {
			ibo->Bind();
		} else {
			DrawElements(renderable.count, GFX::ElementDataType::Uint32, renderable.instanceCount, renderable.firstIndex, renderable.baseVertex, renderable.baseInstance);
		}
	}

	void RenderCommand::DrawElements(
		const Ref<VertexBuffer>& vbo, 
		const Ref<ElementBuffer>& ebo, 
		const Ref<Material>& material, 
		uint32_t elementCount, 
		ElementDataType dataType, 
		uint32_t instanceCount, 
		uint32_t firstElement, 
		int32_t vertexOffset, 
		uint32_t firstInstance) 
	{
		GraphicsPipeline::CreateInfo info;
		info.bufferLayout = vbo->getLayout();
		info.shader = material->shader;
		info.rasterizerState = material->rasterizerState;
		info.blendState = material->blendState;
		info.depthState = material->depthState;
		info.drawMode = material->drawMode;

		auto pipeline = GraphicsPipelineCache::Create(info);

		if (!pipeline) {
			return;
		}

		pipeline->Bind();
		if(material)
			material->Bind();
		vbo->Bind();
		ebo->Bind();

		DrawElements(elementCount, dataType, instanceCount, firstElement, vertexOffset, firstInstance);
	}

	void RenderCommand::DrawArrays(const Ref<VertexBuffer>& vbo, const Ref<Material>& material, uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance)
	{
		GraphicsPipeline::CreateInfo info;
		info.bufferLayout = vbo->getLayout();
		info.shader = material->shader;
		info.rasterizerState = material->rasterizerState;
		info.blendState = material->blendState;
		info.depthState = material->depthState;
		info.drawMode = material->drawMode;

		auto pipeline = GraphicsPipelineCache::Create(info);

		if (!pipeline) {
			return;
		}

		pipeline->Bind();
		material->Bind();
		vbo->Bind();

		DrawArrays(vertexCount, instanceCount, firstVertex, firstInstance);
	}

	void RenderCommand::MemoryBarrier(MemoryBarrierType barrier)
	{
		Submit([barrier]() {
			get().renderer->MemoryBarrierX(barrier);
		});
	}

	void RenderCommand::DrawFullScreenQuad(const Ref<Shader>& shader, const Ref<Material>& material)
	{
		static constexpr float fullscreenQuad[] = {
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f,	0.0f, 0.0f,
			 1.0f, -1.0f, 0.0f,	1.0f, 0.0f,
			 1.0f,  1.0f, 0.0f,	1.0f, 1.0f,
		};

		static auto vbo = VertexBuffer::Create({ { DataType::RGB32_FLOAT, "a_position" }, {DataType::RG32_FLOAT, "a_uv"} }, sizeof(fullscreenQuad), fullscreenQuad, Usage::Static_Draw);

		RenderCommand::DrawArrays(vbo, material, 4, 1, 0, 0);
	}
}