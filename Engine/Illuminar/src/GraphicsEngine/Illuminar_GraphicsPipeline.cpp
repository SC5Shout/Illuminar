#include "Illuminar_pch.h"
#include "Illuminar_GraphicsPipeline.h"

#include "Illuminar_Renderer.h"
#include "OpenGL/Illuminar_GLGraphicsPipeline.h"

namespace Illuminar::GFX {
	MakeID GraphicsPipeline::makeID;

	Ref<GraphicsPipeline> GraphicsPipeline::Create(const CreateInfo& createInfo)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			auto result = Ref<GLGraphicsPipeline>::Create(createInfo);
			makeID.CreateID(result->id);
			return result;
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		}
	}
}