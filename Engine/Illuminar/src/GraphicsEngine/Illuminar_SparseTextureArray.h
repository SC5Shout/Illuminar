#pragma once

#include <numeric>
#include "Illuminar_Texture.h"

#include "glad/glad.h"

namespace Illuminar {
	struct TextureAddress
	{
		Ref<GFX::Texture2DArray> texture = nullptr;
		float page = 0.0f;
		int reserved = 0;
	};

	struct Texture2DContainer
	{
		Texture2DContainer(bool sparse, const GFX::TextureParameters& params, uint32_t width, uint32_t height, uint32_t slices);
		~Texture2DContainer() = default;

		void Image3D(int level, int offsetX, int offsetY, int offsetZ, int w, int h, int depth, const void* data);

		[[nodiscard]] inline bool hasRoom() const { return freeList.size() > 0; }

		[[nodiscard]] inline uint32_t VirtualAlloc()
		{
			uint32_t retVal = freeList.front();
			freeList.pop();
			return retVal;
		}

		void VirtualFree(uint32_t slice)
		{
			freeList.push(slice);
		}

		std::queue<uint32_t> freeList;

		const uint32_t width;
		const uint32_t height;
		const uint32_t slices;

		Ref<GFX::Texture2DArray> texture;
	};

	struct SparseTexture2D
	{
		SparseTexture2D(const std::shared_ptr<Texture2DContainer>& container, uint32_t sliceNum)
			: container(container), sliceNum(sliceNum)
		{
		}

		inline void TextureImage2D(uint32_t w, uint32_t h, const void* data)
		{
			container->Image3D(0, 0, 0, getSliceNum(), w, h, 1, data);
		}

		[[nodiscard]] inline const std::shared_ptr<Texture2DContainer>& getTexture2DContainer() const { return container; }
		[[nodiscard]] inline GLsizei getSliceNum() const { return (GLsizei)sliceNum; }

		[[nodiscard]] inline TextureAddress getAddress() const
		{
			return TextureAddress{ container->texture, (float)sliceNum, 0 };
		}

		[[nodiscard]] inline Ref<GFX::Texture2DArray> getTexture() const { return container->texture; }

		std::shared_ptr<Texture2DContainer> container;
		uint32_t sliceNum;
	};

	struct SparseTextureManager
	{
		std::shared_ptr<SparseTexture2D> Create(uint32_t w, uint32_t h, GFX::TextureParameters params);

		std::shared_ptr<SparseTexture2D> CreateTexture2D(uint32_t w, uint32_t h, const void* pixels, const GFX::TextureParameters& params)
		{
			std::shared_ptr<SparseTexture2D> texture = Create(w, h, params);
			texture->TextureImage2D(w, h, pixels);
			return texture;
		}

		bool sparse = false;

		std::map<std::tuple<uint32_t, uint32_t, GFX::TextureParameters>, std::vector<std::shared_ptr<Texture2DContainer>>> texArrays2D;
	};
}
