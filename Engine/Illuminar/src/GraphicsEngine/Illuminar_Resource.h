#pragma once

namespace Illuminar::GFX {
	struct Resource : RefCount
	{
		Resource() = default;
		virtual ~Resource() = default;

		//Resource(const Resource& other) = delete;
		//Resource& operator =(const Resource& other) = delete;
	};
}