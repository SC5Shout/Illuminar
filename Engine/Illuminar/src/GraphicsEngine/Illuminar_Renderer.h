#pragma once

#include "Illuminar_DataType.h"

namespace Illuminar {
	struct Window;
}
namespace Illuminar::GFX {
	enum struct BufferBit : uint8_t
	{
		Color = BIT(1),
		Depth = BIT(2),
		Stencil = BIT(3)
	};

	enum struct DrawMode : uint8_t
	{
		Points = 0,
		Triangles = 1,
		Patches = 2,
		TriangleStrip = 3,
		TriangleFan = 4,
		Lines = 5,
		LineLoop = 6,
		LineStrip = 7
	};

	//enum struct BufferMode : uint8_t
	//{
	//	None = BIT(0),
	//	Front_Left = BIT(1),
	//	Front_Right = BIT(2),
	//	Back_Left = BIT(3),
	//	Back_Right = BIT(4),
	//	Front = BIT(5),
	//	Back = BIT(6),
	//	Left = BIT(7),
	//	Right = BIT(8),
	//	Front_And_Back = BIT(9)
	//};

	enum struct MemoryBarrierType : uint8_t
	{
		All = BIT(0),
		CommandBarrier = BIT(1),
		ShaderStorage = BIT(2),
		ShaderImageAccess = BIT(3),
		TextureFetch = BIT(4),
	};

	struct DrawArraysIndirectCommand
	{
		uint32_t count;
		uint32_t instanceCount;
		uint32_t first;
		uint32_t baseInstance;
	};

	struct DrawElementsIndirectCommand
	{
		uint32_t count;
		uint32_t instanceCount;
		uint32_t firstIndex;
		uint32_t baseVertex;
		uint32_t baseInstance;
	};

	struct VertexBuffer;
	struct ElementBuffer;
	struct Material;
	struct IndirectDrawBuffer;
	struct GraphicsPipeline;

	struct Renderable
	{
		Ref<VertexBuffer> vbo;
		Ref<ElementBuffer> ebo;
		Ref<GraphicsPipeline> pipeline = nullptr;
		Ref<IndirectDrawBuffer> ibo;

		union {
			struct { DrawElementsIndirectCommand drawCmd; };
			struct {
				uint32_t count;
				uint32_t instanceCount;
				uint32_t firstIndex;
				uint32_t baseVertex;
				uint32_t baseInstance;
			};
		};

		DrawMode drawMode;
	};

	struct Renderer : RefCount
	{
		enum struct API
		{
			OpenGL,
			Vulkan
		};

		static constexpr API api = API::OpenGL;

		[[nodiscard]] static Ref<Renderer> Create();

		Renderer() = default;
		virtual ~Renderer() = default;

		Renderer(const Renderer& other) = delete;
		Renderer& operator=(const Renderer& other) = delete;

		virtual void Init(const Ref<Window>& window) = 0;

		virtual void Clear(BufferBit clearBit) = 0;
		virtual void setViewport(int x, int y, uint32_t w, uint32_t h) = 0;
		virtual void setScissor(int x, int y, uint32_t w, uint32_t h) = 0;

		virtual void setClearColor(uint32_t color) = 0;
		virtual void setClearColor(float r, float g, float b, float a) = 0;

		virtual void setLineThickness(float thickness) = 0;

		virtual void DrawArrays(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const = 0;
		virtual void DrawElements(uint32_t indexCount, ElementDataType dataType, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset, uint32_t firstInstance) const = 0;
		virtual void DrawElementsIndirect(ElementDataType dataType, uint32_t drawCount, uint64_t indirectBufferOffset) const = 0;

		virtual void DispatchCompute(uint32_t groupX, uint32_t groupY, uint32_t groupZ) const = 0;

		//very weird because the compiler gives this error when the function is called MemoryBarrier
		//Error	C3668	'Illuminar::GFX::GLRenderer::__faststorefence': method with override specifier 'override' did not override any base class methods
		virtual void MemoryBarrierX(MemoryBarrierType barrier) const = 0;
	};
}