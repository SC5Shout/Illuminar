#pragma once

#include "Illuminar_Resource.h"

namespace Illuminar::GFX {
	enum struct SamplerType : uint8_t
	{
		TEXTURE_NULL,
		TEXTURE_2D,
		TEXTURE_3D,
		TEXTURE_CUBEMAP
	};

	//leave these comments, we might need em later
	enum struct TextureFormat : uint8_t
	{
		//R8_SRGB,
		R8_INT,
		//R8_FLOAT,
		R32_FLOAT,

		RGB8,
		RGBA8_SRGB,
		//RGBA8_FLOAT,

		RGB32_FLOAT,
		RGBA32_FLOAT,

		D24_S8,
		D32_FLOAT,
		S8_UINT

		//RED16,
		//RGB8,
		//RGB32F,
		//RGBA8,
		//BGRA8,
		//R11_G11_B10F,
		//RGBA16F,
		//RGBA32F,
		//
		//BC1,
		//BC1_SRGB,
		//BC2,
		//BC2_SRGB,
		//BC3,
		//BC3_SRGB,
		//BC4,
		//BC5,
		//ETC1,
		//
		//RED32_UINT,
		//RED32_FLOAT,
	};

	[[nodiscard]] static inline uint32_t TextureFormatToBytesPerPixels(TextureFormat format)
	{
		switch (format) {
			case TextureFormat::R8_INT:
			case TextureFormat::RGBA8_SRGB:
			case TextureFormat::S8_UINT:
				//case TextureFormat::RGBA8: 
				//case TextureFormat::SRGBA8:
				//case TextureFormat::BGRA8: 
				return 1;

				//case TextureFormat::RED16: 
				//case TextureFormat::RGBA16F: 
				//	return 2;

				//case TextureFormat::RGB32F:
				//case TextureFormat::R11_G11_B10F: 	
			case TextureFormat::RGBA32_FLOAT:
				//case TextureFormat::RED32_UINT:
				//case TextureFormat::RED32_FLOAT:
			case TextureFormat::D32_FLOAT:
			case TextureFormat::D24_S8:
				return 4;

				//case BC1: return 0;
				//case BC1_SRGB: return 0;
				//case BC2: return 0;
				//case BC2_SRGB: return 0;
				//case BC3: return 0;
				//case BC3_SRGB: return 0;
				//case BC4: return 0;
				//case BC5: return 0;
				//case ETC1: return 0;
		}
	}

	enum struct TextureFilter : uint8_t
	{
		None = 0,
		Linear = 1,
		Nearest = 2,
		Nearest_Mipmap_Linear = 3,
		Nearest_Mipmap_Nearest = 4,
		Linear_Mipmap_Nearest = 5,
		Linear_Mipmap_Linear = 6
	};

	enum struct TextureWrap : uint8_t
	{
		None = 0,
		Repeat = 1,
		Clamp = 2,
		Mirrored_Repeat = 3,
		Clamp_To_Edge = 4,
		Clamp_To_Border = 5,
		Mirror_Clamp_To_Edge = 6,
	};

	struct TextureParameters
	{
		TextureParameters& Format(TextureFormat format)
		{
			this->format = format;
			return *this;
		}

		TextureParameters& FilterMAG(TextureFilter mag)
		{
			filterMAG = mag;
			return *this;
		}

		TextureParameters& FilterMIN(TextureFilter min)
		{
			filterMIN = min;
			return *this;
		}

		TextureParameters& Wrap(TextureWrap wrap)
		{
			this->wrap = wrap;
			return *this;
		}

		TextureParameters& Miplevel(uint32_t miplevel)
		{
			this->miplevel = miplevel;
			return *this;
		}

		auto operator<=>(const TextureParameters&) const = default;

		TextureFormat format = TextureFormat::RGBA8_SRGB;

		TextureFilter filterMAG = TextureFilter::Linear;
		TextureFilter filterMIN = TextureFilter::Linear_Mipmap_Linear;

		TextureWrap wrap = TextureWrap::Repeat;

		uint32_t miplevel = 0;

		[[nodiscard]] inline size_t Hash() const
		{
			std::array<uint32_t, 5> values = {
				(uint32_t)format,
				(uint32_t)filterMAG,
				(uint32_t)filterMIN,
				(uint32_t)wrap,
				(uint32_t)miplevel
			};
			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	enum struct ImageMapBit
	{
		Read = BIT(1),
		Write = BIT(2),
		ReadWrite = BIT(3)
	};

	struct Texture : Resource
	{
		Texture() = default;
		virtual ~Texture() = default;

		Texture(const Texture& other) = delete;
		Texture& operator=(const Texture& other) = delete;

		virtual void Bind(uint32_t slot = 0) const = 0;
		virtual void BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered = false, uint32_t layer = 0) const = 0;
		virtual void GenerateMipmap() = 0;

		[[nodiscard]] virtual Handle<uint32_t> getTexture() const = 0;
		[[nodiscard]] virtual uint32_t getTarget() const = 0;

		[[nodiscard]] virtual SamplerType getSamplerType() const = 0;
	};

	struct Texture2D : Texture
	{
		struct CreateInfo
		{
			std::string_view name;

			uint32_t w = 0;
			uint32_t h = 0;
			const void* pixels = nullptr;
			TextureParameters parameters;

			//todo: rename/remove?/change
			bool shadow = false;

			[[nodiscard]] inline size_t Hash() const
			{
				std::array<uint64_t, 4> values = {
					//std::hash<std::string_view>{}(name),
					std::hash<uint32_t>{}(w),
					std::hash<uint32_t>{}(h),
					parameters.Hash(),
					std::hash<bool>{}(shadow)
				};

				return std::_Hash_array_representation(values.data(), values.size());
			}
		};

		Texture2D() = default;
		virtual ~Texture2D() override = default;

		Texture2D(const Texture2D& other) = delete;
		Texture2D& operator=(const Texture2D& other) = delete;

		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Texture2D> Create();
		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Texture2D> Create(const CreateInfo& createInfo);

		//for imgui texture
		virtual void setTextureHandle(void* handle) = 0;

		virtual void UpdateData(const void* pixels) = 0;
		virtual void Resize(uint32_t width, uint32_t height) = 0;

		[[nodiscard]] virtual uint32_t getWidth() const = 0;
		[[nodiscard]] virtual uint32_t getHeight() const = 0;

		[[nodiscard]] inline SamplerType getSamplerType() const { return SamplerType::TEXTURE_2D; }

		[[nodiscard]] virtual CreateInfo getInfo() const = 0;
	};

	struct Texture2DArray : Texture
	{
		struct CreateInfo
		{
			std::string_view name;

			uint32_t w = 0;
			uint32_t h = 0;
			uint32_t depth = 0;
			const void* pixels = nullptr;
			TextureParameters parameters;

			//todo: rename/remove?/change
			bool shadow = false;

			constexpr auto operator<(const CreateInfo& other) const {
				return w > other.w &&
					h > other.w &&
					depth > other.w &&
					parameters > other.parameters;
			}

			[[nodiscard]] inline size_t Hash() const
			{
				std::array<uint64_t, 6> values = {
					//std::hash<std::string_view>{}(name),
					std::hash<uint32_t>{}(w),
					std::hash<uint32_t>{}(h),
					std::hash<uint32_t>{}(depth),
					parameters.Hash(),
					std::hash<bool>{}(shadow)
				};

				return std::_Hash_array_representation(values.data(), values.size());
			}
		};

		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Texture2DArray> Create(const CreateInfo& createInfo);

		virtual void AddData(uint32_t layer, uint32_t size, const void* data) = 0;

		[[nodiscard]] virtual CreateInfo getInfo() const = 0;

		[[nodiscard]] inline SamplerType getSamplerType() const { return SamplerType::TEXTURE_3D; }
	};

	struct TextureCubemap : Texture
	{
		struct CreateInfo
		{
			static constexpr uint8_t MAX_FACE_COUNT = 6;

			struct PixelsWithOffsets
			{
				const void* pixels = nullptr;
				std::array<uint32_t, MAX_FACE_COUNT> offsets{ 0 };

				const void* const operator[](size_t faceIndex) const
				{
					return (const char*)pixels + offsets[faceIndex];
				}
			};

			struct FacesPixels
			{
				std::array<void*, MAX_FACE_COUNT> pixels{ nullptr };

				const void* operator[](size_t i) const
				{
					return pixels[i];
				}

				void*& operator[](size_t i)
				{
					return pixels[i];
				}
			};

			std::string_view name;
			uint32_t w = 0;
			uint32_t h = 0;
			std::variant<PixelsWithOffsets, FacesPixels> data;
			TextureParameters parameters;
		};

		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<TextureCubemap> Create(const CreateInfo& createInfo);

		[[nodiscard]] virtual CreateInfo getInfo() const = 0;

		[[nodiscard]] inline SamplerType getSamplerType() const { return SamplerType::TEXTURE_CUBEMAP; }
	};
}