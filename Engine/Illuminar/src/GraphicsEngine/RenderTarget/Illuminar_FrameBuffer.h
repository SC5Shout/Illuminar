#pragma once

#include "Illuminar_RenderTarget.h"
#include "GraphicsEngine/Illuminar_Texture.h"
#include "GraphicsEngine/Illuminar_GraphicsPipelineState.h"

namespace Illuminar::GFX {
	enum struct FrameBufferTarget
	{
		Read = 1,
		Draw = 2,
		ReadAndDraw = 3
	};

	enum struct DrawBuffers
	{
		None = 0,
		Front_Left = 1,
		Front_Right = 2,
		Back_Left = 3,
		Back_Right = 4,
		Color_Attachment = 5
	};

	/*
	
	struct FrameBufferAttachments
	{
		static constexpr uint8_t MIN_COLOR_ATTACHMENT_COUNT = 0;
		static constexpr uint8_t MAX_COLOR_ATTACHMENT_COUNT = 8;

		static constexpr uint8_t MIN_DEPTH_STENCIL_ATTACHMENT_COUNT = 0;
		static constexpr uint8_t MIN_DEPTH_STENCIL_ATTACHMENT_COUNT = 1;

		static constexpr uint8_t MIN_DEPTH_ATTACHMENT_COUNT = 0;
		static constexpr uint8_t MIN_DEPTH_ATTACHMENT_COUNT = 1;

		static constexpr uint8_t MIN_STENCIL_ATTACHMENT_COUNT = 0;
		static constexpr uint8_t MIN_STENCIL_ATTACHMENT_COUNT = 1;

		static constexpr uint8_t MAX_ATTACHMENTS_COUNT = MAX_COLOR_ATTACHMENT_COUNT + MIN_DEPTH_STENCIL_ATTACHMENT_COUNT + MIN_DEPTH_ATTACHMENT_COUNT + MIN_STENCIL_ATTACHMENT_COUNT;

		FrameBufferAttachments() = default;
		~FrameBufferAttachments() = default;
		FrameBufferAttachments(const std::initializer_list<Ref<GFX::Texture>>& attachments)
			: attachments(attachments) {}

		FrameBufferAttachments(auto ... attachments)
		{
			(attachments.push_back(attachments), ...);
		}

		std::vector<Ref<GFX::Texture>> attachments;
	};

	*/

	struct FrameBuffer : RenderTarget
	{
		struct CreateInfo
		{
			static constexpr uint8_t MIN_COLOR_ATTACHMENT_COUNT = 0;
			static constexpr uint8_t MAX_COLOR_ATTACHMENT_COUNT = 32;

			uint8_t colorAttachmentCount = MIN_COLOR_ATTACHMENT_COUNT;
			std::array<Ref<GFX::Texture>, MAX_COLOR_ATTACHMENT_COUNT> colorAttachments;
			Ref<GFX::Texture> depthStencilTexture = nullptr;
			FrameBufferTarget target = FrameBufferTarget::ReadAndDraw;
		};

		struct State
		{
			ViewportState viewport;
			uint32_t clearColor = 0xffffffff;
			double clearDepth = 0.0;
			uint32_t clearStencil = 0;
		};

		FrameBuffer() = default;
		virtual ~FrameBuffer() override = default;

		FrameBuffer(const FrameBuffer& other) = delete;
		FrameBuffer& operator =(const FrameBuffer& other) = delete;

		static Ref<FrameBuffer> Create(const CreateInfo& createInfo);

		virtual void Bind(const State& params) const = 0;
		virtual void Unbind() const override = 0;

		virtual void Resize(uint32_t w, uint32_t h) = 0;

		virtual void Blit(const Ref<FrameBuffer>& other) = 0;
	};
}