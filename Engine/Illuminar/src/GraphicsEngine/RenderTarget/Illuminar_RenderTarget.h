#pragma once

#include "GraphicsEngine/Illuminar_Resource.h"

namespace Illuminar::GFX {
	struct RenderTarget : Resource
	{
		RenderTarget() = default;
		virtual ~RenderTarget() override = default;

		RenderTarget(const RenderTarget& other) = delete;
		RenderTarget& operator =(const RenderTarget& other) = delete;

		virtual void Bind() const {}
		virtual void Unbind() const = 0;
	};
}