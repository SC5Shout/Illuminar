#include "Illuminar_pch.h"
#include "Illuminar_FrameBuffer.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLFrameBuffer.h"

#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar::GFX {
	Ref<FrameBuffer> FrameBuffer::Create(const CreateInfo& createInfo)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLFrameBuffer>::Create(createInfo);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}
