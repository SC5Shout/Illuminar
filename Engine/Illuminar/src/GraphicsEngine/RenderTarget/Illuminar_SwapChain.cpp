#include "Illuminar_pch.h"
#include "Illuminar_SwapChain.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLSwapChain.h"

#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar::GFX {
	Ref<SwapChain> SwapChain::Create()
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLSwapChain>::Create();
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}