#pragma once

#include "Illuminar_RenderTarget.h"

namespace Illuminar {
	struct Window;
}

namespace Illuminar::GFX {
	struct SwapChain : RenderTarget
	{
		SwapChain() = default;
		virtual ~SwapChain() override = default;

		SwapChain(const SwapChain& other) = delete;
		SwapChain& operator =(const SwapChain& other) = delete;

		static Ref<SwapChain> Create();

		void Bind() const override = 0;
		void Unbind() const override {}

		virtual void Present(const Ref<Window>& window) const = 0;
	};
}