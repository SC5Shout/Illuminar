#pragma once

#include "Illuminar_Resource.h"

namespace Illuminar {
	namespace GFX {
		enum struct FenceStatus : int8_t
		{
#undef ERROR
			Error = -1,
			Signaled = 0,
			NotSignaled = 1,
		};

		struct Fence : Resource
		{
			virtual ~Fence() override = default;

			[[nodiscard]] static Ref<Fence> Create();

			[[nodiscard]] virtual void Wait() = 0;

			[[nodiscard]] virtual FenceStatus getStatus() = 0;
		};
	}
}