#include "Illuminar_pch.h"
#include "Illuminar_BufferLayout.h"

namespace Illuminar {
	namespace GFX {
		uint32_t BufferElement::getComponentCount() const
		{
			switch (type) {
				case DataType::R32_INT: return 1;
				case DataType::R8_INT: return 1;
				case DataType::R32_FLOAT: return 1;

				case DataType::R32_UINT: return 1;
				case DataType::R8_UINT: return 1;
				case DataType::RGBA8_UNORM: return 4;

				case DataType::RG32_FLOAT: return 2;
				case DataType::RGB32_FLOAT: return 3;
				case DataType::RGBA32_FLOAT: return 4;

				case DataType::RG32_INT: return 2;
				case DataType::RGB32_INT: return 3;
				case DataType::RGBA32_INT: return 4;

				case DataType::Mat3: return 3 * 3;
				case DataType::Mat4: return 4 * 4;
			} return 0;
		}

		void BufferLayout::CalculateOffsetAndStride()
		{
			offset = 0;
			stride = 0;
			for (auto& element : elements) {
				element.offset = offset;
				offset += element.size;
				stride += element.size;
			}
		}
	}
}