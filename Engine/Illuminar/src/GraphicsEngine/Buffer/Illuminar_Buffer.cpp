#include "Illuminar_pch.h"
#include "GraphicsEngine/OpenGL/Illuminar_GLBuffer.h"
//#include "GraphicsEngine/Vulkan/Illuminar_VkBuffer.h"

#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar::GFX {
	Ref<BufferResource> BufferResource::Create(uint64_t size, const void* data, Usage usage)
	{
		return Ref<GLBufferResource>::Create(size, data, usage);
	}

	Ref<BufferResource> BufferResource::Create(uint64_t size, const void* data, MapBit mapBit)
	{
		return Ref<GLBufferResource>::Create(size, data, mapBit);
	}

	Ref<VertexBuffer> VertexBuffer::Create(const BufferLayout& layout, uint64_t size, const void* data, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLVertexBuffer>::Create(layout, size, data, usage);
		} else {
			//return Ref<VkVertexBuffer>::MakeRef(size, data, usage);
		} return nullptr;
	}

	Ref<VertexBuffer> VertexBuffer::Create(const BufferLayout& layout, uint64_t size, const void* data, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLVertexBuffer>::Create(layout, size, data, mapBit);
		} else {
			//return Ref<VkVertexBuffer>::MakeRef(size, data, mapBit);
		} return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------------------

	Ref<ElementBuffer> ElementBuffer::Create(uint64_t size, const void* data, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLElementBuffer>::Create(size, data, usage);
		} else {
			//return Ref<VkElementBuffer>::MakeRef(size, data, usage);
		} return nullptr;
	}

	Ref<ElementBuffer> ElementBuffer::Create(uint64_t size, const void* data, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLElementBuffer>::Create(size, data, mapBit);
		} else {
			//return Ref<VkElementBuffer>::MakeRef(size, data, mapBit);
		} return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------------------

	Ref<TextureBuffer> TextureBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTextureBuffer>::Create(size, data, bindingPoint, usage);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<TextureBuffer> TextureBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLTextureBuffer>::Create(size, data, bindingPoint, mapBit);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------------------

	Ref<UniformBuffer> UniformBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLUniformBuffer>::Create(size, data, bindingPoint, usage);
		} else {
			//return Ref<VkUniformBuffer>::MakeRef(size, data, bindingPoint, usage);
		} return nullptr;
	}

	Ref<UniformBuffer> UniformBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLUniformBuffer>::Create(size, data, bindingPoint, mapBit);
		} else {
			//return Ref<VkUniformBuffer>::MakeRef(size, data, bindingPoint, mapBit);
		} return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------------------

	Ref<ShaderStorageBuffer> ShaderStorageBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLShaderStorageBuffer>::Create(size, data, bindingPoint, usage);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<ShaderStorageBuffer> ShaderStorageBuffer::Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLShaderStorageBuffer>::Create(size, data, bindingPoint, mapBit);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<PixelBuffer> PixelBuffer::Create(uint64_t size, const void* data, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLPixelBuffer>::Create(size, data, usage);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<PixelBuffer> PixelBuffer::Create(uint64_t size, const void* data, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLPixelBuffer>::Create(size, data, mapBit);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------------------

	Ref<IndirectDrawBuffer> IndirectDrawBuffer::Create(uint64_t size, const void* data, Usage usage)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLIndirectDrawBuffer>::Create(size, data, usage);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<IndirectDrawBuffer> IndirectDrawBuffer::Create(uint64_t size, const void* data, MapBit mapBit)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLIndirectDrawBuffer>::Create(size, data, mapBit);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}

	Ref<VertexElementBuffer> VertexElementBuffer::Create(uint64_t vertexDataSize, const void* vertexData, uint64_t elementDataSize, const void* elementData)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLVertexElementBuffer>::Create(vertexDataSize, vertexData, elementDataSize, elementData);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}