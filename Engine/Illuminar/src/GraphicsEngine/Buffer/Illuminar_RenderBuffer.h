#pragma once

#include "GraphicsEngine/Illuminar_Resource.h"
#include "GraphicsEngine/Illuminar_Texture.h"

namespace Illuminar::GFX {
	//TODO: maybe treat RenderBuffer as a special case for 2D texture?
	struct RenderBuffer : Resource
	{
		RenderBuffer() = default;
		virtual ~RenderBuffer() = default;

		RenderBuffer(const RenderBuffer& other) = delete;
		RenderBuffer& operator=(const RenderBuffer& other) = delete;

		static Ref<RenderBuffer> Create(uint32_t w, uint32_t h, TextureFormat format = TextureFormat::RGBA8_SRGB, uint32_t samples = 1);

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual void Resize(uint32_t w, uint32_t h, TextureFormat format = TextureFormat::RGBA8_SRGB, uint32_t samples = 1) = 0;
	};
}