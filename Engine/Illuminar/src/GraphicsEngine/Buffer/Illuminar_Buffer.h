#pragma once

#include "GraphicsEngine/Illuminar_Resource.h"
#include "Illuminar_BufferLayout.h"

namespace Illuminar::GFX {
	enum struct Usage
	{
		Static_Draw = 0,  /* ** Objects will not be updated very often ** */
		Dynamic_Draw = 1, /* ** Objects will be updated very often ** */
		Stream_Draw = 2,  /* ** Objects will be updated sometimes ** */

		Static_Copy = 3,  /* ** ** */
		Dynamic_Copy = 4, /* ** ** */
		Stream_Copy = 5,  /* ** ** */

		Static_Read = 6,  /* ** ** */
		Dynamic_Read = 7, /* ** ** */
		Stream_Read = 8,  /* ** ** */
	};

	enum struct MapBit
	{
		/* ** Common ** */
		Read = BIT(0),				/* ** The buffer data store will be mapped to read ** */
		Write = BIT(1),				/* ** The buffer data store will be mapped to write ** */
		Persistent = BIT(2),		/* ** The buffer data store will be permanently mapped ** */
		Coherent = BIT(3),			/* ** The mapping will be consistent ** */

		/* ** MapBuffer() only ** */
		Invalidate_Range = BIT(4),  /* ** Informs that we are no longer interest in data stored in this range ** */
		Invalidate_Buffer = BIT(5), /* ** Informs that we are no longer interest in any data in a whole buffer ** */
		Flush_Explicit = BIT(6),	/* ** We promie to inform about any change of data in a mapped range ** */
		Unsynchronized = BIT(7),	/* ** Informs that we handle synchronization ourselves ** */

		/* ** Reserve() only ** */
		Dynamic_Storage = BIT(8),       /* ** The buffer data might be updated directly ** */
		Client_Storage = BIT(9),        /* ** If all other conditions can be made, prefer local storage (CPU), than remote (GPU) ** */
	};

	enum struct BufferType
	{
		Vertex,
		Element,
		Uniform,
		ShaderStorage,
		Pixel,
		Texture,
		IndirectDraw
	};

	struct Buffer : Resource
	{
		Buffer() = default;
		virtual ~Buffer() = default;

		Buffer(const Buffer& other) = delete;
		Buffer& operator=(const Buffer& other) = delete;

		virtual void Bind() const {};
		virtual void Bind(BufferType type, uint32_t bindingPoint) const {}

		virtual void BindRange(uint64_t offset, uint64_t size) const {}
		virtual void BindRange(BufferType type, uint32_t bindingPoint, uint64_t offset, uint64_t size) const {}

		[[nodiscard]] virtual std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) = 0;
		virtual void Unmap() = 0;
	};

	struct BufferResource : Buffer
	{
		static Ref<BufferResource> Create(uint64_t size, const void* data, Usage usage);
		static Ref<BufferResource> Create(uint64_t size, const void* data, MapBit mapBit);

		BufferResource() = default;
		virtual ~BufferResource() = default;

		BufferResource(const BufferResource& other) = delete;
		BufferResource& operator=(const BufferResource& other) = delete;

		BufferResource(BufferResource&& other) = default;
		BufferResource& operator=(BufferResource&& other) = default;

		virtual void Bind(BufferType type) const {}
		virtual void Bind(BufferType type, uint32_t bindingPoint) const = 0;
		virtual void BindRange(BufferType type, uint32_t bindingPoint, uint64_t offset, uint64_t size) const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;
		virtual void AddSubData(uint64_t size, uint64_t offset, const void* data) = 0;

		template<typename T>
		void AddSubData(const T& data, uint64_t offset)
		{
			AddSubData(sizeof(T), offset, &data);
		}
	};

	struct VertexBuffer : Buffer
	{
		static Ref<VertexBuffer> Create(const BufferLayout& layout, uint64_t size, const void* data, Usage usage);
		static Ref<VertexBuffer> Create(const BufferLayout& layout, uint64_t size, const void* data, MapBit mapBit);

		VertexBuffer() = default;
		virtual ~VertexBuffer() = default;

		VertexBuffer(const VertexBuffer& other) = delete;
		VertexBuffer& operator=(const VertexBuffer& other) = delete;

		VertexBuffer(VertexBuffer&& other) = default;
		VertexBuffer& operator=(VertexBuffer&& other) = default;

		virtual void Bind(uint32_t bindingPoint = 0, uint32_t offset = 0) const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		[[nodiscard]] virtual const BufferLayout& getLayout() const = 0;

		static constexpr BufferType getBufferType() { return BufferType::Vertex; }
	};

	struct ElementBuffer : Buffer
	{
		static Ref<ElementBuffer> Create(uint64_t size, const void* data, Usage usage);
		static Ref<ElementBuffer> Create(uint64_t size, const void* data, MapBit mapBit);

		ElementBuffer() = default;
		virtual ~ElementBuffer() = default;

		ElementBuffer(const ElementBuffer& other) = delete;
		ElementBuffer& operator=(const ElementBuffer& other) = delete;

		virtual void Bind() const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		static constexpr BufferType getBufferType() { return BufferType::Element; }
	};

	//vertex and element buffer joined together into a one big buffer
	//immutable for now, which means that the buffer size cannot be changed
	struct VertexElementBuffer : Buffer
	{
		static Ref<VertexElementBuffer> Create(uint64_t vertexDataSize, const void* vertexData, uint64_t elementDataSize, const void* elementData);

		VertexElementBuffer() = default;
		virtual ~VertexElementBuffer() = default;

		VertexElementBuffer(const VertexElementBuffer& other) = delete;
		VertexElementBuffer& operator=(const VertexElementBuffer& other) = delete;

		virtual void Bind() const = 0;
	};

	struct TextureBuffer : Buffer
	{
		static Ref<TextureBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage);
		static Ref<TextureBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit);

		TextureBuffer() = default;
		virtual ~TextureBuffer() = default;

		TextureBuffer(const TextureBuffer& other) = delete;
		TextureBuffer& operator=(const TextureBuffer& other) = delete;

		virtual void Bind() const = 0;
		virtual void BindRange(uint64_t offset, uint64_t size) const = 0;

		static constexpr BufferType getBufferType() { return BufferType::Texture; }
	};

	struct UniformBuffer : Buffer
	{
		static Ref<UniformBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage);
		static Ref<UniformBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit);

		UniformBuffer() = default;
		virtual ~UniformBuffer() = default;

		UniformBuffer(const UniformBuffer& other) = delete;
		UniformBuffer& operator=(const UniformBuffer& other) = delete;

		virtual void Bind() const = 0;
		virtual void BindRange(uint64_t offset, uint64_t size) const = 0;

		virtual void AddSubData(uint64_t size, uint64_t offset, const void* data) = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		static constexpr BufferType getBufferType() { return BufferType::Uniform; }
	};

	struct ShaderStorageBuffer : Buffer
	{
		static Ref<ShaderStorageBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage);
		static Ref<ShaderStorageBuffer> Create(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit);

		ShaderStorageBuffer() = default;
		virtual ~ShaderStorageBuffer() = default;

		ShaderStorageBuffer(const ShaderStorageBuffer& other) = delete;
		ShaderStorageBuffer& operator=(const ShaderStorageBuffer& other) = delete;

		virtual void Bind() const = 0;
		virtual void BindRange(uint64_t offset, uint64_t size) const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		template<typename T>
		void AddVector(std::vector<T>& data)
		{
			AddData(sizeof(T) * data.size(), data.data());
			data.clear();
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		static constexpr BufferType getBufferType() { return BufferType::ShaderStorage; }
	};

	struct PixelBuffer : Buffer
	{
		static Ref<PixelBuffer> Create(uint64_t size, const void* data, Usage usage);
		static Ref<PixelBuffer> Create(uint64_t size, const void* data, MapBit mapBit);

		PixelBuffer() = default;
		virtual ~PixelBuffer() = default;

		PixelBuffer(const PixelBuffer& other) = delete;
		PixelBuffer& operator=(const PixelBuffer& other) = delete;

		virtual void Bind() const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		static constexpr BufferType getBufferType() { return BufferType::Pixel; }
	};

	struct IndirectDrawBuffer : Buffer
	{
		static Ref<IndirectDrawBuffer> Create(uint64_t size, const void* data, Usage usage);
		static Ref<IndirectDrawBuffer> Create(uint64_t size, const void* data, MapBit mapBit);

		IndirectDrawBuffer() = default;
		virtual ~IndirectDrawBuffer() = default;

		IndirectDrawBuffer(const IndirectDrawBuffer& other) = delete;
		IndirectDrawBuffer& operator=(const IndirectDrawBuffer& other) = delete;

		virtual void Bind() const = 0;

		template<typename T>
		void AddData(const T& data)
		{
			AddData(sizeof(T), (const void*)&data);
		}

		virtual void AddData(uint64_t size, const void* data) = 0;

		static constexpr BufferType getBufferType() { return BufferType::IndirectDraw; }
	};
}