#pragma once

#include "GraphicsEngine/Illuminar_DataType.h"

namespace Illuminar::GFX {
	static uint32_t ShaderLayoutDataTypeSize(DataType type)
	{
		switch (type) {
			case DataType::R32_INT: return 4;
			case DataType::R8_INT: return 1;
			case DataType::R32_FLOAT: return 4;

			case DataType::R32_UINT: return 4;
			case DataType::R8_UINT: return 4;
			case DataType::RGBA8_UNORM: return 4;

			case DataType::RG32_FLOAT: return 4 * 2;
			case DataType::RGB32_FLOAT: return 4 * 3;
			case DataType::RGBA32_FLOAT: return 4 * 4;

			case DataType::RG32_INT: return 4 * 2;
			case DataType::RGB32_INT: return 4 * 3;
			case DataType::RGBA32_INT: return 4 * 4;

			case DataType::Mat3: return 4 * 3 * 3;
			case DataType::Mat4: return 4 * 4 * 4;
		} return 0;
	}

	struct BufferElement
	{
		BufferElement() = default;

		BufferElement(DataType type, std::string_view name)
			: name(name), type(type), size(ShaderLayoutDataTypeSize(type))
		{
		}

		[[nodiscard]] uint32_t getComponentCount() const;

		std::string_view name;
		uint32_t size = 0;
		uint32_t offset = 0;
		DataType type = DataType::None;

		[[nodiscard]] inline size_t Hash() const
		{
			size_t nameHash = std::hash<std::string_view>()(name);
			std::array<size_t, 4> values = { (size_t)size, (size_t)offset, (size_t)type, nameHash };

			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	struct BufferLayout
	{
		BufferLayout() = default;
		~BufferLayout() = default;

		BufferLayout(const std::initializer_list<BufferElement>& elements, bool instanced = false)
			: elements(elements), instanced(instanced)
		{
			CalculateOffsetAndStride();
		}

		[[nodiscard]] inline bool isInstanced() const { return instanced; }
		[[nodiscard]] inline uint32_t getStride() const { return stride; }
		[[nodiscard]] inline uint32_t getOffset() const { return offset; }
		[[nodiscard]] inline const std::vector<BufferElement>& getElements() const { return elements; }
		[[nodiscard]] inline size_t size() const { return elements.size(); }

		[[nodiscard]] inline std::vector<BufferElement>::iterator begin() { return elements.begin(); }
		[[nodiscard]] inline std::vector<BufferElement>::iterator end() { return elements.end(); }
		[[nodiscard]] inline std::vector<BufferElement>::const_iterator begin() const { return elements.begin(); }
		[[nodiscard]] inline std::vector<BufferElement>::const_iterator end() const { return elements.end(); }

		[[nodiscard]] inline size_t Hash() const
		{
			std::vector<size_t> values(elements.size() + 1);
			for (size_t i = 0; i < values.size() - 1; ++i) {
				values[i] = elements[i].Hash();
			} values.back() = std::hash<uint32_t>()(stride);

			return std::_Hash_array_representation(values.data(), values.size());
		}

		inline bool operator!=(const BufferLayout& other) const
		{
			return Hash() != other.Hash();
		}

	private:
		void CalculateOffsetAndStride();

		std::vector<BufferElement> elements;

		uint32_t stride = 0;
		uint32_t offset = 0;

		bool instanced = false;
	};
}