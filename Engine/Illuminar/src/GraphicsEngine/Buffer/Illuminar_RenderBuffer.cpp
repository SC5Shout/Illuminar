#include "Illuminar_pch.h"
#include "Illuminar_RenderBuffer.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLRenderBuffer.h"
#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar::GFX {
	Ref<RenderBuffer> RenderBuffer::Create(uint32_t w, uint32_t h, TextureFormat format, uint32_t samples)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLRenderBuffer>::Create(w, h, format, samples);
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}