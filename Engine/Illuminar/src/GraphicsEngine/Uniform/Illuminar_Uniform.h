#pragma once

#include <string>
#include <unordered_map>

namespace Illuminar::GFX {
	enum struct UniformType : uint8_t
	{
		None,
		Bool,
		Int,
		UInt64,
		Float,
		Vec2,
		Vec3,
		Vec4,
		Mat4,
		Sampler2D,
		Sampler3D,
	};

	enum struct UniformBlockType : uint8_t
	{
		PushConstant,
		UniformBuffer,
		ShaderStorage
	};

	//struct UniformInfo
	//{
	//	UniformType type = UniformType::None;
	//	std::string_view name;
	//};

	struct Uniform
	{
		UniformType type = UniformType::None;
		std::string name;
		uint32_t offset = 0;
		size_t size = 0;
	};

	struct SampledUniform
	{
		UniformType type = UniformType::Sampler2D;
		std::string name;
		uint32_t binding = 0;
	};

	struct UniformBlock
	{
		UniformBlockType blockType = UniformBlockType::PushConstant;
		std::string blockName;
		size_t size = 0;
		size_t offset = 0;
		std::unordered_map<std::string, Uniform> uniforms;
	};
}