#include "Illuminar_pch.h"
#include "Illuminar_UniformBlockBuffer.h"

namespace Illuminar::GFX {
	UniformBlockBuffer::UniformBlockBuffer(size_t size)
		: buffer(storage), m_size(size)
	{
		if (size > sizeof(storage)) {
			buffer = malloc(size);
		}
		memset(buffer, 0, size);
	}

	UniformBlockBuffer::UniformBlockBuffer(UniformBlockBuffer&& other) noexcept
		: buffer(other.buffer), m_size(other.m_size), dirty(other.dirty)
	{
		if (other.isLocalStorage()) {
			buffer = storage;
			memcpy(buffer, other.buffer, m_size);
		}
		other.buffer = nullptr;
		other.m_size = 0;
	}

	UniformBlockBuffer& UniformBlockBuffer::operator=(UniformBlockBuffer&& other) noexcept
	{
		if (this == &other) {
			return *this;
		}

		dirty = other.dirty;
		if (other.isLocalStorage()) {
			buffer = storage;
			m_size = other.m_size;
			memcpy(buffer, other.buffer, other.m_size);
		} else {
			std::swap(buffer, other.buffer);
			std::swap(m_size, other.m_size);
		}

		return *this;
	}

	UniformBlockBuffer& UniformBlockBuffer::setUniforms(const UniformBlockBuffer& other)
	{
		if (this == &other) {
			return *this;
		}

		if (m_size != other.m_size) {
			// first free our storage if any
			if (buffer && !isLocalStorage()) {
				free(buffer);
			}

			buffer = storage;
			m_size = other.m_size;
			if (m_size > sizeof(storage)) {
				buffer = malloc(m_size);
			}
		}
		memcpy(buffer, other.buffer, other.m_size);

		dirty = true;

		return *this;
	}
}
