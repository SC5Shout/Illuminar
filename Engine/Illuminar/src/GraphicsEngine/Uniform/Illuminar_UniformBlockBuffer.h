#pragma once

namespace Illuminar::GFX {
	template<typename T>
	concept SupportedUniformType =
		std::is_same_v<bool, T> ||
		std::is_same_v<float, T> ||
		std::is_same_v<int32_t, T> ||
		std::is_same_v<uint32_t, T> ||
		std::is_same_v<uint64_t, T> ||
		std::is_same_v<Vector2<float>, T> ||
		std::is_same_v<Vector3<float>, T> ||
		std::is_same_v<Vector4<float>, T> ||
		std::is_same_v<Mat4, T>;

	struct UniformBlockBuffer
	{
		UniformBlockBuffer() = default;

		UniformBlockBuffer(size_t size);

		UniformBlockBuffer(const UniformBlockBuffer& other) = delete;
		UniformBlockBuffer& operator=(const UniformBlockBuffer& other) = delete;

		UniformBlockBuffer(UniformBlockBuffer&& other) noexcept;
		UniformBlockBuffer& operator=(UniformBlockBuffer&& other) noexcept;

		inline ~UniformBlockBuffer() { if (buffer && !isLocalStorage()) { free(buffer); } }

		//Consider using operator= instead of this function
		[[nodiscard]] UniformBlockBuffer& setUniforms(const UniformBlockBuffer& other);

		template<SupportedUniformType T>
		inline static void setUniform(void* addr, size_t offset, const T& v)
		{
			addr = static_cast<char*>(addr) + offset;
			T* p = static_cast<T*>(addr);
			*p = v;
		}

		void setUniform(size_t offset, const SupportedUniformType auto& v) noexcept
		{
			dirty = true;
			setUniform(static_cast<char*>(buffer) + offset, 0, v);
		}

		template<SupportedUniformType T>
		[[nodiscard]] inline const T getUniform(size_t offset) const
		{
			return *reinterpret_cast<T const*>(static_cast<char const*>(buffer) + offset);
		}

		template<SupportedUniformType T>
		[[nodiscard]] inline const T* tryGetUniform(size_t offset) const
		{
			return reinterpret_cast<T const*>(static_cast<char const*>(buffer) + offset);
		}

		template<SupportedUniformType T>
		[[nodiscard]] inline T& getUniformRef(size_t offset) const
		{
			return *reinterpret_cast<T*>(static_cast<char*>(buffer) + offset);
		}

		[[nodiscard]] inline const void* getBuffer() const { return buffer; }
		[[nodiscard]] inline const size_t size() const { return m_size; }
		[[nodiscard]] inline const bool isDirty() const { return dirty; }
		inline void Clean() { dirty = false; }

		[[nodiscard]] inline bool isLocalStorage() const { return buffer == storage; }

	private:
		//128 a push constant supported block size
		char storage[128];
		void* buffer = nullptr;

		//m_ to avoid collision with the size() method
		size_t m_size = 0;
		bool dirty = true;
	};
}
