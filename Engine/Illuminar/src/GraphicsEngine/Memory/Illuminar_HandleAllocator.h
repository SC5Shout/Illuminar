#pragma once

#include "Thread/Illuminar_TicketMutex.h"

namespace Illuminar::GFX {
	/* Allows allocating a new thread-safe handle which represents a handle into a GPU resource.
	* This class is useful to extend the handle lifetime, because it allows to store the handle on the main thread
	* and delete it on the render thread, which does not run synchronously with the main thread.
	*
	* The render thread runs 2 frames behind the main thread which means that when
	* eg. Ref<VertexBuffer> when invokes its destructor it will invalidate the graphics API handle (like GLuint)
	* but the render thread still could need this handle. This allocator will allow a user to ensure that the handle will exist 'till it gets destroyed by the render thread
	* (or by the main thread, it depends on how the user will use the alllocator).
	*/

	/* Allocator allows user to have a handle of a base class and construct the object a derived class
	* eg.
	* struct Base
	* {
	* };
	* 
	* struct Derived : Base
	* {
	*	Derived(int x)
	*		: x(x) {}
	*	int x = 0;
	* };
	*
	* //handle type is Base but the created object is of type Derived
	* Handle<Base> handle = handleAllocator.new_object<Derived>(5);
	*
	* delete_object() function needs to have 2 template types - T (derived) and U (base),
	* because Handle type is Base, but Derived is the type to destroy, 
	* 
	* same with reconstruct(). Handle type is Base, but Derived is the type to reconstruct
	* same with handle_cast
	*/
	template<typename Tp, typename U>
	concept is_t_same_or_base_of_u = std::is_base_of_v<U, std::remove_pointer_t<Tp>> || std::is_same_v<U, std::remove_pointer_t<Tp>>;

	template<typename cTp, typename U>
	concept is_const_t_same_or_base_of_u = std::is_same_v<std::add_const_t<U>, std::remove_pointer_t<cTp>> || std::is_base_of_v<std::add_const_t<U>, std::remove_pointer_t<cTp>>;

	template<typename cTp>
	concept ConstTypePtr = std::is_pointer_v<cTp> && std::is_const_v<std::remove_pointer_t<cTp>>;

	template<typename Tp>
	concept TypePtr = std::is_pointer_v<Tp>;

	//same as std::synchronized_pool_resource but with a custom LockingPolicy
	template<typename LockingPolicy>
	struct synchronized_pool_resource : public std::pmr::unsynchronized_pool_resource 
	{
		using unsynchronized_pool_resource::unsynchronized_pool_resource;

		void release() 
		{
			std::lock_guard guard{ lock };
			unsynchronized_pool_resource::release();
		}

	protected:
		virtual void* do_allocate(const size_t bytes, const size_t _Align) override 
		{
			std::lock_guard guard{ lock };
			return unsynchronized_pool_resource::do_allocate(bytes, _Align);
		}

		virtual void do_deallocate(void* const _Ptr, const size_t bytes, const size_t _Align) override
		{
			std::lock_guard guard{ lock };
			unsynchronized_pool_resource::do_deallocate(_Ptr, bytes, _Align);
		}

	private:
		mutable LockingPolicy lock;
	};

	struct HandleAllocator
	{
		HandleAllocator()
			: base(std::malloc(10_MB)), linearArena{ base, 10_MB }, pool{ &linearArena }
		{
		}
	
		~HandleAllocator() = default;

		static constexpr uint32_t ALIGNMENT_SHIFT = 4;
	
		template<typename T, typename ... Args>
		[[nodiscard]] inline Handle<T> new_object(Args&& ... args)
		{
			Handle<T> h{ allocate(sizeof(T)) };

			T* addr = handle_cast<T*>(h);
			new(addr) T(std::forward<Args>(args)...);
			return h;
		}
	
		template<typename T, typename U>
		void delete_object(const Handle<U>& handle)
		{
			const T* t = handle_const_cast<const T*>(handle);
			if (t) {
				t->~T();
				deallocate(handle.getID(), sizeof(T));
				//handle = HandleBase::INVALID_HANDLE_ID;
			}
		}

		template<typename T, typename U, typename ... Args>
		[[nodiscard]] inline T* reconstruct(const Handle<U>& handle, Args&& ... args) requires is_t_same_or_base_of_u<T, U>
		{
			T* addr = handle_cast<T*>(handle);

			addr->~T();
			new(addr) T(std::forward<Args>(args)...);

			return addr;
		}

		//returns mutable pointer to the mutable data
		template<TypePtr Tp, typename U>
		[[nodiscard]] inline Tp handle_cast(Handle<U>& handle) requires is_t_same_or_base_of_u<Tp, U>
		{
			return handle.valid() ? static_cast<Tp>(IDtoMem(handle.getID())) : nullptr;
		}
	
		//returns mutable pointer to the mutable data
		template<TypePtr Tp, typename U>
		[[nodiscard]] inline Tp handle_cast(const Handle<U>& handle) requires is_t_same_or_base_of_u<Tp, U>
		{
			return handle_cast<Tp>(const_cast<Handle<U>&>(handle));
		}

		//returns mutable pointer to the immutable data
		template<ConstTypePtr cTp, typename U>
		[[nodiscard]] inline cTp handle_const_cast(Handle<U>& handle) requires is_const_t_same_or_base_of_u<cTp, U>
		{
			return handle.valid() ? static_cast<cTp>(IDtoMem(handle.getID())) : nullptr;
		}

		template<ConstTypePtr cTp, typename U>
		[[nodiscard]] inline cTp handle_const_cast(const Handle<U>& handle) requires is_const_t_same_or_base_of_u<cTp, U>
		{
			return handle_const_cast<cTp>(const_cast<Handle<U>&>(handle));
		}

		//should here be functions that return immutable pointer to the mutable data and immutable pointer to the immutable data?

	private:
		[[nodiscard]] inline HandleBase::HandleID allocate(size_t size)
		{
			return MemtoID(pool.allocate(size, 16));
		}

		inline void deallocate(HandleBase::HandleID id, size_t size)
		{
			void* mem = IDtoMem(id);
			pool.deallocate(mem, size, 16);
		}

		[[nodiscard]] inline HandleBase::HandleID MemtoID(void* mem)
		{
			char* const base = (char*)this->base;
			size_t offset = (char*)mem - base;
	
			return (uint32_t)(offset >> (size_t)ALIGNMENT_SHIFT);
		}
	
		[[nodiscard]] inline void* IDtoMem(HandleBase::HandleID ID)
		{
			char* const base = (char*)this->base;
			size_t offset = size_t(ID << ALIGNMENT_SHIFT);
			return static_cast<void*>(base + offset);
		}

		void* base = nullptr;
		std::pmr::monotonic_buffer_resource linearArena;
		synchronized_pool_resource<TicketMutex> pool;
	};
}