#pragma once

#include "GraphicsEngine/Buffer/Illuminar_Buffer.h"

namespace Illuminar::GFX {
	template<BufferType BufferT, uint32_t MAX_SIZE>
	struct GPULinearAllocator
	{
		GPULinearAllocator()
		{
			buffer = BufferResource::Create(MAX_SIZE, nullptr, MapBit::Dynamic_Storage | MapBit::Write);
		}

		~GPULinearAllocator() = default;

		template<typename T>
		[[nodiscard]] inline uint32_t allocate(const T& data)
		{
			return allocate(&data, sizeof(T));
		}

		template<typename T>
		[[nodiscard]] inline uint32_t allocate(const std::vector<T>& data)
		{
			return allocate(data.data(), (uint32_t)data.size() * sizeof(T));
		}

		[[nodiscard]] inline uint32_t allocate(const void* data, uint32_t size)
		{
			const uint32_t result = currentOffset;
			buffer->AddSubData(size, result, data);
			currentOffset += size;
			currentOffset = PadUniformBufferSize(currentOffset);
			++m_size;
			return result;
		}

		void Bind(uint32_t bindingIndex, uint32_t offset, uint32_t size)
		{
			buffer->BindRange(BufferT, bindingIndex, offset, size);
		}

		void Bind()
		{
			buffer->Bind(BufferT);
		}

		void clear()
		{
			m_size = 0;
			currentOffset = 0;
		}

		[[nodiscard]] inline Ref<BufferResource> getBuffer() const { return buffer; }
		[[nodiscard]] inline uint32_t size() const { return m_size; }
		[[nodiscard]] inline uint32_t max_size() const { return MAX_SIZE; }
		[[nodiscard]] inline bool empty() const { return m_size == 0; }

	private:
		[[nodiscard]] inline uint32_t PadUniformBufferSize(uint32_t size)
		{
			auto align = [](uint32_t alignment, uint32_t size) {
				uint32_t alignedSize = size;
				if (alignment > 0) {
					alignedSize = (alignedSize + alignment - 1) & ~(alignment - 1);
				}
				return alignedSize;
			};

			if constexpr (BufferT == BufferType::Uniform) {
				int alignment = 256;
				//glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &alignment);

				return align(alignment, size);
			} else if constexpr (BufferT == BufferType::ShaderStorage) {
				int alignment = 16;
				//glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT, &alignment);

				return align(alignment, size);
			}

			return size;
		}

		Ref<BufferResource> buffer = nullptr;
		uint32_t currentOffset = 0;
		uint32_t m_size;
	};
}