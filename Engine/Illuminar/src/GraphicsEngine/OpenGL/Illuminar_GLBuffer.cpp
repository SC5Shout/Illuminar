#include "Illuminar_pch.h"
#include "Illuminar_GLBuffer.h"

#include "Illuminar_GLRenderer.h"
#include "Illuminar_GLGraphicsPipeline.h"
#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	GLenum ConvertUsageToOpenGL(Usage usage)
	{
		switch (usage) {
			case Usage::Static_Draw: return GL_STATIC_DRAW;
			case Usage::Dynamic_Draw: return GL_DYNAMIC_DRAW;
			case Usage::Stream_Draw: return GL_STREAM_DRAW;

			case Usage::Static_Copy: return GL_STATIC_COPY;
			case Usage::Dynamic_Copy: return GL_DYNAMIC_COPY;
			case Usage::Stream_Copy: return GL_STREAM_COPY;

			case Usage::Static_Read: return GL_STATIC_READ;
			case Usage::Dynamic_Read: return GL_DYNAMIC_READ;
			case Usage::Stream_Read: return GL_STREAM_READ;
		}
		return NULL;
	}

	GLbitfield ConvertMapBitToOpenGL(MapBit bit)
	{
		/* ** Common ** */
		GLbitfield result = (bit & MapBit::Read) == MapBit::Read ? GL_MAP_READ_BIT : 0;
		result += (bit & MapBit::Write) == MapBit::Write ? GL_MAP_WRITE_BIT : 0;
		result += (bit & MapBit::Persistent) == MapBit::Persistent ? GL_MAP_PERSISTENT_BIT : 0;
		result += (bit & MapBit::Coherent) == MapBit::Coherent ? GL_MAP_COHERENT_BIT : 0;

		/* ** MapBuffer() only ** */
		result += (bit & MapBit::Invalidate_Range) == MapBit::Invalidate_Range ? GL_MAP_INVALIDATE_RANGE_BIT : 0;
		result += (bit & MapBit::Invalidate_Buffer) == MapBit::Invalidate_Buffer ? GL_MAP_INVALIDATE_BUFFER_BIT : 0;
		result += (bit & MapBit::Flush_Explicit) == MapBit::Flush_Explicit ? GL_MAP_FLUSH_EXPLICIT_BIT : 0;
		result += (bit & MapBit::Unsynchronized) == MapBit::Unsynchronized ? GL_MAP_UNSYNCHRONIZED_BIT : 0;

		//* ** Reserve() only ** */
		result += (bit & MapBit::Dynamic_Storage) == MapBit::Dynamic_Storage ? GL_DYNAMIC_STORAGE_BIT : 0;
		result += (bit & MapBit::Client_Storage) == MapBit::Client_Storage ? GL_CLIENT_STORAGE_BIT : 0;

		return result;
	}

	GLuint ConvertBufferTypeToOpenGL(BufferType type)
	{
		switch (type) {
			case BufferType::Vertex: return GL_ARRAY_BUFFER;
			case BufferType::Element: return GL_ELEMENT_ARRAY_BUFFER;
			case BufferType::Uniform: return GL_UNIFORM_BUFFER;
			case BufferType::ShaderStorage: return GL_SHADER_STORAGE_BUFFER;
			case BufferType::Texture: return GL_TEXTURE_BUFFER;
			case BufferType::IndirectDraw: return GL_DRAW_INDIRECT_BUFFER;
			case BufferType::Pixel: return GL_PIXEL_UNPACK_BUFFER;
		}

		//should never happen
		return GL_ARRAY_BUFFER;
	}

	GLGeneralBuffer::GLGeneralBuffer(uint64_t size, const void* data, Usage usage)
		: renderer(RenderCommand::as<GLRenderer>())
	{
		std::vector<uint8_t> ptr;
		if (data && size > 0) {
			ptr.resize(size);
			memcpy(ptr.data(), data, size);
		}

		if (size > 0) {
			dataPrereserved = true;
		}

		handle = RenderCommand::CreateHandle<GLuint>([dataPrereserved = dataPrereserved, ptr = std::move(ptr), size, usage](GLuint& buffer) {
			glCreateBuffers(1, &buffer);
			if (dataPrereserved) {
				glNamedBufferData(buffer, size, nullptr, ConvertUsageToOpenGL(usage));
			}
			if (!ptr.empty() && size > 0) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
			}
		});
		Bind();
	}

	GLGeneralBuffer::GLGeneralBuffer(uint64_t size, const void* data, MapBit mapBit)
		: renderer(RenderCommand::as<GLRenderer>())
	{
		std::vector<uint8_t> ptr;
		if (data && size > 0) {
			ptr.resize(size);
			memcpy(ptr.data(), data, size);
		}

		if (size > 0) {
			dataPrereserved = true;
		}

		handle = RenderCommand::CreateHandle<GLuint>([dataPrereserved = dataPrereserved, ptr = std::move(ptr), size, mapBit](GLuint& buffer) {
			glCreateBuffers(1, &buffer);
			glNamedBufferStorage(buffer, size, nullptr, ConvertMapBitToOpenGL(mapBit));
			if (!ptr.empty() && bool(mapBit & MapBit::Dynamic_Storage)) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
			}
		});
		Bind();
	}

	GLGeneralBuffer::~GLGeneralBuffer()
	{
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& buffer) {
			glDeleteBuffers(1, &buffer);
		});
	}

	void GLBufferResource::Bind(BufferType type) const
	{
		RenderCommand::Submit(handle, [type](GLuint& buffer) {
			glBindBuffer(ConvertBufferTypeToOpenGL(type), buffer);
		});
	}

	void GLBufferResource::Bind(BufferType type, uint32_t bindingPoint) const
	{
		RenderCommand::Submit(handle, [type, bindingPoint = bindingPoint](GLuint& buffer) {
			glBindBufferBase(ConvertBufferTypeToOpenGL(type), bindingPoint, buffer);
		});
	}

	void GLBufferResource::BindRange(BufferType type, uint32_t bindingPoint, uint64_t offset, uint64_t size) const
	{
		RenderCommand::Submit(handle, [type, bindingPoint = bindingPoint, offset, size](GLuint& buffer) {
			glBindBufferRange(ConvertBufferTypeToOpenGL(type), bindingPoint, buffer, offset, size);
		});
	}

	std::shared_ptr<void*> GLBufferResource::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLBufferResource::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	void GLBufferResource::AddSubData(uint64_t size, uint64_t offset, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [ptr = std::move(ptr), size, offset](GLuint& buffer) {
			glNamedBufferSubData(buffer, offset, size, ptr.data());
		});
	}

	void GLBufferResource::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLVertexBuffer::Bind(uint32_t bindingPoint, uint32_t offset) const
	{
		RenderCommand::Submit(handle, [renderer = renderer, bindingPoint, offset, layout = layout](GLuint& buffer) {
			auto pipeline = renderer->currentPipeline;
			if (!pipeline) {
				EngineLogError("VertexBuffer::Bind(): the current pipeline is nullptr. Always Bind() pipeline before everything");
				__debugbreak();
			}

			GLuint& vao = *RenderCommand::handle_cast<GLuint*>(pipeline->handle);
			glVertexArrayVertexBuffer(vao, bindingPoint, buffer, offset, layout.getStride());

			//should we glVertexArrayAttribBinding, glVertexArrayAttribFormat and glEnableVertexArrayAttrib here or in GLPipeline?
		});
	}

	std::shared_ptr<void*> GLVertexBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLVertexBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLVertexBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	void GLElementBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [renderer = renderer](GLuint& buffer) {
			auto pipeline = renderer->currentPipeline;
			if (!pipeline) {
				EngineLogError("ElementBuffer::Bind(): the current pipeline is nullptr. Always Bind() pipeline before everything");
				__debugbreak();
			}
			GLuint& vao = *RenderCommand::handle_cast<GLuint*>(pipeline->handle);
			glVertexArrayElementBuffer(vao, buffer);
		});
	}

	void GLElementBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	std::shared_ptr<void*> GLElementBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLElementBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLTextureBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint](GLuint& buffer) {
			glBindBufferBase(GL_TEXTURE_BUFFER, bindingPoint, buffer);
		});
	}

	void GLTextureBuffer::BindRange(uint64_t offset, uint64_t size) const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint, offset, size](GLuint& buffer) {
			glBindBufferRange(GL_TEXTURE_BUFFER, bindingPoint, buffer, offset, size);
		});
	}

	std::shared_ptr<void*> GLTextureBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLTextureBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLUniformBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint](GLuint& buffer) {
			glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, buffer);
		});
	}

	void GLUniformBuffer::BindRange(uint64_t offset, uint64_t size) const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint, offset, size](GLuint& buffer) {
			glBindBufferRange(GL_UNIFORM_BUFFER, bindingPoint, buffer, offset, size);
		});
	}

	void GLUniformBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	void GLUniformBuffer::AddSubData(uint64_t size, uint64_t offset, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [ptr = std::move(ptr), size, offset](GLuint& buffer) {
			glNamedBufferSubData(buffer, offset, size, ptr.data());
		});
	}

	std::shared_ptr<void*> GLUniformBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLUniformBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLShaderStorageBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint](GLuint& buffer) {
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPoint, buffer);
		});
	}

	void GLShaderStorageBuffer::BindRange(uint64_t offset, uint64_t size) const
	{
		RenderCommand::Submit(handle, [bindingPoint = bindingPoint, offset, size](GLuint& buffer) {
			glBindBufferRange(GL_SHADER_STORAGE_BUFFER, bindingPoint, buffer, offset, size);
		});
	}

	void GLShaderStorageBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	std::shared_ptr<void*> GLShaderStorageBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLShaderStorageBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLPixelBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
		});
	}

	void GLPixelBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	std::shared_ptr<void*> GLPixelBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLPixelBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLIndirectDrawBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glBindBuffer(GL_DRAW_INDIRECT_BUFFER, buffer);
		});
	}

	void GLIndirectDrawBuffer::AddData(uint64_t size, const void* data)
	{
		if (!data || size == 0) {
			//LogError("ElementBuffer::AddData(): data cannot be nullptr");
			return;
		}

		std::vector<uint8_t> ptr(size);
		memcpy(ptr.data(), data, size);

		RenderCommand::Submit(handle, [dataPrereserved = dataPrereserved, ptr = std::move(ptr), size](GLuint& buffer) {
			if (dataPrereserved) {
				glNamedBufferSubData(buffer, 0, size, ptr.data());
				//TODO: change GL_DYNAMIC_DRAW
			} else glNamedBufferData(buffer, size, ptr.data(), GL_DYNAMIC_DRAW);
		});
	}

	std::shared_ptr<void*> GLIndirectDrawBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLIndirectDrawBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}

	void GLVertexElementBuffer::Bind() const
	{
		RenderCommand::Submit(handle, [renderer = renderer](GLuint& buffer) {
			auto pipeline = renderer->currentPipeline;
			if (!pipeline) {
				EngineLogError("ElementBuffer::Bind(): the current pipeline is nullptr. Always Bind() pipeline before everything");
				__debugbreak();
			}

			//const auto& layout = pipeline->getInfo().bufferLayouts[0];
			//glVertexArrayVertexBuffer(pipeline->vao, 0, buffer, elementAlignedSize, layout.getStride());
			//glVertexArrayElementBuffer(pipeline->vao, buffer);
		});
	}

	std::shared_ptr<void*> GLVertexElementBuffer::Map(size_t offset, size_t size, MapBit mapBit)
	{
		std::shared_ptr<void*> begin = std::make_shared<void*>(nullptr);
		RenderCommand::Submit(handle, [begin, offset, size, mapBit](GLuint& buffer) {
			*begin = glMapNamedBufferRange(buffer, offset, size, ConvertMapBitToOpenGL(mapBit));
		});
		return begin;
	}

	void GLVertexElementBuffer::Unmap()
	{
		RenderCommand::Submit(handle, [](GLuint& buffer) {
			glUnmapNamedBuffer(buffer);
		});
	}
}