#pragma once

#include "GraphicsEngine/Illuminar_GraphicsPipeline.h"
#include "Illuminar_GLGraphicsPipelineState.h"

#include <glad/glad.h>

namespace Illuminar::GFX {
	struct GLRenderer;
	struct GLGraphicsPipeline : GraphicsPipeline
	{
		GLGraphicsPipeline(const CreateInfo& createInfo);
		~GLGraphicsPipeline() override;

		GLGraphicsPipeline(const GLGraphicsPipeline& other) = delete;
		GLGraphicsPipeline& operator =(const GLGraphicsPipeline& other) = delete;

		void Bind() const override;
		void Rebind(const CreateInfo& createInfo) override;

		const CreateInfo& getInfo() const { return info; }
		CreateInfo& getInfoRef() { return info; }

		//private:
		mutable Ref<GLRenderer> renderer = nullptr;

		CreateInfo info;

		GLRasterizerState raster;
		GLStencilState stencil;
		GLDepthState depth;
		GLBlendState blend;

		Handle<GLuint> handle;
	};
}