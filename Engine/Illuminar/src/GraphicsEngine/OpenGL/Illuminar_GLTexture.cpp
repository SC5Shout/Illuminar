#include "Illuminar_pch.h"
#include "Illuminar_GLTexture.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	static unsigned int calculate_stride( unsigned int width, unsigned int pad = 4)
	{
		unsigned int channels = 4;

		uint32_t typesize = 4;
		unsigned int stride = channels * width;

		stride = (stride + (pad - 1)) & ~(pad - 1);

		return stride;
	}

	static unsigned int calculate_face_size(uint32_t width, uint32_t height)
	{
		unsigned int stride = calculate_stride(width);

		return stride * height;
	}

	GLTexture2D::GLTexture2D(const CreateInfo& createInfo)
		: info(createInfo)
	{
		CreateFromSource();
	}

	GLTexture2D::~GLTexture2D()
	{
		if (!handle) return;
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& texture) {
			if (texture) {
				glDeleteTextures(1, &texture);
			}
		});
	}

	void GLTexture2D::Bind(uint32_t slot) const
	{
		RenderCommand::Submit(handle, [slot](GLuint& texture) {
			glBindTextureUnit(slot, texture);
		});
	}

	void GLTexture2D::BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered, uint32_t layer) const
	{
		RenderCommand::Submit(handle, [slot, level, mapBit, format, layered, layer](GLuint& texture) {
			glBindImageTexture(slot, texture, level, layered, layer, ConvertImageMapBitToOpenGL(mapBit), ConvertInternalFormatToOpenGL(format));
		});
	}

	void GLTexture2D::GenerateMipmap()
	{
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;

		RenderCommand::Submit(handle, [miplevel](GLuint& texture) {
			if (miplevel) {
				glGenerateTextureMipmap(texture);
				glTextureParameterf(texture, GL_TEXTURE_LOD_BIAS, -0.8f);
			}
		});
	}

	void GLTexture2D::CreateFromSource()
	{
		const uint32_t format = ConvertFormatToOpenGL(info.parameters.format);
		const uint32_t internalFormat = ConvertInternalFormatToOpenGL(info.parameters.format);
		const uint32_t type = ConvertTextureFormatToOpenGLDataType(info.parameters.format);
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;
		const uint32_t w = info.w;
		const uint32_t h = info.h;
		const bool shadow = info.shadow;

		const uint32_t wrap = ConvertWrapToOpenGL(info.parameters.wrap);
		const uint32_t filterMag = ConvertFilterToOpenGL(info.parameters.filterMAG);
		const uint32_t filterMin = ConvertFilterToOpenGL(info.parameters.filterMIN);

		handle = RenderCommand::CreateHandle<GLuint>([ptr = info.pixels, format, internalFormat, type, miplevel, shadow, w, h, wrap, filterMag, filterMin, name = info.name](GLuint& texture) {
			glCreateTextures(target, 1, &texture);
			glBindTexture(target, texture);

			glTextureStorage2D(texture, miplevel, internalFormat, w, h);
			//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			if (ptr) {
				glTextureSubImage2D(texture, 0, 0, 0, w, h, format, type, ptr);
			}

			if (miplevel) {
				glGenerateTextureMipmap(texture);
				glTextureParameterf(texture, GL_TEXTURE_LOD_BIAS, -0.8f);
			}

			if (wrap != 0) {
				glTextureParameteri(texture, GL_TEXTURE_WRAP_S, wrap);
				glTextureParameteri(texture, GL_TEXTURE_WRAP_T, wrap);
			}

			if (filterMag != 0) {
				glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, filterMag);
			}

			if (filterMin != 0) {
				glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, filterMin);
			}

			if (shadow) {
				glTextureParameteri(texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTextureParameteri(texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
				glTextureParameterfv(texture, GL_TEXTURE_BORDER_COLOR, borderColor);
			}
		});

	}

	void GLTexture2D::UpdateData(const void* pixels)
	{
		static_assert(true, "Texture2D::UpdateData(): currently not implemented");
	}

	void GLTexture2D::Resize(uint32_t width, uint32_t height)
	{
		if (info.w == width && info.h == height) {
			return;
		}

		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& texture) {
			if (texture) {
				glDeleteTextures(1, &texture);
			}
		});

		info.w = width;
		info.h = height;
		CreateFromSource();
	}

	GLTexture2DArray::GLTexture2DArray(const CreateInfo& createInfo)
		: info(createInfo)
	{
		const auto& textureParameters = info.parameters;
		const uint32_t format = ConvertFormatToOpenGL(textureParameters.format);
		const uint32_t internalFormat = ConvertInternalFormatToOpenGL(textureParameters.format);
		const uint32_t type = ConvertTextureFormatToOpenGLDataType(textureParameters.format);
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;

		const uint32_t wrap = ConvertWrapToOpenGL(info.parameters.wrap);
		const uint32_t filterMag = ConvertFilterToOpenGL(info.parameters.filterMAG);
		const uint32_t filterMin = ConvertFilterToOpenGL(info.parameters.filterMIN);

		handle = RenderCommand::CreateHandle<GLuint>([ptr = info.pixels, format, internalFormat, type, miplevel, w = info.w, h = info.h, depth = info.depth, wrap, filterMag, filterMin, shadow = info.shadow](GLuint& texture) {
			glCreateTextures(target, 1, &texture);
			glTextureStorage3D(texture, miplevel, internalFormat, w, h, depth);
			
			if (ptr) {
				glTextureSubImage3D(texture, 0, 0, 0, 0, w, h, 1, format, type, ptr);
			
				if (miplevel) {
					glGenerateTextureMipmap(texture);
					glTextureParameterf(texture, GL_TEXTURE_LOD_BIAS, -0.8f);
				}
			}
			
			if (wrap != 0) {
				glTextureParameteri(texture, GL_TEXTURE_WRAP_S, wrap);
				glTextureParameteri(texture, GL_TEXTURE_WRAP_T, wrap);
			}
			
			if (filterMag != 0) {
				glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, filterMag);
			}
			
			if (filterMin != 0) {
				glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, filterMin);
			}
			
			if (shadow) {
				glTextureParameteri(texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTextureParameteri(texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
				glTextureParameterfv(texture, GL_TEXTURE_BORDER_COLOR, borderColor);
			}
		});
	}

	GLTexture2DArray::~GLTexture2DArray()
	{
		if (!handle) return;
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& texture) {
			if (texture) {
				glDeleteTextures(1, &texture);
			}
		});
	}

	void GLTexture2DArray::Bind(uint32_t slot) const
	{
		RenderCommand::Submit(handle, [slot](GLuint& texture) {
			glBindTextureUnit(slot, texture);
		});
	}

	void GLTexture2DArray::BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered, uint32_t layer) const
	{
		RenderCommand::Submit(handle, [slot, level, mapBit, format, layered, layer](GLuint& texture) {
			glBindImageTexture(slot, texture, level, layered, layer, ConvertImageMapBitToOpenGL(mapBit), ConvertInternalFormatToOpenGL(format));
		});
	}

	void GLTexture2DArray::GenerateMipmap()
	{
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;

		RenderCommand::Submit(handle, [miplevel](GLuint& texture) {
			if (miplevel) {
				glGenerateTextureMipmap(texture);
				glTextureParameterf(texture, GL_TEXTURE_LOD_BIAS, -0.8f);
			}
		});
	}

	void GLTexture2DArray::AddData(uint32_t layer, uint32_t size, const void* data)
	{
		const uint32_t format = ConvertFormatToOpenGL(info.parameters.format);
		const uint32_t type = ConvertTextureFormatToOpenGLDataType(info.parameters.format);
		RenderCommand::Submit(handle, [layer, width = info.w, height = info.h, ptr = data, format, type](GLuint& texture) {
			glTextureSubImage3D(texture, 0, 0, 0, layer, width, height, 1, format, type, ptr);
		});
	}

	GLTextureCubemap::GLTextureCubemap(const CreateInfo& createInfo)
		: info(createInfo)
	{
		TextureParameters& parameters = info.parameters;
		uint32_t format = ConvertFormatToOpenGL(parameters.format);
		uint32_t internalFormat = ConvertInternalFormatToOpenGL(parameters.format);
		uint32_t type = ConvertTextureFormatToOpenGLDataType(parameters.format);
		uint32_t wrap = ConvertWrapToOpenGL(parameters.wrap);
		uint32_t minFilter = ConvertFilterToOpenGL(parameters.filterMIN);
		uint32_t magFilter = ConvertFilterToOpenGL(parameters.filterMAG);
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;

		auto&& variant = std::move(info.data);

		texture = RenderCommand::CreateHandle<GLuint>([format, internalFormat, type, wrap, minFilter, magFilter, miplevel, w = info.w, h = info.h, data = std::move(variant)](GLuint& gl) {
			glCreateTextures(target, 1, &gl);
			glBindTexture(target, gl);

			glTextureStorage2D(gl, miplevel, internalFormat, w, h);

			using PixelsWithOffsets = CreateInfo::PixelsWithOffsets;
			using FacesPixels = CreateInfo::FacesPixels;

			bool isPixelsWithOffsets = std::visit([](auto&& args) {
				using T = std::decay_t<decltype(args)>;
				if constexpr (std::is_same_v<T, PixelsWithOffsets>) {
					return true;
				} return false;
			}, data);

			if (isPixelsWithOffsets) {
				const PixelsWithOffsets& pixelsWithOffset = std::get<PixelsWithOffsets>(data);

				unsigned int face_size = calculate_face_size(w, h);
				if (const void* pixels = pixelsWithOffset.pixels) {
					for (size_t i = 0; i < pixelsWithOffset.offsets.size(); ++i) {
						uint32_t offset = pixelsWithOffset.offsets[i];
						glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, w, h, format, type, (const char*)pixels + offset);
					}
				}
			} else {
				const FacesPixels& facesPixels = std::get<FacesPixels>(data);
				auto& pixels = facesPixels.pixels;

				for (size_t i = 0; i < pixels.size(); ++i) {
					glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, w, h, format, type, pixels[i]);
				}
			}

			if (miplevel) {
				glGenerateTextureMipmap(gl);
				glTextureParameterf(gl, GL_TEXTURE_LOD_BIAS, -0.8f);
			}

			if (wrap != (uint32_t)TextureWrap::None) {
				glTextureParameteri(gl, GL_TEXTURE_WRAP_S, wrap);
				glTextureParameteri(gl, GL_TEXTURE_WRAP_T, wrap);
				glTextureParameteri(gl, GL_TEXTURE_WRAP_R, wrap);
			}

			if (magFilter != (uint32_t)TextureFilter::None) {
				glTextureParameteri(gl, GL_TEXTURE_MAG_FILTER, magFilter);
			}

			if (minFilter != (uint32_t)TextureFilter::None) {
				glTextureParameteri(gl, GL_TEXTURE_MIN_FILTER, minFilter);
			}
		});
	}

	GLTextureCubemap::~GLTextureCubemap()
	{
		RenderCommand::DestroyHandle<GLuint>(texture, [](GLuint& texture) {
			if (texture) {
				glDeleteTextures(1, &texture);
			}
		});
	}

	void GLTextureCubemap::Bind(uint32_t slot) const
	{
		RenderCommand::Submit(texture, [this, slot](GLuint& texture) {
			glBindTextureUnit(slot, texture);
		});
	}

	void GLTextureCubemap::BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered, uint32_t layer) const
	{
		RenderCommand::Submit(texture, [this, slot, level, mapBit, format, layered, layer](GLuint& texture) {
			glBindImageTexture(slot, texture, level, layered, layer, ConvertImageMapBitToOpenGL(mapBit), ConvertInternalFormatToOpenGL(format));
		});
	}

	void GLTextureCubemap::GenerateMipmap()
	{
		const uint32_t miplevel = info.parameters.miplevel == 0 ? CalculateMipCount(info.w, info.h) : info.parameters.miplevel;

		RenderCommand::Submit(texture, [miplevel](GLuint& texture) {
			if (miplevel) {
				glGenerateTextureMipmap(texture);
				glTextureParameterf(texture, GL_TEXTURE_LOD_BIAS, -0.8f);
			}
		});
	}

	uint32_t ConvertFilterToOpenGL(TextureFilter filter)
	{
		switch (filter) {
			case TextureFilter::None: return NULL;
			case TextureFilter::Linear: return GL_LINEAR;
			case TextureFilter::Nearest: return GL_NEAREST;
			case TextureFilter::Nearest_Mipmap_Linear: return GL_NEAREST_MIPMAP_LINEAR;
			case TextureFilter::Nearest_Mipmap_Nearest: return GL_NEAREST_MIPMAP_NEAREST;
			case TextureFilter::Linear_Mipmap_Nearest: return GL_LINEAR_MIPMAP_NEAREST;
			case TextureFilter::Linear_Mipmap_Linear: return GL_LINEAR_MIPMAP_LINEAR;
		} return NULL;
	}

	uint32_t ConvertWrapToOpenGL(TextureWrap wrap)
	{
		switch (wrap) {
			case TextureWrap::None: return NULL;
			case TextureWrap::Repeat: return GL_REPEAT;
			case TextureWrap::Clamp: return GL_CLAMP;
			case TextureWrap::Mirrored_Repeat: return GL_MIRRORED_REPEAT;
			case TextureWrap::Clamp_To_Edge: return GL_CLAMP_TO_EDGE;
			case TextureWrap::Clamp_To_Border: return GL_CLAMP_TO_BORDER;
			case TextureWrap::Mirror_Clamp_To_Edge: return GL_MIRROR_CLAMP_TO_EDGE;
		} return NULL;
	}

	uint32_t ConvertFormatToOpenGL(TextureFormat format)
	{
		switch (format) {
			case TextureFormat::RGB8: return GL_RGB;
			case TextureFormat::R8_INT: return GL_RED;
			case TextureFormat::RGBA8_SRGB: return GL_RGBA;
				//case TextureFormat::RGBA8_FLOAT: return GL_RGBA;
			case TextureFormat::RGB32_FLOAT: return GL_RGB;
			case TextureFormat::RGBA32_FLOAT: return GL_RGBA;
			case TextureFormat::D24_S8: return GL_DEPTH_STENCIL;
			case TextureFormat::D32_FLOAT: return GL_DEPTH_COMPONENT;
			case TextureFormat::S8_UINT: return GL_STENCIL_INDEX;
			case TextureFormat::R32_FLOAT: return GL_RED;

				//I left them because maybe I might want some of these later.
				//case TextureFormat::RED8: return GL_RED;
				//case TextureFormat::RED16: return GL_RED;
				//case TextureFormat::RGB8: return GL_RGB;
				//case TextureFormat::RGB32F: return GL_RGB;
				//case TextureFormat::RGBA8: return GL_RGBA;
				//case TextureFormat::SRGBA8: return GL_RGBA;
				//case TextureFormat::BGRA8: return GL_BGRA;
				//case TextureFormat::R11_G11_B10F: return GL_RGB;
				//case TextureFormat::RGBA16F: return GL_RGBA;
				//case TextureFormat::RGBA32F: return GL_RGBA;
				//case TextureFormat::BC1: return GL_COMPRESSED_RGBA_S3TC;
				//case TextureFormat::BC1_SRGB: return GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT;
				//case TextureFormat::BC2
				//case TextureFormat::BC2_SRGB
				//case TextureFormat::BC3
				//case TextureFormat::BC3_SRGB
				//case TextureFormat::BC4
				//case TextureFormat::BC5
				//case TextureFormat::ETC1: return 0;
				//case TextureFormat::RED32_UINT: return GL_RED;
				//case TextureFormat::RED32_FLOAT: return GL_RED;
				//case TextureFormat::DEPTH32_FLOAT: return GL_DEPTH_COMPONENT;
				//case TextureFormat::DEPTH24_STENCIL8: return GL_DEPTH_STENCIL;
			default: return GL_RGBA;
		}
	}

	uint32_t ConvertInternalFormatToOpenGL(TextureFormat internalFormat)
	{
		switch (internalFormat) {
			case TextureFormat::RGB8: return GL_RGB8;
			case TextureFormat::R8_INT: return GL_R8I;
				//case TextureFormat::RGBA8_SRGB: return GL_SRGB8_ALPHA8;
			case TextureFormat::RGBA8_SRGB: return GL_RGBA8;
				//case TextureFormat::RGBA8_FLOAT: return GL_SRGB8_ALPHA8;
			case TextureFormat::RGB32_FLOAT: return GL_RGB32F;
			case TextureFormat::RGBA32_FLOAT: return GL_RGBA32F;
			case TextureFormat::D24_S8: return GL_DEPTH24_STENCIL8;
			case TextureFormat::D32_FLOAT: return GL_DEPTH_COMPONENT32F;
			case TextureFormat::S8_UINT: return GL_STENCIL_INDEX8;
			case TextureFormat::R32_FLOAT: return GL_R32F;

				//leave it, we might need em later
				//case TextureFormat::RED8: return GL_R8;
				//case TextureFormat::RED16: return GL_R16;
				//case TextureFormat::RGB8: return GL_RGB8;
				//case TextureFormat::RGBA8: return GL_RGBA8;
				//case TextureFormat::RGB32F: return GL_RGB32F;
				//case TextureFormat::SRGBA8: return GL_SRGB8_ALPHA8;
				//case TextureFormat::BGRA8: return GL_RGBA8;
				//case TextureFormat::R11_G11_B10F: return GL_R11F_G11F_B10F;
				//case TextureFormat::RGBA16F: return GL_RGBA16F;
				//case TextureFormat::RGBA32F: return GL_RGBA32F;
				//
				//case TextureFormat::ETC1: return 0;
				//
				////case TextureFormat::BC1: return GL_COMPRESSED_RGBA_S3TC;
				////case TextureFormat::BC1_SRGB: return GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT;
				////case TextureFormat::BC2
				////case TextureFormat::BC2_SRGB
				////case TextureFormat::BC3
				////case TextureFormat::BC3_SRGB
				////case TextureFormat::BC4
				////case TextureFormat::BC5
				//
				//case TextureFormat::RED32_UINT: return GL_R32UI;
				//case TextureFormat::RED32_FLOAT: return GL_R32F;
				//case TextureFormat::DEPTH32_FLOAT: return GL_DEPTH_COMPONENT32;
				//case TextureFormat::DEPTH24_STENCIL8: return GL_DEPTH24_STENCIL8;
				//default: return GL_RGB8;
		}

		//should never happen
		return GL_RGBA32F;
	}

	uint32_t ConvertTextureFormatToOpenGLDataType(TextureFormat format)
	{
		switch (format) {
			case TextureFormat::RGB8: return GL_UNSIGNED_BYTE;
			case TextureFormat::R8_INT: return GL_INT;
			case TextureFormat::RGBA8_SRGB: return GL_UNSIGNED_BYTE;
				//case TextureFormat::RGBA8_FLOAT: return GL_FLOAT;
			case TextureFormat::RGB32_FLOAT: return GL_FLOAT;
			case TextureFormat::RGBA32_FLOAT: return GL_FLOAT;
			case TextureFormat::D24_S8: return GL_UNSIGNED_INT_24_8;
			case TextureFormat::D32_FLOAT: return GL_FLOAT;
			case TextureFormat::S8_UINT: return GL_UNSIGNED_INT;
		};

		//should never happen
		return GL_UNSIGNED_BYTE;
	}

	uint32_t ConvertImageMapBitToOpenGL(ImageMapBit mapBit)
	{
		uint32_t result = (mapBit & ImageMapBit::Read) == ImageMapBit::Read ? GL_READ_ONLY : 0;
		result += (mapBit & ImageMapBit::Write) == ImageMapBit::Write ? GL_WRITE_ONLY : 0;
		result += (mapBit & ImageMapBit::ReadWrite) == ImageMapBit::ReadWrite ? GL_READ_WRITE : 0;
		return result;
	}
}