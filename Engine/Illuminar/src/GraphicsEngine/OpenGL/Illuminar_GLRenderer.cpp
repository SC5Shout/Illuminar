#include "Illuminar_pch.h"
#include "Illuminar_GLRenderer.h"

#include "GameEngine/Window/Illuminar_GLWindow.h"

#include "glad/glad.h"
#include "SDL.h"

#include "Illuminar_GLBuffer.h"
#include "GraphicsEngine/Illuminar_Material.h"
#include "GraphicsEngine/Illuminar_GraphicsPipelineCache.h"

namespace Illuminar::GFX {
	SDL_GLContext GLRenderer::context = nullptr;

	static void OpenGLLogMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
	{
		if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
			if (strcmp(message, "Texture state usage warning: The texture object") > 0) {
				return;
			}
			LogError("{0}", message);
			__debugbreak();
		} //else LogTrace("{0}", message);
	}

	GLRenderer::GLRenderer()
	{
		//if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		//	std::cout << fmt::format("SDL: failed to init SDL {}", SDL_GetError()) << "\n";
		//	__debugbreak();
		//}
		//
		//SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		//SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		//SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
		//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
		//SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
	}

	GLRenderer::~GLRenderer()
	{
		SDL_GL_DeleteContext(context);
	}

	void GLRenderer::Init(const Ref<Window>& window)
	{
		context = SDL_GL_CreateContext(window->getHandle());
		if (!context) {
			throw std::runtime_error("GLWindow: failed to create a context");
		}

		gladLoadGLLoader(SDL_GL_GetProcAddress);

		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

		glDebugMessageCallback(OpenGLLogMessage, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void GLRenderer::Clear(BufferBit clearBit)
	{
		glClear(ConvertBufferBitToOpenGL(clearBit));
	}

	void GLRenderer::setViewport(int x, int y, uint32_t w, uint32_t h)
	{
		glViewport(x, y, w, h);
	}

	void GLRenderer::setScissor(int x, int y, uint32_t w, uint32_t h)
	{
		glScissor(x, y, w, h);
	}

	void GLRenderer::setClearColor(uint32_t color)
	{
		uint32_t r;
		uint32_t g;
		uint32_t b;
		uint32_t a;

		a = (color >> 24) & 0xff;
		b = (color >> 16) & 0xff;
		g = (color >> 8) & 0xff;
		r = color & 0xff;

		glClearColor((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, (float)a / 255.0f);
	}

	void GLRenderer::setClearColor(float r, float g, float b, float a)
	{
		glClearColor(r, g, b, a);
	}

	void GLRenderer::setLineThickness(float thickness)
	{
		glLineWidth(thickness);
	}

	void GLRenderer::DrawArrays(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const
	{
		if (!currentPipeline) {
			EngineLogError("RenderCommand::DrawArrays() error: Pipeline is nullptr");
			__debugbreak();
		}

		auto mode = currentPipeline->getInfo().drawMode;
		glDrawArraysInstancedBaseInstance(ConvertDrawModeToOpenGL(mode), firstVertex, vertexCount, instanceCount, firstInstance);
	}

	void GLRenderer::DrawElements(uint32_t elementCount, ElementDataType dataType, uint32_t instanceCount, uint32_t firstElement, int32_t vertexOffset, uint32_t firstInstance) const
	{
		if (!currentPipeline) {
			EngineLogError("RenderCommand::DrawElements() error: Pipeline is nullptr");
			__debugbreak();
		}

		auto mode = currentPipeline->getInfo().drawMode;
		const void* offset = (const void*)((dataType == ElementDataType::Uint32 ? 4 : 2) * firstElement);
		glDrawElementsInstancedBaseVertexBaseInstance(ConvertDrawModeToOpenGL(mode), elementCount, ConvertElementDataTypeToOpenGL(dataType), offset, instanceCount, vertexOffset, firstInstance);
	}

	void GLRenderer::DrawElementsIndirect(ElementDataType dataType, uint32_t drawCount, uint64_t indirectBufferOffset) const
	{
		if (!currentPipeline) {
			EngineLogError("RenderCommand::DrawElementsIndirect() error: Pipeline is nullptr");
			__debugbreak();
		}

		auto mode = currentPipeline->getInfo().drawMode;
		if (drawCount == 1) {
			glDrawElementsIndirect(ConvertDrawModeToOpenGL(mode), ConvertElementDataTypeToOpenGL(dataType), (const void*)(indirectBufferOffset));
		} else glMultiDrawElementsIndirect(ConvertDrawModeToOpenGL(mode), ConvertElementDataTypeToOpenGL(dataType), (const void*)(indirectBufferOffset), drawCount, sizeof(GFX::DrawElementsIndirectCommand));
	}

	void GLRenderer::DispatchCompute(uint32_t groupX, uint32_t groupY, uint32_t groupZ) const
	{
		//if (!currentPipeline) {
		//	EngineLogError("RenderCommand::DispatchCompute() error: Pipeline is nullptr");
		//	__debugbreak();
		//}

		glDispatchCompute(groupX, groupY, groupZ);
	}

	void GLRenderer::MemoryBarrierX(MemoryBarrierType barrier) const
	{
		glMemoryBarrier(ConvertBarrierTypeToOpenGL(barrier));
	}

	uint32_t GLRenderer::ConvertBufferBitToOpenGL(BufferBit type)
	{
		uint32_t result = (type & BufferBit::Color) == BufferBit::Color ? GL_COLOR_BUFFER_BIT : NULL;
		result += (type & BufferBit::Depth) == BufferBit::Depth ? GL_DEPTH_BUFFER_BIT : NULL;
		result += (type & BufferBit::Stencil) == BufferBit::Stencil ? GL_STENCIL_BUFFER_BIT : NULL;

		return result;
	}

	uint32_t GLRenderer::ConvertDrawModeToOpenGL(DrawMode mode)
	{
		switch (mode) {
			case DrawMode::Points: return GL_POINTS;
			case DrawMode::Triangles: return GL_TRIANGLES;
			case DrawMode::Patches: return GL_PATCHES;
			case DrawMode::TriangleFan: return GL_TRIANGLE_FAN;
			case DrawMode::TriangleStrip: return GL_TRIANGLE_STRIP;
			case DrawMode::Lines: return GL_LINES;
			case DrawMode::LineLoop: return GL_LINE_LOOP;
			case DrawMode::LineStrip: return GL_LINE_STRIP;
		} return NULL;
	}

	int GLRenderer::ConvertElementDataTypeToOpenGL(ElementDataType type)
	{
		switch (type) {
			case ElementDataType::Uint16: return GL_UNSIGNED_SHORT;
			case ElementDataType::Uint32: return GL_UNSIGNED_INT;
		} return GL_UNSIGNED_INT;
	}

	uint32_t GLRenderer::ConvertBarrierTypeToOpenGL(MemoryBarrierType barrier)
	{
		uint32_t result = (barrier & MemoryBarrierType::All) == MemoryBarrierType::All ? GL_ALL_BARRIER_BITS : NULL;
		result += (barrier & MemoryBarrierType::CommandBarrier) == MemoryBarrierType::CommandBarrier ? GL_COMMAND_BARRIER_BIT : NULL;
		result += (barrier & MemoryBarrierType::ShaderStorage) == MemoryBarrierType::ShaderStorage ? GL_SHADER_STORAGE_BARRIER_BIT : NULL;
		result += (barrier & MemoryBarrierType::ShaderImageAccess) == MemoryBarrierType::ShaderImageAccess ? GL_SHADER_IMAGE_ACCESS_BARRIER_BIT : NULL;
		result += (barrier & MemoryBarrierType::TextureFetch) == MemoryBarrierType::TextureFetch ? GL_TEXTURE_FETCH_BARRIER_BIT : NULL;

		return result;
	}
}