#pragma once

#include "GraphicsEngine/Buffer//Illuminar_RenderBuffer.h"

#include "glad/glad.h"

namespace Illuminar::GFX {
	struct GLRenderBuffer : RenderBuffer
	{
		GLRenderBuffer(uint32_t w, uint32_t h, TextureFormat format, uint32_t samples);
		~GLRenderBuffer() override;

		void Bind() const override;
		void Unbind() const override;

		void Resize(uint32_t w, uint32_t h, TextureFormat format = TextureFormat::RGBA8_SRGB, uint32_t samples = 1) override;

	private:
		GLuint buffer = 0;

		[[nodiscard]] uint32_t ConvertInternalFormatToOpenGL(TextureFormat internalFormat);
	};
}