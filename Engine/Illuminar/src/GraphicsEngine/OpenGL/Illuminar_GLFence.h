#pragma once

#include "GraphicsEngine/Illuminar_Fence.h"

#include "glad/glad.h"

namespace Illuminar {
	namespace GFX {
		struct GLFence : Fence
		{
			GLFence();
			~GLFence() override;

			[[nodiscard]] void Wait() override;
			[[nodiscard]] FenceStatus getStatus() override;

		private:
			Handle<GLsync> handle;

			using Status = std::atomic<GLenum>;
			std::shared_ptr<Status> result{ std::make_shared<Status>(GL_TIMEOUT_EXPIRED) };
		};
	}
}