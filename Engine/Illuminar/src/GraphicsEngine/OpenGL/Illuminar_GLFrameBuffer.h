#pragma once

#include "GraphicsEngine/RenderTarget/Illuminar_FrameBuffer.h"

#include "glad/glad.h"

namespace Illuminar::GFX {
	struct GLFrameBuffer : FrameBuffer
	{
		GLFrameBuffer(const CreateInfo& createInfo);
		~GLFrameBuffer() override;

		GLFrameBuffer(const GLFrameBuffer& other) = delete;
		GLFrameBuffer& operator =(const GLFrameBuffer& other) = delete;

		void Bind(const State& params) const override;
		void Unbind() const override;

		void Resize(uint32_t w, uint32_t h) override;

		void Blit(const Ref<FrameBuffer>& other) override;

		//private:
		void CreateFrameBuffer();
		bool CheckFrameBufferStatus();
		bool status = false;

		CreateInfo info;

		Handle<GLuint> handle;

		static void AttachColorTextures(GLuint& buffer, uint32_t count, const std::array<Ref<GFX::Texture>, CreateInfo::MAX_COLOR_ATTACHMENT_COUNT>& texture, uint32_t target);
		static void AttachDepthStencilTexture(GLuint& buffer, uint32_t colorCount, const Ref<Texture>& texture, uint32_t target);

		static const uint32_t ConvertFrameBufferTargetToOpenGL(FrameBufferTarget target);
	};
}