#include "Illuminar_pch.h"
#include "Illuminar_GLGraphicsPipeline.h"

#include "Illuminar_GLRenderer.h"
#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	GLenum ConvertDataTypeToOpenGL(DataType type)
	{
		switch (type) {
			case DataType::R8_UINT: return GL_UNSIGNED_BYTE;
			case DataType::RGBA8_UNORM: return GL_UNSIGNED_BYTE;
			case DataType::R16_UINT: return GL_UNSIGNED_SHORT;
			case DataType::R32_UINT: return GL_UNSIGNED_INT;
			case DataType::R32_FLOAT: return GL_FLOAT;

			case DataType::R32_INT: return GL_INT;
			case DataType::R8_INT: return GL_BOOL;

			case DataType::RG32_FLOAT: return GL_FLOAT;
			case DataType::RGB32_FLOAT: return GL_FLOAT;
			case DataType::RGBA32_FLOAT: return GL_FLOAT;

			case DataType::RG32_INT: return GL_INT;
			case DataType::RGB32_INT: return GL_INT;
			case DataType::RGBA32_INT: return GL_INT;

			case DataType::Mat3: return GL_FLOAT;
			case DataType::Mat4: return GL_FLOAT;
		} return NULL;
	}

	bool IsDataTypeNormalized(DataType type)
	{
		switch (type) {
			case DataType::R8_UINT: return false;
			case DataType::RGBA8_UNORM: return true;
			case DataType::R16_UINT: return false;
			case DataType::R32_UINT: return false;
			case DataType::R32_FLOAT: return false;

			case DataType::R32_INT: return false;
			case DataType::R8_INT: return false;

			case DataType::RG32_FLOAT: return false;
			case DataType::RGB32_FLOAT: return false;
			case DataType::RGBA32_FLOAT: return false;

			case DataType::RG32_INT: return false;
			case DataType::RGB32_INT: return false;
			case DataType::RGBA32_INT: return false;

			case DataType::Mat3: return false;
			case DataType::Mat4: return false;
		} return false;
	}

	GLGraphicsPipeline::GLGraphicsPipeline(const CreateInfo& createInfo)
		: info(createInfo),
		  raster(info.rasterizerState),
		  depth(info.depthState),
		  stencil(info.stencilState),
		  blend(info.blendState),
		  renderer(RenderCommand::as<GLRenderer>())
	{
		handle = RenderCommand::CreateHandle<GLuint>([this, layout = createInfo.bufferLayout](GLuint& vao) {
			glCreateVertexArrays(1, &vao);

			//should we do this there or in GLVertexBuffer::Bind() ???
			uint32_t i = 0;
			//for (uint32_t i = 0; i < layouts.size(); ++i) {
				//const auto& layout = layouts[i];
				uint32_t vertexBufferAttributeIndex = 0;
				for (auto&& element : layout) {
					glVertexArrayAttribBinding(vao, vertexBufferAttributeIndex, i);
					glVertexArrayAttribFormat(vao, vertexBufferAttributeIndex, element.getComponentCount(), ConvertDataTypeToOpenGL(element.type), IsDataTypeNormalized(element.type), element.offset);
					glEnableVertexArrayAttrib(vao, vertexBufferAttributeIndex);
					++vertexBufferAttributeIndex;
				}
			//}
		});
	}

	GLGraphicsPipeline::~GLGraphicsPipeline()
	{
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& vao) {
			glDeleteVertexArrays(1, &vao);	
		});
	}

	void GLGraphicsPipeline::Bind() const
	{
		raster.Bind();
		depth.Bind();
		stencil.Bind();
		blend.Bind();

		if (info.shader) {
			info.shader->Bind();
		}

		Ref<const GLGraphicsPipeline> ref = this;
		RenderCommand::Submit(handle, [ref = std::move(ref), &renderer = renderer](GLuint& vao) {
			glBindVertexArray(vao);
			renderer->currentPipeline = ref;
		});
	}

	void GLGraphicsPipeline::Rebind(const CreateInfo& createInfo)
	{
		info = createInfo;
		raster = info.rasterizerState;
		depth = info.depthState;
		stencil = info.stencilState;
		blend = info.blendState;

		Bind();
	}
}