#include "Illuminar_pch.h"
#include "Illuminar_GLShader.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	GLShader::GLShader(const std::filesystem::path& filepath, std::unordered_map<uint32_t, std::vector<uint32_t>>&& binaries)
	{
		this->filepath = filepath.generic_string();
		this->name = filepath.filename().stem().string();

		handle = RenderCommand::CreateHandle<GLuint>([binaries = std::move(binaries)](GLuint& program) {
			if (program)
				glDeleteProgram(program);

			program = glCreateProgram();

			CompileFromBinaries(program, binaries);
		});
		//getPushConstantLocations();
	}

	GLShader::~GLShader()
	{
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& program) {
			glDeleteProgram(program);
		});
	}

	void GLShader::Bind() const
	{
		RenderCommand::Submit(handle, [](GLuint& program) {
			glUseProgram(program);
		});
	}

	void GLShader::setUniform(const std::string& name, int value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniform1i(shaderProgram, location, value);
		});
	}

	void GLShader::setUniform(const std::string& name, uint64_t value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniformHandleui64ARB(shaderProgram, location, value);
		});
	}

	void GLShader::setUniform(const std::string& name, float value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniform1f(shaderProgram, location, value);
		});
	}

	void GLShader::setUniform(const std::string& name, const Vector2<float>& value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniform2f(shaderProgram, location, value.x, value.y);
		});
	}

	void GLShader::setUniform(const std::string& name, const Vector3<float>& value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniform3f(shaderProgram, location, value.x, value.y, value.z);
		});
	}

	void GLShader::setUniform(const std::string& name, const Vector4<float>& value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniform4f(shaderProgram, location, value.x, value.y, value.z, value.w);
		});
	}

	void GLShader::setUniform(const std::string& name, const Mat4& value) const
	{
		GFX::RenderCommand::Submit(handle, [self = Ref<const GLShader>(this), name, value](GLuint& shaderProgram) {
			//GLint location = self->pushConstantLocations.at(name);
			GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());
			glProgramUniformMatrix4fv(shaderProgram, location, 1, GL_FALSE, glm::value_ptr(value));
		});
	}

	void GLShader::getPushConstantLocations()
	{
		RenderCommand::Submit(handle, [self = Ref<GLShader>(this)](GLuint& program) mutable {
			for (const auto& [name, buffer] : self->pushConstantBlocks) {
				for (const auto& [name, uniform] : buffer.uniforms) {
					GLint location = glGetProgramResourceLocation(program, GL_UNIFORM, name.c_str());
					if (location == -1) {
						EngineLogWarn("{0}: could not find uniform location {1}", name.c_str(), location);
					}
			
					self->pushConstantLocations[name] = location;
				}
			}
		});
	}

	void GLShader::getSampledUniformLocations()
	{
		RenderCommand::Submit(handle, [self = Ref<GLShader>(this)](GLuint& shaderProgram) {
			glUseProgram(shaderProgram);
			for (const auto& [name, uniform] : self->sampledUniforms) {
				GLint location = glGetProgramResourceLocation(shaderProgram, GL_UNIFORM, name.c_str());

				if (location == -1) {
					EngineLogWarn("{0}: could not find uniform location {0}", name.c_str(), location);
				}

				glUniform1i(location, uniform.binding);
			}
		});
	}

	void GLShader::CompileFromBinaries(GLuint& program, const std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries)
	{
		std::array<uint32_t, (uint32_t)ShaderType::Count> shadersIDs;
		uint32_t shadersIdIndex = 0;
		for (const auto [type, binary] : binaries) {
			uint32_t id = glCreateShader(type);

			glShaderBinary(1, &id, GL_SHADER_BINARY_FORMAT_SPIR_V, binary.data(), binary.size() * sizeof(uint32_t));
			glSpecializeShader(id, "main", 0, nullptr, nullptr);

			ShaderCompiledError(id, type);

			shadersIDs[shadersIdIndex++] = id;
			glAttachShader(program, id);
		}

		glLinkProgram(program);
		ShaderLinkedError(program, shadersIDs, shadersIdIndex);

		for (uint32_t i = 0; i < shadersIdIndex; ++i) {
			glDetachShader(program, shadersIDs[i]);
			glDeleteShader(shadersIDs[i]);
		}
	}

	void GLShader::ShaderCompiledError(uint32_t shaderID, uint32_t type)
	{
		int isCompiled = 0;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			int maxLength = 0;
			glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<char> infoLog(maxLength);
			glGetShaderInfoLog(shaderID, maxLength, &maxLength, &infoLog[0]);

			glDeleteShader(shaderID);

			std::string shaderType;
			switch (type) {
				case GL_VERTEX_SHADER: shaderType = "Vertex shader\n"; break;
				case GL_FRAGMENT_SHADER: shaderType = "Fragment shader\n"; break;
				case GL_TESS_CONTROL_SHADER: shaderType = "Tess Control shader\n"; break;
				case GL_TESS_EVALUATION_SHADER: shaderType = "Tess evaluation shader\n"; break;
				case GL_GEOMETRY_SHADER: shaderType = "Fragment shader\n"; break;
				case GL_COMPUTE_SHADER: shaderType = "Compute shader\n"; break;
			}

			EngineLogError("{}: {}", shaderType, std::string(&infoLog[0]));

			return;
		}
	}

	void GLShader::ShaderLinkedError(GLuint& shaderProgram, std::array<uint32_t, (uint32_t)ShaderType::Count> shaders, uint32_t currentShadersCount)
	{
		int isLinked = 0;
		glGetProgramiv(shaderProgram, GL_LINK_STATUS, (int*)&isLinked);
		if (isLinked == GL_FALSE) {
			int maxLength = 0;
			glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<char> infoLog(maxLength);
			glGetProgramInfoLog(shaderProgram, maxLength, &maxLength, &infoLog[0]);

			glDeleteProgram(shaderProgram);
			for (uint32_t i = 0; i < currentShadersCount; ++i) {
				glDeleteShader(shaders[i]);
			}

			EngineLogError(&infoLog[0]);
;
			return;
		}
	}
}