#pragma once

#include "GraphicsEngine/Illuminar_Renderer.h"
#include "GraphicsEngine/OpenGL/Illuminar_GLGraphicsPipeline.h"

#include <SDL.h>

namespace Illuminar {
	struct Window;
}

namespace Illuminar::GFX {
	struct VertexBuffer;
	struct ElementBuffer;
	struct Material;
	struct IndirectDrawBuffer;

	struct GLRenderer final : Renderer
	{
		GLRenderer();
		~GLRenderer() override;

		void Init(const Ref<Window>& window) override;

		void Clear(BufferBit clearBit) override;
		void setViewport(int x, int y, uint32_t w, uint32_t h) override;
		void setScissor(int x, int y, uint32_t w, uint32_t h) override;

		void setClearColor(uint32_t color) override;
		void setClearColor(float r, float g, float b, float a) override;

		void setLineThickness(float thickness) override;

		void DrawArrays(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const override;
		void DrawElements(uint32_t elementCount, ElementDataType dataType, uint32_t instanceCount, uint32_t firstElement, int32_t vertexOffset, uint32_t firstInstance) const override;
		void DrawElementsIndirect(ElementDataType dataType, uint32_t drawCount, uint64_t indirectBufferOffset) const override;

		void DispatchCompute(uint32_t groupX, uint32_t groupY, uint32_t groupZ) const override;

		//Error	C3668	'Illuminar::GFX::GLRenderer::__faststorefence': method with override specifier 'override' did not override any base class methods	ImGui	C : \Users\mprze\source\repos\Illuminar\Engine\Illuminar\src\GraphicsEngine\OpenGL\Illuminar_GLRenderer.h	35
		void MemoryBarrierX(MemoryBarrierType barrier) const override;

		Ref<const GLGraphicsPipeline> defaultPipeline = nullptr;
		Ref<const GLGraphicsPipeline> currentPipeline = nullptr;

	//private:
		static uint32_t ConvertBufferBitToOpenGL(BufferBit type);
		static uint32_t ConvertDrawModeToOpenGL(DrawMode mode);
		static int ConvertElementDataTypeToOpenGL(ElementDataType type);
		static uint32_t ConvertBarrierTypeToOpenGL(MemoryBarrierType barrier);

		static SDL_GLContext context;
	};
}