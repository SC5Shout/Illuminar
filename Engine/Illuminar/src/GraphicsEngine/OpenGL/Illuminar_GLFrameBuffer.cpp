#include "Illuminar_pch.h"
#include "Illuminar_GLFrameBuffer.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLTexture.h"

namespace Illuminar::GFX {
	GLFrameBuffer::GLFrameBuffer(const CreateInfo& createInfo)
		: info(createInfo)
	{
		CreateFrameBuffer();
	}

	GLFrameBuffer::~GLFrameBuffer()
	{
		RenderCommand::DestroyHandle<GLuint>(handle, [](GLuint& buffer) {
			glDeleteFramebuffers(1, &buffer);
		});
	}

	template<typename T>
	auto CalculateDepthStencilBits = [](const Ref<Texture>&depthStencilTexture) {
		uint32_t depthStencilBits = 0;

		const auto& glTexture = depthStencilTexture.as<T>();
		
		switch (glTexture->info.parameters.format) {
			case TextureFormat::D32_FLOAT: {
				depthStencilBits = GL_DEPTH_BUFFER_BIT;
			} break;
			case TextureFormat::D24_S8: {
				depthStencilBits = GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
			} break;
			case TextureFormat::S8_UINT: {
				depthStencilBits = GL_STENCIL_BUFFER_BIT;
			} break;
		}

		return depthStencilBits;
	};

	void GLFrameBuffer::Bind(const State& params) const
	{
		RenderCommand::Submit(handle, [info = info, params = params](GLuint& buffer) {
			glBindFramebuffer(ConvertFrameBufferTargetToOpenGL(info.target), buffer);

			const auto& viewport = params.viewport;
			glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
			glDepthRangef(viewport.minDepth, viewport.maxDepth);

			uint32_t a = (params.clearColor >> 24) & 0xff;
			uint32_t b = (params.clearColor >> 16) & 0xff;
			uint32_t g = (params.clearColor >> 8) & 0xff;
			uint32_t r = params.clearColor & 0xff;
			
			glClearColor((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, (float)a / 255.0f);
				
			//TODO: cleaner way
			uint32_t depthStencilBits = 0;
			if (const auto& depthStencilTexture = info.depthStencilTexture) {
				switch (depthStencilTexture->getSamplerType()) {
					case SamplerType::TEXTURE_2D: {
						depthStencilBits = CalculateDepthStencilBits<GLTexture2D>(depthStencilTexture);
					} break;
					case SamplerType::TEXTURE_3D: {
						depthStencilBits = CalculateDepthStencilBits<GLTexture2DArray>(depthStencilTexture);
					} break;
				}
			}
			
			glClear((info.colorAttachmentCount == 0 ? 0 : GL_COLOR_BUFFER_BIT) | depthStencilBits);
		});
	}

	void GLFrameBuffer::Unbind() const
	{
		RenderCommand::Submit([this]() {
			glBindFramebuffer(ConvertFrameBufferTargetToOpenGL(info.target), 0);
		});
	}

	void GLFrameBuffer::Resize(uint32_t w, uint32_t h)
	{
		//if (currentParams.viewport.width == w && currentParams.viewport.height == h) {
		//	return;
		//}
		//
		//for (uint8_t i = 0; i < info.colorAttachmentCount; ++i) {
		//	auto& colorTexture = info.colorAttachments[i];
		//
		//	switch (colorTexture->getTarget()) {
		//		case GL_TEXTURE_2D:
		//			{
		//				GLTexture2D& glTexture = (GLTexture2D&)*colorTexture.get();
		//				glTexture.Resize(w, h);
		//			}
		//			break;
		//		case GL_TEXTURE_3D:
		//			EngineLogError("FrameBuffer::Resize(): resize 3d color attachment is not supported yet");
		//			break;
		//		case GL_TEXTURE_2D_MULTISAMPLE:
		//			EngineLogError("FrameBuffer::Resize(): resize 2d multisample color attachment is not supported yet");
		//			break;
		//		case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
		//			EngineLogError("FrameBuffer::Resize(): resize 2d multisample array color attachment is not supported yet");
		//			break;
		//		default:
		//			break;
		//	}
		//}
		//
		//auto& depthStencilTexture = info.depthStencilTexture;
		//if (depthStencilTexture) {
		//	GLTexture2D& glTexture = (GLTexture2D&)*depthStencilTexture.get();
		//	glTexture.Resize(w, h);
		//}
		//
		//RenderCommand::Submit(handle, [](GLuint& buffer) {
		//	glDeleteFramebuffers(1, &buffer);
		//});
		//
		//CreateFrameBuffer();
	}

	void GLFrameBuffer::Blit(const Ref<FrameBuffer>& other)
	{
		//RenderCommand::Submit(handle, [this, other = other, &currentParams = currentParams](GLuint& buffer) {
		//	auto otherBuffer = other.as<GLFrameBuffer>();
		//	auto otherHandle = otherBuffer->handle;
		//	GLuint& otherBufferID = *RenderCommand::handle_cast<GLuint*>(otherHandle);
		//	glBlitNamedFramebuffer(
		//		buffer, otherBufferID, 
		//		currentParams.viewport.x, 
		//		currentParams.viewport.y, 
		//		currentParams.viewport.width, 
		//		currentParams.viewport.width, 
		//		otherBuffer->currentParams.viewport.x,
		//		otherBuffer->currentParams.viewport.y,
		//		otherBuffer->currentParams.viewport.width,
		//		otherBuffer->currentParams.viewport.width,
		//		GL_COLOR_BUFFER_BIT, 
		//		GL_NEAREST
		//	);
		//});
	}

	void GLFrameBuffer::CreateFrameBuffer()
	{
		CheckFrameBufferStatus();
		uint32_t target = ConvertFrameBufferTargetToOpenGL(info.target);
		handle = RenderCommand::CreateHandle<GLuint>([colorCount = info.colorAttachmentCount, colorTextures = info.colorAttachments, depthStencilTexture = info.depthStencilTexture, target](GLuint& buffer) {
			glCreateFramebuffers(1, &buffer);

			AttachColorTextures(buffer, colorCount, colorTextures, target);
			if (depthStencilTexture) {
				AttachDepthStencilTexture(buffer, colorCount, depthStencilTexture, target);
			}
		});
	}

	bool GLFrameBuffer::CheckFrameBufferStatus()
	{
		//if (info.w <= 0 || info.h <= 0) {
		//	EngineLogError("FrameBuffer status: neither width nor height can be 0");
		//	return false;
		//}
		if (info.colorAttachmentCount < info.MIN_COLOR_ATTACHMENT_COUNT) {
			EngineLogError("FrameBuffer status: color attachment count cannot be less than 1");
			return false;
		}
		const uint8_t colorAttachmentCount = info.colorAttachmentCount;
		for (uint8_t i = 0; i < colorAttachmentCount; ++i) {
			if (!info.colorAttachments[i]) {
				EngineLogError("FrameBuffer status: color attachment [{}] cannot be nullptr", i);
				return false;
			}
		}

		return true;
	}

	void GLFrameBuffer::AttachColorTextures(GLuint& buffer, uint32_t count, const std::array<Ref<GFX::Texture>, CreateInfo::MAX_COLOR_ATTACHMENT_COUNT>& textures, uint32_t target)
	{
		for (uint8_t i = 0; i < count; ++i) {
			auto& colorTexture = textures[i];
	
			if (colorTexture->getTarget() == GL_TEXTURE_2D_ARRAY) {
				Handle<GLuint> textureHandle = (Handle<GLuint>)colorTexture.as<GLTexture2DArray>()->handle;
				GLuint& textureID = *RenderCommand::handle_cast<GLuint*>(textureHandle);

				glNamedFramebufferTextureLayer(buffer, GL_COLOR_ATTACHMENT0 + i, textureID, 0, 0);
			} else {
				Handle<GLuint> textureHandle = (Handle<GLuint>)colorTexture.as<GLTexture2D>()->handle;
				GLuint& textureID = *RenderCommand::handle_cast<GLuint*>(textureHandle);

				glNamedFramebufferTexture(buffer, GL_COLOR_ATTACHMENT0 + i, textureID, 0);
			}
		}
	}
	
	void GLFrameBuffer::AttachDepthStencilTexture(GLuint& buffer, uint32_t colorCount, const Ref<Texture>& texture, uint32_t target)
	{
		//if (texture->getTarget() == GL_TEXTURE_2D_ARRAY) {
		//	auto glDepthTexture = (GLTexture2DArray*)texture.get();
		//
		//	Handle<GLuint> textureHandle = (Handle<GLuint>)glDepthTexture->getTexture();
		//	GLuint& textureID = *RenderCommand::handle_cast<GLuint*>(textureHandle);
		//
		//	if (glDepthTexture->info.parameters.format == TextureFormat::D24_S8) {
		//		glNamedFramebufferTextureLayer(buffer, GL_DEPTH_STENCIL_ATTACHMENT, textureID, 0, 0);
		//	} else glNamedFramebufferTextureLayer(buffer, GL_DEPTH_ATTACHMENT, textureID, 0, 0);
		//} else {
			auto glDepthTexture = (GLTexture2D*)texture.get();

			Handle<GLuint> textureHandle = (Handle<GLuint>)glDepthTexture->getTexture();
			GLuint& textureID = *RenderCommand::handle_cast<GLuint*>(textureHandle);

			if (glDepthTexture->info.parameters.format == TextureFormat::D24_S8) {
				glNamedFramebufferTexture(buffer, GL_DEPTH_STENCIL_ATTACHMENT, textureID, 0);
			} else glNamedFramebufferTexture(buffer, GL_DEPTH_ATTACHMENT, textureID, 0);
		//}

		if (colorCount == 0) {
			glNamedFramebufferDrawBuffer(buffer, GL_NONE);
			glNamedFramebufferReadBuffer(buffer, GL_NONE);
		}
	}

	const uint32_t GLFrameBuffer::ConvertFrameBufferTargetToOpenGL(FrameBufferTarget target)
	{
		switch (target) {
			case FrameBufferTarget::Read: return GL_READ_FRAMEBUFFER;
			case FrameBufferTarget::Draw: return GL_DRAW_FRAMEBUFFER;
			case FrameBufferTarget::ReadAndDraw: return GL_FRAMEBUFFER;
		} return GL_FRAMEBUFFER;
	}
}