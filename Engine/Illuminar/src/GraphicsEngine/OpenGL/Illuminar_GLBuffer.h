#pragma once

#include "GraphicsEngine/Buffer/Illuminar_Buffer.h"

#include <Glad/glad.h>

namespace Illuminar::GFX {
	struct GLRenderer;
	//This class is just to handle general gl calls that does not have any buffer type information (whether it is array, index, shader storage, etc. buffer).
	struct GLGeneralBuffer
	{
		//protected:
		GLGeneralBuffer(uint64_t size, const void* data, Usage usage);
		GLGeneralBuffer(uint64_t size, const void* data, MapBit mapBit);

		GLGeneralBuffer(const GLGeneralBuffer& other) = delete;
		GLGeneralBuffer& operator=(const GLGeneralBuffer& other) = delete;

		virtual ~GLGeneralBuffer();

		virtual void Bind() const {}

		Ref<GLRenderer> renderer = nullptr;
		Handle<GLuint> handle;
		bool dataPrereserved = false;
	};

	struct GLBufferResource : BufferResource, GLGeneralBuffer
	{
		GLBufferResource(uint64_t size, const void* data, Usage usage)
			: GLGeneralBuffer(size, data, usage)
		{
		}

		GLBufferResource(uint64_t size, const void* data, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit)
		{
		}

		~GLBufferResource() override = default;

		GLBufferResource(const GLBufferResource& other) = delete;
		GLBufferResource& operator=(const GLBufferResource& other) = delete;

		void Bind(BufferType type) const override;
		void Bind(BufferType type, uint32_t bindingPoint) const override;
		void BindRange(BufferType type, uint32_t bindingPoint, uint64_t offset, uint64_t size) const override;

		void AddData(uint64_t size, const void* data) override;
		void AddSubData(uint64_t size, uint64_t offset, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();
	};

	struct GLVertexBuffer final : VertexBuffer, GLGeneralBuffer
	{
		GLVertexBuffer(const BufferLayout& layout, uint64_t size, const void* data, Usage usage)
			: GLGeneralBuffer(size, data, usage), layout(layout)
		{
		}

		GLVertexBuffer(const BufferLayout& layout, uint64_t size, const void* data, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit), layout(layout)
		{
		}

		~GLVertexBuffer() override = default;

		GLVertexBuffer(const GLVertexBuffer& other) = delete;
		GLVertexBuffer& operator=(const GLVertexBuffer& other) = delete;

		void Bind(uint32_t bindingPoint, uint32_t offset) const override;
		void AddData(uint64_t size, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();

		const BufferLayout& getLayout() const override { return layout; }

		BufferLayout layout;
	};

	struct GLElementBuffer final : ElementBuffer, GLGeneralBuffer
	{
		GLElementBuffer(uint64_t size, const void* data, Usage usage)
			: GLGeneralBuffer(size, data, usage)
		{
		}

		GLElementBuffer(uint64_t size, const void* data, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit)
		{
		}

		~GLElementBuffer() override = default;

		GLElementBuffer(const GLElementBuffer& other) = delete;
		GLElementBuffer& operator=(const GLElementBuffer& other) = delete;

		void Bind() const override;
		void AddData(uint64_t size, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();
	};

	struct GLVertexElementBuffer final : VertexElementBuffer, GLGeneralBuffer
	{
		GLVertexElementBuffer(uint64_t vertexDataSize, const void* vertexData, uint64_t elementDataSize, const void* elementData)
			: GLGeneralBuffer(align(vertexDataSize, ALIGNMENT) + align(elementDataSize, ALIGNMENT), nullptr, MapBit::Dynamic_Storage), vertexDataSize(vertexDataSize)
		{
			vertexAlignedSize = align(vertexDataSize, ALIGNMENT);
			elementAlignedSize = align(elementDataSize, ALIGNMENT);

			//glNamedBufferSubData(buffer, 0, elementDataSize, elementData);
			//glNamedBufferSubData(buffer, elementAlignedSize, vertexDataSize, vertexData);
		}

		void Bind() const override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();

		uint64_t vertexDataSize = 0;
		uint64_t vertexAlignedSize = 0;
		uint64_t elementAlignedSize = 0;

		static constexpr size_t ALIGNMENT = 64;
	};

	struct GLTextureBuffer final : TextureBuffer, GLGeneralBuffer
	{
		GLTextureBuffer(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
			: GLGeneralBuffer(size, data, usage), bindingPoint(bindingPoint)
		{
		}

		GLTextureBuffer(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit), bindingPoint(bindingPoint)
		{
		}

		~GLTextureBuffer() override = default;

		GLTextureBuffer(const GLTextureBuffer& other) = delete;
		GLTextureBuffer& operator=(const GLTextureBuffer& other) = delete;

		void Bind() const override;
		void BindRange(uint64_t offset, uint64_t size) const override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();

		uint32_t bindingPoint = 0;
	};

	struct GLUniformBuffer final : UniformBuffer, GLGeneralBuffer
	{
		GLUniformBuffer(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
			: GLGeneralBuffer(size, data, usage), bindingPoint(bindingPoint)
		{
		}

		GLUniformBuffer(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit), bindingPoint(bindingPoint)
		{
		}

		~GLUniformBuffer() override = default;

		GLUniformBuffer(const GLUniformBuffer& other) = delete;
		GLUniformBuffer& operator=(const GLUniformBuffer& other) = delete;

		void Bind() const override;
		void BindRange(uint64_t offset, uint64_t size) const override;

		void AddData(uint64_t size, const void* data) override;
		void AddSubData(uint64_t size, uint64_t offset, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();

		uint32_t bindingPoint = 0;
	};

	struct GLShaderStorageBuffer final : ShaderStorageBuffer, GLGeneralBuffer
	{
		GLShaderStorageBuffer(uint64_t size, const void* data, uint32_t bindingPoint, Usage usage)
			: GLGeneralBuffer(size, data, usage), bindingPoint(bindingPoint)
		{
		}

		GLShaderStorageBuffer(uint64_t size, const void* data, uint32_t bindingPoint, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit), bindingPoint(bindingPoint)
		{
		}

		~GLShaderStorageBuffer() override = default;

		GLShaderStorageBuffer(const GLShaderStorageBuffer& other) = delete;
		GLShaderStorageBuffer& operator=(const GLShaderStorageBuffer& other) = delete;

		void Bind() const override;
		void BindRange(uint64_t offset, uint64_t size) const override;

		void AddData(uint64_t size, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();

		uint32_t bindingPoint = 0;
	};

	struct GLPixelBuffer final : PixelBuffer, GLGeneralBuffer
	{
		GLPixelBuffer(uint64_t size, const void* data, Usage usage)
			: GLGeneralBuffer(size, data, usage)
		{
		}

		GLPixelBuffer(uint64_t size, const void* data, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit)
		{
		}

		~GLPixelBuffer() override = default;

		GLPixelBuffer(const GLPixelBuffer& other) = delete;
		GLPixelBuffer& operator=(const GLPixelBuffer& other) = delete;

		void Bind() const override;
		void AddData(uint64_t size, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();
	};

	struct GLIndirectDrawBuffer final : IndirectDrawBuffer, GLGeneralBuffer
	{
		GLIndirectDrawBuffer(uint64_t size, const void* data, Usage usage)
			: GLGeneralBuffer(size, data, usage)
		{
		}

		GLIndirectDrawBuffer(uint64_t size, const void* data, MapBit mapBit)
			: GLGeneralBuffer(size, data, mapBit)
		{
		}

		~GLIndirectDrawBuffer() override = default;

		GLIndirectDrawBuffer(const GLIndirectDrawBuffer& other) = delete;
		GLIndirectDrawBuffer& operator=(const GLIndirectDrawBuffer& other) = delete;

		void Bind() const override;
		void AddData(uint64_t size, const void* data) override;

		[[nodiscard]] std::shared_ptr<void*> Map(size_t offset, size_t size, MapBit mapBit) override;
		void Unmap();
	};
}