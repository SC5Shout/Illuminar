#include "Illuminar_pch.h"
#include "Illuminar_GLGraphicsPipelineState.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

#include "glad/glad.h"

namespace Illuminar::GFX {
	uint32_t ConvertStencilOpFuncToOpenGL(StencilOpFunc test)
	{
		switch (test) {
			case StencilOpFunc::Keep: return GL_KEEP;
			case StencilOpFunc::Zero: return GL_ZERO;
			case StencilOpFunc::Replace: return GL_REPLACE;
			case StencilOpFunc::Incr: return GL_INCR;
			case StencilOpFunc::IncrWrap: return GL_INCR_WRAP;
			case StencilOpFunc::Decr: return GL_DECR;
			case StencilOpFunc::DecrWrap: return GL_DECR_WRAP;
			case StencilOpFunc::Invert: return GL_INVERT;
		} return GL_KEEP;
	}

	uint32_t ConvertEquationToOpenGL(BlendEquation equation)
	{
		switch (equation) {
			case BlendEquation::Add: return GL_FUNC_ADD;
			case BlendEquation::Subtract: return GL_FUNC_SUBTRACT;
			case BlendEquation::Reverse_Subtract: return GL_FUNC_REVERSE_SUBTRACT;
			case BlendEquation::Min: return GL_MIN;
			case BlendEquation::Max: return GL_MAX;
		}

		//should never happen
		return GL_FUNC_ADD;
	}

	uint32_t ConvertBlendToOpenGL(Blend blend)
	{
		switch (blend) {
			case Blend::Zero: return GL_ZERO;
			case Blend::One: return GL_ONE;
			case Blend::Source_Alpha: return GL_SRC_ALPHA;
			case Blend::Destination_Alpha: return GL_DST_ALPHA;
			case Blend::One_Minus_Source_Alpha: return GL_ONE_MINUS_SRC_ALPHA;
			case Blend::One_Minus_Destination_Alpha: return GL_ONE_MINUS_DST_ALPHA;
			case Blend::Source_Color: return GL_SRC_COLOR;
			case Blend::One_Minus_Source_Color: return GL_ONE_MINUS_SRC_ALPHA;
		} return GL_SRC_COLOR;
	}

	uint32_t ConvertCullToOpenGL(CullMode cull)
	{
		switch (cull) {
			case CullMode::Back: return GL_BACK;
			case CullMode::Front: return GL_FRONT;
			case CullMode::Front_And_Back: return GL_FRONT_AND_BACK;
		} return GL_BACK;
	}

	uint32_t ConvertFrontFaceToOpenGL(FrontFace face)
	{
		switch (face) {
			case FrontFace::CCW: return GL_CCW;
			case FrontFace::CW: return GL_CW;
		} return GL_CCW;
	}

	uint32_t ConvertFuncTestToOpenGL(FuncTest test)
	{
		switch (test) {
			case FuncTest::Always: return GL_ALWAYS;
			case FuncTest::Never: return GL_NEVER;
			case FuncTest::Less: return GL_LESS;
			case FuncTest::Equal: return GL_EQUAL;
			case FuncTest::LEqual: return GL_LEQUAL;
			case FuncTest::Greater: return GL_GREATER;
			case FuncTest::NotEqual: return GL_NOTEQUAL;
			case FuncTest::GEqual: return GL_GEQUAL;
		} return GL_ALWAYS;
	}

	RasterizerState GLRasterizerState::currentState;
	GLRasterizerState::GLRasterizerState(const RasterizerState& rasterizerState)
		: state(rasterizerState)
	{
	}

	GLRasterizerState& GLRasterizerState::operator=(const RasterizerState& other)
	{
		state = other;
		return *this;
	}

	void GLRasterizerState::Bind() const
	{
		RenderCommand::Submit([state = state, &currentState = currentState]() {
			switch (state.fillMode) {
				case FillMode::Wireframe:
					UpdateState(currentState.fillMode, state.fillMode, [&]() {
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					});
					break;
				case FillMode::Solid:
					UpdateState(currentState.fillMode, state.fillMode, [&]() {
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					});
					break;
			}

			switch (state.cullMode) {
				case CullMode::None:
					glDisable(GL_CULL_FACE);
					break;
				case CullMode::Front:
					glEnable(GL_CULL_FACE);
					UpdateState(currentState.cullMode, state.cullMode, [&]() {
						glCullFace(GL_FRONT);
					});
					break;
				case CullMode::Back:
					glEnable(GL_CULL_FACE);
					UpdateState(currentState.cullMode, state.cullMode, [&]() {
						glCullFace(GL_BACK);
					});
					break;
				case CullMode::Front_And_Back:
					glEnable(GL_CULL_FACE);
					UpdateState(currentState.cullMode, state.cullMode, [&]() {
						glCullFace(GL_FRONT_AND_BACK);
					});
					break;
			}

			if (state.scissorEnabled) {
				glEnable(GL_SCISSOR_TEST);
			} else glDisable(GL_SCISSOR_TEST);
		});
	}

	DepthState GLDepthState::currentState;
	GLDepthState::GLDepthState(const DepthState& depthState)
		: state(depthState)
	{
	}

	GLDepthState& GLDepthState::operator=(const DepthState& other)
	{
		state = other;
		return *this;
	}

	void GLDepthState::Bind() const
	{
		RenderCommand::Submit([](DepthState state) {
			if (state.hasDepthTesting()) {
				glEnable(GL_DEPTH_TEST);
				UpdateState(currentState.mask, state.mask, [&]() {
					glDepthMask((bool)state.mask);
				});
				UpdateState(currentState.func, state.func, [&]() {
					glDepthFunc(ConvertFuncTestToOpenGL(state.func));
				});
			} else glDisable(GL_DEPTH_TEST);
		}, state);
	}

	BlendState GLBlendState::currentState;
	GLBlendState::GLBlendState(const BlendState& blendState)
		: state(blendState)
	{
	}

	GLBlendState& GLBlendState::operator=(const BlendState& other)
	{
		state = other;
		return *this;
	}

	void GLBlendState::Bind() const
	{
		RenderCommand::Submit([state = state, &currentState = currentState]() {
			if (state.hasBlending()) {
				glEnable(GL_BLEND);
				if (NeedsUpdateState(currentState.equationRGB, state.equationRGB) || NeedsUpdateState(currentState.equationAlpha, state.equationAlpha)) {
					glBlendEquationSeparate(ConvertEquationToOpenGL(state.equationRGB), ConvertEquationToOpenGL(state.equationRGB));
				}

				if (NeedsUpdateState(currentState.sourceRGB, state.sourceRGB) ||
					NeedsUpdateState(currentState.sourceAlpha, state.sourceAlpha) ||
					NeedsUpdateState(currentState.destinationRGB, state.destinationRGB) ||
					NeedsUpdateState(currentState.destinationAlpha, state.destinationAlpha)) {
					glBlendFunc(ConvertBlendToOpenGL(state.sourceRGB), ConvertBlendToOpenGL(state.destinationRGB));
				}
			} else glDisable(GL_BLEND);
		});
	}

	StencilState GLStencilState::currentState;
	GLStencilState::GLStencilState(const StencilState& stencilState)
		: state(stencilState)
	{
	}

	GLStencilState& GLStencilState::operator=(const StencilState& other)
	{
		state = other;
		return *this;
	}

	void GLStencilState::Bind() const
	{
		RenderCommand::Submit([state = state, &currentState = currentState]() {
			if (state.enabled) {
				glEnable(GL_STENCIL_TEST);

				UpdateState(currentState.mask, state.mask, [&]() {
					glStencilMask(state.mask);
				});
				if (NeedsUpdateState(currentState.func, state.func) || 
					NeedsUpdateState(currentState.ref, state.ref) ||
					NeedsUpdateState(currentState.funcMask, state.funcMask)) {
					glStencilFunc(ConvertFuncTestToOpenGL(state.func), state.ref, state.funcMask);
				}

				if (NeedsUpdateState(currentState.stencilFail, state.stencilFail) || 
					NeedsUpdateState(currentState.stencilPassDepthFail, state.stencilPassDepthFail) ||
					NeedsUpdateState(currentState.stencilDepthPass, currentState.stencilDepthPass)) {

					glStencilOp(ConvertStencilOpFuncToOpenGL(state.stencilFail), ConvertStencilOpFuncToOpenGL(state.stencilPassDepthFail), ConvertStencilOpFuncToOpenGL(state.stencilDepthPass));
				}
			} else glDisable(GL_STENCIL_TEST);
		});
	}
}