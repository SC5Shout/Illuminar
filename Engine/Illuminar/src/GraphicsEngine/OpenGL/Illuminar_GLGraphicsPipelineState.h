#pragma once

#include "GraphicsEngine/Illuminar_GraphicsPipelineState.h"

namespace Illuminar::GFX {
	struct GLRasterizerState final : State
	{
		GLRasterizerState() = default;
		GLRasterizerState(const RasterizerState& rasterizerState);
		~GLRasterizerState() override = default;

		GLRasterizerState& operator=(const RasterizerState& other);

		void Bind() const override;

		static RasterizerState currentState;
		RasterizerState state;
	};

	struct GLDepthState final : State
	{
		GLDepthState() = default;
		GLDepthState(const DepthState& depthState);
		~GLDepthState() override = default;

		GLDepthState& operator=(const DepthState& other);

		void Bind() const override;

		static DepthState currentState;
		DepthState state;
	};

	struct GLStencilState final : State
	{
		GLStencilState() = default;
		GLStencilState(const StencilState& stencilState);
		~GLStencilState() override = default;

		GLStencilState& operator=(const StencilState& other);

		void Bind() const override;

		static StencilState currentState;
		StencilState state;
	};

	struct GLBlendState final : State
	{
		GLBlendState() = default;
		GLBlendState(const BlendState& blendState);
		~GLBlendState() override = default;

		GLBlendState& operator=(const BlendState& other);

		void Bind() const override;

		static BlendState currentState;
		BlendState state;
	};
}