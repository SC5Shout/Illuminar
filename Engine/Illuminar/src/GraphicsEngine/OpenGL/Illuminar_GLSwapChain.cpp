#include "Illuminar_pch.h"
#include "Illuminar_GLSwapChain.h"

#include "glad/glad.h"
#include "GraphicsEngine/Illuminar_RenderCommand.h"

#include "GameEngine/Window/Illuminar_Window.h"

namespace Illuminar::GFX {
	void GLSwapChain::Bind() const
	{
		RenderCommand::Submit([]() {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		});
	}

	void GLSwapChain::Present(const Ref<Window>& window) const
	{
		RenderCommand::Submit([window]() {
			SDL_GL_SwapWindow(window->getHandle());
		});
	}
}
