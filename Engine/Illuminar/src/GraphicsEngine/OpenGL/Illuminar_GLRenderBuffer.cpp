#include "Illuminar_pch.h"
#include "Illuminar_GLRenderBuffer.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	GLRenderBuffer::GLRenderBuffer(uint32_t w, uint32_t h, TextureFormat format, uint32_t samples)
	{
		RenderCommand::Submit([this, w, h, format, samples]() {
			glCreateRenderbuffers(1, &buffer);
			glBindRenderbuffer(GL_RENDERBUFFER, buffer);

			if (samples == 1) {
				glNamedRenderbufferStorage(buffer, ConvertInternalFormatToOpenGL(format), w, h);
			} else glNamedRenderbufferStorageMultisample(buffer, samples, ConvertInternalFormatToOpenGL(format), w, h);
		});
	}

	GLRenderBuffer::~GLRenderBuffer()
	{
		RenderCommand::Submit([buffer = buffer]() {
			glDeleteRenderbuffers(1, &buffer);
		});
	}

	void GLRenderBuffer::Bind() const
	{
		RenderCommand::Submit([this]() {
			glBindRenderbuffer(GL_RENDERBUFFER, buffer);
		});
	}

	void GLRenderBuffer::Unbind() const
	{
		RenderCommand::Submit([this]() {
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		});
	}

	void GLRenderBuffer::Resize(uint32_t w, uint32_t h, TextureFormat format, uint32_t samples)
	{
		//TODO: I am not sure if it does not require to recreate a render buffer. Need to test it out.
		if (samples == 1) {
			glNamedRenderbufferStorage(buffer, ConvertInternalFormatToOpenGL(format), w, h);
		} else glNamedRenderbufferStorageMultisample(buffer, samples, ConvertInternalFormatToOpenGL(format), w, h);
	}

	uint32_t GLRenderBuffer(TextureFormat internalFormat)
	{
		switch (internalFormat) {
			case TextureFormat::R8_INT: return GL_R8I;
			case TextureFormat::RGBA8_SRGB: return GL_SRGB8_ALPHA8;
				//case TextureFormat::RGBA8_FLOAT: return GL_SRGB8_ALPHA8;
			case TextureFormat::RGBA32_FLOAT: return GL_RGBA32F;
			case TextureFormat::D24_S8: return GL_DEPTH24_STENCIL8;
			case TextureFormat::D32_FLOAT: return GL_DEPTH_COMPONENT32F;
			case TextureFormat::S8_UINT: return GL_STENCIL_INDEX8;

				//leave it, we might need em later
				//case TextureFormat::RED8: return GL_R8;
				//case TextureFormat::RED16: return GL_R16;
				//case TextureFormat::RGB8: return GL_RGB8;
				//case TextureFormat::RGBA8: return GL_RGBA8;
				//case TextureFormat::RGB32F: return GL_RGB32F;
				//case TextureFormat::SRGBA8: return GL_SRGB8_ALPHA8;
				//case TextureFormat::BGRA8: return GL_RGBA8;
				//case TextureFormat::R11_G11_B10F: return GL_R11F_G11F_B10F;
				//case TextureFormat::RGBA16F: return GL_RGBA16F;
				//case TextureFormat::RGBA32F: return GL_RGBA32F;
				//
				//case TextureFormat::ETC1: return 0;
				//
				////case TextureFormat::BC1: return GL_COMPRESSED_RGBA_S3TC;
				////case TextureFormat::BC1_SRGB: return GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT;
				////case TextureFormat::BC2
				////case TextureFormat::BC2_SRGB
				////case TextureFormat::BC3
				////case TextureFormat::BC3_SRGB
				////case TextureFormat::BC4
				////case TextureFormat::BC5
				//
				//case TextureFormat::RED32_UINT: return GL_R32UI;
				//case TextureFormat::RED32_FLOAT: return GL_R32F;
				//case TextureFormat::DEPTH32_FLOAT: return GL_DEPTH_COMPONENT32;
				//case TextureFormat::DEPTH24_STENCIL8: return GL_DEPTH24_STENCIL8;
				//default: return GL_RGB8;
		}

		//should never happen
		return GL_RGBA32F;
	}
}