#include "Illuminar_pch.h"
#include "Illuminar_GLFence.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar {
	namespace GFX {
		GLFence::GLFence()
		{
			handle = RenderCommand::CreateHandle<GLsync>([](GLsync& gl) {
				gl = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
			});
		}

		GLFence::~GLFence()
		{
			RenderCommand::DestroyHandle<GLsync>(handle, [](GLsync& gl) {
				glDeleteSync(gl);
			});
		}

		void GLFence::Wait()
		{
			std::weak_ptr<GLFence::Status> weak = result;
			RenderCommand::Submit(handle, [weak = weak](GLsync& gl) {
				static constexpr GLuint64 ONE_SECOND_IN_NANO_SECONDS = 1000000000;
		
				auto result = weak.lock();
				if (result) {
					GLbitfield waitFlags = 0;
					GLuint64 waitDuration = 0;
					while (true) {
						GLenum status = glClientWaitSync(gl, waitFlags, waitDuration);
						result->store(status, std::memory_order_relaxed);
						if (status == GL_ALREADY_SIGNALED || status == GL_CONDITION_SATISFIED) {
							return;
						}
					
						if (status == GL_WAIT_FAILED) {
							assert(!"Not sure what to do here. Probably raise an exception or something.");
							return;
						}
					
						// After the first time, need to start flushing, and wait for a looong time.
						waitFlags = GL_SYNC_FLUSH_COMMANDS_BIT;
						waitDuration = ONE_SECOND_IN_NANO_SECONDS;				
					}
				}
			});
		}	

		FenceStatus GLFence::getStatus()
		{
			if (!result) {
				return FenceStatus::NotSignaled;
			}
			auto status = result->load(std::memory_order_relaxed);
			switch (status) {
				case GL_CONDITION_SATISFIED:
				case GL_ALREADY_SIGNALED:
					return FenceStatus::Signaled;
				case GL_TIMEOUT_EXPIRED:
					return FenceStatus::NotSignaled;
				case GL_WAIT_FAILED:
				default:
					return FenceStatus::Error;
			}
		}
	}
}