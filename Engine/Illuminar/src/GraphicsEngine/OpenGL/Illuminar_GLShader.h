#pragma once

#include "GraphicsEngine/Illuminar_Shader.h"

#include "glad/glad.h"
#include <spirv_glsl.hpp>
#include <shaderc/shaderc.hpp>
#include <spirv_cross.hpp>

namespace Illuminar::GFX {
	struct GLShader final : Shader
	{
		GLShader(const std::filesystem::path& filepath, std::unordered_map<uint32_t, std::vector<uint32_t>>&& binaries);
		~GLShader() override;

		void Compile(std::string_view filepath) override {}

		void Bind() const override;

		void setUniform(const std::string& name, int value) const override;
		void setUniform(const std::string& name, uint64_t value) const override;
		void setUniform(const std::string& name, float value) const override;
		void setUniform(const std::string& name, const Vector2<float>& value) const override;
		void setUniform(const std::string& name, const Vector3<float>& value) const override;
		void setUniform(const std::string& name, const Vector4<float>& value) const override;
		void setUniform(const std::string& name, const Mat4& value) const override;

		const std::unordered_map<std::string, UniformBlock>& getPushConstantBlocks() const override
		{
			return pushConstantBlocks;
		}

		const std::unordered_map<std::string, SampledUniform>& getSampledUniforms() const override
		{
			return sampledUniforms;
		}

	//private:
		static void CompileFromBinaries(GLuint& program, const std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries);
		static void ShaderCompiledError(uint32_t shaderID, uint32_t type);
		static void ShaderLinkedError(GLuint& shaderProgram, std::array<uint32_t, (uint32_t)ShaderType::Count> shaders, uint32_t currentShadersCount);

		void getPushConstantLocations();
		void getSampledUniformLocations();

		std::unordered_map<std::string, SampledUniform> sampledUniforms;

		std::unordered_map<std::string, UniformBlock> pushConstantBlocks;
		std::unordered_map<std::string, uint32_t> pushConstantLocations;

		Handle<uint32_t> handle;
	};

	//struct GLShader final : Shader
	//{
	//	GLShader(std::string_view filepath);
	//	GLShader(std::string_view name, std::string_view filepath);
	//	GLShader(const std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries);
	//
	//	~GLShader() override;
	//
	//	GLShader(const GLShader& other) = delete;
	//	GLShader& operator=(const GLShader& other) = delete;
	//
	//	void Compile(std::string_view filepath) override;
	//
	//	void Bind() const override;
	//
	//	[[nodiscard]] inline std::string_view getName() const { return name; }
	//
	//	//[[nodiscard]] inline const std::unordered_map<std::string, UniformBlock>& getPushConstantBlocks() const override
	//	//{
	//	//	return pushConstantBlocks;
	//	//}
	//
	//	//[[nodiscard]] inline const std::unordered_map<std::string, SampledUniform>& getSampledUniforms() const override
	//	//{
	//	//	return sampledUniforms;
	//	//}
	//
	//	//[[nodiscard]] inline const std::unordered_map<std::string, uint32_t>& getPushConstantLocations() const override
	//	//{
	//	//	return pushConstantLocations;
	//	//}
	//
	//	//private:
	//
	//	void CompileFromBinaries(GLuint& shaderProgram, const std::unordered_map<uint32_t, std::vector<uint32_t>>& binaries);
	//
	//	[[nodiscard]] std::unordered_map<uint32_t, std::string> Preprocess(std::string_view source);
	//
	//	void CompileGLSLToVulkanSpirvAndCache(std::unordered_map<uint32_t, std::vector<uint32_t>>& vulkanBinaries);
	//	void CompileFromVulkanToOpengl(const std::unordered_map<uint32_t, std::vector<uint32_t>>& vulkanBinaries);
	//
	//	std::unordered_map<uint32_t, std::vector<uint32_t>> ReadCachedBinaryData(const std::string& cacheFormat);
	//	void CompileCachedBinary(GLuint& shaderProgram);
	//
	//	void getPushConstantLocations(GLuint& shaderProgram);
	//	void getSampledUniformLocations();
	//
	//	void ParsePushConstant(spirv_cross::ShaderResources& res, const spirv_cross::CompilerGLSL& compiler);
	//	void ParseUniformBuffers(const spirv_cross::Compiler& compiler);
	//
	//	[[nodiscard]] uint32_t ShaderTypeFromString(std::string_view type);
	//
	//	void setUniform(const std::string& name, int value) const override;
	//	void setUniform(const std::string& name, uint64_t value) const override;
	//	void setUniform(const std::string& name, float value) const override;
	//	void setUniform(const std::string& name, const Vector2<float>& value) const override;
	//	void setUniform(const std::string& name, const Vector3<float>& value) const override;
	//	void setUniform(const std::string& name, const Vector4<float>& value) const override;
	//	void setUniform(const std::string& name, const Mat4& value) const override;
	//
	//	Handle<uint32_t> handle;
	//
	//	void ShaderCompiledError(uint32_t shaderID, uint32_t type) const;
	//	void ShaderLinkedError(GLuint& shaderProgram, std::array<uint32_t, (uint32_t)ShaderType::Count> shaders, uint32_t currentShadersCount) const;
	//
	//	[[nodiscard]] uint32_t ConvertTypeToOpenGL(ShaderType type) const;
	//
	//	shaderc::Compiler compiler;
	//	shaderc::CompileOptions options;
	//
	//	//Push constants in opengl are just raw uniforms
	//	std::unordered_map<std::string, UniformBlock> pushConstantBlocks;
	//	//because of that, we need to get their locations
	//	std::unordered_map<std::string, uint32_t> pushConstantLocations;
	//
	//	std::unordered_map<std::string, SampledUniform> sampledUniforms;
	//
	//	std::string filepath;
	//	std::string cachedFilename;
	//};
}