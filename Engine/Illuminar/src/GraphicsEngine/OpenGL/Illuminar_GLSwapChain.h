#pragma once

#include "GraphicsEngine/RenderTarget/Illuminar_SwapChain.h"

namespace Illuminar::GFX {
	struct GLSwapChain : SwapChain
	{
		GLSwapChain() = default;
		virtual ~GLSwapChain() override = default;

		GLSwapChain(const GLSwapChain& other) = delete;
		GLSwapChain& operator =(const GLSwapChain& other) = delete;

		void Bind() const override;
		void Unbind() const override {}

		void Present(const Ref<Window>& window) const override;
	};
}