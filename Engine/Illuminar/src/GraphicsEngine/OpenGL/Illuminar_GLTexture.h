#pragma once

#include "GraphicsEngine/Illuminar_Texture.h"

#include "glad/glad.h"

namespace Illuminar::GFX {
	[[nodiscard]] uint32_t ConvertFormatToOpenGL(TextureFormat format);
	[[nodiscard]] uint32_t ConvertFilterToOpenGL(TextureFilter filter);
	[[nodiscard]] uint32_t ConvertWrapToOpenGL(TextureWrap wrap);
	[[nodiscard]] uint32_t ConvertInternalFormatToOpenGL(TextureFormat internalFormat);
	[[nodiscard]] uint32_t ConvertImageMapBitToOpenGL(ImageMapBit mapBit);
	[[nodiscard]] uint32_t ConvertTextureFormatToOpenGLDataType(TextureFormat format);

	struct GLTexture2D final : Texture2D
	{
		GLTexture2D() = default;
		GLTexture2D(const CreateInfo& createInfo);
		~GLTexture2D() override;

		GLTexture2D(const GLTexture2D& other) = delete;
		GLTexture2D& operator=(const GLTexture2D& other) = delete;

		void Bind(uint32_t slot = 0) const override;
		void BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered = false, uint32_t layer = 0) const override;

		void GenerateMipmap() override;

		inline uint32_t getWidth() const override { return info.w; }
		inline uint32_t getHeight() const override { return info.h; }

		void UpdateData(const void* pixels) override;
		void Resize(uint32_t width, uint32_t height) override;

		Handle<uint32_t> getTexture() const override { return handle; }
		uint32_t getTarget() const override { return target; }

		//only used by ImGui layer, probably will be removed soon
		void setTextureHandle(void* handle) override
		{
			this->handle = Handle<uint32_t>((uint32_t)handle);
		}

		CreateInfo getInfo() const override { return info; }

	//private:
		Handle<GLuint> handle;
		
		Texture2D::CreateInfo info;

		static constexpr uint32_t target = GL_TEXTURE_2D;

		void CreateFromSource();
	};

	struct GLTexture2DArray final : Texture2DArray
	{
		GLTexture2DArray(const CreateInfo& createInfo);
		~GLTexture2DArray();

		GLTexture2DArray(const GLTexture2DArray& other) = delete;
		GLTexture2DArray& operator=(const GLTexture2DArray& other) = delete;

		void Bind(uint32_t slot = 0) const override;
		void BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered = false, uint32_t layer = 0) const override;

		void GenerateMipmap() override;

		void AddData(uint32_t layer, uint32_t size, const void* data) override;

		Handle<uint32_t> getTexture() const override { return handle; }
		uint32_t getTarget() const override { return target; }

		CreateInfo getInfo() const override { return info; }

		Handle<GLuint> handle;
		CreateInfo info;

		static constexpr uint32_t target = GL_TEXTURE_2D_ARRAY;
	};

	struct GLTextureCubemap final : TextureCubemap
	{
		GLTextureCubemap(const CreateInfo& createInfo);
		~GLTextureCubemap();

		GLTextureCubemap(const GLTextureCubemap& other) = delete;
		GLTextureCubemap& operator=(const GLTextureCubemap& other) = delete;

		void Bind(uint32_t slot = 0) const override;
		void BindImage(uint32_t slot, uint32_t level, ImageMapBit mapBit, TextureFormat format, bool layered = false, uint32_t layer = 0) const override;

		void GenerateMipmap() override;

		Handle<uint32_t> getTexture() const { return texture; }
		uint32_t getTarget() const override { return target; }

		CreateInfo getInfo() const override { return info; }

		Handle<GLuint> texture;
		CreateInfo info;

		static constexpr uint32_t target = GL_TEXTURE_CUBE_MAP;
	};
}