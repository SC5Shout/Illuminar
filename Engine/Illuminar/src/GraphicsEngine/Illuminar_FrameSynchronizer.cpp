#include "Illuminar_pch.h"
#include "Illuminar_FrameSynchronizer.h"

namespace Illuminar::GFX {
	bool FrameSkipper::Acquire()
	{
		auto& syncs = delayedSyncs;
		auto sync = syncs.front();
		if (sync) {
			sync->Wait();
			auto status = sync->getStatus();
			if (status == FenceStatus::NotSignaled) {
				// Sync not ready, skip frame
				return false;
			}
			sync.Reset();
		}
		// shift all fences down by 1
		std::move(syncs.begin() + 1, syncs.end(), syncs.begin());
		syncs.back() = {};
		return true;
	}

	void FrameSkipper::Release()
	{
		auto& sync = delayedSyncs[last];
		if (sync) {
			sync.Reset();
		} sync = Fence::Create();
	}
}