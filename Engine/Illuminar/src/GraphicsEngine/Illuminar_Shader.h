#pragma once

#include <GraphicsEngine/Uniform/Illuminar_Uniform.h>

#include "GraphicsEngine/Illuminar_Resource.h"
#include "GraphicsEngine/Buffer/Illuminar_Buffer.h"
#include "GraphicsEngine/Illuminar_Texture.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

namespace Illuminar::GFX {
	enum struct ShaderType
	{
		Vertex = 0,
		Fragment = 1,
		Geometry = 2,
		TessControl = 3,
		TessEvaluation = 4,
		Compute = 5,
		Count = 6
	};

	struct Shader : Resource
	{
		Shader() = default;
		virtual ~Shader() = default;
	
		Shader(const Shader& other) = delete;
		Shader& operator=(const Shader& other) = delete;
	
		//[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Shader> Create(std::string_view filepath);
		//[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Shader> Create(std::string_view name, const std::string_view filepath);
		[[nodiscard("Cannot discard graphics api create calls")]] static Ref<Shader> Create(const std::filesystem::path& filepath, std::unordered_map<uint32_t, std::vector<uint32_t>>&& binaries);
	
		virtual void Compile(std::string_view filepath) = 0;
		inline void Compile() { Compile(filepath); }
	
		virtual void Bind() const = 0;
	
		[[nodiscard]] std::string_view getName() const { return name; }
	
		virtual void setUniform(const std::string& name, int value) const = 0;
		virtual void setUniform(const std::string& name, uint64_t value) const = 0;
		virtual void setUniform(const std::string& name, float value) const = 0;
		virtual void setUniform(const std::string& name, const Vector2<float>& value) const = 0;
		virtual void setUniform(const std::string& name, const Vector3<float>& value) const = 0;
		virtual void setUniform(const std::string& name, const Vector4<float>& value) const = 0;
		virtual void setUniform(const std::string& name, const Mat4& value) const = 0;
	
		virtual const std::unordered_map<std::string, UniformBlock>& getPushConstantBlocks() const = 0;
		virtual const std::unordered_map<std::string, SampledUniform>& getSampledUniforms() const = 0;

		std::string name;
		std::string filepath;
	
		[[nodiscard]] static const std::string LoadFromFile(const std::string_view filePath);
	};

	//temp
	struct ShaderCache
	{
		//[[nodiscard]] static inline Ref<Shader> AddOrGetShader(const std::string& name, std::string_view filepath)
		//{
		//	if (get().shaders.contains(name)) {
		//		return get().shaders[name];
		//	}
		//
		//	auto result = Shader::Create(filepath);
		//	get().shaders[name] = result;
		//	return result;
		//}

		static inline void AddExistedShader(const std::string& name, const Ref<Shader>& shader)
		{
			if (get().shaders.contains(name)) {
				return;
			} get().shaders[name] = shader;
		}

		static inline void AddExistedShader(const std::string& name, Ref<Shader>&& shader)
		{
			if (get().shaders.contains(name)) {
				return;
			} get().shaders[name] = std::move(shader);
		}

		[[nodiscard]] static inline Ref<Shader> getShader(const std::string& name) 
		{
			if (get().shaders.contains(name)) {
				return get().shaders[name];
			}
			return nullptr;
		}

		static ShaderCache& get()
		{
			static ShaderCache instance;
			return instance;
		}

		std::unordered_map<std::string, Ref<Shader>> shaders;
	};

	using ShaderResourceID = uint32_t;
	struct ShaderResourceCache
	{
		static inline void AddExistedShader(const std::string& name, ShaderResourceID shader)
		{
			if (get().shaders.contains(name)) {
				return;
			} get().shaders[name] = shader;
		}

		[[nodiscard]] static inline ShaderResourceID getShader(const std::string& name)
		{
			if (get().shaders.contains(name)) {
				return get().shaders[name];
			}
			return INVALID_RESOURCE_ID;
		}

		static ShaderResourceCache& get()
		{
			static ShaderResourceCache instance;
			return instance;
		}

		std::unordered_map<std::string, ShaderResourceID> shaders;
	};
}