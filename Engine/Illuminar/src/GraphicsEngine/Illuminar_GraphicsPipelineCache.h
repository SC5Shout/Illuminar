#pragma once

#include "Illuminar_GraphicsPipeline.h"
#include "Illuminar_Material.h"

namespace Illuminar::GFX {
	struct GraphicsPipelineCache
	{
		static Ref<GraphicsPipeline> Create(const GraphicsPipeline::CreateInfo& info)
		{
			PipelineCacheInfo cacheInfo;
			cacheInfo.info = info;

			if (!get().pipelineCache.contains(cacheInfo)) {
				get().pipelineCache[cacheInfo] = GraphicsPipeline::Create(info);
			} return get().pipelineCache[cacheInfo];
		}

	private:
		static GraphicsPipelineCache& get()
		{
			static GraphicsPipelineCache instance;
			return instance;
		}

		struct PipelineCacheInfo
		{
			GraphicsPipeline::CreateInfo info;

			[[nodiscard]] inline bool operator==(const PipelineCacheInfo& other) const
			{
				if (other.info.bufferLayout != info.bufferLayout) {
					return false;
				}

				if (other.info.shader != info.shader) {
					return false;
				}

				if (info.rasterizerState != other.info.rasterizerState) {
					return false;
				}

				if (info.stencilState != other.info.stencilState) {
					return false;
				}

				if (info.blendState != other.info.blendState) {
					return false;
				}

				if (info.depthState != other.info.depthState) {
					return false;
				}

				if (info.drawMode != other.info.drawMode) {
					return false;
				}

				return true;
			}

			[[nodiscard]] inline size_t Hash() const
			{
				std::array<size_t, 8> values = {
					info.bufferLayout.Hash(),
					std::hash<void*>{}((void*)info.shader.get()),
					info.rasterizerState.Hash(),
					info.stencilState.Hash(),
					info.blendState.Hash(),
					info.depthState.Hash(),
					std::hash<uint8_t>()((uint8_t)info.drawMode)
				};

				return std::_Hash_array_representation(values.data(), values.size());
			}
		};

		struct PipelineHash
		{
			[[nodiscard]] inline size_t operator()(const PipelineCacheInfo& info) const { return info.Hash(); }
		};

		std::unordered_map<PipelineCacheInfo, Ref<GraphicsPipeline>, PipelineHash> pipelineCache;
	};
}