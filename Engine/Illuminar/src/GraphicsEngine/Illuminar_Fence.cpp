#include "Illuminar_pch.h"
#include "OpenGL/Illuminar_GLFence.h"
#include "Illuminar_Fence.h"

namespace Illuminar {
	namespace GFX {
		Ref<Fence> Fence::Create()
		{
			return Ref<GLFence>::Create();
		}
	}
}