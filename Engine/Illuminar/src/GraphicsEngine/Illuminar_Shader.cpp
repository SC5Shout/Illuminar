#include "Illuminar_pch.h"
#include "GraphicsEngine/OpenGL/Illuminar_GLShader.h"

#include "GraphicsEngine/Illuminar_Renderer.h"

namespace Illuminar::GFX {
	const std::string Shader::LoadFromFile(std::string_view filePath)
	{
		std::ifstream openFile{ std::string(filePath) };
		std::stringstream sstream;

		if (!openFile.is_open()) {
			std::cout << "Could not find the file: " << filePath << std::endl;
		}

		sstream << openFile.rdbuf() << std::endl;

		return sstream.str();
	}

	//Ref<Shader> Shader::Create(std::string_view filepath)
	//{
	//	if (Renderer::api == Renderer::API::OpenGL) {
	//		return Ref<GLShader>::Create(filepath);
	//	} else {
	//		EngineLogError("Only OpenGL GFX avaliable at the moment");
	//		return nullptr;
	//	} return nullptr;
	//}
	//
	//Ref<Shader> Shader::Create(std::string_view name, std::string_view filepath)
	//{
	//	if (Renderer::api == Renderer::API::OpenGL) {
	//		return Ref<GLShader>::Create(name, filepath);
	//	} else {
	//		EngineLogError("Only OpenGL GFX avaliable at the moment");
	//		return nullptr;
	//	} return nullptr;
	//}

	Ref<Shader> Shader::Create(const std::filesystem::path& filepath, std::unordered_map<uint32_t, std::vector<uint32_t>>&& binaries)
	{
		if (Renderer::api == Renderer::API::OpenGL) {
			return Ref<GLShader>::Create(filepath, std::move(binaries));
		} else {
			EngineLogError("Only OpenGL GFX avaliable at the moment");
			return nullptr;
		} return nullptr;
	}
}