#pragma once

namespace Illuminar::GFX {
	enum struct DataType : uint8_t
	{
		None = 0,

		R32_INT,
		R8_INT,
		R32_FLOAT,

		R32_UINT,
		R8_UINT,
		RGBA8_UNORM, //uint32 -> vec4 of uchars, useful with colors, we do not want a color to be vec4 - 4 * sizeof(float) = 16 - uint is better sollution - sizeof(uint) = 4 - it is 4 times smaller
		R16_UINT,

		RG32_FLOAT,
		RGB32_FLOAT,
		RGBA32_FLOAT,

		RG32_INT,
		RGB32_INT,
		RGBA32_INT,

		RG32_UINT,
		RGB32_UINT,
		RGBA32_UINT,

		Mat3,
		Mat4,
	};

	enum struct ElementDataType : uint8_t
	{
		Uint32,
		Uint16
	};
}