#pragma once

#include "Illuminar_Renderer.h"
#include "Illuminar_CommandBuffer.h"

#include "GraphicsEngine/Illuminar_Texture.h"

#include "Illuminar_FrameSynchronizer.h"
#include "Memory/Illuminar_HandleAllocator.h"

namespace Illuminar {
	struct Window;
}

namespace Illuminar::GFX {
	struct Shader;
	struct Material;
	struct VertexBuffer;
	struct ElementBuffer;
	struct IndirectDrawBuffer;
	struct GraphicsPipeline;
	struct Renderable;

	struct RenderCommand
	{
		~RenderCommand() = default;

		static void Init(const Ref<Window>& window);

		[[nodiscard]] static bool BeginFrame();
		static void EndFrame();

		static void Clear(BufferBit clearBit = BufferBit::Color | BufferBit::Depth | BufferBit::Stencil);
		static void setViewport(int x, int y, uint32_t w, uint32_t h);
		static void setScissor(int x, int y, int w, int h);

		static void setClearColor(uint32_t color);
		static void setClearColor(float r, float g, float b, float a);

		static void setLineThickness(float thickness);

		static void DrawArrays(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance);
		static void DrawElements(uint32_t elementCount, ElementDataType dataType, uint32_t instanceCount, uint32_t firstElement, int32_t vertexOffset, uint32_t firstInstance);
		static void DrawElementsIndirect(ElementDataType dataType, uint32_t drawCount = 1, uint64_t indirectBufferOffset = 0);

		static void DispatchCompute(uint32_t groupX, uint32_t groupY, uint32_t groupZ);

		static void Draw(Renderable renderable, const Ref<Material>& material);

		static void DrawElements(
			const Ref<VertexBuffer>& vbo,
			const Ref<ElementBuffer>& ebo,
			const Ref<Material>& material,
			uint32_t elementCount, 
			ElementDataType dataType,
			uint32_t instanceCount, 
			uint32_t firstElement, 
			int32_t vertexOffset,
			uint32_t firstInstance);

		static void DrawArrays(const Ref<VertexBuffer>& vbo, const Ref<Material>& material, uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance);

		static void MemoryBarrier(MemoryBarrierType barrier);

		template<typename T>
		[[nodiscard]] static constexpr size_t getAlignedUniformBufferOffset()
		{
			//int alignment = 0;
			//glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &alignment);

			constexpr size_t alignment = 256;
			return sizeof(T) + alignment - sizeof(T) % alignment;
		}

		[[nodiscard]] static const size_t getAlignedUniformBufferOffsetFromSize(size_t size)
		{
			//int alignment = 0;
			//glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &alignment);

			constexpr size_t alignment = 256;
			//return size + alignment - size % alignment;

			return (size + alignment - 1) & ~(alignment - 1);
		}
		
		template<typename FuncT, typename ... Args>
		static inline void Submit(FuncT&& funcT, Args&& ... args)
		{
			funcT(std::forward<Args>(args)...);
			//get().queue.Submit(std::forward<FuncT>(funcT), std::forward<Args>(args)...);
		}

		static inline void Flush()
		{
			get().queue.Flush();
		}

		[[nodiscard]] static inline bool Execute()
		{
			return get().queue.Execute();
		}

		static inline void Quit()
		{
			get().queue.Quit();
		}

		//Creates a new handle with args FROM the main thread
		template<typename T, typename ... Args>
		[[nodiscard]] static inline Handle<T> CreateHandle(Args&& ... args)
		{
			return get().handleAllocator.new_object<T>(std::forward<Args>(args)...);
		}
		
		//Creates a new handle with default construct FROM the main thread
		//and enqueues the createInteral function with args into the command queue
		template<typename T, typename FuncT, typename ... Args>
		[[nodiscard]] static inline Handle<T> CreateHandle(FuncT&& createInternal, Args&& ... internalArgs)
		{
			auto handle = get().handleAllocator.new_object<T>();
			Submit(handle, std::forward<FuncT>(createInternal), std::forward<Args>(internalArgs)...);
			return handle;
		}
		
		template<typename T, typename FuncT, typename ... Args>
		static inline void Submit(Handle<T> handle, FuncT&& funcT, Args&& ... args)
		{
			Submit([](Handle<T> handle, FuncT&& func, Args&& ... args) {
				if (handle.valid()) {
					T* gl = get().handleAllocator.handle_cast<T*>(handle);
					func(*gl, std::forward<Args>(args)...);
				}
			}, handle, std::forward<FuncT>(funcT), std::forward<Args>(args)...);
		}
		
		//Enqueues the destroyInternal function with args into the render thread queue (command queue) 
		//and destroyes the handle FROM the render thread
		template<typename T, typename U, typename FuncT, typename ... Args>
		static void DestroyHandle(Handle<U> handle, FuncT&& destroyInternal, Args&& ... internalArgs)
		{
			Submit([](Handle<U> handle, FuncT&& func, Args&& ... args) {
				if (handle.valid()) {
					T* gl = get().handleAllocator.handle_cast<T*>(handle);
			
					func(*gl, std::forward<Args>(args)...);
			
					get().handleAllocator.delete_object<T>(handle);
				}
			}, handle, std::forward<FuncT>(destroyInternal), std::forward<Args>(internalArgs)...);
		}
		
		template<typename T, typename U>
		static void DestroyHandle(Handle<U> handle)
		{
			Submit([](Handle<U> handle) {
				if (handle.valid()) {
					T* gl = get().handleAllocator.handle_cast<T*>(handle);
					get().handleAllocator.delete_object<T>(handle);
				}
			}, handle);
		}

		//Tp is not thread-safe
		template<typename Tp, typename U>
		[[nodiscard]] static inline Tp handle_cast(const Handle<U>& handle)
		{
			return get().handleAllocator.handle_cast<Tp>(const_cast<Handle<U>&>(handle));
		}

		template<typename T>
		[[nodiscard]] static inline Ref<T> as() //requires std::is_base_of_v<Renderer, T>
		{
			return get().renderer.as<T>();
		}

		[[nodiscard]] static inline Ref<Texture2D> getWhiteTexture()
		{
			static uint32_t color = 0xffffffff;
			static Texture2D::CreateInfo info;
			info.name = "white";
			info.w = 1;
			info.h = 1;
			info.pixels = &color;
			static Ref<Texture2D> result = Texture2D::Create(info);
			return result;
		}

		static void DrawFullScreenQuad(const Ref<Shader>& shader, const Ref<Material>& material);

		[[nodiscard]] inline static const HandleAllocator& getHandleAllocator() 
		{
			return get().handleAllocator;
		}

	private:
		RenderCommand() = default;
		RenderCommand(const RenderCommand& other) = delete;
		RenderCommand& operator=(const RenderCommand& other) = delete;

		inline static RenderCommand& get()
		{
			static RenderCommand instance;
			return instance;
		}

		//it is important to put FrameSkipper and CommandQueue classes in a specific order because it defines how these classes are createdand destroyed
		// (firstly queue is created, then the FrameSkipper, destruction is the other way around, firstly FrameSkipper then the queue).
		//The FrameSkipper has to be destroyed after the queue because FrameSkipper's destructor enqueues commands into the CommandQueue. 
		//This is very important because if they'd not be in the correct order, frame skipper would try to enqueue the command into the queue that does not exist anymore. 
		CommandQueue queue;
		FrameSkipper frameSkipper;
		HandleAllocator handleAllocator;

		Ref<Renderer> renderer{ Renderer::Create() };
	};
}