#pragma once

namespace Illuminar::GFX {
	enum struct FillMode : uint8_t
	{
		Solid,
		Wireframe
	};

	enum struct CullMode : uint8_t
	{
		None = BIT(0),
		Back = BIT(1),
		Front = BIT(2),
		Front_And_Back = BIT(3)
	};

	enum struct FrontFace : uint8_t
	{
		CCW = BIT(0),
		CW = BIT(1)
	};

	enum struct FuncTest : uint8_t
	{
		Always = 1,
		Never = 2,
		Less = 3,
		Equal = 4,
		LEqual = 5,
		Greater = 6,
		NotEqual = 7,
		GEqual = 8,
	};

	using DepthTest = FuncTest;
	using StencilTest = FuncTest;

	enum struct DepthMask : uint8_t
	{
		Zero = 0,
		All = 1
	};

	enum struct StencilOpFunc : uint8_t
	{
		Keep = 1,
		Zero = 2,
		Replace = 3,
		Incr = 4,
		IncrWrap = 5,
		Decr = 6,
		DecrWrap = 7,
		Invert = 8,
	};

	enum struct Blend : uint8_t
	{
		Zero = BIT(0),
		One = BIT(1),
		Source_Alpha = BIT(2),
		Destination_Alpha = BIT(3),
		One_Minus_Source_Alpha = BIT(4),
		One_Minus_Destination_Alpha = BIT(5),
		One_Minus_Source_Color = BIT(6),
		Source_Color = BIT(7)
	};

	enum struct BlendEquation : uint8_t
	{
		Add,
		Subtract,
		Reverse_Subtract,
		Min,
		Max
	};

	struct RasterizerState
	{
		static inline const RasterizerState& getDefault()
		{
			static RasterizerState state = {
				FillMode::Solid,
				CullMode::Back,
				FrontFace::CCW,
				false
			}; return state;
		}

		FillMode fillMode = FillMode::Solid;
		CullMode cullMode = CullMode::Back;
		FrontFace frontFace = FrontFace::CCW;
		bool scissorEnabled = false;

		auto operator<=>(const RasterizerState& other) const = default;

		[[nodiscard]] inline size_t Hash() const
		{
			std::array<uint8_t, 4> values = {
				(uint8_t)fillMode,
				(uint8_t)cullMode,
				(uint8_t)frontFace,
				(uint8_t)scissorEnabled,
			};
			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	struct DepthState
	{
		static inline const DepthState& getDefault()
		{
			static constexpr DepthState state = {
				FuncTest::Less,
				DepthMask::All,
			}; return state;
		}

		DepthTest func = DepthTest::Less;
		DepthMask mask = DepthMask::All;

		[[nodiscard]] inline bool hasDepthTesting() const
		{
			return !(func == DepthTest::Always && mask == DepthMask::Zero);
		}

		auto operator<=>(const DepthState& other) const = default;

		[[nodiscard]] inline size_t Hash() const
		{
			std::array<uint8_t, 2> values = {
				(uint8_t)func,
				(uint8_t)mask,
			};
			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	struct StencilState
	{
		static inline const StencilState& getDefault()
		{
			static constexpr StencilState state = {
				false,
				StencilTest::Always,
				1,
				0xff,
				0xff,
				StencilOpFunc::Keep,
				StencilOpFunc::Keep,
				StencilOpFunc::Keep
			};
			return state;
		}

		bool enabled = false;
		StencilTest func = StencilTest::Always;
		int ref = 1;
		uint32_t funcMask = 0xff;
		uint32_t mask = 0xff;
		StencilOpFunc stencilFail = StencilOpFunc::Keep;
		StencilOpFunc stencilPassDepthFail = StencilOpFunc::Keep;
		StencilOpFunc stencilDepthPass = StencilOpFunc::Keep;

		auto operator<=>(const StencilState& other) const = default;

		[[nodiscard]] inline size_t Hash() const
		{
			std::array<uint8_t, 8> values = {
				(uint8_t)enabled,
				(uint8_t)func,
				(uint8_t)ref,
				(uint8_t)funcMask,
				(uint8_t)mask,
				(uint8_t)stencilFail,
				(uint8_t)stencilPassDepthFail,
				(uint8_t)stencilDepthPass,
			};
			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	struct BlendState
	{
		static inline const BlendState& getDefault()
		{
			static constexpr BlendState state = {
				BlendEquation::Add,
				BlendEquation::Add,
				Blend::One,
				Blend::One,
				Blend::Zero,
				Blend::Zero
			}; return state;
		}

		BlendEquation equationRGB = BlendEquation::Add;
		BlendEquation equationAlpha = BlendEquation::Add;
		Blend sourceRGB = Blend::One;	
		Blend sourceAlpha = Blend::One;
		Blend destinationRGB = Blend::Zero;
		Blend destinationAlpha = Blend::Zero;

		auto operator<=>(const BlendState& other) const = default;

		[[nodiscard]] inline bool hasBlending() const
		{
			return !(
					equationRGB == BlendEquation::Add &&
					equationAlpha == BlendEquation::Add &&
					sourceRGB == Blend::One && 
					sourceAlpha == Blend::One && 
					destinationRGB == Blend::Zero && 
					destinationAlpha == Blend::Zero);
		}

		[[nodiscard]] inline size_t Hash() const
		{
			std::array<uint8_t, 4> values = {
				(uint8_t)sourceRGB,
				(uint8_t)sourceAlpha,
				(uint8_t)destinationRGB,
				(uint8_t)destinationAlpha,
			};
			return std::_Hash_array_representation(values.data(), values.size());
		}
	};

	struct ViewportState
	{
		static inline const ViewportState& getDefault()
		{
			static constexpr ViewportState state = {
				0,
				0,
				std::numeric_limits<uint32_t>::max(),
				std::numeric_limits<uint32_t>::max(),
				0.0f,
				1.0f
			}; return state;
		}

		int x = 0;
		int y = 0;
		uint32_t width = std::numeric_limits<uint32_t>::max();
		uint32_t height = std::numeric_limits<uint32_t>::max();
		float minDepth = 0.0f;
		float maxDepth = 1.0f;

		auto operator<=>(const ViewportState& other) const = default;
	};

	struct State
	{
		virtual ~State() = default;
		virtual void Bind() const = 0;
	};
}