#include "Illuminar_pch.h"
#include "Illuminar_Renderer.h"

#include "OpenGL/Illuminar_GLRenderer.h"

namespace Illuminar::GFX {
	Ref<Renderer> Renderer::Create()
	{
		switch (api) {
			case API::OpenGL:
				return Ref<GLRenderer>::Create();
			case API::Vulkan:
				return nullptr;
		}

		return nullptr;
	}
}