#pragma once

#include "Illuminar_Texture.h"
#include <unordered_map>

#include "Memory/Illuminar_Ref.h"

namespace Illuminar::GFX {
	struct TextureCache
	{
		static TextureCache& get()
		{
			static TextureCache instance;
			return instance;
		}

		static Ref<Texture2D> Create(const Texture2D::CreateInfo& info)
		{
			auto hash = info.Hash();
			if (!get().textureCache.contains(hash)) {
				get().textureCache[hash] = Texture2D::Create(info);
			} return get().textureCache[hash];
		}

		static Ref<Texture2DArray> Create(const Texture2DArray::CreateInfo& info)
		{
			auto hash = info.Hash();
			if (!get().textureCache.contains(hash)) {
				get().textureCache[hash] = Texture2DArray::Create(info);
			} return get().textureCache[hash];
		}

		std::unordered_map<size_t, Ref<Texture>> textureCache;
	};
}