#include "Illuminar_pch.h"
#include "Illuminar_SparseTextureArray.h"

#include "Illuminar_RenderCommand.h"

namespace Illuminar {
	Texture2DContainer::Texture2DContainer(bool sparse, const GFX::TextureParameters& params, uint32_t width, uint32_t height, uint32_t slices)
		: width(width), height(height), slices(slices)
	{
		GFX::Texture2DArray::CreateInfo info;
		info.w = width;
		info.h = height;
		info.depth = slices;
		info.parameters = params;
		texture = GFX::Texture2DArray::Create(info);

		for (uint32_t i = 0; i < slices; ++i) {
			freeList.push(i);
		}
	}

	void Texture2DContainer::Image3D(int level, int offsetX, int offsetY, int offsetZ, int w, int h, int depth, const void* data)
	{
		texture->AddData(offsetZ, w * h, data);

		//GFX::RenderCommand::Submit(texture->getTexture(), [level, offsetX, offsetY, offsetZ, w, h, depth, data](GLuint& texture) {
		//	glTextureSubImage3D(texture, level, offsetX, offsetY, offsetZ, w, h, depth, GL_RGB, GL_UNSIGNED_BYTE, data);
		//});
	}

	std::shared_ptr<SparseTexture2D> SparseTextureManager::Create(uint32_t w, uint32_t h, GFX::TextureParameters params)
	{
		std::shared_ptr<Texture2DContainer> container = nullptr;

		auto texType = std::make_tuple(w, h, params);
		if (texArrays2D.contains(texType)) {
			auto& containers = texArrays2D[texType];
			for (auto&& cont : containers) {
				if (cont->hasRoom()) {
					container = cont;
					break;
				}
			}
		} else texArrays2D[texType] = std::vector<std::shared_ptr<Texture2DContainer>>();

		std::vector<std::shared_ptr<Texture2DContainer>>& vec = texArrays2D[texType];

		if (container == nullptr) {
			container = std::make_shared<Texture2DContainer>(sparse, params, w, h, 10);
			vec.push_back(container);
		}
		assert(container);

		return std::make_shared<SparseTexture2D>(container, container->VirtualAlloc());
	}
}
