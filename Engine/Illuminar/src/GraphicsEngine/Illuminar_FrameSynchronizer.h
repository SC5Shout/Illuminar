#pragma once

#include "Illuminar_Fence.h"

namespace Illuminar {
	namespace GFX {
		/*
		This class is used to synchronize the GPU with the CPU.
		The main thread pushes render commands into the render thread that pushes them into the graphics queue.
		If a given frame takes too much time in the GPU, the GPU will be behind the CPU.
		The display will draw the same frame twice producing a stutter.
		To prevent stuttering, the rendering part of this frame has to be skipped.
		The frame is being skipped until the GPU fence will notify that the GPU has completed the work this frame.
		*/
		struct FrameSkipper
		{
			static constexpr size_t MAX_FRAME_LATENCY = 2;

			FrameSkipper(size_t latency = 1)
				: last(latency)
			{
				assert(latency <= MAX_FRAME_LATENCY);
			}

			~FrameSkipper() = default;

			//Checks if the GPU is ready to process a new frame
			//Returns true if the frame is ready to submit more commands
			//false if the frame needs to wait until the GPU will complete the work
			bool Acquire();

			//Destroys/releases the current frame's fence and creates a new one for the next frame 
			void Release();

		private:
			using Container = std::array<Ref<Fence>, MAX_FRAME_LATENCY>;
			mutable Container delayedSyncs{};

			size_t last;
		};
	}
}