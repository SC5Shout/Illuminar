#pragma once

extern std::unique_ptr<Illuminar::Application> Illuminar::CreateApplication();


int main(int argc, char* args[])
{
	auto app = Illuminar::CreateApplication();
	app->MainLoop();

	return 0;
}