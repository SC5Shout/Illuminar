project "Editor"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    files { 
        "src/**.h", 
        "src/**.c", 
        "src/**.hpp", 
        "src/**.cpp",
    }

    includedirs {       
        "/",
        "%{wks.location}/Engine/Illuminar/src",
        "%{wks.location}/Engine/ImGui",

		"%{IncludeDir.SDL2}",
        "%{IncludeDir.Glad}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.assimp}",
        "%{IncludeDir.spdlog}",
        "%{IncludeDir.spirv_cross}",
        "%{IncludeDir.shaderc}",
        "%{IncludeDir.yaml_cpp}",
    }

    links { 
        "Illuminar",
        "ImGui",

        "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2main.lib",
        "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2.lib",
        "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2_image.lib",

        "%{wks.location}/Engine/Illuminar/dependencies/assimp5/lib/Debug/assimp-vc142-mtd.lib",
    }

    postbuildcommands {
        '{COPY} "assets" "%{cfg.targetdir}/assets"',
        '{COPY} "**.proj" "%{cfg.targetdir}"',
        '{COPY} "imgui.ini" "%{cfg.targetdir}"'
    }

    filter "system:windows"
        systemversion "latest"
        
        defines { 
            "IILLUMINAR_PLATFORM_WINDOWS"
        }

    filter "configurations:Debug"
        defines "ILLUMINAR_DEBUG"
        symbols "On"
        
        postbuildcommands {
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/assimp5/lib/Debug/assimp-vc142-mtd.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2main.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2_image.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/libpng16-16.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/zlib1.dll" "%{cfg.targetdir}"',
        }
        
    filter "configurations:Release"
        defines "ILLUMINAR_RELEASE"
        optimize "On"
        
        links { 
            "%{wks.location}/Engine/Illuminar/dependencies/assimp5/lib/Release/assimp-vc142-mt.lib"
        }

        postbuildcommands {
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/assimp5/lib/Release/assimp-vc142-mt.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2main.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/SDL2_image.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/libpng16-16.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/zlib1.dll" "%{cfg.targetdir}"',
            '{COPY} "%{wks.location}/Engine/Illuminar/dependencies/SDL2/lib/x64/libpng16-16.dll" "%{cfg.targetdir}"',
        }