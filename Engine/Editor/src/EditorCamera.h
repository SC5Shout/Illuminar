#pragma once

#include "GraphicsEngine/Renderer/Illuminar_Camera.h"

namespace Illuminar {
	enum Camera_Movement
	{
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT
	};

	struct EditorCamera : Camera
	{
		static constexpr float YAW = 0.0f;
		static constexpr float PITCH = 0.0f;
		static constexpr float ZOOM = 0.0f;

		EditorCamera(const Vector3<float>& position = 0.0f,
			   const Vector3<float>& up = Vector3<float>(0.0f, 1.0f, 0.0f),
			   float yaw = YAW, 
			   float pitch = PITCH) 
			: 
			front(0.0f, 0.0f, -1.0f), 
			zoom(ZOOM),
			position(position),
			worldUp(up),
			yaw(yaw),
			pitch(pitch)
		{
			UpdateCameraVectors();
		}

		inline void setViewport(uint32_t w, uint32_t h)
		{
			glm::perspectiveFov(glm::radians(45.0f), (float)w, (float)h, 0.01f, 1000.0f);
		}

		const Mat4 getViewMatrix() const
		{
			return glm::lookAt(position, position + front, up);
		}

		void UpdateCameraVectors()
		{
			front.x = cos(Radians(yaw)) * cos(Radians(pitch));
			front.y = sin(Radians(pitch));
			front.z = sin(Radians(yaw)) * cos(Radians(pitch));
			front = glm::normalize(front);

			right = glm::normalize(glm::cross(front, worldUp));
			up = glm::normalize(glm::cross(right, front));

			dirty = true;
		}

		void ProcessKeyboard(Camera_Movement direction, float deltaTime)
		{
			float velocity = movementSpeed;
			if(direction == FORWARD)
				position += front * velocity;
			if(direction == BACKWARD)
				position -= front * velocity;
			if(direction == LEFT)
				position -= right * velocity;
			if(direction == RIGHT)
				position += right * velocity;
		}

		void ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch = true)
		{
			xoffset *= mouseSensitivity;
			yoffset *= mouseSensitivity;

			yaw += xoffset;
			pitch += yoffset;

			if(constrainPitch) {
				if(pitch > 89.0f)
					pitch = 89.0f;
				if(pitch < -89.0f)
					pitch = -89.0f;
			}

			UpdateCameraVectors();
		}

		Vector3<float> position = 0.0f;
		Vector3<float> front = 0.0f;
		Vector3<float> up = 0.0f;
		Vector3<float> right = 0.0f;
		Vector3<float> worldUp = 0.0f;
	
		float yaw = 0.0f;
		float pitch = 0.0f;
		
		float movementSpeed = 0.5f;
		float mouseSensitivity = 0.1f;
		float zoom = 0.0f;
	};
}