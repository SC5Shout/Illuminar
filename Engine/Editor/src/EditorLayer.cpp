#include "Illuminar_pch.h"

#include "EditorLayer.h"

#include "GameEngine/Illuminar_Application.h"
#include "stb_image.h"

#include "GameEngine/LightBuilder/Illuminar_LightBuilder.h"

namespace Illuminar {	
	Entity pointLight;

	EditorLayer::EditorLayer(Application& app)
	{
		this->app = &app;

		state = EditorState { 
			.camera = EditorCamera{ {0.0f, 0.0f, 0.0f} },
			.selectedEntity = {}, 
			.currentScene = editorScene, 
			.sceneState = SceneState::Edit 
		};
		editorCamera = &state.camera;
		editorCamera->projection = glm::perspectiveFov(glm::radians(45.0f), (float)sceneW, (float)sceneH, 0.1f, 1000.0f);

		onScenePlay = [this]() {
			//state.sceneState = SceneState::Play;
			//runtimeScene = Scene::Copy(editorScene);
			//state.currentScene = runtimeScene;
		};

		onSceneStop = [this]() {
			//state.sceneState = SceneState::Edit;
			//
			//runtimeScene = nullptr;
			//state.currentScene = editorScene;
		};

		project = Project::Load("Editor.proj");

		auto sceneHierarchyPanel = panelManager.Add<SceneHierarchyPanel>("sceneHierarchy", state);
		auto propertiesPanel = panelManager.Add<PropertiesPanel>("properties", state);
		auto mainBarPanel = panelManager.Add<MainBarPanel>("MainBar", state);
		auto scenePanel = panelManager.Add<ScenePanel>("Scene", state, onScenePlay, onSceneStop, sceneWindowFocused);
		auto assetPanel = panelManager.Add<AssetPanel>("Assets", state, project);
		auto loadingPanel = panelManager.Add<LoadingPanel>("LoadingPanel", state, project);

		auto entity = editorScene->CreateEntity("Sky");
		auto comp = entity.AddComponent<SkylightComponent>();
		comp->environment = Environment::CreatePreethamSky(comp->turbidityAzimuthInclination.x, comp->turbidityAzimuthInclination.y, comp->turbidityAzimuthInclination.z);
		comp->dynamicSky = true;
		
		LightBuilder(LightType::Directional).
			Direction({ Radians(-45.0f), Radians(-175.0f), Radians(160.0f) }).
			Build("directionalLight", editorScene);
			
		LightBuilder(LightType::Point).
			Position({ 0.0f, 0.5f, 0.0f }).
			Radiance({1.0f, 1.0f, 1.0f}).
			Build("point light 2", editorScene);
		
		static constexpr glm::vec3 LIGHT_MIN_BOUNDS = glm::vec3(-8.0f, 0.5f, -10.0f);
		static constexpr glm::vec3 LIGHT_MAX_BOUNDS = glm::vec3(10.0f, 4.0f, 10.0f);
		
		auto RandomPosition = [](std::mt19937 gen) {
			std::uniform_real_distribution<float> x(LIGHT_MIN_BOUNDS.x, LIGHT_MAX_BOUNDS.x);
			std::uniform_real_distribution<float> y(LIGHT_MIN_BOUNDS.y, LIGHT_MAX_BOUNDS.y);
			std::uniform_real_distribution<float> z(LIGHT_MIN_BOUNDS.z, LIGHT_MAX_BOUNDS.z);
		
			return Vector3<float>(
				x(gen),
				y(gen),
				z(gen)
			);
		};
		
		auto allLights = editorScene->CreateEntity("All point lights");

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> dis(0, 1);
		for (int i = 0; i < 100; i++) {
			auto pointLight = LightBuilder(LightType::Point).
				Position(RandomPosition(gen)).
				Radiance(Vector3<float>(1.0f + dis(gen), 1.0f + dis(gen), 1.0f + dis(gen))).
				Radius(3.0f).
				Build("Point light " + std::to_string(i), editorScene, allLights);
		}
	}

	void EditorLayer::DrawGUI()
	{
		RenderInADockspace([&]() {
			ImGui::ShowDemoWindow();

			for (auto& pass : editorScene->sceneRenderer->debugPasses) {
				Widget::ScopedWindow depth(pass->getName().data());
				ImGui::Image((ImTextureID)pass->getTexture()->getTexture().getID(), ImGui::GetContentRegionAvail(), {0.0f, 1.0f}, {1.0f, 0.0f});
			}

			panelManager.DrawGUI();
		});
	}

	void EditorLayer::Update([[maybe_unused]] Timestep deltaTime)
	{		
		panelManager.Update(deltaTime);
		panelManager.Draw();
		
		if(state.sceneState == SceneState::Edit) {
			if(sceneWindowFocused) {
				if(SDL_GetRelativeMouseMode()) {
					auto& camera = *editorCamera;
					if(Input::KeyDown(Keyboard::A)) {
						camera.ProcessKeyboard(LEFT, (float)deltaTime);
					} else if(Input::KeyDown(Keyboard::D)) {
						camera.ProcessKeyboard(RIGHT, (float)deltaTime);
					}
		
					if(Input::KeyDown(Keyboard::W)) {
						camera.ProcessKeyboard(FORWARD, (float)deltaTime);
					} else if(Input::KeyDown(Keyboard::S)) {
						camera.ProcessKeyboard(BACKWARD, (float)deltaTime);
					}
				}
			}
		}
		
		//editorCamera.projection = glm::perspectiveFov(glm::radians(45.0f), (float)sceneW, (float)sceneH, 0.01f, 1000.0f);
		
		//temp until I find out how to get rid of Rasterization quality warning: A non-fullscreen clear caused a fallback from CSAA to MSAA.
		//The problem is that ImGui sets its own state and doesn't return to the old one
		//but it shouldn't be necessary, because each render pass sets its state every frame so... I don't know
		GFX::RenderCommand::Submit([]() { glDisable(GL_SCISSOR_TEST); });
		switch(state.sceneState) {
			case SceneState::Edit:
				editorScene->UpdateEditor(deltaTime, *editorCamera, editorCamera->getViewMatrix());
				break;
			case SceneState::Play:
				state.currentScene->UpdateRuntime(deltaTime);
				break;
		}
	}

	void EditorLayer::OnEvent([[maybe_unused]] SDL_Event& event)
	{
		panelManager.OnEvent(event);

		if(sceneWindowFocused && Input::ButtonDown(Mouse::Right)) {
			SDL_SetRelativeMouseMode(SDL_TRUE);
			auto& camera = *editorCamera;
			camera.ProcessMouseMovement(Input::getMouseRelX(), -Input::getMouseRelY());
		} else SDL_SetRelativeMouseMode(SDL_FALSE);
	}
}