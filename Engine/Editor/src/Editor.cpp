#include <Illuminar_pch.h>
#include <GameEngine/Illuminar_Application.h>

#include "EditorLayer.h"

#include "ImGui.h"


namespace Illuminar {
	struct Editor : Application
	{
		Editor(const ApplicationCreateInfo& createInfo)
			: Application(createInfo)
		{
			EngineLogInfo("==========================================");
			EngineLogInfo("Editor is more like a sandbox right now");
			EngineLogInfo("==========================================\n");
		}

		~Editor() override = default;

		void OnEvent(SDL_Event& event) override
		{
			editor.OnEvent(event);
			gui.OnEvent(event);
		}

		void Update([[maybe_unused]] Timestep deltaTime) override
		{
			gui.Render([this]() {
				editor.DrawGUI();
			});
			
			editor.Update(deltaTime);
		}

		GUI gui{ window };
		EditorLayer editor{ *this };
	};

	std::unique_ptr<Application> CreateApplication()
	{
		ApplicationCreateInfo editorInfo;
		editorInfo.name = "Editor";
		editorInfo.w = 1600;
		editorInfo.h = 900;

		return std::make_unique<Editor>(editorInfo);
	}
}

#include <Illuminar_EntryPoint.h>
