#pragma once

#include "Panels/SceneHierarchyPanel.h"
#include "Panels/PropertiesPanel.h"
#include "Panels/MainBarPanel.h"
#include "Panels/LoadingPanel.h"
#include "Panels/ScenePanel.h"
#include "Panels/PanelManager.h"
#include "Panels/AssetPanel.h"

#include "EditorCamera.h"

#include "GameEngine/Illuminar_Timestep.h"
#include "GameEngine/Scene/Illuminar_Scene.h"

#include "GameEngine/Input/Illuminar_Input.h"

#include "GameEngine/Project/Illuminar_Project.h"

#include "Widgets.h"

namespace Illuminar {
	struct Application;

	static std::shared_ptr<Project> project = nullptr;

	struct EditorLayer
	{
		EditorLayer(Application& app);
		~EditorLayer() = default;

		void DrawGUI();
		void Update([[maybe_unused]] Timestep deltaTime);

		void OnEvent([[maybe_unused]] SDL_Event& event);

		template<typename FuncT>
		inline void RenderInADockspace(FuncT&& funcToRenderInADockspace);

		Application* app = nullptr;

		EditorState state;
		EditorCamera* editorCamera = nullptr;

		std::function<void()> onScenePlay;
		std::function<void()> onSceneStop;

		PanelManager panelManager;

		Ref<Scene> editorScene = Ref<Scene>::Create();

		uint32_t sceneW = 1920;
		uint32_t sceneH = 1080;

		bool sceneWindowFocused = false;
	};

	template<typename FuncT>
	inline void EditorLayer::RenderInADockspace(FuncT&& funcToRenderInADockspace)
	{
		static ImGuiDockNodeFlags opt_flags = ImGuiDockNodeFlags_None;

		ImGuiViewport* viewport = ImGui::GetMainViewport();

		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);

		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		Widget::ScopedWindow dockspace("MainDockspaceWindow", nullptr, window_flags);
		ImGui::PopStyleVar(3);

		ImGuiIO& io = ImGui::GetIO();
		if(io.ConfigFlags & ImGuiConfigFlags_DockingEnable) {
			ImGuiID dockspace_id = ImGui::GetID("MainDockspace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), opt_flags);
		}

		funcToRenderInADockspace();
	}
}

