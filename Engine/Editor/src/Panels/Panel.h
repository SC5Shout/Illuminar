#pragma once

#include "SDL.h"

#include "External/imgui.h"
#include "External/imgui_internal.h"
#include "External/ImGuizmo.h"

#include "Widgets.h"

#include "GameEngine/Illuminar_Timestep.h"
#include "EditorState.h"

namespace Illuminar {
	struct Scene;
	struct Panel : RefCount
	{
		Panel(EditorState& editorState)
			: editorState(editorState)
		{
		}
		virtual ~Panel() = default;

		virtual void OnEvent(SDL_Event& event) {}
		virtual void Update([[maybe_unused]] Timestep deltaTime) {}
		virtual void Draw() {}
		virtual void DrawGUI() {}

	protected:
		EditorState& editorState;
	};
}