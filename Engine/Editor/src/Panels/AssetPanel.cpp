#include "Illuminar_pch.h"
#include "AssetPanel.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"
#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar {
	bool IsWindowClicked(ImGuiMouseButton mouse_button = 0)
	{
		return ImGui::IsMouseClicked(mouse_button) && ImGui::IsWindowHovered(ImGuiHoveredFlags_None);
	}

	static constexpr float padding = 2.0f;
	static constexpr float thumbnailSize = 96.0f;

	AssetPanel::AssetPanel(EditorState& editorState, const std::shared_ptr<Project>& project)
		: Panel(editorState), project(project)
	{
		//temp
		struct TexturePack
		{
			AssetID assetID;
			std::string name;
			TextureResourceID id;
		};
		std::vector<TexturePack> texturePacks;
		auto& editorTextures = AssetManager::AddAssetPackage("EditorTextures");
		for (const auto& entry : std::filesystem::recursive_directory_iterator("assets/editor/textures")) {
			if (std::filesystem::path(entry).extension() == ".png") {
				std::string id = entry.path().filename().string();
				std::string name = entry.path().filename().stem().string();

				editorTextures.AddAsset(entry.path().filename().string(), entry.path().generic_string().c_str());
				texturePacks.emplace_back(id, name, 0);
			}
		}

		auto& textureManager = ResourceManagers::get().textureResourceManager;
		for (auto& [assetID, name, id] : texturePacks) {
			id = textureManager.LoadTextureResourceByAssetID(assetID, "png");
		}

		ResourceStreamer::FlushAllQueues();

		for (auto& [assetID, name, id] : texturePacks) {
			textures[name] = textureManager.getResourceByID(id).texture.as<GFX::Texture2D>();
		}
	}

	void AssetPanel::DrawGUI()
	{
		Widget::ScopedWindow window("Assets");

		ImGuiTableFlags flags = ImGuiTableFlags_Resizable
			| ImGuiTableFlags_SizingFixedFit
			| ImGuiTableFlags_BordersInnerV;

		Widget::ScopedTable table("##assetsTable", 2, flags, ImVec2(0.0f, 0.0f));

		ImGui::TableSetupColumn("Assets Structure", 0, 300.0f);
		ImGui::TableSetupColumn("Directory Content", ImGuiTableColumnFlags_WidthStretch);

		ImGui::TableNextRow();
		ImGui::TableSetColumnIndex(0);

		Widget::ScopedStyle spacing(ImGuiStyleVar_ItemSpacing, ImVec2(4.0f, 4.0f));
		Widget::ScopedStyle cellPadding(ImGuiStyleVar_CellPadding, ImVec2(10.0f, 2.0f));

		{
			Widget::ScopedChildWindow folders("##Assets");

			if (ImGui::CollapsingHeader("Assets", nullptr, ImGuiTreeNodeFlags_DefaultOpen)) {
				auto baseDirectory = ProcessDirectories(Project::getAssetDirectory());

				for (auto& child : baseDirectory.children) {
					DrawDirectories(child);
				}

				for (auto& asset : baseDirectory.assets) {
					DrawAssets(asset);
					RegisterDragSource(asset);
				}
			}
		}

		ImGui::TableSetColumnIndex(1);

		{
			const float topBarHeight = 26.0f;
			Widget::ScopedChildWindow folders("##Content", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetWindowHeight() - topBarHeight));

			ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.0f);
			RenderTopBar(topBarHeight);
			ImGui::PopStyleVar();

			const float paddingForOutline = 2.0f;
			const float scrollBarrOffset = 20.0f + ImGui::GetStyle().ScrollbarSize;
			float panelWidth = ImGui::GetContentRegionAvail().x - scrollBarrOffset;
			float cellSize = thumbnailSize + padding + paddingForOutline;
			int columnCount = (int)(panelWidth / cellSize);
			if (columnCount < 1) columnCount = 1;

			{
				const float rowSpacing = 12.0f;
				Widget::ScopedStyle spacing(ImGuiStyleVar_ItemSpacing, ImVec2(paddingForOutline, rowSpacing));
				ImGui::Columns(columnCount, 0, false);

				Widget::ScopedStyle border(ImGuiStyleVar_FrameBorderSize, 0.0f);
				Widget::ScopedStyle padding(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
				RenderItems();				
			}
		}
	}

	AssetDirectory AssetPanel::ProcessDirectories(const std::filesystem::path& path)
	{
		AssetDirectory directory{ path };

		for (auto entry : std::filesystem::directory_iterator(path)) {
			if (entry.is_directory()) {
				auto& dir = directory.children.emplace_back() = ProcessDirectories(entry);
				directory.subdirectories.push_back(dir);
				continue;
			}

			if (entry.path().extension() == ".png") {
				directory.assets.push_back(PngAssetItem::Create(entry.path()));
			} else if (entry.path().extension() == ".fbx" || entry.path().extension() == ".gltf") {
				directory.assets.push_back(FbxAssetItem::Create(entry.path()));
			} else directory.assets.push_back(FileAssetItem::Create(entry.path()));
		}

		return directory;
	}

	void AssetPanel::DrawDirectories(AssetDirectory& directory)
	{
		const std::string& dirName = directory.getName();
		const std::string id = directory.getPath().string() + "_TreeNode";

		//auto* window = ImGui::GetCurrentWindow();
		//window->DC.CurrLineSize.y = 20.0f;
		//window->DC.CurrLineTextBaseOffset = 3.0f;
		//const ImRect itemRect = { window->WorkRect.Min.x, window->DC.CursorPos.y,
		//						  window->WorkRect.Max.x, window->DC.CursorPos.y + window->DC.CurrLineSize.y };
		//
		//auto fillWithColour = [&](const ImColor& colour)
		//{
		//	const ImU32 bgColour = ImGui::ColorConvertFloat4ToU32(colour);
		//	ImGui::GetWindowDrawList()->AddRectFilled(itemRect.Min, itemRect.Max, bgColour);
		//};
		//
		//auto checkIfAnyDescendantSelected = [&](AssetDirectory& directory, auto isAnyDescendantSelected) -> bool
		//{
		//	if (directory.getPath() == currentItem)
		//		return true;
		//
		//	if (!directory.subdirectories.empty())
		//	{
		//		for (auto& childDir : directory.subdirectories)
		//		{
		//			if (isAnyDescendantSelected(childDir, isAnyDescendantSelected))
		//				return true;
		//		}
		//	}
		//
		//	return false;
		//};
		//
		//const bool isItemClicked = [&itemRect, &id]
		//{
		//	if (ImGui::ItemHoverable(itemRect, ImGui::GetID(id.c_str())))
		//	{
		//		return ImGui::IsMouseDown(ImGuiMouseButton_Left) || ImGui::IsMouseReleased(ImGuiMouseButton_Left);
		//	}
		//	return false;
		//}();
		//
		//const bool isActiveDirectory = directory.getPath() == currentItem;
		//const bool isAnyDescendantSelected = checkIfAnyDescendantSelected(directory, checkIfAnyDescendantSelected);
		//if (isActiveDirectory || isItemClicked)
		//{
		//	fillWithColour(IM_COL32(237, 192, 119, 255));
		//
		//	ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(26, 26, 26, 255));
		//}
		//else if (isAnyDescendantSelected)
		//{
		//	fillWithColour(IM_COL32(237, 201, 142, 23));
		//}

		auto flags = (currentItem == directory.getID()) ? ImGuiTreeNodeFlags_Selected : 0 | ImGuiTreeNodeFlags_SpanAvailWidth;

		const bool open = Widget::ImageTreeNode(textures[std::string(directory.getAssetTypeString())], id.c_str(), flags, dirName.c_str());

		//if (isActiveDirectory || isItemClicked)
		//	ImGui::PopStyleColor();

		if (ImGui::IsItemClicked()) {
			currentItem = directory.getID();
			currentDirectory = directory.getPath();
		}

		if (open) {
			for (auto& child : directory.children) {
				DrawDirectories(child);
			}

			for (auto& asset : directory.assets) {
				DrawAssets(asset);
				RegisterDragSource(asset);
			}
		}

		if (open) {
			ImGui::TreePop();
		}
	}

	static inline ImRect getItemRect()
	{
		return ImRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax());
	}

	static inline ImRect RectExpanded(const ImRect& rect, float x, float y)
	{
		ImRect result = rect;
		result.Min.x -= x;
		result.Min.y -= y;
		result.Max.x += x;
		result.Max.y += y;
		return result;
	}

	static ImU32 ColorWithMultipliedValue(const ImColor& color, float multiplier)
	{
		const ImVec4& colRow = color.Value;
		float hue, sat, val;
		ImGui::ColorConvertRGBtoHSV(colRow.x, colRow.y, colRow.z, hue, sat, val);
		return ImColor::HSV(hue, sat, std::min(val * multiplier, 1.0f));
	}

	void AssetPanel::RenderItem(const std::filesystem::path& filepath, std::string name, const uint32_t id, const Ref<GFX::Texture>& texture, bool displayAssetType, bool directory)
	{
		ImGui::PushID(id);
		ImGui::BeginGroup();

		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));

		const float edgeOffset = 4.0f;

		const float textLineHeight = ImGui::GetTextLineHeightWithSpacing() * 2.0f + edgeOffset * 2.0f;
		const float infoPanelHeight = std::max(displayAssetType ? thumbnailSize * 0.5f : textLineHeight, textLineHeight);

		const ImVec2 topLeft = ImGui::GetCursorScreenPos();
		const ImVec2 thumbBottomRight = { topLeft.x + thumbnailSize, topLeft.y + thumbnailSize };
		const ImVec2 infoTopLeft = { topLeft.x, topLeft.y + thumbnailSize };
		const ImVec2 bottomRight = { topLeft.x + thumbnailSize, topLeft.y + thumbnailSize + infoPanelHeight * 0.5f };

		const bool isSelected = currentItem == id;
		if(!directory) {
			auto* drawList = ImGui::GetWindowDrawList();
			drawList->AddRectFilled(topLeft, thumbBottomRight, 0xff575757, 6.0f, ImDrawFlags_RoundCornersTop);
			drawList->AddRectFilled(infoTopLeft, bottomRight, 0xff575757, 6.0f, ImDrawFlags_RoundCornersBottom);
		} else if (ImGui::ItemHoverable(ImRect(topLeft, bottomRight), ImGui::GetID(&id)) || isSelected) {
			auto* drawList = ImGui::GetWindowDrawList();
			drawList->AddRectFilled(topLeft, bottomRight, 0xff575757, 6.0f);
		}

		const float imageOffsetFromButton = -6.0f;
		ImGui::InvisibleButton("##thumbnailButton", ImVec2{ thumbnailSize, thumbnailSize });
		Widget::Image(texture, RectExpanded(getItemRect(), imageOffsetFromButton, imageOffsetFromButton));
		
		const auto textSize = ImGui::CalcTextSize(name.c_str(), nullptr);
		const float cropSize = thumbnailSize - edgeOffset * 2.0f;

		if (textSize.x >= cropSize) {
			name = name.substr(0, 10);
			for (int i = 7; i < 10; ++i) {
				name[i] = '.';
			}
		}

		const auto wrapSize = ImGui::GetCursorPosX() + (thumbnailSize - edgeOffset * 2.0f);
		const ImVec2 cursor = ImGui::GetCursorPos();
		ImGui::SetCursorPos(ImVec2(cursor.x + edgeOffset, cursor.y - edgeOffset));
		ImGui::PushTextWrapPos(wrapSize);
		ImGui::Text(name.c_str());
		ImGui::PopTextWrapPos();

		ImGui::SetCursorPos(ImVec2(cursor.x - edgeOffset, cursor.y + edgeOffset));

		ImGui::PopStyleVar();
		ImGui::EndGroup();
		
		if (isSelected || ImGui::IsItemHovered()) {
			ImRect itemRect = getItemRect();
			auto* drawList = ImGui::GetWindowDrawList();
		
			if (isSelected) {
				const bool mouseDown = ImGui::IsMouseDown(ImGuiMouseButton_Left) && ImGui::IsItemHovered();
				ImColor colTransition = ColorWithMultipliedValue(IM_COL32(237, 192, 119, 255), 0.8f);

				drawList->AddRect(
					itemRect.Min,
					itemRect.Max,
					mouseDown ? colTransition.operator unsigned int() : IM_COL32(237, 192, 119, 255), 
					6.0f);
			} else { // isHovered
				if (!directory) {
					drawList->AddRect(
						itemRect.Min,
						itemRect.Max,
						IM_COL32(77, 77, 77, 255), 
						6.0f);
				}
			}
		}

		if (ImGui::IsItemClicked()) {
			currentItem = id;

			if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
				if (directory) {
					currentItem = id;
					prevDirectoryStack.push_back(currentDirectory);
					currentDirectory = filepath;

					if (!nextDirectoryStack.empty()) {
						const auto& prev = prevDirectoryStack.back();
						const auto& next = nextDirectoryStack.back();
						if (next != prev) {
							nextDirectoryStack.clear();
						}
					}
				}
			}
		}

		ImGui::PopID();
		ImGui::NextColumn();
	}

	void AssetPanel::RenderTopBar(float height)
	{
		const auto GoBack = [&]() {
			if (prevDirectoryStack.empty()) { return; }

			nextDirectoryStack.push_back(currentDirectory);
			currentDirectory = prevDirectoryStack.back();
			prevDirectoryStack.pop_back();
		};

		const auto GoNext = [&]() {
			if (nextDirectoryStack.empty()) { return; }

			prevDirectoryStack.push_back(currentDirectory);
			currentDirectory = nextDirectoryStack.back();
			nextDirectoryStack.pop_back();
		};

		Widget::ScopedChildWindow window("##top_bar", ImVec2(0.0f, height));
		if (ImGui::Button("<")) {
			GoBack();
		} 

		ImGui::SameLine();

		if (ImGui::Button(">")) {
			GoNext();
		}

		auto directoryStack = prevDirectoryStack;
		directoryStack.push_back(currentDirectory);
		uint32_t i = 0;
		for (auto& dir : directoryStack) {
			ImGui::SameLine();
			if (ImGui::Button(dir.filename().string().c_str())) {
				if (dir != currentDirectory) {
					for (uint32_t j = 0; j < directoryStack.size() - 1 - i; ++j) {
						GoBack();
					}
				}
			}
		
			if (++i != directoryStack.size()) {
				ImGui::SameLine();
				ImGui::Text("->");
			}
		}
	}

	void AssetPanel::RenderItems()
	{
		if (currentItem == 0) { return; }

		auto directory = ProcessDirectories(currentDirectory);

		for (auto& child : directory.children) {
			RenderItem(child.getPath(), child.getName(), child.getID(), textures[std::string(child.getAssetTypeString())], true, true);
		}

		for (auto& asset : directory.assets) {
			RenderItem(asset->getPath(), asset->getName(), asset->getID(), textures[std::string(asset->getAssetTypeString())], true, false);
			RegisterDragSource(asset);
		}
	}

	void AssetPanel::DrawAssets(const std::shared_ptr<AssetItem>& asset)
	{
		const std::string& assetName = asset->getName();
		const std::string id = assetName + "_TreeNode";

		ImGuiTreeNodeFlags flags = ((currentItem == asset->getID()) ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth;
		flags |= ImGuiTreeNodeFlags_Leaf;

		const bool open = Widget::ImageTreeNode(textures[std::string(asset->getAssetTypeString())], id.c_str(), flags, assetName.c_str());

		if (ImGui::IsItemClicked()) {
			currentItem = asset->getID();
		}

		if (open) {
			ImGui::TreePop();
		}
	}

	void AssetPanel::RegisterDragSource(std::shared_ptr<AssetItem> asset)
	{
		bool dragging = false;
		if (dragging = ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
			isDragging = true;
			
			Widget::Image(textures[std::string(asset->getAssetTypeString())], ImVec2(20, 20));
			ImGui::SameLine();
			const auto& name = asset->getName();
			ImGui::TextUnformatted(name.c_str());

			uint32_t id = asset->getID().id;

			ImGui::SetDragDropPayload("asset_payload", &id, sizeof(id));
			ImGui::EndDragDropSource();
		}

		isDragging = dragging;
	}
}