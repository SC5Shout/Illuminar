#include "Illuminar_pch.h"
#include "LoadingPanel.h"

namespace Illuminar {
	LoadingPanel::LoadingPanel(EditorState& editorState, const std::shared_ptr<Project>& project)
		: Panel(editorState), textureManager(ResourceManagers::get().textureResourceManager)
	{
		package = project->getAssetPackage();
		for (auto& [id, virtualFilename] : package->assets) {
			if (virtualFilename.find("editor") != std::string::npos) {
				continue;
			}
			auto extension = std::filesystem::path(virtualFilename).extension();
			if (extension == ".png") {
				//textureManager.LoadTextureResourceByAssetID(id, "png");
			} else if (extension == ".fbx" || extension == ".gltf") {
				//ResourceManagers::get().meshResourceManager.LoadMeshResourceByAssetID(id);
			}
		}
	}
	
	void LoadingPanel::DrawGUI()
	{
		if (showpopup) {
			ImGui::OpenPopup("Loading...");
			showpopup = false;
		}
		
		ImVec2 center = ImGui::GetMainViewport()->GetCenter();
		ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
		ImGui::SetNextWindowSize(ImVec2{ 400, 0 });
		if (ImGui::BeginPopupModal("Loading...", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
			float size = (float)ResourceStreamer::getLoadingResourceCount();
			float progress = 1.0f - (float)(size / (float)textureManager.getResourceCount());
			ImGui::ProgressBar(progress, ImVec2(0, 0));
		
			auto maxCount = textureManager.getResourceCount() + ResourceManagers::get().meshResourceManager.getResourceCount();
			ImGui::Text("%i/%i", maxCount - ResourceStreamer::getLoadingResourceCount(), maxCount);
		
			if(size == 0) {
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
	}
}
