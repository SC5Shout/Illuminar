#pragma once

#include "Panel.h"
#include "GameEngine/Project/Illuminar_Project.h"

#include "stb_image.h"

namespace Illuminar {
	enum struct AssetType
	{
		NONE,
		PNG,
		FBX
	};

	struct AssetItem
	{
		AssetItem(const std::filesystem::path& path)
		{
			name = path.filename().string();
			id = AssetID(path.generic_string().c_str());
		}
		virtual ~AssetItem() = default;

		[[nodiscard]] virtual constexpr std::string_view getAssetTypeString() const = 0;

		[[nodiscard]] AssetID getID() const { return id; }
		[[nodiscard]] const std::string& getName() const { return name; }
		[[nodiscard]] const Ref<GFX::Texture2D>& getIcon() const { return icon; }
		[[nodiscard]] const std::filesystem::path& getPath() const { return path; }

	private:
		Ref<GFX::Texture2D> icon = nullptr;
		AssetID id;
		std::string name;
		std::filesystem::path path;
	};

	struct FileAssetItem : AssetItem
	{
		[[nodiscard]] static std::shared_ptr<FileAssetItem> Create(const std::filesystem::path& path)
		{
			return std::make_shared<FileAssetItem>(path);
		}

		FileAssetItem(const std::filesystem::path& path)
			: AssetItem(path)
		{
		}
		~FileAssetItem() override = default;

		[[nodiscard]] constexpr std::string_view getAssetTypeString() const override { return "file"; }
	};

	struct PngAssetItem : AssetItem
	{
		[[nodiscard]] static std::shared_ptr<PngAssetItem> Create(const std::filesystem::path& path)
		{
			return std::make_shared<PngAssetItem>(path);
		}

		PngAssetItem(const std::filesystem::path& path)
			: AssetItem(path)
		{
		}
		~PngAssetItem() override = default;

		[[nodiscard]] constexpr std::string_view getAssetTypeString() const override { return "png"; }
	};

	struct FbxAssetItem : AssetItem
	{
		static std::shared_ptr<FbxAssetItem> Create(const std::filesystem::path& path)
		{
			return std::make_shared<FbxAssetItem>(path);
		}

		FbxAssetItem(const std::filesystem::path& path)
			: AssetItem(path)
		{
		}
		~FbxAssetItem() override = default;

		[[nodiscard]]  constexpr std::string_view getAssetTypeString() const override { return "fbx"; }
	};

	struct AssetDirectory
	{
		AssetDirectory() = default;
		AssetDirectory(const std::filesystem::path& path)
			: path(path)
		{
			name = path.filename().string();
			id = AssetID(path.generic_string().c_str());
		}

		bool operator==(const AssetDirectory& other) const
		{
			return path == other.path && name == other.name && id == other.id;
		}

		std::vector<AssetDirectory> subdirectories;
		std::vector<AssetDirectory> children;
		std::vector<std::shared_ptr<AssetItem>> assets;

		[[nodiscard]] AssetID getID() const { return id; }
		[[nodiscard]] inline const std::string& getName() const { return name; }
		[[nodiscard]] inline const std::filesystem::path& getPath() const { return path; }
		[[nodiscard]] constexpr std::string_view getAssetTypeString() const { return "folder"; }

	private:
		AssetID id;
		std::string name;
		std::filesystem::path path;
	};

	struct AssetPanel : Panel
	{
		AssetPanel(EditorState& editorState, const std::shared_ptr<Project>& project);

		void DrawGUI() override;

	private:
		void RenderTopBar(float height);
		void RenderItems();

		void DrawAssets(const std::shared_ptr<AssetItem>& asset);
		void DrawDirectories(AssetDirectory& directory);
		[[nodiscard]] AssetDirectory ProcessDirectories(const std::filesystem::path& path);

		void RenderItem(const std::filesystem::path& filepath, std::string name, const uint32_t id, const Ref<GFX::Texture>& texture, bool displayAssetType, bool directory);

		void RegisterDragSource(std::shared_ptr<AssetItem> asset);

		std::shared_ptr<Project> project = nullptr;
		AssetID currentItem{ Project::getAssetDirectory().generic_string() };
		std::filesystem::path currentDirectory = Project::getAssetDirectory();

		std::vector<std::filesystem::path> prevDirectoryStack;
		std::vector<std::filesystem::path> nextDirectoryStack;

		std::unordered_map<std::string, Ref<GFX::Texture2D>> textures;

		bool isDragging = false;
	};
}