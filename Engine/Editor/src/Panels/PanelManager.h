#pragma once

#include "Panel.h"

namespace Illuminar {
	struct PanelManager
	{
		template<typename T, typename ... Args>
		[[nodiscard]] inline Ref<T> Add(const std::string& name, Args&& ... args) requires std::is_base_of_v<Panel, T>
		{
			if (panels.contains(name)) {
				return panels[name];
			}

			Ref<T> panel = Ref<T>::Create(std::forward<Args>(args)...);
			panels.emplace(name, panel);
			return panel;
		}

		template<typename T> requires std::is_base_of_v<Panel, T>
		[[nodiscard]] inline Ref<T> Add(const std::string& name, const Ref<T>& panel)
		{
			if (panels.contains(name)) {
				return panels[name];
			}

			panels.emplace(name, panel);
			return panel;
		}

		template<typename T>
		[[nodiscard]] inline Ref<T> get(const std::string& name) requires std::is_base_of_v<Panel, T>
		{
			return panels.contains(name) ? panels.at(name) : nullptr;
		}

		[[nodiscard]] inline bool Remove(const std::string& name)
		{
			if (panels.contains(name)) {
				panels.erase(name);
				return true;
			}

			return false;
		}

		void OnEvent(SDL_Event& event);
		void Update([[maybe_unused]] Timestep deltaTime);
		void Draw();
		void DrawGUI();
		void setCurrentScene(const Ref<Scene>& currentScene);

	private:
		std::unordered_map<std::string, Ref<Panel>> panels;
	};
}