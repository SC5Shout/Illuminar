#include "Illuminar_pch.h"
#include "ScenePanel.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

namespace Illuminar {
	void ScenePanel::DrawGUI()
	{
		{
			Widget::ScopedStyle style(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
			ImGui::Begin("Scene", nullptr, ImGuiWindowFlags_MenuBar);
		}

		DrawLoadingAssetPanel();

		{
			DrawMenuBar();

			{
				Widget::ScopedChildWindow sceneWindow("SceneChild");

				const auto viewportOffset = ImGui::GetCursorPos();
				const auto viewportSize = ImGui::GetContentRegionAvail();	

				auto& scene = editorState.currentScene;
				if (scene) {
					const auto& finalTexture = scene->sceneRenderer->compositePass.colorMap;
					scene->setViewport(viewportSize.x, viewportSize.y);
					Widget::Image(finalTexture, viewportSize, { 0.0f, 1.0f }, { 1.0f, 0.0f });

					sceneWindowFocused = ImGui::IsWindowFocused();
				}

				RegisterAssetDragTarget();

				DrawGuizmo();
			}
		} ImGui::End();
	}

	void ScenePanel::Update(Timestep deltaTime)
	{
		if (editorState.sceneState == SceneState::Edit) {
			if (sceneWindowFocused) {
				if (!SDL_GetRelativeMouseMode()) {
					if (Input::KeyDown(Keyboard::W)) {
						guizmoType = ImGuizmo::OPERATION::TRANSLATE;
					}
					else if (Input::KeyDown(Keyboard::E)) {
						guizmoType = ImGuizmo::OPERATION::ROTATE;
					}
					else if (Input::KeyDown(Keyboard::R)) {
						guizmoType = ImGuizmo::OPERATION::SCALE;
					}
				}
			}
		}
	}

	void ScenePanel::DrawMenuBar()
	{
		Widget::ScopedMenuBar menubar;

		ImGui::SetCursorPosX(ImGui::GetWindowWidth() * 0.5f - ImGui::GetFrameHeight());

		const TextureResource* playResource = ResourceManagers::get().textureResourceManager.tryGetResourceByID(playSceneID);
		if (playResource && playResource->Loaded()) {
			if (Widget::ImageButton(playResource->getTexture(), ImVec2(15.0f, 15.0f))) {
				onScenePlay();
			}
		}

		const TextureResource* pauseResource = ResourceManagers::get().textureResourceManager.tryGetResourceByID(pauseSceneID);
		if (pauseResource && pauseResource->Loaded()) {
			if (Widget::ImageButton(pauseResource->getTexture(), ImVec2(15.0f, 15.0f))) {
				onSceneStop();
			}
		}
	}

	void ScenePanel::RegisterAssetDragTarget()
	{
		auto& scene = editorState.currentScene;
		auto& meshManager = ResourceManagers::get().meshResourceManager;
		if (ImGui::BeginDragDropTarget()) {
			const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("asset_payload", ImGuiDragDropFlags_AcceptNoDrawDefaultRect);

			if (payload) {
				uint32_t assetID = *((uint32_t*)payload->Data);
				if (assetID != INVALID_ASSET_ID) {
					assetDragData.id = meshManager.LoadMeshResourceByAssetID(assetID);

					showLoadingAssetPanel = true;
				}
			}

			ImGui::EndDragDropTarget();
		}
	}

	[[nodiscard]] float ScenePanel::getSnapValue() const
	{
		switch(guizmoType) {
			case ImGuizmo::OPERATION::TRANSLATE: return 0.5f;
			case ImGuizmo::OPERATION::ROTATE: return 45.0f;
			case ImGuizmo::OPERATION::SCALE: return 0.5f;
		} return 0.0f;
	}

	bool showpopup = true;

	void ScenePanel::DrawLoadingAssetPanel()
	{
		if (showLoadingAssetPanel) {
			ImGui::OpenPopup("Loading...");
			
			auto& textureManager = ResourceManagers::get().textureResourceManager;
			auto& meshManager = ResourceManagers::get().meshResourceManager;
			ImVec2 center = ImGui::GetMainViewport()->GetCenter();
			ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
			ImGui::SetNextWindowSize(ImVec2{ 400, 0 });
			if (ImGui::BeginPopupModal("Loading...", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
				float size = (float)ResourceStreamer::getLoadingResourceCount();
				float progress = 1.0f - (float)(size / (float)(textureManager.getResourceCount() + meshManager.getResourceCount()));
				ImGui::ProgressBar(progress, ImVec2(0, 0));

				auto maxCount = textureManager.getResourceCount() + meshManager.getResourceCount();
				ImGui::Text("%i/%i", maxCount - ResourceStreamer::getLoadingResourceCount(), maxCount);

				if (size == 0) {
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}

			auto& scene = editorState.currentScene;
			auto id = assetDragData.id;
			MeshResource& resource = meshManager.getResourceByID(id);

			if (resource.Loaded()) {
				[[maybe_unused]] auto mesh = scene->CreateMeshEntity(resource.mesh->name, id);

				showLoadingAssetPanel = false;
			}
		}
	}

	void ScenePanel::DrawGuizmo()
	{
		auto& selectedEntity = editorState.selectedEntity;
		auto& scene = editorState.currentScene;
		auto& camera = editorState.camera;

		if (guizmoType != -1 && selectedEntity != INVALID_ENTITY) {
			float rw = (float)ImGui::GetWindowWidth();
			float rh = (float)ImGui::GetWindowHeight();
			ImGuizmo::SetOrthographic(false);
			ImGuizmo::SetDrawlist();
			ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, rw, rh);
		
			TransformComponent& entityTransform = *selectedEntity.getComponent<TransformComponent>();
			Mat4 transform = scene->getTransformRelativeToParent(selectedEntity);
		
			float snapValue = getSnapValue();
			float snapValues[3] = { snapValue, snapValue, snapValue };
		
			bool snap = false;
			ImGuizmo::Manipulate(glm::value_ptr(camera.getViewMatrix()),
				glm::value_ptr(camera.projection),
				(ImGuizmo::OPERATION)guizmoType,
				ImGuizmo::LOCAL,
				glm::value_ptr(transform),
				nullptr,
				snap ? snapValues : nullptr);
		
			if (ImGuizmo::IsUsing()) {
				Entity parent = scene->FindEntityByID(selectedEntity.getParentID());
				if (parent) {
					glm::mat4 parentTransform = scene->getTransformRelativeToParent(parent);
					entityTransform = glm::inverse(parentTransform) * transform;
				} else entityTransform = transform;
			}
		}
	}
}