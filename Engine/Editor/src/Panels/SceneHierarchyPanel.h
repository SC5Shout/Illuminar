#pragma once

#include "Panel.h"

namespace Illuminar {
	struct SceneHierarchyPanel : Panel
	{
		SceneHierarchyPanel(EditorState& editorState);
		~SceneHierarchyPanel() override = default;

		void DrawGUI() override;

	private:
		void DrawEntityNode(Entity entity);
	
		std::vector<Entity> entitiesToDelete;
	};
}