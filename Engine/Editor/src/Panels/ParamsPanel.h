#pragma once

#include "Panel.h"
#include "../EditorCamera.h"

namespace Illuminar {
	struct ParamsPanel : Panel
	{
		ParamsPanel(EditorState& editorState, EditorCamera& editorCamera)
			: Panel(editorState), editorCamera(editorCamera)
		{}

		void DrawGUI() override
		{
			Widget::ScopedWindow window("Params");

			ImGui::Text("Camera pos %0.2f, %0.2f, %0.2f", editorCamera.position.x, editorCamera.position.y, editorCamera.position.z);
		}
		
		EditorCamera& editorCamera;
	};
}