#pragma once

#include "Panel.h"
#include "EditorState.h"

namespace Illuminar {
	struct MainBarPanel : Panel
	{
		MainBarPanel(EditorState& editorState)
			: Panel(editorState) {}
		~MainBarPanel() override = default;

		void DrawGUI() override;
	
	private:
		template<typename Component, typename ... Args>
		inline void AddComponentAsMenu(const std::string& buttonName, Args&& ... args);
	
		void DrawFileMenu();
		void DrawComponentMenu();
	};
	
	template<typename Component, typename ...Args>
	inline void MainBarPanel::AddComponentAsMenu(const std::string& buttonName, Args&& ...args)
	{
		const auto& selectedEntity = editorState.selectedEntity;
		if (ImGui::MenuItem(buttonName.c_str())) {
			if (selectedEntity.handle) {
				if (!selectedEntity.getComponent<Component>()) {
					selectedEntity.AddComponent<Component>(std::forward<Args>(args)...);
				}
			}
		}
	}
}