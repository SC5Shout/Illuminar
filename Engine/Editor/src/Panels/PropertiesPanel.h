#pragma once

#include "Panel.h"
#include "EditorState.h"

namespace Illuminar {
	struct PropertiesPanel : Panel
	{
		PropertiesPanel(EditorState& editorState)
			: Panel(editorState) {}
		~PropertiesPanel() override = default;

		void DrawGUI() override;
	
	private:
		template<typename Component, typename ... Args>
		inline void AddComponent(const std::string& buttonName, Args&& ... args);
	
		template<typename Component, typename FuncT>
		inline void DrawComponent(const std::string& name, Entity entity, FuncT&& func);
	
		void DrawEntityComponents(Entity entity);
	
		void DrawTagComponent(Entity entity);
		void DrawTransformComponent(Entity entity);
		void DrawCameraComponent(Entity entity);
		void DrawMeshComponent(Entity entity);
		void DrawSubmeshComponent(Entity entity);
		void DrawLightComponent(Entity entity);
		void DrawDirectionalLightComponent(LightComponent& light);
		void DrawPointLightComponent(LightComponent& light);
		void DrawSpotLightComponent(LightComponent& light);
		void DrawSpriteComponent(Entity entity);
		void DrawNativeScriptComponent(Entity entity);
		void DrawSkylightComponent(Entity entity);
		void DrawRigidBody2DComponent(Entity entity);
		void DrawBoxCollider2DComponent(Entity entity);
	};
	
	template<typename Component, typename ...Args>
	inline void PropertiesPanel::AddComponent(const std::string& buttonName, Args&& ...args)
	{
		const auto& selectedEntity = editorState.selectedEntity;
		if (!selectedEntity.getComponent<Component>()) {
			if (ImGui::Button(buttonName.c_str())) {
				selectedEntity.AddComponent<Component>(std::forward<Args>(args)...);
				ImGui::CloseCurrentPopup();
			}
		}
	}
	
	template<typename Component, typename FuncT>
	inline void PropertiesPanel::DrawComponent(const std::string& name, Entity entity, FuncT&& func)
	{
		const ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_AllowItemOverlap;
		if (auto componentPtr = entity.getComponent<Component>()) {
			ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();
	
			float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			bool open = ImGui::TreeNodeEx((void*)Component::ID, treeNodeFlags, name.c_str());
	
			bool removeComponent = false;
			if (ImGui::BeginPopupContextItem()) {
				if (ImGui::MenuItem("Remove", nullptr, nullptr, Component::ID != TransformComponent::ID)) {
					removeComponent = true;
				}
				ImGui::EndPopup();
			}
	
			ImGui::SameLine(contentRegionAvailable.x - lineHeight * 0.5f);
			//if(ImGui::Button("+", ImVec2{ lineHeight, lineHeight })) {
			if (ImGui::SmallButton("+")) {
				ImGui::OpenPopup("Component settings");
			}
	
			if (ImGui::BeginPopup("Component settings")) {
				if (ImGui::MenuItem("Remove", nullptr, nullptr, Component::ID != TransformComponent::ID)) {
					removeComponent = true;
				}
	
				ImGui::EndPopup();
			}
	
			if (open) {
				func(componentPtr);
				ImGui::TreePop();
			}
	
			if (removeComponent && (Component::ID != TransformComponent::ID)) {
				entity.RemoveComponent<Component>();
			}
	
			ImGui::Separator();
		}
	}
}