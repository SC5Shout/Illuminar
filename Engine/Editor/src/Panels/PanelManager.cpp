#include "Illuminar_pch.h"
#include "PanelManager.h"

namespace Illuminar {
	void PanelManager::OnEvent(SDL_Event& event)
	{
		for (auto& [name, panel] : panels) {
			panel->OnEvent(event);
		}
	}

	void PanelManager::Update(Timestep deltaTime)
	{
		for (auto& [name, panel] : panels) {
			panel->Update(deltaTime);
		}
	}

	void PanelManager::Draw()
	{
		for (auto& [name, panel] : panels) {
			panel->Draw();
		}
	}

	void PanelManager::DrawGUI()
	{
		for (auto& [name, panel] : panels) {
			panel->DrawGUI();
		}
	}
}