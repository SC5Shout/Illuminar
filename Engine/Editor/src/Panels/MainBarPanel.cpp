#include "Illuminar_pch.h"
#include "MainBarPanel.h"

#include "GameEngine/Serializer/Illuminar_Serializer.h"

namespace Illuminar {
	void MainBarPanel::DrawGUI()
	{
		if (ImGui::BeginMenuBar()) {
			DrawFileMenu();
			DrawComponentMenu();
	
			ImGui::EndMenuBar();
		}
	}
	
	void MainBarPanel::DrawFileMenu()
	{
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("New Scene", "Ctrl-N")) {
			}
	
			if (ImGui::MenuItem("Open Scene...", "Ctrl+O")) {
				//SceneSerializer serializer(*scene);
				//serializer.Deserialize("scene.yaml");
			}
	
			ImGui::Separator();
	
			if (ImGui::MenuItem("Save Scene", "Ctrl+S")) {
				//SceneSerializer serializer(*scene);
				//serializer.Serialize("scene.yaml");
			}
	
			if (ImGui::MenuItem("Save Scene As...", "Ctrl+Shift+S")) {
			}
	
			ImGui::Separator();
	
			if (ImGui::MenuItem("Exit")) {
			}
			ImGui::EndMenu();
		}
	}
	
	void MainBarPanel::DrawComponentMenu()
	{
		if (ImGui::BeginMenu("Component")) {
			AddComponentAsMenu<TagComponent>("Tag", "Empty Entity");
			AddComponentAsMenu<TransformComponent>("Transform", Mat4(1.0f));
			AddComponentAsMenu<CameraComponent>("Camera");
			AddComponentAsMenu<MeshComponent>("Mesh");
			AddComponentAsMenu<NativeScriptComponent>("Native script");
			AddComponentAsMenu<SpriteComponent>("Sprite");
			AddComponentAsMenu<SkylightComponent>("Sky");
			ImGui::EndMenu();
		}
	}
}