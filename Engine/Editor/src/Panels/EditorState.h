#pragma once

#include "GameEngine/Scene/Illuminar_Scene.h"
#include "GameEngine/Scene/Illuminar_Entity.h"
#include "../EditorCamera.h"

namespace Illuminar {
	enum struct SceneState
	{
		Edit = 0, Play = 1
	};

	//state of every editor properties
	struct EditorState
	{
		EditorCamera camera;
		Entity selectedEntity;
		Ref<Scene> currentScene = nullptr;
		SceneState sceneState = SceneState::Edit;
	};
}