#pragma once

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"
#include "GraphicsEngine/Illuminar_Texture.h"

#include "SceneHierarchyPanel.h"
#include "EditorState.h"

#include "External/ImGuizmo.h"

namespace Illuminar {
	struct ScenePanel : Panel
	{
		ScenePanel(EditorState& editorState, const std::function<void()>& onScenePlay, const std::function<void()>& onSceneStop, bool& sceneWindowFocused)
			: Panel(editorState), onScenePlay(onScenePlay), onSceneStop(onSceneStop), sceneWindowFocused(sceneWindowFocused)
		{
			//playSceneID = ResourceManagers::get().textureResourceManager.LoadTextureResourceByAssetID("folder_hierarchy.png", false, "png");
			//pauseSceneID = ResourceManagers::get().textureResourceManager.LoadTextureResourceByAssetID("pause_scene.png", false, "png");
		}

		~ScenePanel() override = default;

		void DrawGUI() override;
		void Update([[maybe_unused]] Timestep deltaTime) override;

	private:
		void DrawMenuBar();
		void RegisterAssetDragTarget();
		void DrawGuizmo();	
		[[nodiscard]] float getSnapValue() const;

		void DrawLoadingAssetPanel();

		const std::function<void()>& onScenePlay;
		const std::function<void()>& onSceneStop;

		TextureResourceID playSceneID = INVALID_RESOURCE_ID;
		TextureResourceID pauseSceneID = INVALID_RESOURCE_ID;

		bool& sceneWindowFocused;
		int8_t guizmoType = -1;

		struct AssetDragData
		{
			ResourceID id = INVALID_RESOURCE_ID;
		} assetDragData;

		bool showLoadingAssetPanel = false;
	};
}