#include "Illuminar_pch.h"
#include "SceneHierarchyPanel.h"

namespace Illuminar {
	SceneHierarchyPanel::SceneHierarchyPanel(EditorState& editorState)
		: Panel(editorState)
	{
		entitiesToDelete.reserve(128);
	}
	
	void SceneHierarchyPanel::DrawGUI()
	{
		auto& scene = editorState.currentScene;

		if (!scene) {
			return;
		}

		Widget::ScopedWindow sceneHierarchy("Scene Hierarchy"); {
			if(ImGui::BeginPopupContextWindow(0, 1, false)) {
				if(ImGui::MenuItem("Create Empty Entity")) {
					scene->CreateEntity("Empty Entity");
				}
		
				if (ImGui::BeginMenu("2D")) {
					if (ImGui::MenuItem("Sprite 2D")) {
						auto entity = scene->CreateEntity("Sprite");
						entity.AddComponent<SpriteComponent>();
					}
		
					if (ImGui::MenuItem("Physics Sprite 2D")) {
						auto entity = scene->CreateEntity("Sprite");
						entity.AddComponent<SpriteComponent>();
						entity.AddComponent<RigidBody2DComponent>();
						entity.AddComponent<BoxCollider2DComponent>();
					}
		
					ImGui::EndMenu();
				}
		
				if(ImGui::BeginMenu("Light")) {
					if(ImGui::MenuItem("Directional light")) {
						scene->CreateLightEntity("Directional light", LightType::Directional);
					}
		
					if(ImGui::MenuItem("Point light")) {
						scene->CreateLightEntity("Point light", LightType::Point);
					}
		
					if(ImGui::MenuItem("Spot light")) {
						scene->CreateLightEntity("Spot light", LightType::Spot);
					}
		
					ImGui::EndMenu();
				}
		
				if(ImGui::BeginMenu("Camera")) {
					if(ImGui::MenuItem("Perspective")) {
						scene->CreateCameraEntity("Camera", SceneCamera::ProjectionType::Perspective);
					}
		
					if(ImGui::MenuItem("Orthographic")) {
						scene->CreateCameraEntity("Camera", SceneCamera::ProjectionType::Orthographic);
					}
		
					ImGui::EndMenu();
				}
		
				//if (ImGui::MenuItem("Sky")) {
				//	auto entity = scene->CreateEntity("Sky");
				//	auto comp = entity.AddComponent<SkylightComponent>();
				//	comp->environment = Environment::CreatePreethamSky(comp->turbidityAzimuthInclination.x, comp->turbidityAzimuthInclination.y, comp->turbidityAzimuthInclination.z);
				//	comp->dynamicSky = true;
				//}
		
				ImGui::EndPopup();
			}
		
			scene->ecs.each([&](auto entityHandle) {
				Entity entity(entityHandle, scene.get());
				if(!entity.getParentID()) {
					DrawEntityNode(entity);
				}
			});
		
			ImRect windowRect = { ImGui::GetWindowContentRegionMin(), ImGui::GetWindowContentRegionMax() };
			if(ImGui::BeginDragDropTargetCustom(windowRect, ImGui::GetCurrentWindow()->ID)) {
				const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("entityHierarchy", ImGuiDragDropFlags_AcceptNoDrawDefaultRect);
		
				if(payload) {
					uint64_t droppedHandle = *((uint64_t*)payload->Data);
					Entity e = scene->FindEntityByID(droppedHandle);
					scene->UnparentEntity(e);
				}
		
				ImGui::EndDragDropTarget();
			}
		}
		
		if(!entitiesToDelete.empty()) {
			for(auto&& entity : entitiesToDelete) {
				scene->DestroyEntity(entity);
			} entitiesToDelete.resize(0);
		}
	}
	
	void SceneHierarchyPanel::DrawEntityNode(Entity entity)
	{
		auto& selectedEntity = editorState.selectedEntity;
		auto& scene = editorState.currentScene;

		const char* name = "Unnamed Entity";
		if(const auto tagComponent = entity.getComponent<TagComponent>()) {
			name = tagComponent->tag.c_str();
		}
	
		ImGuiTreeNodeFlags flags = ((selectedEntity == entity) ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth;
		if(entity.getChildrenID().empty()) {
			flags |= ImGuiTreeNodeFlags_Leaf;
		}
	
		bool opened = ImGui::TreeNodeEx(entity, flags, name);
		if(ImGui::IsItemClicked()) {
			selectedEntity = entity;
		}
	
		bool entityDeleted = false;
		if(ImGui::BeginPopupContextItem()) {
			if(ImGui::MenuItem("Delete")) {
				entityDeleted = true;
			}
			ImGui::EndPopup();
		}
	
		if(Input::KeyDown(Keyboard::DELETE)) {
			if(entity == selectedEntity) {
				entityDeleted = true;
			}
		}
	
		if(ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
			uint64_t entityID = entity.getComponent<IDComponent>()->id;
			ImGui::Text(entity.getComponent<TagComponent>()->tag.c_str());
			ImGui::SetDragDropPayload("entityHierarchy", &entityID, sizeof(entityID));
			ImGui::EndDragDropSource();
		}
	
		if(ImGui::BeginDragDropTarget()) {
			const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("entityHierarchy", ImGuiDragDropFlags_AcceptNoDrawDefaultRect);
	
			if(payload) {
				uint64_t droppedHandle = *((uint64_t*)payload->Data);
				Entity e = scene->FindEntityByID(droppedHandle);
				scene->ParentEntity(e, entity);
			}
	
			ImGui::EndDragDropTarget();
		}
	
		if(opened) {
			for(auto child : entity.getChildrenID()) {
				Entity e = scene->FindEntityByID(child);
				if(e) {
					DrawEntityNode(e);
				}
			}
	
			ImGui::TreePop();
		}
	
		if(entityDeleted) {
			entitiesToDelete.push_back(entity);
			if (entity == selectedEntity) {
				selectedEntity = {};
			}
		}
	}
}