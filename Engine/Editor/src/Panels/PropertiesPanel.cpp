#include "Illuminar_pch.h"
#include "PropertiesPanel.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"

namespace Illuminar {
	void PropertiesPanel::DrawGUI()
	{
		const auto& selectedEntity = editorState.selectedEntity;
	
		Widget::ScopedWindow propertiesWindow("Properties", nullptr, ImGuiWindowFlags_NoResize);
	
		if (selectedEntity) {
			DrawEntityComponents(selectedEntity);
	
			if (ImGui::Button("Add Component", { -1.0f, 0.0f })) {
				ImGui::OpenPopup("AddComponentPanel");
			}
	
			if (ImGui::BeginPopup("AddComponentPanel")) {
				AddComponent<TagComponent>("Tag", "Empty Entity");
				AddComponent<TransformComponent>("Transform", Mat4(1.0f));
				AddComponent<CameraComponent>("Camera");
				AddComponent<MeshComponent>("Mesh");
				AddComponent<NativeScriptComponent>("Native script");
				AddComponent<SpriteComponent>("Sprite");
				AddComponent<SkylightComponent>("Sky Light");
				AddComponent<RigidBody2DComponent>("Rigid body 2D");
				AddComponent<BoxCollider2DComponent>("Box collider 2D");
				ImGui::EndPopup();
			}
		}
		ImGui::Separator();
	}
	
	void PropertiesPanel::DrawEntityComponents(Entity entity)
	{
		DrawTagComponent(entity);
		DrawTransformComponent(entity);
		DrawCameraComponent(entity);
		DrawMeshComponent(entity);
		DrawSubmeshComponent(entity);
		DrawLightComponent(entity);
		DrawSpriteComponent(entity);
		DrawNativeScriptComponent(entity);
		DrawSkylightComponent(entity);
		DrawRigidBody2DComponent(entity);
		DrawBoxCollider2DComponent(entity);
	}
	
	void PropertiesPanel::DrawTagComponent(Entity entity)
	{
		DrawComponent<TagComponent>("tag", entity, [](TagComponent* tagPtr) {
			auto& tag = tagPtr->tag;
			char buffer[256];
			memset(buffer, 0, 256);
			memcpy(buffer, tag.c_str(), tag.length());
			if (ImGui::InputText("##Tag", buffer, 256)) {
				tag = std::string(buffer);
			}
		});
	
		DrawComponent<IDComponent>("UUID", entity, [](IDComponent* idPtr) {
			ImGui::Text("UUID %llu", (uint64_t)idPtr->id);
		});
	}
	
	void PropertiesPanel::DrawTransformComponent(Entity entity)
	{
		DrawComponent<TransformComponent>("Transform", entity, [](TransformComponent* transformPtr) {
			auto& translation = transformPtr->translation;
			auto& rotation = transformPtr->rotation;
			auto& scale = transformPtr->scale;
	
			Widget::ScopedTable transformTable("Transform", 4);
			
			Widget::Vector3DragProperty("Translation", translation);
			
			Vector3<float> rotationInDegrees = Degrees(rotation);
			Widget::Vector3DragProperty("Rotation", rotationInDegrees);
			rotation = Radians(rotationInDegrees);
			
			Widget::Vector3DragProperty("Scale", scale);
		});
	}
	
	void PropertiesPanel::DrawCameraComponent(Entity entity)
	{
		DrawComponent<CameraComponent>("Camera", entity, [](CameraComponent* cameraPtr) {
			auto& camera = cameraPtr->camera;
			constexpr std::array<const char*, 2> projectionTypes = { "Perspective" , "Orthographic" };
			const char* currentType = projectionTypes[(int)camera.projectionType];
			if (ImGui::BeginCombo("Projection", currentType)) {
				for (size_t type = 0; type < projectionTypes.size(); type++) {
					bool isSelected = (currentType == projectionTypes[type]);
					if (ImGui::Selectable(projectionTypes[type], isSelected)) {
						currentType = projectionTypes[type];
						camera.projectionType = ((SceneCamera::ProjectionType)type);
					}
					if (isSelected) {
						ImGui::SetItemDefaultFocus();
					}
				}
	
				ImGui::EndCombo();
			}
	
			Widget::ScopedTable cameraTable("Camera", 2);
			Widget::ScopedStyle style(ImGuiStyleVar_ItemSpacing, ImVec2{ 2, 2 });
	
			if (camera.projectionType == SceneCamera::ProjectionType::Perspective) {
				Widget::DragProperty("Field of View", camera.perspectiveFov);
				Widget::DragProperty("Near plane", camera.perspectiveNear);
				Widget::DragProperty("Far plane", camera.perspectiveFar);
			} else {
				Widget::DragProperty("Size", camera.orthographicSize);
				Widget::DragProperty("Near plane", camera.orthographicNear);
				Widget::DragProperty("Far plane", camera.orthographicFar);
			}
		});
	}
	
	void MaterialEditorProperties(Ref<GFX::Material>& materialAsset)
	{
		auto& col = materialAsset->get<Vector4<float>>(GFX::MaterialTypes::baseColor);

		ImGuiColorEditFlags misc_flags = ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_AlphaPreview | ImGuiColorEditFlags_NoOptions;
		if (ImGui::ColorEdit4("baseColor", (float*)&col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel | misc_flags)) {
			materialAsset->set(GFX::MaterialTypes::baseColor, Vector4<float>(col[0], col[1], col[2], col[3]));
		}

		{
			Widget::ScopedTable cameraTable("MaterialTable", 2);
			Widget::ScopedStyle style(ImGuiStyleVar_ItemSpacing, ImVec2{ 2, 2 });

			static constexpr std::array<const char*, 2> domainTypes = { "Surface" , "PostProcess" };
			if (Widget::DropDownProperty(domainTypes, materialAsset->materialDomain, "Material domain")) {
				//TODO: recreate material
				//	should material id stay the same?
			}

			static constexpr std::array<const char*, 7> blendTypes = { "Opaque", "Transparent", "Additive", "Masked", "Fade", "Multiply", "Screen" };
			if (Widget::DropDownProperty(blendTypes, materialAsset->blendMode, "Blend mode")) {
				//TODO: recreate material
				//	should material id stay the same?
			}

			static constexpr std::array<const char*, 4> shadingModel = { "Unlit", "Lit", "Subsurface", "Cloth" };
			if (Widget::DropDownProperty(shadingModel, materialAsset->shadingModel, "Shading mode")) {
				//TODO: recreate material
				//	should material id stay the same?
			}
		}

		ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding;
		if (ImGui::TreeNodeEx("Advanced", treeNodeFlags)) {
			{
				Widget::ScopedTable cameraTable("MaterialTable_advanced", 2);
				Widget::ScopedStyle style(ImGuiStyleVar_ItemSpacing, ImVec2{ 2, 2 });

				//TODO: proper cast shadow impl
				static bool castsShadow = true;
				Widget::Property("Casts shadow", castsShadow);

				//should only back cull mode be available?
				auto& cullMode = materialAsset->rasterizerState.cullMode;
				bool twoSided = cullMode == GFX::CullMode::Back ? false : true;
				if (Widget::Property("Two sided", twoSided)) {
					if (cullMode == GFX::CullMode::None) {
						cullMode = GFX::CullMode::Back;
					} else cullMode = GFX::CullMode::None;
				}

				auto& wireframe = materialAsset->rasterizerState.fillMode;
				bool isWireframe = wireframe == GFX::FillMode::Solid ? false : true;
				if (Widget::Property("Wireframe", isWireframe)) {
					if (wireframe == GFX::FillMode::Solid) {
						wireframe = GFX::FillMode::Wireframe;
					} else wireframe = GFX::FillMode::Solid;
				}
			}

			ImGui::TreePop();
		}
	}

	void PropertiesPanel::DrawMeshComponent(Entity entity)
	{
		DrawComponent<MeshComponent>("Mesh", entity, [this, entity](MeshComponent* meshPtr) {
			auto& scene = editorState.currentScene;

			auto& meshComponent = *meshPtr;
			auto& meshManager = ResourceManagers::get().meshResourceManager;
			MeshResource* meshResource = meshManager.tryGetResourceByID(meshComponent.meshID);
	
			ImGui::Button(meshComponent.name.c_str());
	
			if (ImGui::BeginDragDropTarget()) {
				auto data = ImGui::AcceptDragDropPayload("asset_payload");
				if (data) {
					uint64_t count = data->DataSize / sizeof(AssetID);
	
					for (uint64_t i = 0; i < count; i++) {
						AssetID assetID = *(((AssetID*)data->Data) + i);
						if (assetID != INVALID_ASSET_ID) {
							Asset asset = AssetManager::getAssetByAssetID(assetID);
	
							auto path = std::filesystem::path(asset.virtualFilename);
							if (path.extension() == ".fbx") {
								meshResource = meshManager.getResourceByAssetID(assetID);

								auto& mesh = meshResource->mesh;
								meshComponent.meshID = meshResource->id;
								meshComponent.name = mesh->name;
	
								if (mesh->submeshes.size() > 1) {
									uint32_t index = 0;
									for (const auto& submesh : mesh->submeshes) {
										Entity meshEntity = scene->CreateEntity(submesh.name, entity);
										auto submeshComponent = meshEntity.AddComponent<SubmeshComponent>();
										//submeshComponent->submesh = submesh;
										submeshComponent->submeshIndex = index++;
										submeshComponent->meshID = meshResource->id;
									}
								}
	
								//SelectEntity(entity);
							}
						}
					}
				}
	
				ImGui::EndDragDropTarget();
			}
	
			if (meshResource && meshResource->Loaded()) {
				ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding | ImGuiTreeNodeFlags_DefaultOpen;
				if (ImGui::TreeNodeEx("Materials", treeNodeFlags)) {
					auto& mesh = meshResource->mesh;
					auto& meshMaterialTable = mesh->materials;
					if (meshComponent.materialList->materials.size() < meshMaterialTable->materials.size()) {
						meshComponent.materialList->materials.resize(meshMaterialTable->materials.size());
					}
	
					for (size_t i = 0; i < meshComponent.materialList->materials.size(); i++) {
						if (i == meshMaterialTable->materials.size()) {
							ImGui::Separator();
						}
	
						auto& meshMaterialTable = mesh->materials;

						bool hasLocalMaterial = meshComponent.materialList->hasMaterial(i);
						bool hasMeshMaterial = meshMaterialTable->hasMaterial(i);

						Ref<GFX::Material> meshMaterialAsset;
						if (hasMeshMaterial) {
							meshMaterialAsset = meshMaterialTable->getMaterial(i);
						}

						auto materialAsset = hasLocalMaterial ? meshComponent.materialList->getMaterial(i) : meshMaterialAsset;

						std::string label = fmt::format("[Material {0}] - {1}", i, materialAsset->name.c_str());
	
						ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding;
						if (ImGui::TreeNodeEx(label.c_str(), treeNodeFlags)) {
							MaterialEditorProperties(materialAsset);

							ImGui::TreePop();
						}
					}
	
					ImGui::TreePop();
				}
			}
		});
	}
	
	void PropertiesPanel::DrawSubmeshComponent(Entity entity)
	{
		DrawComponent<SubmeshComponent>("Submesh", entity, [this](SubmeshComponent* submeshPtr) {
			auto& submeshComponent = *submeshPtr;
	
			auto& meshManager = ResourceManagers::get().meshResourceManager;
			MeshResource* meshResource = meshManager.tryGetResourceByID(submeshComponent.meshID);
			if (meshResource && meshResource->Loaded()) {
				ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding | ImGuiTreeNodeFlags_DefaultOpen;
				if (ImGui::TreeNodeEx("Materials", treeNodeFlags)) {
					auto& materialAsset = submeshComponent.submesh.material;
					
					MaterialEditorProperties(materialAsset);

					ImGui::TreePop();
				}
			}
		});
	}
	
	void PropertiesPanel::DrawLightComponent(Entity entity)
	{
		DrawComponent<LightComponent>("Light", entity, [this](LightComponent* lightPtr) {
			auto& light = *lightPtr;
	
			constexpr std::array<const char*, 3> lightTypes = { "Directional" , "Point", "Spot" };
			const char* currentType = lightTypes[(int)light.type];
			if (ImGui::BeginCombo("Type", currentType)) {
				for (size_t type = 0; type < lightTypes.size(); type++) {
					bool isSelected = (currentType == lightTypes[type]);
					if (ImGui::Selectable(lightTypes[type], isSelected)) {
						currentType = lightTypes[type];
						light.type = (LightType)type;
					}
					if (isSelected) {
						ImGui::SetItemDefaultFocus();
					}
				}
	
				ImGui::EndCombo();
			}
	
			{
				Widget::ScopedTable radiancetable("Radiance", 4);
				Widget::Vector3DragProperty("Radiance", light.radiance);
			}
	
			{
				Widget::ScopedTable radiancetable("Intensity", 2);
				Widget::DragProperty("Intensity", light.intensity);
			}
	
			switch (light.type) {
				case LightType::Directional: DrawDirectionalLightComponent(light); break;
				case LightType::Point: DrawPointLightComponent(light); break;
				case LightType::Spot: DrawSpotLightComponent(light); break;
			}
		});
	}
	
	void PropertiesPanel::DrawDirectionalLightComponent(LightComponent& light)
	{
		Widget::ScopedTable uvRadius("dirlight", 2);
		Widget::DragProperty("uvRadius", light.lightSize);
		Widget::Property("Soft shadows", light.softShadows);
		Widget::Property("Nvidia pcss shadows", light.nvidiaPcss);
	}
	
	void PropertiesPanel::DrawPointLightComponent(LightComponent& light)
	{
		Widget::ScopedTable pointLight("##PointLight", 2);
		Widget::DragProperty("Radius", light.radius);
		Widget::DragProperty("Fall off", light.fallOff);
	}
	
	void PropertiesPanel::DrawSpotLightComponent(LightComponent& light)
	{
		Widget::ScopedTable spotLight("##SpotLight", 2);
		Widget::DragProperty("Radius", light.radius);
		Widget::DragProperty("Fall off", light.fallOff);
		Widget::DragProperty("Inner cut off", light.innerCutOff);
		Widget::DragProperty("Outer cut off", light.outerCutOff);
	}
	
	void PropertiesPanel::DrawSpriteComponent(Entity entity)
	{
		DrawComponent<SpriteComponent>("Sprite", entity, [](SpriteComponent* spriteComponentPtr) {
	
		});
	}
	
	void PropertiesPanel::DrawNativeScriptComponent(Entity entity)
	{
		DrawComponent<NativeScriptComponent>("Native script", entity, [](NativeScriptComponent* scriptComponentPtr) {
			auto currentName = nativeScriptTypes[(size_t)scriptComponentPtr->scriptType].first;
			//if(scriptTypes.size() > 1) {
			if (ImGui::BeginCombo("Native scripts", currentName)) {
				for (const auto [name, type] : nativeScriptTypes) {
	
					if (ImGui::Selectable(name)) {
						BindNativeScript(type, *scriptComponentPtr);
						scriptComponentPtr->scriptType = type;
					}
				}
	
				ImGui::EndCombo();
			}
			//} else {
	
			//}
		});
	}
	
	void PropertiesPanel::DrawSkylightComponent(Entity entity)
	{
		DrawComponent<SkylightComponent>("Sky Light", entity, [](SkylightComponent* skylightComponentPtr) {
			Widget::ScopedTable table("##SkyLight", 2);
	
			bool modified = Widget::Property("Dynamic sky", skylightComponentPtr->dynamicSky);
			modified |= Widget::DragProperty("Intensity", skylightComponentPtr->intensity);
	
			auto& env = skylightComponentPtr->environment;
			
			if (skylightComponentPtr->dynamicSky) {
				bool dirty = !env;
			
				auto& turbidityAzimuthInclination = skylightComponentPtr->turbidityAzimuthInclination;
				modified |= Widget::DragProperty("Turbidity", turbidityAzimuthInclination.x);
				modified |= Widget::DragProperty("Azimuth", turbidityAzimuthInclination.y);
				modified |= Widget::DragProperty("Inclination", turbidityAzimuthInclination.z);
			
				if (dirty || modified) {
					env = Environment::CreatePreethamSky(turbidityAzimuthInclination.x, turbidityAzimuthInclination.y, turbidityAzimuthInclination.z);
					dirty = false;
					modified = false;
				}
			} else {
				if (modified) {
					env = Environment::CreateFromHDRTexture();
					modified = false;
				}
			}
		});
	}
	
	void PropertiesPanel::DrawRigidBody2DComponent(Entity entity)
	{
		DrawComponent<RigidBody2DComponent>("Rigid body 2D", entity, [](RigidBody2DComponent* componentPtr) {
			auto& rigidBody = *componentPtr;
		
			constexpr std::array<const char*, 3> bodyTypes = { "Static" , "Dynamic", "Kinematic	" };
			const char* currentType = bodyTypes[(int)componentPtr->bodyType];
			if (ImGui::BeginCombo("Type", currentType)) {
				for (size_t type = 0; type < bodyTypes.size(); type++) {
					bool isSelected = (currentType == bodyTypes[type]);
					if (ImGui::Selectable(bodyTypes[type], isSelected)) {
						currentType = bodyTypes[type];
						rigidBody.bodyType = (RigidBodyType)type;
					}
					if (isSelected) {
						ImGui::SetItemDefaultFocus();
					}
				}
		
				ImGui::EndCombo();
			}
		
			if (rigidBody.bodyType == RigidBodyType::Dynamic) {
				Widget::ScopedTable table("##FixedRotationTable", 2);
				Widget::Property("Fixed Rotation", rigidBody.fixedRotation);
			}
		});
	}
	
	void PropertiesPanel::DrawBoxCollider2DComponent(Entity entity)
	{
		DrawComponent<BoxCollider2DComponent>("Box collider 2D", entity, [](BoxCollider2DComponent* componentPtr) {
			{
				Widget::ScopedTable table("##OffsetSize", 3);
				Widget::Vector2DragProperty("Offset", componentPtr->offset);
				Widget::Vector2DragProperty("Size", componentPtr->size);
			}
	
			Widget::ScopedTable boxCollider("##DensityFriction", 2);
			Widget::DragProperty("Density", componentPtr->density);
			Widget::DragProperty("Friction", componentPtr->friction);
			Widget::DragProperty("Restitution", componentPtr->restitution);
			Widget::DragProperty("Restitution threshold", componentPtr->restitutionThreshold);
		});
	}
}