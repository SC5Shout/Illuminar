#pragma once

#include "Panel.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"
#include "GameEngine/Project/Illuminar_Project.h"

#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"

namespace Illuminar {
	struct LoadingPanel : Panel
	{
		LoadingPanel(EditorState& editorState, const std::shared_ptr<Project>& project);
		~LoadingPanel() override = default;

		void DrawGUI();
	
		bool showpopup = true;
	
		AssetPackage* package = nullptr;
		TextureResourceManager& textureManager;
	};
}