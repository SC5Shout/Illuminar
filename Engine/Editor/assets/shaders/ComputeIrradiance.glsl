#type compute
#version 450 core
const float PI = 3.141592;
const float EPSILON = 0.00001;

layout(binding = 0) uniform samplerCube u_radianceMap;
layout(binding = 0, rgba32f) restrict writeonly uniform imageCube o_irradianceMap;

vec3 getCubeMapTexCoord()
{
    vec2 st = gl_GlobalInvocationID.xy / vec2(imageSize(o_irradianceMap));
    vec2 uv = 2.0 * vec2(st.x, 1.0 - st.y) - vec2(1.0);

    vec3 ret;
    if (gl_GlobalInvocationID.z == 0)      ret = vec3(  1.0, uv.y, -uv.x);
    else if (gl_GlobalInvocationID.z == 1) ret = vec3( -1.0, uv.y,  uv.x);
    else if (gl_GlobalInvocationID.z == 2) ret = vec3( uv.x,  1.0, -uv.y);
    else if (gl_GlobalInvocationID.z == 3) ret = vec3( uv.x, -1.0,  uv.y);
    else if (gl_GlobalInvocationID.z == 4) ret = vec3( uv.x, uv.y,   1.0);
    else if (gl_GlobalInvocationID.z == 5) ret = vec3(-uv.x, uv.y,  -1.0);
    return normalize(ret);
}

//computes tangent and bitangent
void ComputeBasisVectors(const vec3 N, out vec3 U, out vec3 V)
{
    V = cross(N, vec3(0.0, 1.0, 0.0));
	V = mix(cross(N, vec3(1.0, 0.0, 0.0)), V, step(EPSILON, dot(V, V)));

	V = normalize(V);
	U = normalize(cross(N, V));
}

//converts point from tangent space to the world space
vec3 TangentToWorld(const vec3 point, const vec3 N, const vec3 U, const vec3 V)
{
    return U * point.x + V * point.y + N * point.z;
}

layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;
void main()
{
    vec3 N = getCubeMapTexCoord();

    vec3 U, V;
    ComputeBasisVectors(N, U, V);
  
    vec3 irradiance = vec3(0.0);  

    float sampleDelta = 0.025;
    float nrSamples = 0.0; 
    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta) {
        for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta) {
            vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
			vec3 sampleVec = TangentToWorld(tangentSample, N, U, V);
            irradiance += texture(u_radianceMap, sampleVec).rgb * cos(theta) * sin(theta);
            nrSamples++;
        }
    }
    irradiance = PI * irradiance * (1.0 / float(nrSamples));

    imageStore(o_irradianceMap, ivec3(gl_GlobalInvocationID), vec4(irradiance, 1.0));
//    vec3 U, V;
//    ComputeBasisVectors(N, U, V);
//    uint samples = 64;
//    samples = 64 * 1024;
//
//    vec3 irradiance = vec3(0.0f);
//    for(uint i = 0; i < samples; ++i) {
//        vec2 U = 
//    }
//
//
//    float phi = atan(N.z, N.x);
//    float theta = acos(N.y);
//    vec2 uv = vec2(phi / (2.0 * PI) + 0.5, theta / PI);
//
//    vec4 color = texture(u_radianceMap, uv);
//    imageStore(o_irradianceMap, ivec3(gl_GlobalInvocationID), color);
}