#type compute
#version 450 core

layout(binding = 0) uniform sampler2D u_inputTexture;
layout(binding = 0, rgba8) restrict writeonly uniform image2D o_texture;

float near_plane = 0.001f;
float far_plane = 10000.0f;
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0;
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));	
}

layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;
void main()
{
    vec2 st = gl_GlobalInvocationID.xy / vec2(imageSize(o_texture));
    vec2 uv = 2.0 * vec2(st.x, 1.0 - st.y) - vec2(1.0);

    float r = texture(u_inputTexture, vec2(uv)).r;
    float d = LinearizeDepth(r);
    imageStore(o_texture, ivec2(gl_GlobalInvocationID.xy), vec4(d, d, d, 1.0f));
}