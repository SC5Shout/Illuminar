#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec3 a_tangent;
layout(location = 3) in vec3 a_binormal;
layout(location = 4) in vec2 a_uv;

layout(location = 0) out vec3 v_position;
layout(location = 1) out vec3 v_normal;
layout(location = 2) out vec2 v_uv;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
};

layout (std140, binding = 1) uniform Model
{
    mat4 u_model;
};

void main()
{
	vec4 localPosition = vec4(a_position, 1.0f);

	gl_Position = u_viewProjection * u_model * localPosition;

	v_uv = vec2(a_uv.x, 1.0 - a_uv.y);
	v_normal = mat3(transpose(inverse(u_model))) * a_normal;
	v_position = vec3(u_model * localPosition);
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_uv;

layout(push_constant) uniform MaterialInput
{
	vec4 baseColor;
} u_material;

layout(binding = 3) uniform sampler2D u_baseMap;

void main()
{	
	f_color = texture(u_baseMap, v_uv) * u_material.baseColor;
}
