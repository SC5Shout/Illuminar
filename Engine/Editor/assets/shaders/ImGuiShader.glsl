#type vertex
#version 460 core

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec2 a_uv;
layout(location = 2) in vec4 a_color;

layout(location = 0) out vec2 v_uv;
layout(location = 1) out vec4 v_color;

layout(push_constant) uniform Camera
{
    mat4 projection;
} u_camera;

void main()
{
    v_uv = a_uv;
    v_color = a_color;

    gl_Position = u_camera.projection * vec4(a_position, 0.0f, 1.0f);
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec2 v_uv;
layout(location = 1) in vec4 v_color;

layout(binding = 0) uniform sampler2D u_texture;

void main()
{
    f_color = v_color * texture(u_texture, v_uv);
}