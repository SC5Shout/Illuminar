#include "Illuminar_pch.h"
#include "ImGui.h"

//temp line: 60
#include "glad/glad.h"
#include "GraphicsEngine/OpenGL/Illuminar_GLRenderer.h"

namespace Illuminar {
    GUI::GUI(const Ref<Window>& window)
        : window(window)
    {
        using namespace GFX;

        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;     
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
        io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
        //io.ConfigViewportsNoAutoMerge = true;
        //io.ConfigViewportsNoTaskBarIcon = true;
        io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset;
        io.BackendFlags |= ImGuiBackendFlags_RendererHasViewports;
        
        ImFont* pFont = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
        io.FontDefault = io.Fonts->Fonts.back();
        
        ImGui::StyleColorsDark();
        ImGuiStyle& style = ImGui::GetStyle();
        
        style.Colors[ImGuiCol_WindowBg] = ImGui::ColorConvertU32ToFloat4(0xff171717);
        style.Colors[ImGuiCol_FrameBg] = ImGui::ColorConvertU32ToFloat4(0xff3A3939);
        style.Colors[ImGuiCol_FrameBgHovered] = ImGui::ColorConvertU32ToFloat4(0xff4A4949);
        style.Colors[ImGuiCol_FrameBgActive] = ImGui::ColorConvertU32ToFloat4(0xff2A2929);
        
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
            style.WindowRounding = 0.0f;
            style.Colors[ImGuiCol_WindowBg].w = 1.0f;
        }
        
        ImGui_ImplSDL2_InitForOpenGL(window->getHandle(), nullptr);
        if ((io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) && (io.BackendFlags & ImGuiBackendFlags_PlatformHasViewports))
            WindowHelper::InitPlatformInterface(window, nullptr);
        
        //material = Material::Create(ShaderCache::AddOrGetShader("imgui", "assets/shaders/ImGuiShader.glsl"), "gui");
        
        vbo = VertexBuffer::Create({
            {DataType::RG32_FLOAT, "a_position"},
            {DataType::RG32_FLOAT, "a_uv"},
            {DataType::RGBA8_UNORM, "a_color"},
        }, 0, nullptr, Usage::Dynamic_Draw);
        ebo = ElementBuffer::Create(0, nullptr, Usage::Dynamic_Draw);
        
        pipeline = CreatePipeline();

        CreateFontAtlas();
        texture = Texture2D::Create();
    }

    void GUI::SetupRenderState(ImDrawData* drawData)
    {
    //    //temp
    //    bool clip_origin_lower_left = true;
    //#if defined(GL_CLIP_ORIGIN) && !defined(__APPLE__)
    //    GLenum current_clip_origin = 0; glGetIntegerv(GL_CLIP_ORIGIN, (GLint*)&current_clip_origin);
    //    if (current_clip_origin == GL_UPPER_LEFT)
    //        clip_origin_lower_left = false;
    //#endif
    
        float L = drawData->DisplayPos.x;
        float R = drawData->DisplayPos.x + drawData->DisplaySize.x;
        float T = drawData->DisplayPos.y;
        float B = drawData->DisplayPos.y + drawData->DisplaySize.y;
        //if (!clip_origin_lower_left) { float tmp = T; T = B; B = tmp; } // Swap top and bottom if origin is upper left
        
        Mat4 proj = glm::ortho(L, R, B, T);
        //material->set("u_camera.projection", proj);
        //material->Bind();
        GFX::ShaderCache::getShader("ImGuiShader")->setUniform("u_camera.projection", proj);


        pipeline->Bind();
        vbo->Bind();
        ebo->Bind();
    }

    void GUI::RenderDrawData(ImDrawData* drawData)
    {
        const uint32_t frameBufferWidth = (uint32_t)(drawData->DisplaySize.x * drawData->FramebufferScale.x);
        const uint32_t frameBufferHeight = (uint32_t)(drawData->DisplaySize.y * drawData->FramebufferScale.y);
        if (frameBufferWidth <= 0 || frameBufferHeight <= 0)
            return;
        
        /*
        
                GLenum last_active_texture; glGetIntegerv(GL_ACTIVE_TEXTURE, (GLint*)&last_active_texture);
        glActiveTexture(GL_TEXTURE0);
        GLuint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, (GLint*)&last_program);
        GLuint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&last_texture);
#ifdef IMGUI_IMPL_OPENGL_MAY_HAVE_BIND_SAMPLER
        GLuint last_sampler; if (bd->GlVersion >= 330) { glGetIntegerv(GL_SAMPLER_BINDING, (GLint*)&last_sampler); }
        else { last_sampler = 0; }
#endif
        GLuint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&last_array_buffer);
#ifdef IMGUI_IMPL_OPENGL_USE_VERTEX_ARRAY
        GLuint last_vertex_array_object; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, (GLint*)&last_vertex_array_object);
#endif
#ifdef IMGUI_IMPL_HAS_POLYGON_MODE
        GLint last_polygon_mode[2]; glGetIntegerv(GL_POLYGON_MODE, last_polygon_mode);
#endif
        GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
        GLint last_scissor_box[4]; glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box);
        GLenum last_blend_src_rgb; glGetIntegerv(GL_BLEND_SRC_RGB, (GLint*)&last_blend_src_rgb);
        GLenum last_blend_dst_rgb; glGetIntegerv(GL_BLEND_DST_RGB, (GLint*)&last_blend_dst_rgb);
        GLenum last_blend_src_alpha; glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint*)&last_blend_src_alpha);
        GLenum last_blend_dst_alpha; glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint*)&last_blend_dst_alpha);
        GLenum last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint*)&last_blend_equation_rgb);
        GLenum last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint*)&last_blend_equation_alpha);
        GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
        GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
        GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
        GLboolean last_enable_stencil_test = glIsEnabled(GL_STENCIL_TEST);
        GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);
#ifdef IMGUI_IMPL_OPENGL_MAY_HAVE_PRIMITIVE_RESTART
        GLboolean last_enable_primitive_restart = (bd->GlVersion >= 310) ? glIsEnabled(GL_PRIMITIVE_RESTART) : GL_FALSE;
#endif

        */

        SetupRenderState(drawData);
        using namespace GFX;
        RenderCommand::setViewport(0, 0, frameBufferWidth, frameBufferHeight);
        
        const ImVec2 clip_off = drawData->DisplayPos;
        const ImVec2 clip_scale = drawData->FramebufferScale;
        
        for (int n = 0; n < drawData->CmdListsCount; n++) {
            const ImDrawList* cmd_list = drawData->CmdLists[n];
        
            vbo->AddData((size_t)cmd_list->VtxBuffer.Size * (int)sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data);
            ebo->AddData((size_t)cmd_list->IdxBuffer.Size * (int)sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data);
        
            for (const auto& cmd : cmd_list->CmdBuffer) {
                ImVec4 clip_rect;
                clip_rect.x = (cmd.ClipRect.x - clip_off.x) * clip_scale.x;
                clip_rect.y = (cmd.ClipRect.y - clip_off.y) * clip_scale.y;
                clip_rect.z = (cmd.ClipRect.z - clip_off.x) * clip_scale.x;
                clip_rect.w = (cmd.ClipRect.w - clip_off.y) * clip_scale.y;
        
                if (clip_rect.x < frameBufferWidth && clip_rect.y < frameBufferHeight && clip_rect.z >= 0.0f && clip_rect.w >= 0.0f) {
                    if (cmd.TextureId) {
                        texture->setTextureHandle(cmd.TextureId);
                        texture->Bind();
                    } else fontTexture->Bind();
                    
                    RenderCommand::setScissor((int)clip_rect.x, (int)(frameBufferHeight - clip_rect.w), (int)(clip_rect.z - clip_rect.x), (int)(clip_rect.w - clip_rect.y));
                    RenderCommand::DrawElements(cmd.ElemCount, sizeof(ImDrawIdx) == 2 ? ElementDataType::Uint16 : ElementDataType::Uint32, 1, cmd.IdxOffset, cmd.VtxOffset, 0);
                }
            }
        }

        /*
        glUseProgram(last_program);
        glBindTexture(GL_TEXTURE_2D, last_texture);
#ifdef IMGUI_IMPL_OPENGL_MAY_HAVE_BIND_SAMPLER
        if (bd->GlVersion >= 330)
            glBindSampler(0, last_sampler);
#endif
        glActiveTexture(last_active_texture);
#ifdef IMGUI_IMPL_OPENGL_USE_VERTEX_ARRAY
        glBindVertexArray(last_vertex_array_object);
#endif
        glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
        glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
        glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb, last_blend_src_alpha, last_blend_dst_alpha);
        if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
        if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
        if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
        if (last_enable_stencil_test) glEnable(GL_STENCIL_TEST); else glDisable(GL_STENCIL_TEST);
        if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
#ifdef IMGUI_IMPL_OPENGL_MAY_HAVE_PRIMITIVE_RESTART
        if (bd->GlVersion >= 310) { if (last_enable_primitive_restart) glEnable(GL_PRIMITIVE_RESTART); else glDisable(GL_PRIMITIVE_RESTART); }
#endif

#ifdef IMGUI_IMPL_HAS_POLYGON_MODE
        glPolygonMode(GL_FRONT_AND_BACK, (GLenum)last_polygon_mode[0]);
#endif
        glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
        glScissor(last_scissor_box[0], last_scissor_box[1], (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);
        */
    }

    Ref<GFX::GraphicsPipeline> GUI::CreatePipeline()
    {
        using namespace GFX;

        GraphicsPipeline::CreateInfo pipelineInfo;
        pipelineInfo.shader = ShaderCache::getShader("ImGuiShader");
        pipelineInfo.bufferLayout = vbo->getLayout();
        pipelineInfo.blendState.sourceRGB = Blend::Source_Alpha;
        pipelineInfo.blendState.sourceAlpha = Blend::Source_Alpha;
        pipelineInfo.blendState.destinationRGB = Blend::One_Minus_Source_Alpha;
        pipelineInfo.blendState.destinationAlpha = Blend::One_Minus_Source_Alpha;
        pipelineInfo.depthState.mask = DepthMask::Zero;
        pipelineInfo.depthState.func = DepthTest::Always;
        pipelineInfo.rasterizerState.cullMode = CullMode::None;
        pipelineInfo.rasterizerState.scissorEnabled = true;
        pipelineInfo.rasterizerState.fillMode = FillMode::Solid;

        return GraphicsPipeline::Create(pipelineInfo);
    }

    void GUI::RenderPlatformWindowsDefault(void* platform_render_arg, void* renderer_render_arg)
    {
        using namespace GFX;
    
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        for (int i = 1; i < platform_io.Viewports.Size; i++) {
            ImGuiViewport* viewport = platform_io.Viewports[i];
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
    
            if (viewport->Flags & ImGuiViewportFlags_Minimized)
                continue;
    
            data->window->MakeCurrent();

            if (!(viewport->Flags & ImGuiViewportFlags_NoRendererClear)) {
                RenderCommand::setViewport(0, 0, (uint32_t)viewport->Size.x, (uint32_t)viewport->Size.y);
                RenderCommand::setClearColor(0xff000000);
                RenderCommand::Clear(BufferBit::Color);  
            }
    
            RenderDrawData(viewport->DrawData);

            data->window->Present();
        }
    }

    void GUI::CreateFontAtlas()
    {
        using namespace GFX;

        ImGuiIO& io = ImGui::GetIO();
        static unsigned char* pixels;
        int width, height;
        io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
        size_t size = (size_t)(width * height * 4);

        auto params = TextureParameters().Miplevel(1);
        fontTexture = Texture2D::Create({ .name = "ImGuiFont", .w = (uint32_t)width, .h = (uint32_t)height, .pixels = pixels, .parameters = params });
    }
}