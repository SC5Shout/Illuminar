#include "Illuminar_pch.h"
#include "ImGuiSDL.h"

namespace Illuminar {
    struct ImGui_ImplSDL2_Data
    {
        Ref<Window> Window = nullptr;
        Uint64      Time = 0;
        bool        MousePressed[3];
        SDL_Cursor* MouseCursors[ImGuiMouseCursor_COUNT];
        char* ClipboardTextData;
        bool        MouseCanUseGlobalState;

        ImGui_ImplSDL2_Data() { memset(this, 0, sizeof(*this)); }
    };

    static ImGui_ImplSDL2_Data* ImGui_ImplSDL2_GetBackendData()
    {
        return ImGui::GetCurrentContext() ? (ImGui_ImplSDL2_Data*)ImGui::GetIO().BackendPlatformUserData : NULL;
    }

#undef CreateWindow
    static void CreateWindow(ImGuiViewport* viewport)
    {
        ImGui_ImplSDL2_Data* bd = ImGui_ImplSDL2_GetBackendData();
        ImGuiViewportData* data = IM_NEW(ImGuiViewportData)();
        viewport->PlatformUserData = data;

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        ImGuiViewportData* main_viewport_data = (ImGuiViewportData*)main_viewport->PlatformUserData;
        main_viewport_data->window->MakeCurrent();

        Uint32 sdl_flags = 0;
        if (data->window) {
            sdl_flags |= data->window->getFlags() & SDL_WINDOW_ALLOW_HIGHDPI;
        }
        sdl_flags |= SDL_WINDOW_HIDDEN;
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_NoDecoration) ? SDL_WINDOW_BORDERLESS : 0;
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_NoDecoration) ? 0 : SDL_WINDOW_RESIZABLE;
#if !defined(_WIN32)
        // See SDL hack in ShowWindow().
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon) ? SDL_WINDOW_SKIP_TASKBAR : 0;
#endif
#if SDL_HAS_ALWAYS_ON_TOP
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_TopMost) ? SDL_WINDOW_ALWAYS_ON_TOP : 0;
#endif
        WindowCreateInfo createInfo;
        createInfo.name = "No Title Yet";
        createInfo.position = { viewport->Pos.x, viewport->Pos.y };
        createInfo.size = { viewport->Size.x, viewport->Size.y };
        createInfo.flags = sdl_flags;
        data->window = Window::Create(createInfo);
        data->windowOwned = true;

        viewport->PlatformHandle = (void*)data->window->getHandle();
#if defined(_WIN32)
        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        if (SDL_GetWindowWMInfo(data->window->getHandle(), &info))
            viewport->PlatformHandleRaw = info.info.win.window;
#endif
    }

    static void DestroyWindow(ImGuiViewport* viewport)
    {
        if (ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData) {
            if (data->window && data->windowOwned) {
                data->window.Reset();
            }
            data->window = NULL;
            IM_DELETE(data);
        }
        viewport->PlatformUserData = viewport->PlatformHandle = NULL;
    }

    static void ShowWindow(ImGuiViewport* viewport)
    {
        ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
#if defined(_WIN32)
        HWND hwnd = (HWND)viewport->PlatformHandleRaw;

        // SDL hack: Hide icon from task bar
        // Note: SDL 2.0.6+ has a SDL_WINDOW_SKIP_TASKBAR flag which is supported under Windows but the way it create the window breaks our seamless transition.
        if (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon)
        {
            LONG ex_style = ::GetWindowLong(hwnd, GWL_EXSTYLE);
            ex_style &= ~WS_EX_APPWINDOW;
            ex_style |= WS_EX_TOOLWINDOW;
            ::SetWindowLong(hwnd, GWL_EXSTYLE, ex_style);
        }

        // SDL hack: SDL always activate/focus windows :/
        if (viewport->Flags & ImGuiViewportFlags_NoFocusOnAppearing)
        {
            ::ShowWindow(hwnd, SW_SHOWNA);
            return;
        }
#endif
        data->window->ShowWindow();
    }

    void WindowHelper::InitPlatformInterface(const Ref<Window>& window, void* sdl_gl_context)
    {
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();

        platform_io.Platform_CreateWindow = CreateWindow;
        platform_io.Platform_DestroyWindow = DestroyWindow;
        platform_io.Platform_ShowWindow = ShowWindow;

        platform_io.Platform_SetWindowPos = [](ImGuiViewport* viewport, ImVec2 pos) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            data->window->setPosition({ pos.x, pos.y });
        };

        platform_io.Platform_GetWindowPos = [](ImGuiViewport* viewport) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            auto windowPos = data->window->getPosition();
            return ImVec2(windowPos.x, windowPos.y);
        };

        platform_io.Platform_SetWindowSize = [](ImGuiViewport* viewport, ImVec2 size) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            data->window->setSize({ size.x, size.y });
        };

        platform_io.Platform_GetWindowSize = [](ImGuiViewport* viewport) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            auto windowSize = data->window->getSize<float>();
            return ImVec2(windowSize.x, windowSize.y);
        };

        platform_io.Platform_SetWindowFocus = [](ImGuiViewport* viewport) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            return data->window->setFocus();
        };

        platform_io.Platform_GetWindowFocus = [](ImGuiViewport* viewport) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            return data->window->isFocused();
        };

        platform_io.Platform_GetWindowMinimized = [](ImGuiViewport* viewport) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            return data->window->isMinimized();
        };

        platform_io.Platform_SetWindowTitle = [](ImGuiViewport* viewport, const char* title) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            SDL_SetWindowTitle(data->window->getHandle(), title);
        };

        platform_io.Platform_RenderWindow = [](ImGuiViewport* viewport, void*) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            if (data->window) {
                data->window->MakeCurrent();
            }
        };

        platform_io.Platform_SwapBuffers = [](ImGuiViewport* viewport, void*) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;

            if (data->window) {
                data->window->MakeCurrent();
                data->window->Present();
            }
        };

#if SDL_HAS_WINDOW_ALPHA
        platform_io.Platform_SetWindowAlpha = [](ImGuiViewport* viewport, float opacity) {
            ImGuiViewportData* data = (ImGuiViewportData*)viewport->PlatformUserData;
            data->window->setOpacity(opacity);
        };
#endif
#if SDL_HAS_VULKAN
        platform_io.Platform_CreateVkSurface = CreateVkSurface;
#endif

        // SDL2 by default doesn't pass mouse clicks to the application when the click focused a window. This is getting in the way of our interactions and we disable that behavior.
#if SDL_HAS_MOUSE_FOCUS_CLICKTHROUGH
        SDL_SetHint(SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH, "1");
#endif

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        ImGuiViewportData* data = IM_NEW(ImGuiViewportData)();
        data->window = window;
        data->windowID = window->getWindowID();
        data->windowOwned = false;
        main_viewport->PlatformUserData = data;
        main_viewport->PlatformHandle = data->window->getHandle();
    }
}