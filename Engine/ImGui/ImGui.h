#pragma once

#include "External/imgui.h"
#include "External/imgui_impl_sdl.h"
#include "External/ImGuizmo.h"

#include "GraphicsEngine/Illuminar_GFX.h"
#include "GameEngine/Window/Illuminar_Window.h"

#include "Memory/Illuminar_Ref.h"

#include "Math/Illuminar_Math.h"

#include "ImGuiSDL.h"

namespace Illuminar {
	struct GUI
	{
		GUI(const Ref<Window>& window);

		inline void OnEvent(SDL_Event& event)
		{
			ImGui_ImplSDL2_ProcessEvent(&event);
		}

		template<typename FuncT>
		void Render(FuncT&& func)
		{
			using namespace GFX;
			
			ImGui_ImplSDL2_NewFrame(window->getHandle());
			ImGui::NewFrame();
			ImGuizmo::BeginFrame();
			
			func();
			
			ImGuiIO& io = ImGui::GetIO();
			io.DisplaySize = ImVec2(window->getSize<float>().x, window->getSize<float>().y);
			
			ImGui::Render();
			
			ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
			RenderCommand::setViewport(0, 0, (uint32_t)io.DisplaySize.x, (uint32_t)io.DisplaySize.y);
			RenderCommand::setClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
			RenderCommand::Clear(BufferBit::Color);
			
			RenderDrawData(ImGui::GetDrawData());
			
			if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
				ImGui::UpdatePlatformWindows();
				RenderPlatformWindowsDefault(nullptr, nullptr);
			
				window->MakeCurrent();
			}
		}

		void CreateFontAtlas();
		void SetupRenderState(ImDrawData* draw_data);
		void RenderDrawData(ImDrawData* draw_data);
		void RenderPlatformWindowsDefault(void* platform_render_arg, void* renderer_render_arg);

		Ref<GFX::GraphicsPipeline> CreatePipeline();

		Ref<GFX::Texture2D> texture = nullptr;
		Ref<GFX::Texture2D> fontTexture = nullptr;

		Ref<GFX::VertexBuffer> vbo = nullptr;
		Ref<GFX::ElementBuffer> ebo = nullptr;
		//Ref<GFX::Material> material = nullptr;

		Ref<GFX::GraphicsPipeline> pipeline = nullptr;

		Ref<Window> window = nullptr;
	};
}