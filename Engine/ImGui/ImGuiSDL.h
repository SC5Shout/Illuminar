#pragma once

#include "Memory/Illuminar_Ref.h"
#include "GameEngine/Window/Illuminar_Window.h"

#include "External/imgui.h"
#include "Math/Illuminar_Math.h"

#include "GraphicsEngine/Illuminar_RenderCommand.h"

#include <SDL.h>
#include <SDL_syswm.h>
#if defined(__APPLE__)
#include "TargetConditionals.h"
#endif

namespace Illuminar {
	struct ImGuiViewportData
	{
		Ref<Window> window = nullptr;
		uint32_t windowID = 0;
		bool windowOwned = false;
	};

    //taken from imgui_impl_sdl and kinda changed to fit into my code.
	struct WindowHelper
	{
		static void InitPlatformInterface(const Ref<Window>& window, void* sdl_gl_context);
	};
}