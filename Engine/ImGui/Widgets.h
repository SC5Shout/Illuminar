#pragma once

#include "External/imgui.h"
#include "External/imgui_internal.h"

#include "GraphicsEngine/Illuminar_Texture.h"

template<typename T>
concept ImGuiAvailableColorTypes = std::is_same_v<T, uint32_t> || std::is_same_v<T, ImVec4>;

namespace Illuminar {
	struct Widget
	{
		struct ScopedStyle
		{
			template<typename StyleValue>
			ScopedStyle(ImGuiStyleVar styleVar, StyleValue value) { ImGui::PushStyleVar(styleVar, value); }
			~ScopedStyle() { ImGui::PopStyleVar(); }

			ScopedStyle(const ScopedStyle&) = delete;
			ScopedStyle operator=(const ScopedStyle&) = delete;
		};

		struct ScopedColor
		{
			ScopedColor(ImGuiCol colorID, ImGuiAvailableColorTypes auto color) { ImGui::PushStyleColor(colorID, color); }
			~ScopedColor() { ImGui::PopStyleColor(); }

			ScopedColor(const ScopedColor&) = delete;
			ScopedColor operator=(const ScopedColor&) = delete;
		};

		struct ScopedFont
		{
			ScopedFont(ImFont* font) { ImGui::PushFont(font); }
			~ScopedFont() { ImGui::PopFont(); }

			ScopedFont(const ScopedFont&) = delete;
			ScopedFont operator=(const ScopedFont&) = delete;
		};

		struct ScopedID
		{
			template<typename T>
			ScopedID(T id) { ImGui::PushID(id); }
			~ScopedID() { ImGui::PopID(); }

			ScopedID(const ScopedID&) = delete;
			ScopedID operator=(const ScopedID&) = delete;
		};

		struct ScopedWindow
		{
			ScopedWindow(const char* name, bool* open = nullptr, ImGuiWindowFlags flags = 0) { ImGui::Begin(name, open, flags); }
			~ScopedWindow() { ImGui::End(); }

			ScopedWindow(const ScopedWindow&) = delete;
			ScopedWindow operator=(const ScopedWindow&) = delete;
		};

		struct ScopedChildWindow
		{
			ScopedChildWindow(const char* name, const ImVec2& size = ImVec2(0, 0), bool border = false, ImGuiWindowFlags flags = 0)
			{ 
				ImGui::BeginChild(name, size, border, flags);
			}
			~ScopedChildWindow() { ImGui::EndChild(); }

			ScopedChildWindow(const ScopedChildWindow&) = delete;
			ScopedChildWindow operator=(const ScopedChildWindow&) = delete;
		};

		struct ScopedMenuBar
		{
			ScopedMenuBar() { ImGui::BeginMenuBar(); }
			~ScopedMenuBar() { ImGui::EndMenuBar(); }

			ScopedMenuBar(const ScopedMenuBar&) = delete;
			ScopedMenuBar operator=(const ScopedMenuBar&) = delete;
		};

		struct ScopedTable
		{
			ScopedTable(const char* name, uint32_t columnsCount, ImGuiTableFlags flags = 0, const ImVec2& outerSize = ImVec2(-FLT_MIN, 0), float innerWidth = 0.0f)
			{
				ImGui::PushStyleVar(ImGuiStyleVar_CellPadding, ImVec2{ 2, 2 });
				ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(1.0f, 0.0f));
				ImGui::BeginTable(name, columnsCount, flags, outerSize, innerWidth);
			}
			~ScopedTable() { ImGui::EndTable(); ImGui::PopStyleVar(2); }

			ScopedTable(const ScopedTable&) = delete;
			ScopedTable operator=(const ScopedTable&) = delete;
		};

		//The count for multistyle is calculated as follow:
		//every ImGui::Push* contains 2 arguments - var/ID and value/color
		//The first var/ID and the value/color are treated as regular parameters so + 1 means one firstVar/ID and one firstValue/Color
		//Other* is treated as a variadic template pair which means that sizeof (Other*) is 2 times bigger
		//and to get the proper count the sizeof needs to be divided by 2

		//Maybe the builder'd be cleaner?
		struct ScopedMultiStyle
		{
			template <typename ValueType, typename... OtherStylePairs>
			ScopedMultiStyle(ImGuiStyleVar firstStyleVar, ValueType firstValue, OtherStylePairs&& ... otherStylePairs)
				: count((sizeof... (otherStylePairs) / 2) + 1)
			{
				static_assert ((sizeof... (otherStylePairs) & 1u) == 0, "ScopedMultiStyle constructor expects a list of pairs of Vars and values as its arguments");

				PushStyle(firstStyleVar, firstValue, std::forward<OtherStylePairs>(otherStylePairs)...);
			}

			~ScopedMultiStyle() { ImGui::PopStyleVar(count); }

			ScopedMultiStyle(const ScopedMultiStyle&) = delete;
			ScopedMultiStyle operator=(const ScopedMultiStyle&) = delete;

		private:
			template <typename ValueType, typename... OtherStylePairs>
			void PushStyle(ImGuiStyleVar styleVar, ValueType value, OtherStylePairs&& ... otherStylePairs)
			{
				ImGui::PushStyleVar(styleVar, value);
				if constexpr (sizeof... (otherStylePairs) != 0) {
					PushStyle(std::forward<OtherStylePairs>(otherStylePairs)...);
				}
			}

			uint32_t count = 0;
		};

		struct ScopedMultiColor
		{
			template<typename ... OtherColors>
			ScopedMultiColor(ImGuiCol firstColorID, ImGuiAvailableColorTypes auto firstColor, OtherColors&& ... otherColors)
				: count((sizeof ... (otherColors)) / 2 + 1)
			{
				PushColor(firstColorID, firstColor, std::forward<OtherColors>(otherColors)...);
			}

			~ScopedMultiColor() { ImGui::PopStyleColor(count); }

			ScopedMultiColor(const ScopedMultiStyle&) = delete;
			ScopedMultiColor operator=(const ScopedMultiStyle&) = delete;

		private:
			template <typename... OtherColors>
			void PushColor(ImGuiCol colorID, ImGuiAvailableColorTypes auto value, OtherColors&& ... otherColors)
			{
				ImGui::PushStyleColor(colorID, value);
				if constexpr (sizeof... (otherColors) != 0) {
					PushColor(std::forward<OtherColors>(otherColors)...);
				}
			}

			uint32_t count = 0;
		};

		static inline bool ImageButton(const Ref<GFX::Texture>& texture, 
			const ImVec2& size,
			const ImVec2& uv0 = ImVec2(0, 0), 
			const ImVec2& uv1 = ImVec2(1, 1), 
			int frame_padding = -1, 
			const ImVec4& bg_col = ImVec4(0, 0, 0, 0), 
			const ImVec4& tint_col = ImVec4(1, 1, 1, 1))
		{
			return ImGui::ImageButton((ImTextureID)texture->getTexture().getID(), size, uv0, uv1, frame_padding, bg_col, tint_col);
		}

		static inline void ImageButton(const Ref<GFX::Texture>& imageNormal, const Ref<GFX::Texture>& imageHovered, const Ref<GFX::Texture>& imagePressed,
			ImU32 tintNormal, ImU32 tintHovered, ImU32 tintPressed,
			ImVec2 rectMin, ImVec2 rectMax)
		{
			auto* drawList = ImGui::GetWindowDrawList();
			if (ImGui::IsItemActive())
				drawList->AddImage((ImTextureID)imagePressed->getTexture().getID(), rectMin, rectMax, ImVec2(0, 0), ImVec2(1, 1), tintPressed);
			else if (ImGui::IsItemHovered())
				drawList->AddImage((ImTextureID)imageHovered->getTexture().getID(), rectMin, rectMax, ImVec2(0, 0), ImVec2(1, 1), tintHovered);
			else
				drawList->AddImage((ImTextureID)imageNormal->getTexture().getID(), rectMin, rectMax, ImVec2(0, 0), ImVec2(1, 1), tintNormal);
		};

		static void ImageButton(const Ref<GFX::Texture>& image,
			ImU32 tintNormal, ImU32 tintHovered, ImU32 tintPressed,
			ImRect rectangle)
		{
			ImageButton(image, image, image, tintNormal, tintHovered, tintPressed, rectangle.Min, rectangle.Max);
		};

		static void Image(const Ref<GFX::Texture>& image, ImRect rectangle)
		{
			auto* drawList = ImGui::GetWindowDrawList();
			drawList->AddImage((ImTextureID)image->getTexture().getID(), rectangle.Min, rectangle.Max);
		};

		static inline void Image(const Ref<GFX::Texture>& texture, 
			const ImVec2& size, 
			const ImVec2& uv0 = ImVec2(0, 0), 
			const ImVec2& uv1 = ImVec2(1, 1), 
			const ImVec4& tint_col = ImVec4(1, 1, 1, 1), 
			const ImVec4& border_col = ImVec4(0, 0, 0, 0))
		{
			ImGui::Image((ImTextureID)texture->getTexture().getID(), size, uv0, uv1, tint_col, border_col);
		}

		static void RenderRoundedFrame(ImVec2 p_min, ImVec2 p_max, ImU32 fill_col, bool border, ImDrawCornerFlags corner = ImDrawCornerFlags_All, float rounding = 0.0f);
		static bool RoundedButton(const char* label, const ImVec2& size_arg, ImDrawCornerFlags corner = ImDrawCornerFlags_All, float rounding = 0.0f);
		static const char* PatchFormatStringFloatToInt(const char* fmt);

		static bool RoundedDragScalar(const char* label, ImGuiDataType data_type, void* p_data, float v_speed, const void* p_min, const void* p_max, const char* format, ImGuiSliderFlags flags, ImDrawCornerFlags corners, float rounding);
		static bool RoundedDragFloat(const char* label, float* v, float v_speed, float v_min, float v_max, const char* format, ImGuiSliderFlags flags, ImDrawCornerFlags corners, float rounding);
	
		static bool Vector2DragProperty(const char* label, Vector2<float>& values, const char* XSign = "X", const char* YSign = "Y");
		static bool Vector3DragProperty(const char* label, Vector3<float>& values, const char* XSign = "X", const char* YSign = "Y", const char* ZSign = "Z");
		static bool DragProperty(const char* label, float& value);

		template<typename StringTypes, typename ValueType>
		static bool DropDownProperty(StringTypes types, ValueType& value, const std::string& name)
		{
			bool modified = false;

			ImGui::TableNextRow();
			ImGui::TableNextColumn();

			ImGui::Text(name.c_str());

			ImGui::TableNextColumn();

			{
				const char* currentType = types[(int)value];
				ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
				if (ImGui::BeginCombo(("###" + name).c_str(), currentType)) {
					for (size_t type = 0; type < types.size(); type++) {
						bool isSelected = (currentType == types[type]);
						if (ImGui::Selectable(types[type], isSelected)) {
							currentType = types[type];
							value = (ValueType)type;
							modified = true;
						}
						if (isSelected) {
							ImGui::SetItemDefaultFocus();
						}
					}

					ImGui::EndCombo();
				}
				ImGui::PopItemWidth();
			}

			return modified;
		}

		static bool Property(const char* label, bool& value);

		static bool ImageTreeNode(const Ref<GFX::Texture>& texture, const char* str_id, ImGuiTreeNodeFlags flags, const char* fmt, ...);
	};
}