project "ImGui"
    kind "StaticLib"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    files { 
        "**.h", 
        "**.c", 
        "**.hpp", 
        "**.cpp",
    }

    includedirs {       
        "/",
        "%{wks.location}/Engine/Illuminar/src",

		"%{IncludeDir.SDL2}",
        "%{IncludeDir.Glad}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.spdlog}",
    }

    links { 
        "Illuminar"
    }
    
    filter "configurations:Debug"
		defines "ILLUMINAR_DEBUG"
		symbols "On"

    filter "configurations:Release"
        defines "ILLUMINAR_RELEASE"
        optimize "On"