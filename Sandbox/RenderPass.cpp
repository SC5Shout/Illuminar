#include "Illuminar_pch.h"
#include "RenderPass.h"

#include "Renderer.h"

namespace v2 {
	void RenderPass::Execute(const std::vector<DrawMesh>& meshes, Renderer* renderer)
	{
		static auto CheckCurrentAndBind = []<typename Current, typename New>(Current& c, const New& n) {
			if (c != n) {
				c = n;
				c->Bind();
			}
		};

		uint32_t i = 0;
		if (!instanceBatches.empty()) {
			Ref<GFX::Material> currentMaterial = nullptr;
			Ref<GFX::VertexBuffer> currentVbo = nullptr;
			Ref<GFX::ElementBuffer> currentEbo = nullptr;
			Ref<GFX::GraphicsPipeline> currentPipeline = nullptr;

			indirectDrawBuffer->Bind();
			modelTransformBufferSsbo->Bind();
			instanceIDsBuffer->Bind();

			for (const auto& multibatch : multiDrawIndirectBatches) {
				auto& instanceDraw = instanceBatches[multibatch.first];
				auto& passObject = objects[multibatch.first];

				Ref<Material>& newMaterial = instanceDraw.material;

				const auto& mesh = meshes[instanceDraw.meshID.getID()].submesh;

				GraphicsPipeline::CreateInfo info;
				info.bufferLayout = mesh.parent->vbo->getLayout();
				info.shader = newMaterial->shader;
				info.rasterizerState = newMaterial->rasterizerState;
				info.blendState = newMaterial->blendState;
				info.depthState = newMaterial->depthState;
				info.drawMode = DrawMode::Triangles;
				
				auto pipeline = GraphicsPipelineCache::Create(info);
				
				CheckCurrentAndBind(currentPipeline, pipeline);
				CheckCurrentAndBind(currentMaterial, newMaterial);
				CheckCurrentAndBind(currentVbo, mesh.parent->vbo);
				CheckCurrentAndBind(currentEbo, mesh.parent->ebo);

				GFX::RenderCommand::DrawElementsIndirect(GFX::ElementDataType::Uint32, multibatch.count, multibatch.first * sizeof(DrawElementsIndirectCommand));
			}
		}

		objects.clear();
	}

	void RenderPass::RefreshPass(std::vector<RenderObject>& renderables, const std::vector<DrawMesh>& meshes)
	{
		BuildFlatBatches(renderables);

		instanceBatches = std::move(BuildInstanceBatches(flatBatches, objects));
		multiDrawIndirectBatches = std::move(BuildMultiIndirectDraw(instanceBatches, meshes));
	}

	void RenderPass::BuildFlatBatches(std::vector<RenderObject>& renderables)
	{
		std::vector<Handle<RenderObject>> newObjects = std::move(unbatchedObjects);
		std::vector<RenderBatch> newBatches;
		newBatches.reserve(newObjects.size());

		for (auto o : newObjects) {
			auto& renderable = renderables[o.getID()];

			Handle<PassObject> objectHandle = Handle<PassObject>{ (uint32_t)objects.size() };
			PassObject& newObject = objects.emplace_back();
			newObject.renderableID = o;
			newObject.meshID = renderable.meshID;
			newObject.material = renderable.material;
			newObject.quantizedDepth = DepthToBits(renderable.distanceFromCamera, DEPTH_NUMBER_OF_BITS);

			renderable.passObjectHandles = objectHandle;

			const uint64_t sortingKey
				= //MakeKey(newObject.pipeline->getID(), PIPELINE_NUMBER_OF_BITS, PIPELINE_SHIFT_OPAQUE) |
				MakeKey(newObject.material == nullptr ? -1 : newObject.material->getID(), MATERIAL_NUMBER_OF_BITS, MATERIAL_SHIFT_OPAQUE) |
				MakeKey(newObject.quantizedDepth, DEPTH_NUMBER_OF_BITS, DEPTH_SHIFT_OPAQUE) |
				MakeKey(newObject.customKey, CUSTOM_NUMBER_OF_BITS, CUSTOM_SHIFT_OPAQUE);

			RenderBatch& newCommand = newBatches.emplace_back();
			newCommand.object = objectHandle;
			newCommand.sortKey = sortingKey;
		}

		std::sort(std::execution::par, newBatches.begin(), newBatches.end(), [](const RenderBatch& A, const RenderBatch& B) {
			if (A.sortKey < B.sortKey) { return true; }
			else if (A.sortKey == B.sortKey) { return A.object < B.object; }
			else { return false; }
		});

		flatBatches = std::move(newBatches);
	}

	std::vector<InstanceBatch> RenderPass::BuildInstanceBatches(std::vector<RenderBatch>& inobjects, const std::vector<PassObject>& objects)
	{
		if (inobjects.size() == 0) return {};

		InstanceBatch newBatch;
		newBatch.firstInstance = 0;
		newBatch.instanceCount = 0;

		newBatch.material = objects[inobjects[0].object.getID()].material;
		newBatch.meshID = objects[inobjects[0].object.getID()].meshID;

		std::vector<InstanceBatch> instanceBatches;
		instanceBatches.reserve(inobjects.size());
		instanceBatches.push_back(newBatch);
		InstanceBatch* back = &instanceBatches.back();

		Ref<Material> lastMat = objects[inobjects[0].object.getID()].material;
		for (int i = 0; i < inobjects.size(); i++) {
			const PassObject& obj = objects[inobjects[i].object.getID()];

			const bool bSameMesh = obj.meshID.getID() == back->meshID.getID();
			bool bSameMaterial = obj.material == lastMat;

			if (!bSameMaterial || !bSameMesh) {
				newBatch.material = obj.material;

				if (newBatch.material == back->material) {
					bSameMaterial = true;
				}
			}

			if (bSameMesh && bSameMaterial) {
				back->instanceCount++;
			} else {
				newBatch.firstInstance = i;
				newBatch.instanceCount = 1;
				newBatch.meshID = obj.meshID;

				instanceBatches.push_back(newBatch);
				back = &instanceBatches.back();
			}
		}

		return instanceBatches;
	}

	std::vector<MultiDrawIndirectBatch> RenderPass::BuildMultiIndirectDraw(const std::vector<InstanceBatch>& instanceBatches, const std::vector<DrawMesh>& meshes)
	{
		if (instanceBatches.size() == 0) return {};

		std::vector<MultiDrawIndirectBatch> multiDrawIndirectBatches;
		multiDrawIndirectBatches.reserve(instanceBatches.size());

		MultiDrawIndirectBatch newbatch;
		newbatch.count = 1;
		newbatch.first = 0;

		for (size_t i = 1; i < instanceBatches.size(); i++) {
			const InstanceBatch& joinbatch = instanceBatches[newbatch.first];
			const InstanceBatch& batch = instanceBatches[i];

			const auto& mesh1 = meshes[joinbatch.meshID.getID()].submesh;
			const auto& mesh2 = meshes[batch.meshID.getID()].submesh;
			const bool bCompatibleMesh = mesh1.parent->vbo == mesh2.parent->vbo;
			const bool bSameMat = bCompatibleMesh && joinbatch.material->getID() == batch.material->getID() && joinbatch.material->getID() == batch.material->getID();

			if (!bSameMat || !bCompatibleMesh) {
				multiDrawIndirectBatches.push_back(newbatch);
				newbatch.count = 1;
				newbatch.first = i;
			} else {
				newbatch.count++;
			}
		}
		multiDrawIndirectBatches.push_back(newbatch);

		return multiDrawIndirectBatches;
	}
}