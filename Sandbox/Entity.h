#pragma once

#include "GameEngine/ECS/Illuminar_ECS.h"

using namespace Illuminar;
namespace Illuminar {
	struct HierarchyComponent;
}

namespace v2 {
	struct Scene;

	struct Entity
	{
		Entity() = default;
		~Entity() = default;

		Entity(Scene* scene);
		Entity(EntityHandle handle, Scene* scene);

		template<typename Component, typename ... Args>
		[[nodiscard]] inline Component* AddComponent(Args&& ... args) const
		{
			return ecs->AddComponent<Component>(handle, std::forward<Args>(args)...);
		}

		template<typename Component>
		inline void RemoveComponent()
		{
			ecs->RemoveComponent<Component>(handle);
		}

		template<typename Component>
		[[nodiscard]] inline Component* getComponent() const
		{
			return ecs->getComponent<Component>(handle);
		}

		void setParentID(UUID parentID);
		[[nodiscard]] UUID getParentID() const;

		//[[nodiscard]] const std::vector<UUID>& getChildrenID();
		[[nodiscard]] std::vector<UUID>& getChildrenID() const;

		//[[nodiscard]] bool HasParent() const;
		//
		[[nodiscard]] bool isAncesterOf(Entity entity) const;
		[[nodiscard]] bool isDescendantOf(Entity entity) const;

		[[nodiscard]] UUID getID() const;

		inline operator uint64_t () const { return (uint64_t)handle; }
		inline operator EntityHandle() const { return handle; }
		inline operator bool() const { return (uint64_t)handle && scene; }

		inline bool operator==(const Entity& other) const
		{
			return handle == other.handle && scene == other.scene;
		}

		inline bool operator!=(const Entity& other) const
		{
			return !(*this == other);
		}

		//private:
		friend struct Scene;

		Scene* scene = nullptr;
		ECS* ecs = nullptr;
		EntityHandle handle = nullptr;
	};

	static const Entity INVALID_ENTITY = Entity{ INVALID_ENTITY_HANDLE, nullptr };
}