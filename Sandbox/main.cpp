#include <Illuminar_pch.h>
#include <GameEngine/Illuminar_Application.h>

#include "ImGui.h"

#include "stb_image.h"

#include "GraphicsEngine/OpenGL/Illuminar_GLTexture.h"

#include "GameEngine/ECS/Illuminar_ECS.h"
#include "GameEngine/Scene/Illuminar_Entity.h"
#include "GameEngine/Scene/Components/Illuminar_Components.h"

#include "GameEngine/Resource/Illuminar_ResourceManagers.h"
#include "GameEngine/Resource/Illuminar_ResourceListener.h"
#include "GameEngine/Project/Illuminar_Project.h"

#include "My2.h"

namespace Illuminar {
    enum Camera_Movement {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT
    };

    // Default camera values
    const float YAW = -90.0f;
    const float PITCH = 0.0f;
    const float SPEED = 200.5f;
    const float SENSITIVITY = 0.5f;
    const float ZOOM = 45.0f;

    class TestCamera : Camera
    {
    public:
        // camera Attributes
        glm::vec3 Position;
        glm::vec3 Front;
        glm::vec3 Up;
        glm::vec3 Right;
        glm::vec3 WorldUp;
        // euler Angles
        float Yaw;
        float Pitch;
        // camera options
        float MovementSpeed;
        float MouseSensitivity;
        float Zoom;

        // constructor with vectors
        TestCamera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
        {
            Position = position;
            WorldUp = up;
            Yaw = yaw;
            Pitch = pitch;
            updateCameraVectors();
        }
        // constructor with scalar values
        TestCamera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
        {
            Position = glm::vec3(posX, posY, posZ);
            WorldUp = glm::vec3(upX, upY, upZ);
            Yaw = yaw;
            Pitch = pitch;
            updateCameraVectors();
        }

        // returns the view matrix calculated using Euler Angles and the LookAt Matrix
        glm::mat4 GetViewMatrix()
        {
            return glm::lookAt(Position, Position + Front, Up);
        }

        // processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
        void ProcessKeyboard(Camera_Movement direction, float deltaTime)
        {
            float velocity = MovementSpeed * deltaTime;
            if (direction == FORWARD)
                Position += Front * velocity;
            if (direction == BACKWARD)
                Position -= Front * velocity;
            if (direction == LEFT)
                Position -= Right * velocity;
            if (direction == RIGHT)
                Position += Right * velocity;
        }

        // processes input received from a mouse input system. Expects the offset value in both the x and y direction.
        void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
        {
            xoffset *= MouseSensitivity;
            yoffset *= MouseSensitivity;

            Yaw += xoffset;
            Pitch += yoffset;

            // make sure that when pitch is out of bounds, screen doesn't get flipped
            if (constrainPitch)
            {
                if (Pitch > 89.0f)
                    Pitch = 89.0f;
                if (Pitch < -89.0f)
                    Pitch = -89.0f;
            }

            // update Front, Right and Up Vectors using the updated Euler angles
            updateCameraVectors();
        }

        // processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
        void ProcessMouseScroll(float yoffset)
        {
            Zoom -= (float)yoffset;
            if (Zoom < 1.0f)
                Zoom = 1.0f;
            if (Zoom > 45.0f)
                Zoom = 45.0f;
        }

    private:
        // calculates the front vector from the Camera's (updated) Euler Angles
        void updateCameraVectors()
        {
            // calculate the new Front vector
            glm::vec3 front;
            front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
            front.y = sin(glm::radians(Pitch));
            front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
            Front = glm::normalize(front);
            // also re-calculate the Right and Up vector
            Right = glm::normalize(glm::cross(Front, WorldUp));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
            Up = glm::normalize(glm::cross(Right, Front));
        }
    };

    static std::shared_ptr<Project> project = nullptr;

    struct Sandbox : Application
    {
        TestCamera camera{ glm::vec3(0.0f, 0.0f, 3.0f) };
        Ref<v2::Scene> scene;

        v2::Entity testSceneEntity;
        v2::Entity testSceneEntity2;
        v2::Entity testSceneEntity3;
        v2::Entity testSceneEntity4;
        v2::Entity testSceneEntity5;

        v2::Entity sponzaEntity;
        v2::Entity sponzaEntity2;

        Sandbox(const ApplicationCreateInfo& createInfo)
            : Application(createInfo)
        {

            project = Project::Load("Sandbox.proj");

            scene = Ref<v2::Scene>::Create();

            auto entity = scene->CreateEntity("Sky");
            auto comp = entity.AddComponent<SkylightComponent>();
            comp->environment = Environment::CreatePreethamSky(comp->turbidityAzimuthInclination.x, comp->turbidityAzimuthInclination.y, comp->turbidityAzimuthInclination.z);
            comp->dynamicSky = true;

            scene->renderer.environment = comp->environment;
            scene->renderer.intensity = comp->intensity;

            v2::LightBuilder(LightType::Directional).
                Direction({ Radians(-45.0f), Radians(-175.0f), Radians(160.0f) }).
                Build("directionalLight", scene);

            auto& manager = ResourceManagers::get().meshResourceManager;
            MeshResourceID sponzaID = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
            MeshResourceID testSceneID = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");

            MeshResourceID sponzaID2 = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
            MeshResourceID testSceneID2 = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
            
            //MeshResourceID sponzaID3 = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
            //MeshResourceID testSceneID3 = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
            //
            //MeshResourceID sponzaID4 = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
            //MeshResourceID testSceneID4 = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
            //
            //MeshResourceID sponzaID5 = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
            //MeshResourceID testSceneID5 = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
            ResourceStreamer::FlushAllQueues();
            {
                sponzaEntity = scene->CreateMeshEntity("sponza", sponzaID);
                testSceneEntity = scene->CreateMeshEntity("TestScene", testSceneID);
            }
            //
            {
                sponzaEntity2 = scene->CreateMeshEntity("sponza2", sponzaID2);
                auto transform = sponzaEntity2.getComponent<TransformComponent>();
                transform->translation = { 10.0f, 0.0f, 0.0f };

                testSceneEntity2 = scene->CreateMeshEntity("TestScene2", testSceneID2);
                auto transform2 = testSceneEntity2.getComponent<TransformComponent>();
                transform2->translation = { 0.0f, 0.0f, 10.0f };
            }
            //
            //{
            //    auto sponzaEntity = scene->CreateMeshEntity("sponza3", sponzaID3);
            //    testSceneEntity3 = scene->CreateMeshEntity("TestScene3", testSceneID3);
            //}
            //
            //{
            //    auto sponzaEntity = scene->CreateMeshEntity("sponza4", sponzaID4);
            //    testSceneEntity4 = scene->CreateMeshEntity("TestScene4", testSceneID4);
            //}
            //
            //{
            //    auto sponzaEntity = scene->CreateMeshEntity("sponza5", sponzaID5);
            //    testSceneEntity5 = scene->CreateMeshEntity("TestScene5", testSceneID5);
            //}
        }

        void OnEvent(SDL_Event& event) override
        {
            camera.ProcessMouseMovement(Input::getMouseRelX(), -Input::getMouseRelY());
        }

        void Update([[maybe_unused]] Timestep deltaTime) override
        {
            GFX::RenderCommand::setViewport(0, 0, 1600, 900);
            GFX::RenderCommand::setClearColor(0.5f, 0.5f, 0.5f, 1.0f);
            GFX::RenderCommand::Clear(GFX::BufferBit::Color | GFX::BufferBit::Depth);

            if (Input::KeyDown(Keyboard::A)) {
                camera.ProcessKeyboard(LEFT, deltaTime);
            }
            else if (Input::KeyDown(Keyboard::D)) {
                camera.ProcessKeyboard(RIGHT, deltaTime);
            }

            if (Input::KeyDown(Keyboard::W)) {
                camera.ProcessKeyboard(FORWARD, deltaTime);
            }
            else if (Input::KeyDown(Keyboard::S)) {
                camera.ProcessKeyboard(BACKWARD, deltaTime);
            }

            {
                static bool done = false;
                if (Input::KeyDown(Keyboard::F1) && !done) {
                    scene->DestroyEntity(testSceneEntity);
                    done = true;
                }
            }

            {
                static bool done2 = false;
                if (Input::KeyDown(Keyboard::F2) && !done2) {
                    scene->DestroyEntity(testSceneEntity2);
                    done2 = true;
                }
            }

            {
                static bool done3 = false;
                if (Input::KeyDown(Keyboard::F3) && !done3) {
                    scene->DestroyEntity(sponzaEntity);
                    done3 = true;
                }
            }

            {
                static bool done4 = false;
                if (Input::KeyDown(Keyboard::F4) && !done4) {
                    scene->DestroyEntity(sponzaEntity2);
                    done4 = true;
                }
            }

            scene->UpdateEditor(deltaTime, (const Camera&)camera, camera.GetViewMatrix());
        }
    };

    std::unique_ptr<Application> CreateApplication()
    {
        ApplicationCreateInfo editorInfo;
        editorInfo.name = "Sandbox";
        editorInfo.w = 1600;
        editorInfo.h = 900;

        return std::make_unique<Sandbox>(editorInfo);
    }
}

#include <Illuminar_EntryPoint.h>