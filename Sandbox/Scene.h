#pragma once

#include "Entity.h"
#include "Renderer.h"

using namespace Illuminar;

namespace v2 {
	struct SubmeshComponent : ECSComponent<SubmeshComponent>
	{
		using MeshResourceID = uint32_t;

		SubmeshComponent(GFX::Submesh submesh, MeshResourceID meshID, uint32_t submeshIndex)
			: submesh(submesh), meshID(meshID), submeshIndex(submeshIndex)
		{}

		GFX::Submesh submesh;
		MeshResourceID meshID = INVALID_RESOURCE_ID;
		uint32_t submeshIndex = 0;

		Handle<v2::RenderObject> renderObject;
	};

	struct Scene;
	struct LightBuilder
	{
		LightBuilder(LightType type);
		~LightBuilder() = default;

		LightBuilder(LightBuilder const& rhs) = default;
		LightBuilder(LightBuilder&& rhs) = default;

		LightBuilder& operator=(LightBuilder const& rhs) = default;
		LightBuilder& operator=(LightBuilder&& rhs) = default;

		Entity Build(const std::string& name, Ref<Scene>& scene, Entity parent = Entity{ INVALID_ENTITY_HANDLE, nullptr });

		LightBuilder& Position(const Vector3<float>& position);
		LightBuilder& Direction(const Vector3<float>& direction);
		LightBuilder& Radiance(const Vector3<float>& radiance);
		LightBuilder& Intensity(float intensity);
		LightBuilder& FallOff(float fallOff);
		LightBuilder& Radius(float radius);
		LightBuilder& MinRadius(float minRadius);
		LightBuilder& OuterCutOff(float outerCutOff);
		LightBuilder& InnerCutOff(float innerCutOff);

		LightBuilder& CastsShadows(bool shadows);
		LightBuilder& SoftShadows(bool soft);
		LightBuilder& NvidiaPcss(bool nvidia);
		LightBuilder& LightSize(float lightSize);

	private:
		friend struct Scene;

		LightType type;
		LightComponent light;

		Vector3<float> position = 1.0f;
		Vector3<float> direction = 0.0f;
	};
	//meshComponent.renderObject = renderer.RegisterObject(meshComponent.submesh);

	struct Scene;
	struct MeshRenderSystem : ECSSystem<TransformComponent, SubmeshComponent>
	{
		MeshRenderSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct DirectionalLightSystem : ECSSystem<TransformComponent, LightComponent>
	{
		DirectionalLightSystem(Scene* scene)
			: scene(scene)
		{
		}

		void Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components);

		Scene* scene;
	};

	struct Scene : RefCount
	{
		[[nodiscard]] inline static Ref<Scene> Create()
		{
			return Ref<Scene>::Create();
		}

		Scene();
		~Scene();

		void setViewport(uint32_t w, uint32_t h);
		void UpdateEditor([[maybe_unused]] Timestep deltaTime, const Camera& camera, const Mat4& viewMatrix);

		[[nodiscard]] Entity CreateEntity(const std::string& name, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateEntityWithID(const std::string& name, UUID ID, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateLightEntity(const std::string& name, LightType lightType, Entity parent = INVALID_ENTITY);
		[[nodiscard]] Entity CreateMeshEntity(const std::string& name, MeshResourceID meshID, Entity parent = INVALID_ENTITY);

		void DestroyEntity(Entity entity);

		[[nodiscard]] Entity FindEntityByID(UUID id);

		void ConvertToLocalSpace(Entity entity);
		void ConvertToWorldSpace(Entity entity);

		[[nodiscard]] Mat4 getTransformRelativeToParent(Entity entity);
		[[nodiscard]] Mat4 getWorldSpaceTransformMatrix(Entity entity);
		[[nodiscard]] TransformComponent getWorldSpaceTransform(Entity entity);

		void ParentEntity(Entity entity, Entity parent);
		void UnparentEntity(Entity entity, bool convertToWorldSpace = true);

		[[nodiscard]] Entity getMainCamera();

		void BuildMeshHierarchy(Entity parent, MeshResource* resource, const void* assimpScene, void* assimpNode);

		void OnLightDestroy(ECS& ecs, EntityHandle handle)
		{
			renderer.sceneData.directionalLightCount = 0;
		}

		Renderer renderer;
		ECSSystemList renderSystems;

		ECS ecs;

		Vector2<uint32_t> viewport = DEFAULT_SCENE_VIEWPORT;
	};
}