#include "Illuminar_pch.h"
#include "Renderer.h"

#include "GraphicsEngine/Illuminar_GraphicsPipelineCache.h"

namespace v2 {
	struct Skybox
	{
		Skybox()
		{
			static constexpr float skyboxVertices[] = {
				-1.0f,  1.0f, 0.1f,
				-1.0f, -1.0f, 0.1f,
				 1.0f, -1.0f, 0.1f,
				 1.0f,  1.0f, 0.1f,
			};
			vbo = VertexBuffer::Create({ { DataType::RGB32_FLOAT, "a_position" } }, sizeof(skyboxVertices), skyboxVertices, Usage::Static_Draw);

			GFX::Material::CreateInfo materialInfo;
			materialInfo.shader = ShaderCache::getShader("Skybox");
			materialInfo.name = "Skybox";
			materialInfo.materialDomain = GFX::MaterialDomain::Surface;
			materialInfo.shadingModel = GFX::ShadingModel::Lit;
			materialInfo.cullMode = GFX::CullMode::Back;
			materialInfo.blendMode = GFX::BlendMode::Opaque;
			materialInfo.drawMode = GFX::DrawMode::TriangleFan;
			material = MaterialCache::Create(materialInfo);
		}

		void Draw(const Ref<TextureCubemap>& cubemap, Renderer& renderer)
		{
			material->set("u_parameters.textureLod", 1.0f);
			material->set("u_parameters.intensity", renderer.intensity);
			material->set("u_skybox", cubemap);

			//this bind is not needed since draw is executed after color pass which binds camera data.
			//renderer.dynamicUbo->BindRange(BufferType::Uniform, 0, renderer.cameraDataOffset, sizeof(ForwardPlusRenderer::CameraData));

			material->depthState.func = GFX::DepthTest::LEqual;
			material->depthState.mask = GFX::DepthMask::All;
			RenderCommand::DrawArrays(vbo, material, 4, 1, 0, 0);
		}

		Ref<VertexBuffer> vbo = nullptr;
		Ref<Material> material = nullptr;
	};

	void Renderer::Begin(const Camera& camera, const Mat4& view)
	{
		frameData.projection = camera.projection;
		frameData.view = view;	

		cameraPosition = glm::inverse(frameData.view)[3];

		for (auto& object : renderObjects) {
			object.clear();
		}
		renderIds.clear();
	}

	void Renderer::End()
	{
		BuildPasses();
		PreperUniformBuffers();

		shadowPass.Draw();
		colorPass.Draw();
		compositePass.Draw();

		shadowPass.lightViewMatrices.clear();

		dynamicUbo.clear();

		for (auto& pass : renderPasses) {
			pass.modelTransformBuffer.clear();
		}
	}

	void Renderer::ShadowPass::Init()
	{
		//GraphicsPipeline::CreateInfo pipelineInfo;
		//pipelineInfo.bufferLayout = {
		//	{ DataType::RGB32_FLOAT, "a_position" },
		//	{ DataType::RGB32_FLOAT, "a_normal" },
		//	{ DataType::RGB32_FLOAT, "a_tangent" },
		//	{ DataType::RGB32_FLOAT, "a_binormal" },
		//	{ DataType::RG32_FLOAT, "a_uv" },
		//};
		//
		//pipelineInfo.shader = ShaderCache::getShader("ShadowMap");
		//pipelineInfo.drawMode = DrawMode::Triangles;
		//pipelineInfo.rasterizerState.cullMode = CullMode::Front;
		//pipeline = GraphicsPipelineCache::Create(pipelineInfo);
		
		Texture2DArray::CreateInfo depthMapInfo;
		depthMapInfo.name = "Shadow";
		depthMapInfo.w = SHADOW_SIZE;
		depthMapInfo.h = SHADOW_SIZE;
		depthMapInfo.depth = 1;
		depthMapInfo.parameters = TextureParameters().
			Format(TextureFormat::D32_FLOAT).
			Miplevel(1).
			FilterMIN(TextureFilter::Nearest).
			FilterMAG(TextureFilter::Nearest).
			Wrap(TextureWrap::Clamp_To_Border);
		depthMapInfo.shadow = true;
		texture = Texture2DArray::Create(depthMapInfo);
		
		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 0;
		frameBufferInfo.depthStencilTexture = texture;
		fbo = FrameBuffer::Create(frameBufferInfo);
		
		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = ShaderCache::getShader("ShadowMap");
		materialInfo.materialDomain = MaterialDomain::Surface;
		materialInfo.blendMode = BlendMode::Opaque;
		materialInfo.shadingModel = ShadingModel::Lit;
		materialInfo.cullMode = GFX::CullMode::Front;
		materialInfo.drawMode = DrawMode::Triangles;
		materialInfo.name = "ShadowPass";

		material = MaterialCache::Create(materialInfo);
	}
	
	std::array<Vector3<float>, 8> getFrustumCornersWorldSpace(const Mat4& viewProj)
	{
		std::array<Vector3<float>, 8> frustumCorners = {
			Vector3<float>(-1.0f,  1.0f, -1.0f),
			Vector3<float>(1.0f,  1.0f, -1.0f),
			Vector3<float>(1.0f, -1.0f, -1.0f),
			Vector3<float>(-1.0f, -1.0f, -1.0f),
			Vector3<float>(-1.0f,  1.0f,  1.0f),
			Vector3<float>(1.0f,  1.0f,  1.0f),
			Vector3<float>(1.0f, -1.0f,  1.0f),
			Vector3<float>(-1.0f, -1.0f,  1.0f),
		};
	
		const Mat4 invCamWorld = glm::inverse(viewProj);
		for (uint32_t i = 0; i < 8; i++) {
			const Vector4<float> invCorner = invCamWorld * Vector4<float>(frustumCorners[i], 1.0f);
			frustumCorners[i] = invCorner / invCorner.w;
		}
	
		return frustumCorners;
	}
	
	std::array<Vector3<float>, 8> getFrustumCornersWorldSpace(const Mat4& proj, const Mat4& view)
	{
		return getFrustumCornersWorldSpace(proj * view);
	}
	
	Mat4 Renderer::ShadowPass::getLightSpaceMatrix(const Vector3<float>& lightDir, float nearClip, float clipRange, float splitDist, float& lastSplitDist, uint32_t cascadeIndex)
	{
		auto corners = getFrustumCornersWorldSpace(renderer->frameData.projection, renderer->frameData.view);
	
		for (uint32_t i = 0; i < 4; i++) {
			Vector3<float> dist = corners[i + 4] - corners[i];
			corners[i + 4] = corners[i] + (dist * splitDist);
			corners[i] = corners[i] + (dist * lastSplitDist);
		}
	
		Vector3<float> center{ 0.0f };
		for (const auto& v : corners) {
			center += v;
		}
		center /= corners.size();
	
		float radius = 0.0f;
		for (uint32_t i = 0; i < corners.size(); i++) {
			float distance = glm::length(corners[i] - center);
			radius = glm::max(radius, distance);
		}
		radius = std::ceil(radius * 16.0f) / 16.0f;
	
		Vector3<float> maxExtents = radius;
		Vector3<float> minExtents = -maxExtents;
	
		float CascadeFarPlaneOffset = 50.0f, CascadeNearPlaneOffset = -50.0f;
		const auto lightView = glm::lookAt(center + lightDir * -minExtents.z, center, Vector3<float>(0.0f, 1.0f, 0.0f));
	
		Mat4 lightProjection = glm::ortho(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f + CascadeNearPlaneOffset, maxExtents.z - minExtents.z + CascadeFarPlaneOffset);
	
		// Offset to texel space to avoid shimmering (from https://stackoverflow.com/questions/33499053/cascaded-shadow-map-shimmering)
		Mat4 shadowMatrix = lightProjection * lightView;
		Vector4<float> shadowOrigin = (shadowMatrix * Vector4<float>(0.0f, 0.0f, 0.0f, 1.0f)) * (float)SHADOW_SIZE / 2.0f;
		Vector4<float> roundedOrigin = glm::round(shadowOrigin);
		Vector4<float> roundOffset = roundedOrigin - shadowOrigin;
		roundOffset = roundOffset * 2.0f / (float)SHADOW_SIZE;
		roundOffset.z = 0.0f;
		roundOffset.w = 0.0f;
	
		lightProjection[3] += roundOffset;
	
		// Store split distance and matrix in cascade
		//cascades[i].SplitDepth = (nearClip + splitDist * clipRange) * -1.0f;
		//cascades[i].ViewProj = lightProjection * lightView;
		//cascades[i].View = lightView;
	
		splitDepth[cascadeIndex] = (nearClip + splitDist * clipRange) * -1.0f;
	
		lastSplitDist = splitDist;
	
		return lightProjection * lightView;
	}
	
	std::vector<Mat4> Renderer::ShadowPass::getLightSpaceMatrices(const Vector3<float>& lightDir)
	{
		const int SHADOW_MAP_CASCADE_COUNT = 4;
		float cascadeSplits[SHADOW_MAP_CASCADE_COUNT];
	
		float nearClip = 0.1f;
		float farClip = 1000.0f;
		float clipRange = farClip - nearClip;
	
		float minZ = nearClip;
		float maxZ = nearClip + clipRange;
	
		float range = maxZ - minZ;
		float ratio = maxZ / minZ;
	
		float CascadeSplitLambda = 0.92f;
	
		for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++) {
			float p = (i + 1) / static_cast<float>(SHADOW_MAP_CASCADE_COUNT);
			float log = minZ * std::pow(ratio, p);
			float uniform = minZ + range * p;
			float d = CascadeSplitLambda * (log - uniform) + uniform;
			cascadeSplits[i] = (d - nearClip) / clipRange;
		}
	
		cascadeSplits[3] = 0.3f;
	
		float lastSplitDist = 0.0;
	
		std::vector<Mat4> ret;
		for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++) {
			ret.push_back(getLightSpaceMatrix(lightDir, nearClip, clipRange, cascadeSplits[i], lastSplitDist, i));
		}
	
		return ret;
	}
	
	void Renderer::ShadowPass::Draw()
	{
		renderer->dynamicUbo.Bind(2, renderer->directionalLightViewsOffset, 4 * sizeof(Mat4));
		
		FrameBuffer::State state;
		state.viewport = { 0, 0, SHADOW_SIZE, SHADOW_SIZE };
		fbo->Bind(state);
		
		renderer->renderPasses[1].Execute(renderer->meshes, renderer);
		
		fbo->Unbind();
	}

	void Renderer::ColorPass::Init()
	{
		Texture2D::CreateInfo colorMapInfo;
		colorMapInfo.name = "ColorPass - color";
		colorMapInfo.w = SCREEN_SIZE.x;
		colorMapInfo.h = SCREEN_SIZE.y;
		colorMapInfo.parameters = TextureParameters().
			Format(TextureFormat::RGBA32_FLOAT);
		colorMap = Texture2D::Create(colorMapInfo);

		Texture2D::CreateInfo depthMapInfo;
		depthMapInfo.name = "ColorPass - depth";
		depthMapInfo.w = SCREEN_SIZE.x;
		depthMapInfo.h = SCREEN_SIZE.y;
		depthMapInfo.parameters = TextureParameters().
			Format(TextureFormat::D32_FLOAT).
			FilterMIN(TextureFilter::Nearest).
			FilterMAG(TextureFilter::Nearest).
			Wrap(TextureWrap::Repeat);
		depthMap = Texture2D::Create(depthMapInfo);

		FrameBuffer::CreateInfo frameBufferInfo;
		frameBufferInfo.colorAttachmentCount = 1;
		frameBufferInfo.colorAttachments[0] = colorMap;
		frameBufferInfo.depthStencilTexture = depthMap;
		fbo = FrameBuffer::Create(frameBufferInfo);
	}

	void Renderer::ColorPass::Draw()
	{
		renderer->shadowPass.texture->Bind(2);
		renderer->directionalLightUbo->Bind();

		renderer->dynamicUbo.Bind(3, renderer->sceneDataOffset, sizeof(SceneData));
		renderer->dynamicUbo.Bind(0, renderer->cameraDataOffset, sizeof(CameraData));
		renderer->dynamicUbo.Bind(2, renderer->directionalLightViewsOffset, 4 * sizeof(Mat4));
		renderer->dynamicUbo.Bind(10, renderer->cascadeLevelsOffset, sizeof(Vector4<float>));

		const auto& environment = renderer->environment;
		if (environment) {
			environment->radianceMap->Bind(0);
			environment->irradianceMap->Bind(1);
		}

		FrameBuffer::State state;
		state.viewport = { 0, 0, SCREEN_SIZE.x, SCREEN_SIZE.y };
		fbo->Bind(state);

		renderer->renderPasses[0].Execute(renderer->meshes, renderer);

		if (environment) {
			static auto skybox = Skybox();
			skybox.Draw(environment->radianceMap, *renderer);
		}

		fbo->Unbind();
	}

	void Renderer::CompositePass::Init()
	{
		//Texture2D::CreateInfo colorMapInfo;
		//colorMapInfo.name = "ColorPass - color";
		//colorMapInfo.w = SCREEN_SIZE.x;
		//colorMapInfo.h = SCREEN_SIZE.y;
		//colorMapInfo.parameters = TextureParameters().Miplevel(1);
		//colorMap = Texture2D::Create(colorMapInfo);
		//
		//FrameBufferCreateInfo frameBufferInfo;
		//frameBufferInfo.colorAttachmentCount = 1;
		//frameBufferInfo.colorAttachments[0] = colorMap;
		//frameBufferInfo.w = SCREEN_SIZE.x;
		//frameBufferInfo.h = SCREEN_SIZE.y;
		//fbo = FrameBuffer::Create(frameBufferInfo);

		auto shader = ShaderCache::getShader("SceneComposite");

		GFX::Material::CreateInfo materialInfo;
		materialInfo.shader = shader;
		materialInfo.name = "SceneComposite";
		materialInfo.materialDomain = GFX::MaterialDomain::Surface;
		materialInfo.shadingModel = GFX::ShadingModel::Lit;
		materialInfo.blendMode = GFX::BlendMode::Opaque;
		materialInfo.cullMode = CullMode::Back;
		materialInfo.drawMode = DrawMode::TriangleFan;

		material = MaterialCache::Create(materialInfo);
		material->depthState.func = DepthTest::Always;
	}

	void Renderer::CompositePass::Draw()
	{
		material->set("u_texture", renderer->colorPass.colorMap);
		//material->set("u_texture", RenderCommand::getWhiteTexture());
		material->set("u_params.exposure", 0.8f);

		//FrameBuffer::State params;
		//params.viewport.width = SCREEN_SIZE.x;
		//params.viewport.height = SCREEN_SIZE.y;
		//fbo->Bind(params);

		RenderCommand::DrawFullScreenQuad(ShaderCache::getShader("SceneComposite"), material);

		//fbo->Unbind();
	}

	Handle<RenderObject> Renderer::RegisterObject(const Submesh& submesh, const Mat4& transform)
	{
		Handle<RenderObject> handle;
		handle = Handle<RenderObject>{ (uint32_t)renderIds.size() };
		renderIds.emplace_back();

		for (auto& pass : renderPasses) {
			pass.modelTransformBuffer.push_back(transform);

			auto& renderObjects = this->renderObjects[(uint32_t)pass.type];
			Handle<RenderObject> handle;
			handle = Handle<RenderObject>{ (uint32_t)renderObjects.size() };
			RenderObject& renderable = renderObjects.emplace_back();
			renderable.meshID = getMeshHandle(submesh);
			renderable.passObjectHandles = HandleBase::INVALID_HANDLE_ID;

			if (pass.type == RenderPassType::Opaque) {
				renderable.material = submesh.material;
				//renderable.pipeline = submesh.parent->pipeline;
			} else {
				renderable.material = shadowPass.material;
				//renderable.pipeline = shadowPass.pipeline;
			}

			renderable.distanceFromCamera = glm::distance(cameraPosition, Vector3<float>(transform[3]));

			renderPasses[(uint32_t)pass.type].unbatchedObjects.push_back(handle);
		}

		return handle;
	}

	Handle<DrawMesh> Renderer::getMeshHandle(const Submesh& mesh)
	{
		if (meshesCache.contains(mesh.name)) {
			return meshesCache.at(mesh.name);
		}

		const auto handle = Handle<DrawMesh>{ (uint32_t)meshes.size() };

		meshes.emplace_back().submesh = mesh;

		meshesCache[mesh.name] = handle;
		return handle;
	}

	void Renderer::BuildPasses()
	{
		std::for_each(renderPasses.begin(), renderPasses.end(), [this](auto& pass) {
			pass.RefreshPass(renderObjects[uint32_t(pass.type)], meshes);
		});
	}

	void Renderer::PreperUniformBuffers()
	{
		SceneData sceneData;
		sceneData.viewPositionEnvironmentMapIntensity = Vector4<float>(cameraPosition.x, cameraPosition.y, cameraPosition.z, intensity);
		sceneData.directionalLightCount = directionalLights.size();
		sceneDataOffset = dynamicUbo.allocate(sceneData);

		auto viewProjection = frameData.projection * frameData.view;
		CameraData cameraData;
		cameraData.viewProjection = viewProjection;
		cameraData.view = frameData.view;
		cameraDataOffset = dynamicUbo.allocate(cameraData);

		static constexpr float cameraFarPlane = 1000.0f;
		static constexpr std::array<float, 4> shadowCascadeLevels{
			cameraFarPlane / 50.0f,
			cameraFarPlane / 25.0f,
			cameraFarPlane / 10.0f,
			cameraFarPlane / 2.0f };
		Vector4<float> cascadeLevels{ shadowCascadeLevels[0], shadowCascadeLevels[1], shadowCascadeLevels[2], shadowCascadeLevels[3] };
		cascadeLevelsOffset = dynamicUbo.allocate(cascadeLevels);

		const auto& light = directionalLights.empty() ? nullDirLight : directionalLights[0];
		shadowPass.lightViewMatrices = shadowPass.getLightSpaceMatrices(light.direction);
		directionalLightViewsOffset = dynamicUbo.allocate(shadowPass.lightViewMatrices);

		if (directionalLightUbo == nullptr) {
			directionalLightUbo = UniformBuffer::Create(sizeof(DirectionalLight) * 1, nullptr, 4, MapBit::Dynamic_Storage | MapBit::Write);
		}
		directionalLightUbo->AddData(light);
		directionalLights.clear();

		for (auto& pass : renderPasses) {
			auto& modelTransformBufferSsbo = pass.modelTransformBufferSsbo;
			auto& indirectDrawBuffer = pass.indirectDrawBuffer;
			auto& instanceIDsBuffer = pass.instanceIDsBuffer;

			auto& renderObjects = this->renderObjects[(uint32_t)pass.type];
			
			if (modelTransformBufferSsbo == nullptr) {
				modelTransformBufferSsbo = ShaderStorageBuffer::Create(sizeof(Mat4) * RenderPass::MAX_TRANSFORM_OBJECTS, nullptr, 1, MapBit::Dynamic_Storage | MapBit::Write);
			}
			modelTransformBufferSsbo->AddData(sizeof(Mat4) * pass.modelTransformBuffer.size(), pass.modelTransformBuffer.data());

			//-----------------------------------------

			std::vector<DrawElementsIndirectCommand> drawIndirectCommands(pass.instanceBatches.size());

			for (uint32_t i = 0; i < pass.instanceBatches.size(); ++i) {
				const auto& batch = pass.instanceBatches[i];

				DrawElementsIndirectCommand cmd;
				cmd.baseInstance = batch.firstInstance;
				cmd.instanceCount = 0;// batch.instanceCount;// instance count is calculated later
				cmd.firstIndex = getMesh(batch.meshID).submesh.firstIndex;
				cmd.baseVertex = getMesh(batch.meshID).submesh.baseVertex;
				cmd.count = getMesh(batch.meshID).submesh.indexCount;

				drawIndirectCommands[i] = cmd;
			}

			struct GPUInstance
			{
				uint32_t renderableID;
				uint32_t batchID;
			};
			std::vector<GPUInstance> gpuInstances(pass.flatBatches.size());
			//go through all objects in a scene. if there are more than 1 instance of this object this loop will go only over the first one
			uint32_t gpuInstanceIndex = 0;
			for (uint32_t i = 0; i < (uint32_t)pass.instanceBatches.size(); i++) {
				const auto& batch = pass.instanceBatches[i];

				for (uint32_t instanceIndex = 0; instanceIndex < batch.instanceCount; instanceIndex++) {
					gpuInstances[gpuInstanceIndex].renderableID = pass.getPassObject(pass.flatBatches[instanceIndex + batch.firstInstance].object).renderableID.getID();
					gpuInstances[gpuInstanceIndex].batchID = i;
					++gpuInstanceIndex;
				}
			}

			//TODO: this should be done by the culling compute shader
			std::vector<uint32_t> instancedObjectInstanceIDBuffer(pass.flatBatches.size());
			for (uint32_t gID = 0; gID < (uint32_t)pass.flatBatches.size(); ++gID) {
				const uint32_t objectID = gpuInstances[gID].renderableID;
			
				const uint32_t batchIndex = gpuInstances[gID].batchID;
				const uint32_t copy = drawIndirectCommands[batchIndex].instanceCount;
				const uint32_t countIndex = copy;
				drawIndirectCommands[batchIndex].instanceCount += 1;
			
				const uint32_t instanceIndex = drawIndirectCommands[batchIndex].baseInstance + countIndex;
			
				instancedObjectInstanceIDBuffer[instanceIndex] = objectID;
			}

			if (instanceIDsBuffer == nullptr) {
				instanceIDsBuffer = ShaderStorageBuffer::Create(RenderPass::MAX_TRANSFORM_OBJECTS * sizeof(uint32_t), nullptr, 15, MapBit::Write | MapBit::Dynamic_Storage);
			}
			instanceIDsBuffer->AddData(sizeof(uint32_t) * instancedObjectInstanceIDBuffer.size(), instancedObjectInstanceIDBuffer.data());

			if (indirectDrawBuffer == nullptr) {
				indirectDrawBuffer = IndirectDrawBuffer::Create(sizeof(DrawElementsIndirectCommand) * RenderPass::MAX_TRANSFORM_OBJECTS, nullptr, MapBit::Write | MapBit::Dynamic_Storage);
			}
			indirectDrawBuffer->AddData(sizeof(DrawElementsIndirectCommand) * drawIndirectCommands.size(), drawIndirectCommands.data());
		}
	}
}