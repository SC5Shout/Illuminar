#type vertex
#version 460 core

layout (location = 0) in vec3 a_position;

layout(std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
	mat4 u_view;
};

layout(std430, binding = 1) buffer Model
{
    mat4 u_model[];
};

void main()
{
	mat4 model = u_model[gl_InstanceIndex];
	vec4 position = vec4(a_position, 1.0);
	gl_Position = u_viewProjection * model * position;
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(std430, binding = 14) readonly buffer VisibleLightIndices
{
	int u_visibleLightIndices[];
};

layout(push_constant) uniform Info 
{
	uint numberOfTilesX;
	uint totalLightCount;
} u_info;

void main() 
{
	ivec2 location = ivec2(gl_FragCoord.xy);
	ivec2 tileID = location / ivec2(16, 16);
	uint index = tileID.y * u_info.numberOfTilesX + tileID.x;

	uint offset = index * 1024;
	uint i;
	for (i = 0; i < 1024 && u_visibleLightIndices[offset + i] != -1; i++);

	float ratio = float(i) / float(u_info.totalLightCount);
	f_color = vec4(vec3(ratio, ratio, ratio), 1.0);
}
