#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec3 a_tangent;
layout(location = 3) in vec3 a_binormal;
layout(location = 4) in vec2 a_uv;

layout(location = 0) out vec3 v_position;
layout(location = 1) out vec3 v_normal;
layout(location = 2) out vec2 v_uv;
layout(location = 4) out vec4 v_positionFromLight;
layout(location = 5) out mat3 v_worldNormals;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
	mat4 u_view;
};

layout (std430, binding = 1) buffer Model
{
    mat4 u_model[];
};

layout(std430, binding = 15) readonly buffer InstanceBuffer{   

	uint IDs[];
} instanceBuffer;

void main()
{
	uint index = instanceBuffer.IDs[gl_InstanceIndex];

	mat4 model = u_model[index];
	v_position = vec3(model * vec4(a_position, 1.0f));
	v_normal = mat3(model) * a_normal;
	v_uv = vec2(a_uv.x, 1.0f - a_uv.y);
	v_worldNormals = mat3(model) * mat3(a_tangent, a_binormal, a_normal);
	v_positionFromLight = vec4(v_position, 1.0);

	gl_Position = u_viewProjection * model * vec4(a_position, 1.0);
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_uv;
layout(location = 4) in vec4 v_positionFromLight;
layout(location = 5) in mat3 v_worldNormals;

layout(binding = 0) uniform samplerCube u_radianceTex;
layout(binding = 1) uniform samplerCube u_irradianceTex;
layout(binding = 2) uniform sampler2DArray u_shadowMap;

layout(binding = 3) uniform sampler2D u_baseMap;
layout(binding = 4) uniform sampler2D u_normalMap;
layout(binding = 5) uniform sampler2D u_roughnessMap;
layout(binding = 6) uniform sampler2D u_metalnessMap;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
	mat4 u_view;
};

layout(std140, binding = 3) uniform Scene
{
	vec4 viewPositionAndEnvironmentMapIntensity;
	uint tilesCountX;
	uint directionalLightCount;
	uint pointLightCount;
	uint spotLightCount;
} u_scene;

layout(push_constant) uniform Material
{
	vec4 baseColor;
	float roughness;
	float metalness;

	int hasBaseMap;
	int hasNormalMap;
	int hasRoughnessMap;
	int hasMetalnessMap;
} u_material;

struct MaterialParams
{
	vec4 baseColor;
	float roughness;
	float metalness;
} material;

struct DirectionalLight
{
	vec3 direction;
	float intensity;
	vec3 radiance;
	float lightSize;

	int nvidiaPcss;
	int softShadows;
};

layout(std140, binding = 4) uniform DirectionalLights
{
	DirectionalLight u_directionalLights[1];
};

struct PointLight
{ 
	vec3 position;
	float intensity;
	vec3 radiance;
	float fallOff;

	float radius;
	float minRadius;
};

layout(std430, binding = 5) buffer PointLights
{
	PointLight u_pointLights[];
};

layout(std430, binding = 14) readonly buffer visibleLightIndices
{
	int u_visibleLightIndices[];
};

layout (std140, binding = 2) uniform LightSpaceMatrices
{
	mat4 u_lightSpaceMatrices[4];
};

layout (std140, binding = 10) uniform CascadePlaneDistances
{ 
   vec4 cascadePlaneDistances;
};

struct SpotLight
{
	vec3 position;
	float intensity;

	vec3 radiance;
	float fallOff;

	vec3 direction;
	float radius;
	float minRadius;

	float innerCutOff;
	float outerCutOff;
};

layout(std430, binding = 6) buffer SpotLighs
{
	SpotLight u_spotLights[];
};

const float PI = 3.14159265359;
const float PI_2 = 2 * PI;
const float epsilon = 0.00001;

vec3 BRDF(vec3 V, vec3 L, vec3 N, float NdotV, vec3 F0);
vec3 CalculatePointLights(vec3 V, vec3 N, float NdotV, vec3 F0);
vec3 CalculateDirectionalLights(vec3 V, vec3 N, float NdotV, vec3 F0);
vec3 CalculateSpotLights(vec3 V, vec3 N, float NdotV, vec3 F0);

vec3 IBL(vec3 N, float NdotV, vec3 view, vec3 F0, vec3 Lr);

float ShadowFade = 1.0;

float getShadowBias(vec3 N, int cascadeLayer, float modifier, float farPlane);
float HardDirectionalShadows(vec3 shadowCoords, float bias);

float PCF_DirectionalLight(vec3 shadowCoords, float bias, float uvRadius);
float NV_PCF_DirectionalLight(vec3 shadowCoords, float bias, float uvRadius);
float PCSS_DirectionalLight(vec3 shadowCoords, float bias, float uvLightSize);

int CalculateShadowCascadeLayer(vec3 fragPosWorldSpace);

int cascadeCount = 4;
float farPlane = 1000.0f;
float ShadowCalculation(vec3 fragPosWorldSpace, vec3 N)
{
    int layer = CalculateShadowCascadeLayer(fragPosWorldSpace);

    vec4 fragPosLightSpace = u_lightSpaceMatrices[layer] * vec4(fragPosWorldSpace, 1.0);
    vec3 projCoords = (fragPosLightSpace.xyz / fragPosLightSpace.w) * 0.5 + 0.5;

    float currentDepth = projCoords.z;

    if (currentDepth > 1.0) {
        return 0.0;
    }

    const float biasModifier = 0.5f;
	float bias = getShadowBias(N, layer, biasModifier, farPlane);

    DirectionalLight dirLight = u_directionalLights[0];
	bool softShadow = bool(dirLight.softShadows);
	bool nvidiaPcss = bool(dirLight.nvidiaPcss);
    float shadow = 1.0f;
	if(softShadow) {
		if(nvidiaPcss) {
			shadow = NV_PCF_DirectionalLight(projCoords, bias, dirLight.lightSize);
		} else shadow = PCSS_DirectionalLight(projCoords, bias, dirLight.lightSize);
	} else shadow = HardDirectionalShadows(projCoords, bias);
        
    return shadow;
}

void main()
{	
	const int hasBaseMap = u_material.hasBaseMap;
	const int hasNormalMap = u_material.hasNormalMap;
	const int hasRoughnessMap = u_material.hasRoughnessMap;
	const int hasMetalnessMap = u_material.hasMetalnessMap;

	material.baseColor = hasBaseMap == 0 ? vec4(u_material.baseColor.rgb, 1.0f) : texture(u_baseMap, v_uv) * vec4(u_material.baseColor.rgb, 1.0f);
	material.roughness = hasRoughnessMap == 0 ? u_material.roughness : texture(u_roughnessMap, v_uv).r * u_material.roughness;
	material.roughness = max(material.roughness, 0.05f);
	material.metalness = hasMetalnessMap == 0 ? u_material.metalness : texture(u_metalnessMap, v_uv).r * u_material.metalness;
	
	vec3 F0 = vec3(0.04f);
	F0 = mix(F0, material.baseColor.rgb, material.metalness);
	
	vec3 N = normalize(v_normal);
	if(hasNormalMap == 1) {
		N = normalize(texture(u_normalMap, v_uv).rgb * 2.0f - 1.0f);
		N = normalize(v_worldNormals * N);
	}
	
	vec3 V = normalize(vec3(u_scene.viewPositionAndEnvironmentMapIntensity.xyz) - v_position);
	float NdotV = max(0.0f, dot(N, V));
	
	vec3 Lr = 2.0 * NdotV * N - V;
	
	float shadow = ShadowCalculation(v_position, N);

	vec3 lightContribution = CalculateDirectionalLights(V, N, NdotV, F0) * shadow;
	lightContribution += CalculatePointLights(V, N, NdotV, F0);
	lightContribution += CalculateSpotLights(V, N, NdotV, F0);
	vec3 iblContribution = IBL(N, NdotV, V, F0, Lr) * u_scene.viewPositionAndEnvironmentMapIntensity.a;
	
	//vec3 lightContribution = texture(u_baseMap, v_uv).rgb;
	
	float alpha = texture(u_baseMap, v_uv).a;
	f_color = vec4(lightContribution + iblContribution, alpha);
}

vec3 CalculateDirectionalLights(vec3 V, vec3 N, float NdotV, vec3 F0)
{
	vec3 result = vec3(0.0f);

	for(uint i = 0; i < u_scene.directionalLightCount; ++i) {
		DirectionalLight light = u_directionalLights[i];

		vec3 L = light.direction;
		vec3 radiance = light.radiance * light.intensity;

		result += BRDF(V, L, N, NdotV, F0) * radiance;
	}

	return result;
}

int getLightBufferIndex(int i)
{
	ivec2 tileID = ivec2(gl_FragCoord) / ivec2(16, 16);
	uint index = tileID.y * u_scene.tilesCountX + tileID.x;

	uint offset = index * 1024;
	return u_visibleLightIndices[offset + i];
}

int getPointLightCount()
{
	int result = 0;
	for (int i = 0; i < u_scene.pointLightCount; i++)
	{
		uint lightIndex = getLightBufferIndex(i);
		if (lightIndex == -1)
			break;

		result++;
	}

	return result;
}

vec3 CalculatePointLights(vec3 V, vec3 N, float NdotV, vec3 F0)
{
	vec3 result = vec3(0);

	for (int i = 0; i < u_scene.pointLightCount; i++) {
		uint lightIndex = getLightBufferIndex(i);
		if (lightIndex == -1)
			break;

		PointLight light = u_pointLights[lightIndex];

		vec3 posToLight = light.position - v_position;
		vec3 L = normalize(posToLight);
		float lightDistance = length(light.position - v_position);

		float attenuation = clamp(1.0 - (lightDistance * lightDistance) / (light.radius * light.radius), 0.0, 1.0);
		attenuation *= mix(attenuation, 1.0, light.fallOff);

		vec3 radiance = light.radiance * light.intensity * attenuation;

		result += BRDF(V, L, N, NdotV, F0) * radiance;
	}

	return result;
}

#define saturate(x) clamp(x, 0.0f, 1.0f);

float getSquareFalloffAttenuation(float distanceSquare, float falloff) 
{
    float factor = distanceSquare * falloff;
    float smoothFactor = saturate(1.0 - factor * factor);
    // We would normally divide by the square distance here
    // but we do it at the call site
    return smoothFactor * smoothFactor;
}

float getDistanceAttenuation(const highp vec3 posToLight, float falloff) 
{
    float distanceSquare = dot(posToLight, posToLight);
    float attenuation = getSquareFalloffAttenuation(distanceSquare, falloff);
    // Assume a punctual light occupies a volume of 1cm to avoid a division by 0
    return attenuation * 1.0 / max(distanceSquare, 1e-4);
}

float getAngleAttenuation(const vec3 lightDir, const vec3 l, const vec2 scaleOffset) {
    float cd = dot(lightDir, l);
    float attenuation = saturate(cd * scaleOffset.x + scaleOffset.y);
    return attenuation * attenuation;
}

vec3 CalculateSpotLights(vec3 V, vec3 N, float NdotV, vec3 F0)
{
	vec3 result = vec3(0);

	for(uint i = 0; i < u_scene.spotLightCount; ++i) {
		SpotLight light = u_spotLights[i];

		vec3 posToLight = light.position - v_position;
		vec3 L = normalize(posToLight);

		float lightDistance = length(light.position - v_position);
		float attenuation = clamp(1.0 - (lightDistance * lightDistance) / (light.radius * light.radius), 0.0, 1.0);
		attenuation *= mix(attenuation, 1.0, light.fallOff);

 		float theta = dot(L, light.direction); 
    	float epsilon = (light.innerCutOff - light.outerCutOff);
    	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

		vec3 radiance = light.radiance * light.intensity * attenuation;

		result += BRDF(V, L, N, NdotV, F0) * radiance * intensity;
	}

	return result;
}

float NDFGGX(float NdotH, float roughness)
{
	float alpha = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (NdotH * NdotH) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

float gaSchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

float gaSchlickGGX(float NdotL, float NdotV, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0;
	return gaSchlickG1(NdotL, k) * gaSchlickG1(NdotV, k);
}

vec3 FresnelSchlick(vec3 F0, float cosTheta)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 FresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness)
{
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 BRDF(vec3 V, vec3 L, vec3 N, float NdotV, vec3 F0)
{
	vec3 H = normalize(L + V);

	float NdotL = max(0.0f, dot(N, L));
	float NdotH = max(0.0f, dot(N, H));

	vec3 F = FresnelSchlickRoughness(F0, max(0.0, dot(H, V)), material.roughness);
	float D = NDFGGX(NdotH, material.roughness);
	float G = gaSchlickGGX(NdotL, NdotV, material.roughness);

	vec3 kd = (1.0f - F) * (1.0f - material.metalness);
	vec3 diffuse = kd * material.baseColor.rgb;

	vec3 specular = (F * D * G) / max(epsilon, 4.0 * NdotL * NdotV);
	specular = clamp(specular, vec3(0.0f), vec3(10.0f));

	return (diffuse + specular) * NdotL;
}

vec3 RotateVectorAboutY(float angle, vec3 vec)
{
	angle = radians(angle);
	mat3x3 rotationMatrix = { vec3(cos(angle),0.0,sin(angle)),
							vec3(0.0,1.0,0.0),
							vec3(-sin(angle),0.0,cos(angle)) };
	return rotationMatrix * vec;
}

float Convert_sRGB_FromLinear(float theLinearValue)
{
	return theLinearValue <= 0.0031308f
		? theLinearValue * 12.92f
		: pow(theLinearValue, 1.0f / 2.4f) * 1.055f - 0.055f;
}

vec3 IBL(vec3 N, float NdotV, vec3 view, vec3 F0, vec3 Lr)
{
	vec3 irradiance = texture(u_irradianceTex, N).rgb;
	vec3 F = FresnelSchlickRoughness(F0, NdotV, material.roughness);
	vec3 kd = (1.0 - F) * (1.0 - material.metalness);
	vec3 diffuseIBL = material.baseColor.rgb * irradiance;

	int envRadianceTexLevels = textureQueryLevels(u_radianceTex);
	float NoV = clamp(NdotV, 0.0, 1.0);
	vec3 R = 2.0 * dot(view, N) * N - view;
	vec3 specularIrradiance = textureLod(u_radianceTex, RotateVectorAboutY(0.0f, Lr), (material.roughness) * envRadianceTexLevels).rgb;
	//vec3 specularIrradiance = texture(u_radianceTex, RotateVectorAboutY(0.0f, Lr)).rgb;
	specularIrradiance = vec3(Convert_sRGB_FromLinear(specularIrradiance.r), Convert_sRGB_FromLinear(specularIrradiance.g), Convert_sRGB_FromLinear(specularIrradiance.b));

	vec2 specularBRDF = vec2(1.0f, 0.0f);//texture(u_BRDFLUTTexture, vec2(NdotV, 1.0 - material.roughness)).rg;
	vec3 specularIBL = specularIrradiance * (F0 * specularBRDF.x + specularBRDF.y);

	return kd * diffuseIBL + specularIBL;
}

float getShadowBias(vec3 N, int cascadeLayer, float modifier, float farPlane)
{
	DirectionalLight dirLight = u_directionalLights[0];
	const float MINIMUM_SHADOW_BIAS = 0.002;
	float bias = max(MINIMUM_SHADOW_BIAS * (1.0 - dot(N, dirLight.direction)), MINIMUM_SHADOW_BIAS);

	if (cascadeLayer == cascadeCount) {
        bias *= 1 / (farPlane * modifier);
    } else {
        bias *= 1 / (cascadePlaneDistances[cascadeLayer] * modifier);
    }

	return bias;
}

float HardDirectionalShadows(vec3 shadowCoords, float bias)
{
	float shadowMapDepth = texture(u_shadowMap, vec3(shadowCoords.x, shadowCoords.y, 0.0f)).r;
	return step(shadowCoords.z, shadowMapDepth + bias) * ShadowFade;
}

float SearchRegionRadiusUV(float zWorld)
{
	const float light_zNear = 0.0;
	const float lightRadiusUV = 0.05;
	return lightRadiusUV * (zWorld - light_zNear) / zWorld;
}

const vec2 PoissonDistribution[64] = vec2[](
	vec2(-0.884081, 0.124488),
	vec2(-0.714377, 0.027940),
	vec2(-0.747945, 0.227922),
	vec2(-0.939609, 0.243634),
	vec2(-0.985465, 0.045534),
	vec2(-0.861367, -0.136222),
	vec2(-0.881934, 0.396908),
	vec2(-0.466938, 0.014526),
	vec2(-0.558207, 0.212662),
	vec2(-0.578447, -0.095822),
	vec2(-0.740266, -0.095631),
	vec2(-0.751681, 0.472604),
	vec2(-0.553147, -0.243177),
	vec2(-0.674762, -0.330730),
	vec2(-0.402765, -0.122087),
	vec2(-0.319776, -0.312166),
	vec2(-0.413923, -0.439757),
	vec2(-0.979153, -0.201245),
	vec2(-0.865579, -0.288695),
	vec2(-0.243704, -0.186378),
	vec2(-0.294920, -0.055748),
	vec2(-0.604452, -0.544251),
	vec2(-0.418056, -0.587679),
	vec2(-0.549156, -0.415877),
	vec2(-0.238080, -0.611761),
	vec2(-0.267004, -0.459702),
	vec2(-0.100006, -0.229116),
	vec2(-0.101928, -0.380382),
	vec2(-0.681467, -0.700773),
	vec2(-0.763488, -0.543386),
	vec2(-0.549030, -0.750749),
	vec2(-0.809045, -0.408738),
	vec2(-0.388134, -0.773448),
	vec2(-0.429392, -0.894892),
	vec2(-0.131597, 0.065058),
	vec2(-0.275002, 0.102922),
	vec2(-0.106117, -0.068327),
	vec2(-0.294586, -0.891515),
	vec2(-0.629418, 0.379387),
	vec2(-0.407257, 0.339748),
	vec2(0.071650, -0.384284),
	vec2(0.022018, -0.263793),
	vec2(0.003879, -0.136073),
	vec2(-0.137533, -0.767844),
	vec2(-0.050874, -0.906068),
	vec2(0.114133, -0.070053),
	vec2(0.163314, -0.217231),
	vec2(-0.100262, -0.587992),
	vec2(-0.004942, 0.125368),
	vec2(0.035302, -0.619310),
	vec2(0.195646, -0.459022),
	vec2(0.303969, -0.346362),
	vec2(-0.678118, 0.685099),
	vec2(-0.628418, 0.507978),
	vec2(-0.508473, 0.458753),
	vec2(0.032134, -0.782030),
	vec2(0.122595, 0.280353),
	vec2(-0.043643, 0.312119),
	vec2(0.132993, 0.085170),
	vec2(-0.192106, 0.285848),
	vec2(0.183621, -0.713242),
	vec2(0.265220, -0.596716),
	vec2(-0.009628, -0.483058),
	vec2(-0.018516, 0.435703)
);

vec2 SamplePoisson(int index)
{
	return PoissonDistribution[index % 64];
}

float FindBlockerDistance_DirectionalLight(vec3 shadowCoords, float bias, float uvLightSize)
{
	int numBlockerSearchSamples = 64;
	int blockers = 0;
	float avgBlockerDistance = 0;

	float searchWidth = SearchRegionRadiusUV(shadowCoords.z);
	for (int i = 0; i < numBlockerSearchSamples; i++) {
		float z = texture(u_shadowMap, vec3(shadowCoords.xy + SamplePoisson(i) * searchWidth, 0.0f)).r;
		if (z < (shadowCoords.z - bias)) {
			blockers++;
			avgBlockerDistance += z;
		}
	}

	if (blockers > 0)
		return avgBlockerDistance / float(blockers);

	return -1;
}

float PCF_DirectionalLight(vec3 shadowCoords, float bias, float uvRadius)
{
	int numPCFSamples = 64;

	float sum = 0;
	for (int i = 0; i < numPCFSamples; i++) {
		vec2 offset = SamplePoisson(i) * uvRadius;
		float z = texture(u_shadowMap, vec3(shadowCoords.xy + offset, 0.0f)).r;
		sum += step(shadowCoords.z - bias, z);
	}
	return sum / numPCFSamples;
}

const vec2 poissonDisk[16] = vec2[](
	vec2(-0.94201624, -0.39906216),
	vec2(0.94558609, -0.76890725),
	vec2(-0.094184101, -0.92938870),
	vec2(0.34495938, 0.29387760),
	vec2(-0.91588581, 0.45771432),
	vec2(-0.81544232, -0.87912464),
	vec2(-0.38277543, 0.27676845),
	vec2(0.97484398, 0.75648379),
	vec2(0.44323325, -0.97511554),
	vec2(0.53742981, -0.47373420),
	vec2(-0.26496911, -0.41893023),
	vec2(0.79197514, 0.19090188),
	vec2(-0.24188840, 0.99706507),
	vec2(-0.81409955, 0.91437590),
	vec2(0.19984126, 0.78641367),
	vec2(0.14383161, -0.14100790)
);

float NV_PCF_DirectionalLight(vec3 shadowCoords, float bias, float uvRadius)
{
	float sum = 0;
	for (int i = 0; i < 16; i++) {
		vec2 offset = poissonDisk[i] * uvRadius;
		float z = texture(u_shadowMap, vec3(shadowCoords.xy + offset, 0.0f)).r;
		sum += step(shadowCoords.z - bias, z);
	}
	return sum / 16.0f;
}

float PCSS_DirectionalLight(vec3 shadowCoords, float bias, float uvLightSize)
{
	float blockerDistance = FindBlockerDistance_DirectionalLight(shadowCoords, bias, uvLightSize);
	if (blockerDistance == -1)
		return 1.0f;

	float penumbraWidth = (shadowCoords.z - blockerDistance) / blockerDistance;

	float NEAR = 0.01;
	float uvRadius = penumbraWidth * uvLightSize * NEAR / shadowCoords.z;
	uvRadius = min(uvRadius, 0.002f);
	return PCF_DirectionalLight(shadowCoords, bias, uvRadius) * ShadowFade;
}

int CalculateShadowCascadeLayer(vec3 fragPosWorldSpace)
{
    vec4 fragPosViewSpace = u_view * vec4(fragPosWorldSpace, 1.0);
    float depthValue = abs(fragPosViewSpace.z);

    int layer = -1; 
    for (int i = 0; i < cascadeCount; ++i) {
        if (depthValue < cascadePlaneDistances[i]) {
            layer = i;
            break;
        }
    }

    if (layer == -1) {
        layer = cascadeCount;
    }

	return layer;
}