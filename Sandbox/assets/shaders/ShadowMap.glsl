#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;

layout (std430, binding = 1) buffer Model
{
    mat4 u_model[];
};

layout(std430, binding = 15) readonly buffer InstanceBuffer{   
	uint IDs[];
} instanceBuffer;

void main()
{
    uint index = instanceBuffer.IDs[gl_InstanceIndex];
    vec4 localPosition = vec4(a_position, 1.0f);

	mat4 model = u_model[index];
	gl_Position = model * localPosition;
}

#type geometry
#version 460 core
    
layout(triangles, invocations = 5) in;
layout(triangle_strip, max_vertices = 3) out;
    
layout (std140, binding = 2) uniform LightSpaceMatrices
{
	mat4 u_lightSpaceMatrices[4];
};

void main()
{          
    for (int i = 0; i < 3; ++i) {
        gl_Position = u_lightSpaceMatrices[0] * gl_in[i].gl_Position;
        gl_Layer = gl_InvocationID;
        EmitVertex();
    }
    EndPrimitive();
} 

#type fragment
#version 460 core

void main()
{
}