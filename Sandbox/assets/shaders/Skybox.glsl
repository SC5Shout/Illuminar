#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;

layout(location = 0) out vec3 v_position;

layout(std140, binding = 0) uniform Camera
{
    mat4 viewProjection;
	mat4 view;
};

void main()
{
	vec4 position = vec4(a_position.xy, 1.0, 1.0);
	gl_Position = position;

	v_position = (inverse(viewProjection) * position).xyz;
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec3 v_position;

layout(binding = 0) uniform samplerCube u_skybox;

layout (push_constant) uniform Parameters
{
	float textureLod;
	float intensity;
} u_parameters;

void main()
{
	f_color = textureLod(u_skybox, v_position, u_parameters.textureLod) * u_parameters.intensity;
}