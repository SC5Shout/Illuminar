#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;

layout(push_constant) uniform Model
{
    mat4 model;
} u_model;

void main()
{
    gl_Position = u_model.model * vec4(a_position, 1.0);
}

#type geometry

#version 460 core

layout(triangles, invocations = 5) in;
layout(triangle_strip, max_vertices = 3) out;

layout (std140, binding = 1) uniform LightSpaceMatrices
{
    mat4 lightSpaceMatrices[16];
};

void main()
{          
	for (int i = 0; i < 3; ++i) {
		gl_Position = lightSpaceMatrices[gl_InvocationID] * gl_in[i].gl_Position;
		gl_Layer = gl_InvocationID;
		EmitVertex();
	}
	EndPrimitive();
}  

#type fragment
#version 460 core

void main()
{
}