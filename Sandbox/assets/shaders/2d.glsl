#type vertex
#version 450 core

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec2 a_uv;
layout(location = 2) in vec4 a_color;
layout(location = 3) in float a_textureIndex;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
};

layout(location = 0) out vec2 v_uv;
layout(location = 1) out vec4 v_color;

layout(location = 2) out flat float v_textureIndex;

void main()
{
    v_uv = vec2(a_uv.x, a_uv.y);
    v_color = a_color;   
    v_textureIndex = a_textureIndex;
    //pvm

    gl_Position = u_viewProjection * vec4(a_position, 0.0f, 1.0f);
}

#type fragment
#version 450 core
//#extension GL_ARB_bindless_texture : require

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec2 v_uv;
layout(location = 1) in vec4 v_color;
layout(location = 2) in flat float v_textureIndex;

layout(binding = 0) uniform sampler2D u_texture[32];

void main()
{ 
    if(v_textureIndex >= 1.0f) {
      int textureIndex = int(v_textureIndex - 0.5f);
      f_color = texture(u_texture[textureIndex], v_uv) * v_color;
    } else f_color = v_color;
}