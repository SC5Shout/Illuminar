#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;

layout(std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
	mat4 u_view;
};

layout(std430, binding = 1) buffer Model
{
    mat4 u_model[];
};

void main()
{
	mat4 model = u_model[gl_InstanceIndex];
	vec4 position = vec4(a_position, 1.0);
	gl_Position = u_viewProjection * model * position;
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

float near_plane = 0.001f;
float far_plane = 10000.0f;
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC 
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));	
}

void main()
{             
    f_color = vec4(vec3(LinearizeDepth(gl_FragCoord.z) / far_plane), 1.0);
}