#type vertex
#version 450 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec4 a_color;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
};

layout(location = 0) out vec4 v_color;

void main()
{
  v_color = a_color;   
  gl_Position = u_viewProjection * vec4(a_position, 1.0f);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec4 v_color;

void main()
{ 
  f_color = v_color;
}