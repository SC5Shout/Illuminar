#type vertex
#version 460 core

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec2 a_uv;

layout(location = 0) out vec2 v_uv;

void main()
{
    v_uv = a_uv;

	vec4 position = vec4(a_position, 0.0, 1.0);
	gl_Position = position;
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec2 v_uv;

layout(binding = 0) uniform sampler2DArray u_texture;

float near_plane = 0.01f;
float far_plane = 1000.0f;
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC 
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));	
}

layout(push_constant) uniform Options
{
	float drawDepth;
} u_options;

void main()
{             
    float depthValue = texture(u_texture, vec3(v_uv.x, v_uv.y, 0.0f)).r;
    if(u_options.drawDepth == 0.0f) {
        f_color = vec4(texture(u_texture, vec3(v_uv.x, v_uv.y, 0.0f)).rgb, 1.0);
    } else if(u_options.drawDepth == 1.0f) {
        f_color = vec4(vec3(depthValue), 1.0); // orthographic
    } else if(u_options.drawDepth == 2.0f) {
        f_color = vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0); // perspective
    }
}