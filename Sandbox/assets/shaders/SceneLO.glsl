#type vertex
#version 460 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_uv;

layout(location = 0) out vec3 v_position;
layout(location = 1) out vec3 v_normal;
layout(location = 2) out vec2 v_uv;

layout (std140, binding = 0) uniform Camera
{
	mat4 u_viewProjection;
};

layout(push_constant) uniform Model
{
    mat4 model;
} u_model;

void main()
{
    mat4 model = u_model.model;
    v_position = vec3(model * vec4(a_position, 1.0f));
	//v_normal = mat3(model) * a_normal;
    v_normal = transpose(inverse(mat3(model))) * a_normal;
	v_uv = vec2(a_uv.x, 1.0f - a_uv.y);
	gl_Position = u_viewProjection * model * vec4(a_position, 1.0);
}

#type fragment
#version 460 core

layout(location = 0) out vec4 f_color;

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_uv;

layout(binding = 0) uniform sampler2D u_baseMap;
layout(binding = 1) uniform sampler2DArray u_shadowMap;

layout (push_constant) uniform Frame
{
	mat4 view;
    vec3 lightDir;
    vec3 viewPos;
    float farPlane;
    int cascadeCount;
} frame;

layout (std140, binding = 1) uniform LightSpaceMatrices
{
    mat4 lightSpaceMatrices[16];
};

layout (std140, binding = 2) uniform CascadePlaneDistances
{ 
   //float cascadePlaneDistances[4];
   vec4 cascadePlaneDistances;
};

float ShadowCalculation(vec3 fragPosWorldSpace)
{
    vec4 fragPosViewSpace = frame.view * vec4(fragPosWorldSpace, 1.0);
    float depthValue = abs(fragPosViewSpace.z);

    int layer = -1; 
    for (int i = 0; i < frame.cascadeCount; ++i) {
        if (depthValue < cascadePlaneDistances[i]) {
            layer = i;
            break;
        }
    }

    if (layer == -1) {
        layer = frame.cascadeCount;
    }

    vec4 fragPosLightSpace = lightSpaceMatrices[layer] * vec4(fragPosWorldSpace, 1.0);
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;

    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if (currentDepth > 1.0) {
        return 0.0;
    }
    // calculate bias (based on depth map resolution and slope)
    vec3 normal = normalize(v_normal);
    float bias = max(0.05 * (1.0 - dot(normal, frame.lightDir)), 0.005);
    const float biasModifier = 0.5f;
    if (layer == frame.cascadeCount) {
        bias *= 1 / (frame.farPlane * biasModifier);
    } else {
        bias *= 1 / (cascadePlaneDistances[layer] * biasModifier);
    }

    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / vec2(textureSize(u_shadowMap, 0));
    for(int x = -1; x <= 1; ++x) {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(u_shadowMap, vec3(projCoords.xy + vec2(x, y) * texelSize, layer)).r;
            shadow += (currentDepth - bias) > pcfDepth ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
        
    return shadow;
}

void main()
{           
    vec3 color = texture(u_baseMap, v_uv).rgb;
    vec3 normal = normalize(v_normal);
    vec3 lightColor = vec3(0.3);
    // ambient
    vec3 ambient = 0.3 * color;
    // diffuse
    float diff = max(dot(frame.lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    // specular
    vec3 viewDir = normalize(frame.viewPos - v_position);
    vec3 reflectDir = reflect(-frame.lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(frame.lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;    
    // calculate shadow
    float shadow = ShadowCalculation(v_position);                      
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;    
    
    f_color = vec4(lighting, 1.0);
}