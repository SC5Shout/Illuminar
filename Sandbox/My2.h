#pragma once

#include "Scene.h"

using namespace Illuminar;
using namespace Illuminar::GFX;

namespace My2 {
	struct MainTest
	{
		MainTest()
		{
			auto& manager = ResourceManagers::get().meshResourceManager;
			auto scene = v2::Scene::Create();

			MeshResourceID sponzaID = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
			MeshResourceID testSceneID = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
			ResourceStreamer::FlushAllQueues();
			auto sponzaEntity = scene->CreateMeshEntity("sponza", sponzaID);

			scene->UpdateEditor(0, {}, Mat4(1.0f));

			scene->DestroyEntity(sponzaEntity);

			scene->CreateMeshEntity("TestScene", testSceneID);
			scene->UpdateEditor(0, {}, Mat4(1.0f));
		}
	
		//MainTest()
		//{
		//	auto& manager = ResourceManagers::get().meshResourceManager;
		//	MeshResourceID testSceneID = manager.LoadMeshResourceByAssetID("assets/TestScene.fbx");
		//	ResourceStreamer::FlushAllQueues();
		//
		//	MeshResource* testScene = manager.tryGetResourceByID(testSceneID);
		//	auto testSceneMesh = testScene->mesh;
		//
		//	renderer.RegisterBatch(testSceneMesh->submeshes.data(), testSceneMesh->submeshes.size());
		//	renderer.RegisterBatch(testSceneMesh->submeshes.data(), testSceneMesh->submeshes.size());
		//	
		//	MeshResourceID sponzaID = manager.LoadMeshResourceByAssetID("assets/Sponza/Sponza.gltf");
		//	ResourceStreamer::FlushAllQueues();
		//	MeshResource* sponza = manager.tryGetResourceByID(sponzaID);
		//	auto sponzaMesh = sponza->mesh;
		//
		//	auto sponza1 = renderer.RegisterBatch(sponzaMesh->submeshes.data(), sponzaMesh->submeshes.size());
		//	renderer.RegisterBatch(sponzaMesh->submeshes.data(), sponzaMesh->submeshes.size());
		//
		//	renderer.BuildPasses();
		//	renderer.ExecutePasses();
		//
		//	renderer.UnregisterBatch(sponza1);
		//
		//	renderer.BuildPasses();
		//	renderer.ExecutePasses();
		//}
		//
		//v2::Renderer renderer;
	};
}