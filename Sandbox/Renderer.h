#pragma once

#include "RenderPass.h"

using namespace Illuminar;

namespace v2 {
	struct Renderer
	{
		constexpr static Vector2<uint32_t> SCREEN_SIZE{ 1600, 900 };
		Renderer()
		{
			renderPasses[0].type = RenderPassType::Opaque;
			renderPasses[1].type = RenderPassType::Shadow;

			shadowPass.Init();
			colorPass.Init();
			compositePass.Init();
		}

		void Begin(const Camera& camera, const Mat4& view);
		void End();

		Handle<RenderObject> RegisterObject(const Submesh& submesh, const Mat4& transform);
		//void UnregisterObject(Handle<RenderObject> renderObject);

		//void UpdateObject(Handle<RenderObject> objectID);

		void BuildPasses();
		void PreperUniformBuffers();

	//private:
		friend struct Skybox;

		struct ShadowPass
		{
			ShadowPass(Renderer* renderer)
				: renderer(renderer)
			{
			}
		
			void Init();
			void Draw();
		
			Mat4 getLightSpaceMatrix(const Vector3<float>& lightDir, float nearClip, float clipRange, float splitDist, float& lastSplitDist, uint32_t cascadeIndex);
			std::vector<Mat4> getLightSpaceMatrices(const Vector3<float>& lightDir);
		
			Vector4<float> splitDepth = 0.0f;
		
			std::vector<Mat4> lightViewMatrices;
		
			static constexpr uint32_t CASCADE_COUNT = 4;
			static constexpr uint32_t SHADOW_SIZE = 4096;
			//Ref<GraphicsPipeline> pipeline = nullptr;
			Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> texture = nullptr;
			Ref<Material> material = nullptr;
		
			Renderer* renderer = nullptr;
		
			uint32_t lightViewOffset = 0;
		} shadowPass{ this };

		struct ColorPass
		{
			ColorPass(Renderer* renderer)
				: renderer(renderer)
			{
			}

			void Init();
			void Draw();

			Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> colorMap = nullptr;
			Ref<Texture> depthMap = nullptr;

			Renderer* renderer = nullptr;
		} colorPass{ this };

		struct CompositePass
		{
			CompositePass(Renderer* renderer)
				: renderer(renderer)
			{
			}

			void Init();
			void Draw();

			Ref<Material> material = nullptr;

			//Ref<FrameBuffer> fbo = nullptr;
			Ref<Texture> colorMap = nullptr;

			Renderer* renderer = nullptr;
		} compositePass{ this };

		struct FrameData
		{
			Mat4 projection = Mat4(1.0f);
			Mat4 view = Mat4(1.0f);
		} frameData;

		struct CameraData
		{
			Mat4 viewProjection = Mat4(1.0f);
			Mat4 view = Mat4(1.0f);
		};

		struct SceneData
		{
			Vector4<float> viewPositionEnvironmentMapIntensity;
			uint32_t tilesCountX = 0;

			uint32_t directionalLightCount = 0;
			uint32_t pointLightCount = 0;
			uint32_t spotLightCount = 0;
		} sceneData;

		struct DirectionalLight
		{
			Vector3<float> direction;
			float intensity;
			Vector3<float> radiance;
			float lightSize;

			int nvidiaPcss;
			int softShadows;

			float padding[2];
		};

		Handle<DrawMesh> getMeshHandle(const Submesh& mesh);

		const DrawMesh& getMesh(Handle<DrawMesh> handle) const { return meshes[handle.getID()]; }
		DrawMesh& getMesh(Handle<DrawMesh> handle) { return meshes[handle.getID()]; }

		//const RenderObject& getRenderable(Handle<RenderObject> handle) const { return renderObjects[handle.getID()]; }
		//RenderObject& getRenderable(Handle<RenderObject> handle) { return renderObjects[handle.getID()]; }

		DirectionalLight nullDirLight;
		std::vector<DirectionalLight> directionalLights;
		Ref<UniformBuffer> directionalLightUbo = nullptr;

		uint32_t sceneDataOffset = 0;
		uint32_t cameraDataOffset = 0;
		uint32_t lightCameraOffset = 0;
		uint32_t directionalLightViewsOffset = 0;
		uint32_t cascadeLevelsOffset = 0;

		GPULinearAllocator<BufferType::Uniform, 1_MB> dynamicUbo;

		std::shared_ptr<Environment> environment = nullptr;
		float intensity = 1.0f;

		Vector3<float> cameraPosition = 0.0f;

		std::array<RenderPass, 2> renderPasses;
		std::array<std::vector<RenderObject>, 2> renderObjects;
		std::vector<Handle<RenderObject>> renderIds;

		std::vector<DrawMesh> meshes;

		std::unordered_map<std::string, Handle<DrawMesh>> meshesCache;
	};
}