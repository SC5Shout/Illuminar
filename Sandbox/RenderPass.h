#pragma once

#include "GraphicsEngine/Illuminar_GFX.h"
#include "GameEngine/Resource/Illuminar_ResourceManagers.h"
#include "GameEngine/Resource/Illuminar_ResourceStreamer.h"
#include "Utility/Illuminar_Handle.h"
#include "Math/Illuminar_Math.h"
#include "Memory/Illuminar_Ref.h"

#include "GraphicsEngine/Illuminar_GraphicsPipelineCache.h"

using namespace Illuminar;
using namespace Illuminar::GFX;

namespace v2 {
	struct Renderer;

	[[nodiscard]] inline constexpr uint32_t FloatFlip(uint32_t f)
	{
		const uint32_t mask = static_cast<uint32_t>(-int(f >> 31) | 0x80000000);
		return (f ^ mask);
	}

	[[nodiscard]] inline constexpr uint32_t DepthToBits(float depth, uint32_t depthBits)
	{
		union { float f; uint32_t i; } f2i;
		f2i.f = depth;
		f2i.i = FloatFlip(f2i.i);
		return (f2i.i >> (32 - depthBits));
	}

	struct DrawMesh {
		Submesh submesh;
	};

	struct PassObject;
	struct RenderObject
	{
		Handle<PassObject> passObjectHandles;
		Ref<Material> material = nullptr;
		//Ref<GraphicsPipeline> pipeline = nullptr;

		Handle<DrawMesh> meshID;
		//Handle<RenderObject> thisHandle;
		uint32_t customKey = 0;
		uint32_t updateIndex = -1;
		float distanceFromCamera = 0.0f;
	};

	struct InstanceBatch
	{
		Handle<DrawMesh> meshID;
		Ref<Material> material = nullptr;
		uint32_t firstInstance = 0;
		uint32_t instanceCount = 0;
	};

	struct PassObject
	{
		//Ref<GraphicsPipeline> pipeline = nullptr;
		Ref<Material> material = nullptr;
		Handle<DrawMesh> meshID;
		Handle<RenderObject> renderableID;
		uint32_t customKey;
		uint32_t quantizedDepth;
	};

	struct RenderBatch
	{
		RenderBatch() = default;
		RenderBatch(uint64_t key)
			: sortKey(key) {}
		Handle<PassObject> object;
		uint64_t sortKey = 0;

		operator uint64_t() const { return sortKey; }

		bool operator==(const RenderBatch& other) const
		{
			return object == other.object && sortKey == other.sortKey;
		}
	};

	struct MultiDrawIndirectBatch
	{
		uint64_t first = 0;
		uint32_t count = 0;
	};

	enum struct RenderPassType
	{
		Opaque,
		Shadow
	};

	struct RenderPass
	{
		template<typename T>
		static uint64_t MakeKey(T value, uint64_t mask, unsigned shift)
		{
			//static_assert(!((uint64_t(value) << shift) & ~mask));
			return uint64_t(value) << shift;
		}

		static constexpr uint64_t PIPELINE_NUMBER_OF_BITS = 12;
		static constexpr uint64_t MATERIAL_NUMBER_OF_BITS = 20;
		static constexpr uint64_t DEPTH_NUMBER_OF_BITS = 24;
		static constexpr uint64_t CUSTOM_NUMBER_OF_BITS = 8;

		static constexpr unsigned PIPELINE_SHIFT_OPAQUE = 64 - PIPELINE_NUMBER_OF_BITS;
		static constexpr unsigned MATERIAL_SHIFT_OPAQUE = PIPELINE_SHIFT_OPAQUE - MATERIAL_NUMBER_OF_BITS;
		static constexpr unsigned DEPTH_SHIFT_OPAQUE = MATERIAL_SHIFT_OPAQUE - DEPTH_NUMBER_OF_BITS;
		static constexpr unsigned CUSTOM_SHIFT_OPAQUE = DEPTH_SHIFT_OPAQUE - CUSTOM_NUMBER_OF_BITS;

		//TODO: proper transparent handling
		//static constexpr uint32_t DEPTH_SHIFT_TRANSPARENT = 64 - DEPTH_NUMBER_OF_BITS;
		//static constexpr uint32_t PIPELINE_SHIFT_TRANSPARENT = DEPTH_SHIFT_TRANSPARENT - PIPELINE_NUMBER_OF_BITS;
		//static constexpr uint32_t CUSTOM_SHIFT_TRANSPARENT = PIPELINE_SHIFT_TRANSPARENT - CUSTOM_NUMBER_OF_BITS;
		//static constexpr uint32_t MATERIAL_GROUP_SHIFT_TRANSPARENT = CUSTOM_SHIFT_TRANSPARENT - MATERIAL_NUMBER_OF_BITS;

		void Execute(const std::vector<DrawMesh>& meshes, Renderer* renderer);

		void RefreshPass(std::vector<RenderObject>& renderables, const std::vector<DrawMesh>& meshes);
		void BuildFlatBatches(std::vector<RenderObject>& renderables);

		const PassObject& getPassObject(Handle<PassObject> handle) const { return objects[handle.getID()]; }
		PassObject& getPassObject(Handle<PassObject> handle) { return objects[handle.getID()]; }

		std::vector<InstanceBatch> BuildInstanceBatches(std::vector<RenderBatch>& inobjects, const std::vector<PassObject>& objects);
		std::vector<MultiDrawIndirectBatch> BuildMultiIndirectDraw(const std::vector<InstanceBatch>& instanceBatches, const std::vector<DrawMesh>& meshes);

		std::vector<MultiDrawIndirectBatch> multiDrawIndirectBatches;
		std::vector<InstanceBatch> instanceBatches;
		std::vector<RenderBatch> flatBatches;
		std::vector<PassObject> objects;

		std::vector<Handle<RenderObject>> unbatchedObjects;
		//std::vector<Handle<PassObject>> reusableObjects;
		//std::vector<Handle<PassObject>> objectsToDelete;

		static constexpr uint32_t MAX_TRANSFORM_OBJECTS = (uint32_t)1_MB;
		std::vector<Mat4> modelTransformBuffer;
		Ref<ShaderStorageBuffer> modelTransformBufferSsbo = nullptr;
		Ref<IndirectDrawBuffer> indirectDrawBuffer = nullptr;
		Ref<ShaderStorageBuffer> instanceIDsBuffer = nullptr;

		RenderPassType type;
	};
}