#include "Illuminar_pch.h"
#include "Scene.h"

#include "GameEngine/Scene/Components/Illuminar_Components.h"

namespace v2 {
	void MeshRenderSystem::Update([[maybe_unused]] Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const SubmeshComponent& meshComponent = *(SubmeshComponent*)components[1];

		MeshResource* meshResource = ResourceManagers::get().meshResourceManager.tryGetResourceByID(meshComponent.meshID);

		Entity e = Entity{ components[0]->entity, scene };
		auto transform = scene->getTransformRelativeToParent(e);

		if (meshResource && meshResource->Loaded()) {
			scene->renderer.RegisterObject(meshComponent.submesh, transform);
		}
	}

	void DirectionalLightSystem::Update(Timestep deltaTime, BaseECSComponent** components)
	{
		const TransformComponent& transformComponent = *(TransformComponent*)components[0];
		const LightComponent& directionalLight = *(LightComponent*)components[1];

		Entity e = Entity{ components[0]->entity, scene };
		const auto& transform = scene->getTransformRelativeToParent(e);

		if (directionalLight.type == LightType::Directional) {
			v2::Renderer::DirectionalLight light;
			light.direction = -glm::normalize(glm::mat3(transform) * glm::vec3(1.0f));
			light.radiance = directionalLight.radiance;
			light.intensity = directionalLight.intensity;
			light.lightSize = directionalLight.lightSize;
			light.nvidiaPcss = directionalLight.nvidiaPcss;
			light.softShadows = directionalLight.softShadows;

			scene->renderer.directionalLights.push_back(light);
		}
	}

	Scene::Scene()
	{
		ecs.OnDestroy<LightComponent>().Connect<&Scene::OnLightDestroy>(this);

		renderSystems.AddSystem<MeshRenderSystem>(this);
		renderSystems.AddSystem<DirectionalLightSystem>(this);
	}

	Scene::~Scene()
	{
	}

	void Scene::setViewport(uint32_t w, uint32_t h)
	{
		viewport.x = w;
		viewport.y = h;
	}

	void Scene::UpdateEditor([[maybe_unused]] Timestep deltaTime, const Camera& camera, const Mat4& viewMatrix)
	{
		auto cameraCopy = camera;
		cameraCopy.projection = glm::perspectiveFov(glm::radians(45.0f), (float)viewport.x, (float)viewport.y, 0.01f, 1000.0f);

		renderer.Begin(cameraCopy, viewMatrix);
		ecs.Update(renderSystems, deltaTime);
		renderer.End();
	}

	Entity Scene::CreateEntity(const std::string& name, Entity parent)
	{
		auto entity = Entity(this);
		auto id = entity.AddComponent<IDComponent>(UUID{});
		auto transform = entity.AddComponent<TransformComponent>();
		auto tag = entity.AddComponent<TagComponent>(name);
		auto hierarchy = entity.AddComponent<HierarchyComponent>();

		if (parent != INVALID_ENTITY) {
			ParentEntity(entity, parent);
		}

		return entity;
	}

	Entity Scene::CreateEntityWithID(const std::string& name, UUID ID, Entity parent)
	{
		auto entity = Entity(this);
		auto id = entity.AddComponent<IDComponent>(ID);
		auto transform = entity.AddComponent<TransformComponent>();
		auto tag = entity.AddComponent<TagComponent>(name);
		auto hierarchy = entity.AddComponent<HierarchyComponent>();

		if (parent != INVALID_ENTITY) {
			ParentEntity(entity, parent);
		}

		return entity;
	}

	Entity Scene::CreateLightEntity(const std::string& name, LightType lightType, Entity parent)
	{
		auto entity = CreateEntity(name, parent);
		auto light = entity.AddComponent<LightComponent>();
		light->type = lightType;

		return entity;
	}

	Entity Scene::CreateMeshEntity(const std::string& name, MeshResourceID meshID, Entity parent)
	{
		auto& meshManager = ResourceManagers::get().meshResourceManager;

		MeshResource* resource = meshManager.tryGetResourceByID(meshID);
		if (!resource) {
			EngineLogFatal("Could not find {}", name);
			__debugbreak();
		}
		//todo better check
		if (!resource->Loaded()) {
			return {};
		}

		Entity entity = CreateEntity(name, parent);
		auto meshComp = entity.AddComponent<MeshComponent>();
		meshComp->meshID = resource->id;
		meshComp->name = resource->mesh->name;

		BuildMeshHierarchy(entity, resource, resource->scene, resource->scene->mRootNode);

		return entity;
	}

	void Scene::DestroyEntity(Entity entity)
	{
		const auto& children = entity.getChildrenID();
		std::for_each(children.crbegin(), children.crend(), [this](auto childID) {
			DestroyEntity(FindEntityByID(childID));
		});

		UnparentEntity(entity);
		ecs.DestroyEntity(entity);
	}

	Entity Scene::FindEntityByID(UUID id)
	{
		const auto& components = ecs.getComponentsOfTypeT<IDComponent>();
		for (const auto component : components) {
			if (component->id == id) {
				return Entity(component->entity, this);
			}
		}

		return Entity{};
	}

	void Scene::ConvertToLocalSpace(Entity entity)
	{
		const Entity parent = FindEntityByID(entity.getParentID());

		if (!parent) {
			return;
		}

		auto transform = entity.getComponent<TransformComponent>();
		const Mat4 parentTransform = getWorldSpaceTransformMatrix(parent);
		const Mat4 localTransform = glm::inverse(parentTransform) * transform->getTransform();
		DecomposeTransform(localTransform, transform->translation, transform->rotation, transform->scale);
	}

	void Scene::ConvertToWorldSpace(Entity entity)
	{
		const Entity parent = FindEntityByID(entity.getParentID());

		if (!parent) {
			return;
		}

		Mat4 transform = getTransformRelativeToParent(entity);
		auto entityTransform = entity.getComponent<TransformComponent>();
		DecomposeTransform(transform, entityTransform->translation, entityTransform->rotation, entityTransform->scale);
	}

	Mat4 Scene::getTransformRelativeToParent(Entity entity)
	{
		Mat4 transform(1.0f);

		Entity parent = FindEntityByID(entity.getParentID());

		if (parent) {
			transform = getTransformRelativeToParent(parent);
		}

		return transform * entity.getComponent<TransformComponent>()->getTransform();
	}

	Mat4 Scene::getWorldSpaceTransformMatrix(Entity entity)
	{
		Mat4 transform = entity.getComponent<TransformComponent>()->getTransform();

		while (Entity parent = FindEntityByID(entity.getParentID())) {
			transform = parent.getComponent<TransformComponent>()->getTransform() * transform;
			entity = parent;
		}

		return transform;
	}

	TransformComponent Scene::getWorldSpaceTransform(Entity entity)
	{
		Mat4 transform = getWorldSpaceTransformMatrix(entity);
		TransformComponent transformComponent;

		DecomposeTransform(transform, transformComponent.translation, transformComponent.rotation, transformComponent.scale);

		//glm::quat rotationQuat = glm::quat(transformComponent.rotation);
		//transformComponent.Up = glm::normalize(glm::rotate(rotationQuat, glm::vec3(0.0f, 1.0f, 0.0f)));
		//transformComponent.Right = glm::normalize(glm::rotate(rotationQuat, glm::vec3(1.0f, 0.0f, 0.0f)));
		//transformComponent.Forward = glm::normalize(glm::rotate(rotationQuat, glm::vec3(0.0f, 0.0f, -1.0f)));

		return transformComponent;
	}

	void Scene::ParentEntity(Entity entity, Entity parent)
	{
		if (parent.isDescendantOf(entity)) {
			UnparentEntity(parent);

			Entity newParent = FindEntityByID(entity.getParentID());
			if (newParent) {
				UnparentEntity(entity);
				ParentEntity(parent, newParent);
			}
		}
		else {
			Entity previousParent = FindEntityByID(entity.getParentID());
			if (previousParent) {
				UnparentEntity(entity);
			}
		}

		entity.setParentID(parent.getID());
		parent.getChildrenID().push_back(entity.getID());

		ConvertToLocalSpace(entity);
	}

	void Scene::UnparentEntity(Entity entity, bool convertToWorldSpace)
	{
		Entity parent = FindEntityByID(entity.getParentID());
		if (!parent) {
			return;
		}

		auto& parentChildren = parent.getChildrenID();
		parentChildren.erase(std::remove(parentChildren.begin(), parentChildren.end(), entity.getID()), parentChildren.end());

		if (convertToWorldSpace) {
			ConvertToWorldSpace(entity);
		}

		entity.setParentID(0);
	}

	Entity Scene::getMainCamera()
	{
		for (const auto& component : ecs.getComponentsOfTypeT<CameraComponent>()) {
			const CameraComponent& cameraComponent = *(CameraComponent*)component;
			return Entity(cameraComponent.entity, this);
		} return Entity(INVALID_ENTITY_HANDLE, nullptr);
	}

	void Scene::BuildMeshHierarchy(Entity parent, MeshResource* resource, const void* assimpScene, void* assimpNode)
	{
		aiScene* aScene = (aiScene*)assimpScene;
		aiNode* node = (aiNode*)assimpNode;

		if (node == aScene->mRootNode && node->mNumMeshes == 0) {
			for (uint32_t i = 0; i < node->mNumChildren; i++)
				BuildMeshHierarchy(parent, resource, assimpScene, node->mChildren[i]);

			return;
		}

		if (node->mNumMeshes == 1) {
			auto nodeName = node->mName.C_Str();

			Entity nodeEntity = CreateEntity(nodeName, parent);

			uint32_t submeshIndex = node->mMeshes[0];
			auto submeshComponent = nodeEntity.AddComponent<SubmeshComponent>(
				resource->mesh->submeshes[submeshIndex],
				resource->id,
				submeshIndex
			);

			auto& transformComponent = *nodeEntity.getComponent<TransformComponent>();
			transformComponent = resource->mesh->submeshes[submeshIndex].transform;
		} else if (node->mNumMeshes > 1) {
			for (uint32_t i = 0; i < node->mNumMeshes; i++) {
				uint32_t submeshIndex = node->mMeshes[i];
				auto meshName = aScene->mMeshes[submeshIndex]->mName.C_Str();

				Entity meshEntity = CreateEntity(meshName, parent);
				auto submeshComponent = meshEntity.AddComponent<SubmeshComponent>(
					resource->mesh->submeshes[submeshIndex],
					resource->id,
					submeshIndex
				);

				auto& transformComponent = *meshEntity.getComponent<TransformComponent>();
				transformComponent = resource->mesh->submeshes[submeshIndex].transform;
			}
		}

		for (uint32_t i = 0; i < node->mNumChildren; i++)
			BuildMeshHierarchy(parent, resource, assimpScene, node->mChildren[i]);
	}

	LightBuilder::LightBuilder(LightType type)
		: type(type)
	{
	}

	Entity LightBuilder::Build(const std::string& name, Ref<Scene>& scene, Entity parent)
	{
		auto entity = scene->CreateLightEntity(name, type, parent);
		auto& light = *entity.getComponent<LightComponent>();
		light.radiance = this->light.radiance;
		light.intensity = this->light.intensity;
		light.fallOff = this->light.fallOff;
		light.minRadius = this->light.minRadius;
		light.radius = this->light.radius;
		light.outerCutOff = this->light.outerCutOff;
		light.innerCutOff = this->light.innerCutOff;
		light.lightSize = this->light.lightSize;
		light.castsShadows = this->light.castsShadows;
		light.softShadows = this->light.softShadows;
		light.nvidiaPcss = this->light.nvidiaPcss;

		auto transformComponent = entity.getComponent<TransformComponent>();
		transformComponent->translation = position;
		transformComponent->rotation = direction;

		Entity e = Entity{ transformComponent->entity, scene.get()};
		const auto& transform = scene->getTransformRelativeToParent(e);

		v2::Renderer::DirectionalLight dirLight;
		dirLight.direction = -glm::normalize(glm::mat3(transform) * glm::vec3(1.0f));
		dirLight.radiance = light.radiance;
		dirLight.intensity = light.intensity;
		dirLight.lightSize = light.lightSize;
		dirLight.nvidiaPcss = light.nvidiaPcss;
		dirLight.softShadows = light.softShadows;

		return entity;
	}

	LightBuilder& LightBuilder::Position(const Vector3<float>& position)
	{
		this->position = position;
		return *this;
	}

	LightBuilder& LightBuilder::Direction(const Vector3<float>& direction)
	{
		this->direction = direction;
		return *this;
	}

	LightBuilder& LightBuilder::Radiance(const Vector3<float>& radiance)
	{
		light.radiance = radiance;
		return *this;
	}

	LightBuilder& LightBuilder::Intensity(float intensity)
	{
		light.intensity = intensity;
		return *this;
	}

	LightBuilder& LightBuilder::FallOff(float fallOff)
	{
		light.fallOff = fallOff;
		return *this;
	}

	LightBuilder& LightBuilder::Radius(float radius)
	{
		light.radius = radius;
		return *this;
	}

	LightBuilder& LightBuilder::MinRadius(float minRadius)
	{
		light.minRadius = minRadius;
		return *this;
	}

	LightBuilder& LightBuilder::OuterCutOff(float outerCutOff)
	{
		light.outerCutOff = outerCutOff;
		return *this;
	}

	LightBuilder& LightBuilder::InnerCutOff(float innerCutOff)
	{
		light.innerCutOff = innerCutOff;
		return *this;
	}

	LightBuilder& LightBuilder::CastsShadows(bool shadows)
	{
		light.castsShadows = shadows;
		return *this;
	}

	LightBuilder& LightBuilder::SoftShadows(bool soft)
	{
		light.softShadows = soft;
		return *this;
	}

	LightBuilder& LightBuilder::NvidiaPcss(bool nvidia)
	{
		light.nvidiaPcss = nvidia;
		return *this;
	}

	LightBuilder& LightBuilder::LightSize(float lightSize)
	{
		light.lightSize = lightSize;
		return *this;
	}
}