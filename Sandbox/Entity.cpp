#include "Illuminar_pch.h"
#include "Entity.h"

#include "Scene.h"

namespace v2 {
	Entity::Entity(Scene* scene)
		: scene(scene), ecs(&scene->ecs)
	{
		handle = ecs->CreateEntity();
	}

	Entity::Entity(EntityHandle handle, Scene* scene)
		: handle(handle), scene(scene), ecs(&scene->ecs)
	{
	}

	void Entity::setParentID(UUID parentID)
	{
		getComponent<HierarchyComponent>()->parent = parentID;
	}

	UUID Entity::getParentID() const
	{
		return getComponent<HierarchyComponent>()->parent;
	}

	std::vector<UUID>& Entity::getChildrenID() const
	{
		return getComponent<Illuminar::HierarchyComponent>()->children;
	}

	bool Entity::isAncesterOf(Entity entity) const
	{
		const auto& children = getChildrenID();

		if (children.empty()) {
			return false;
		}

		for (UUID child : children) {
			if (child == entity.getID()) {
				return true;
			}
		}

		for (UUID child : children) {
			if (scene->FindEntityByID(child).isAncesterOf(entity)) {
				return true;
			}
		}

		return false;
	}

	bool Entity::isDescendantOf(Entity entity) const
	{
		return entity.isAncesterOf(*this);
	}

	UUID Entity::getID() const
	{
		return getComponent<IDComponent>()->id;
	}
}