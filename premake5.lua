workspace "Illuminar"
    architecture "x64"
    targetdir "build"

    configurations {
        "Debug",
        "Release"
    }

    flags {       
        "MultiProcessorCompile"
    }

    startproject "Editor"

    outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
    
    dependencies = "%{wks.location}/Engine/Illuminar/dependencies"

    --TODO: move this into the Engine directory when projects won't need these dependencies
    IncludeDir = {}
    IncludeDir["SDL2"]                  = "%{dependencies}/SDL2/include"
    IncludeDir["Glad"]                  = "%{dependencies}/Glad/include"
    IncludeDir["glm"]                   = "%{dependencies}/glm"
    IncludeDir["spdlog"]                = "%{dependencies}/spdlog/include"
    IncludeDir["assimp"]                = "%{dependencies}/assimp5/include"
    IncludeDir["yaml_cpp"]              = "%{dependencies}/yaml-cpp/include"
    IncludeDir["FastNoise"]             = "%{dependencies}/FastNoise2/include"
    IncludeDir["Vulkan"]                = "%{dependencies}/Vulkan/include"
    IncludeDir["Box2D"]                 = "%{dependencies}/Box2d/include"
    IncludeDir["spirv_cross"]           = "%{dependencies}/spirv-cross"
    IncludeDir["shaderc"]               = "%{dependencies}/shaderc/libshaderc/include"
    IncludeDir["googletest"]            = "%{dependencies}/googletest/googletest/include"

    group "Dependencies"
        include "Engine/Illuminar/dependencies/Glad"
        include "Engine/Illuminar/dependencies/spdlog"
        include "Engine/Illuminar/dependencies/spirv-cross"
        include "Engine/Illuminar/dependencies/yaml-cpp"
        include "Engine/Illuminar/dependencies/Box2d"
        include "Engine/Illuminar/dependencies/googletest"
    group ""

    group "Engine"
        include "Engine/Illuminar"
        include "Engine/Editor"
        include "Engine/ImGui"
    group ""

    include "Sandbox"